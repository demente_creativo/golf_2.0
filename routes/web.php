<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
	require (__DIR__ . '/admins.php');
	require (__DIR__ . '/categories.php');
	require (__DIR__ . '/clubes.php');
	require (__DIR__ . '/companies.php');
	require (__DIR__ . '/day_draws.php');
	require (__DIR__ . '/draws.php');
	require (__DIR__ . '/golf_courses.php');
	require (__DIR__ . '/groups.php');
	require (__DIR__ . '/home.php');
	require (__DIR__ . '/registrations.php');
	require (__DIR__ . '/lockers.php');
	require (__DIR__ . '/locker_loans.php');
	require (__DIR__ . '/members.php');
	require (__DIR__ . '/modality.php');
	require (__DIR__ . '/monthly_payments.php');
	require (__DIR__ . '/partners.php');
	require (__DIR__ . '/payment_plans.php');
	require (__DIR__ . '/payments.php');
	require (__DIR__ . '/periods.php');
	require (__DIR__ . '/rangers.php');
	require (__DIR__ . '/member_partners.php');
	require (__DIR__ . '/member_reservations.php');
	require (__DIR__ . '/reports.php');
	require (__DIR__ . '/reservations.php');
	require (__DIR__ . '/reservation_definitions.php');
	require (__DIR__ . '/roles.php');
	require (__DIR__ . '/start_times.php');
	require (__DIR__ . '/starters.php');
	require (__DIR__ . '/storages.php');
	require (__DIR__ . '/students.php');
	require (__DIR__ . '/timeframes.php');
	require (__DIR__ . '/tournaments.php');
	require (__DIR__ . '/tournament_inscriptions.php');
	require (__DIR__ . '/teachers.php');
	require (__DIR__ . '/users.php');
	require (__DIR__ . '/profile.php');
	require (__DIR__ . '/green_fee.php');
	require (__DIR__ . '/day_blockeds.php');
	require (__DIR__ . '/collector.php');
	require (__DIR__ . '/financiers.php');
	require (__DIR__ . '/messages.php');
	require (__DIR__ . '/validation.php');
	//Route::post('reservations','ReservationController@Reserva');

	Route::get('department', function () {
		return view('department');
	})->name('all_department');

	//Route::resource('torneos', 'TournamentController',);

});

Route::get('/', function () {
	if (Auth::guest()) {
		return view('home');
	} else {
		return redirect()->route('login');
	}
})->name('/');

Route::get('/','HomeController@index')->name('home');

Route::get('login', function () {
	if (Auth::guest()) {
		return view('home');
	} else {
		return view('auth.login');
	}
})->name('login');

Route::get('registrarse', 'RegisterController@create')->name('get_register');
Route::post('registrarse', 'RegisterController@store')->name('get_register');
Route::get('socios/validar_email_socio', 'MemberController@validate_email_member')->name('socios.validate_email_member');
Route::get('socios/validar_cedula_socio', 'MemberController@validate_identity_card_member')->name('socios.validate_identity_card_member');

// Route::get('socios/autocomplete', 'MemberController@autocomplete');

Route::get('recuperar_contrasena', function () {
	if (Auth::guest()) {
		return view('auth.passwords.email');
	} else {
		return view('login');
	}
})->name('forgot_password');


Auth::routes();
