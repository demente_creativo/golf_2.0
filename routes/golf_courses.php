<?php

//GolfCourses

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//GolfCourses

Route::group([ 'middleware' => ['auth'], 'prefix' => 'campo_golf'], function () {

	Route::get('/','GolfCourseController@index')->name('campo_golf.index','[0-9]+');

	// confirmar uso
	Route::put('/disponibilidad/{element}','GolfCourseController@change_status')->name('campo_golf.change_status');

});
