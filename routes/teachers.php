<?php

//Members

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//teacher
Route::get('profesores/validar_email_profesor', 'TeacherController@validate_email_teacher')->name('teachers.validate_email_teacher');

Route::group([ 'middleware' => ['auth'], 'prefix' => 'escuela/{user_id}'], function () {

	Route::resource('profesores', 'TeacherController',
		['parameters' =>['profesores' =>'teacher_id']]
	);


});
