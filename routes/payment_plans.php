<?php

//PaymentPlans

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//PaymentPlans

Route::group(['middleware' => ['auth'], 'prefix' => 'finanzas/{user_id}'], function () {

	Route::resource('planes', 'PaymentPlanController', 
		['parameters' =>['planes' =>'payment_plan_id']]
	);

	Route::get('agregar','PaymentPlanController@store_ajax')->name('planes.store_ajax');

});
