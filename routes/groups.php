<?php

//Roles

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Roles

Route::group([ 'middleware' => ['auth'], 'prefix' => 'escuela/{user_id}'], function () {

	Route::resource('grupos', 'GroupController',
		['parameters' =>['grupos' =>'group_id']]
	);

	Route::get('grupos/agregar_ajax','GroupController@store_ajax')->name('grupos.store_ajax');

});

Route::group([ 'middleware' => ['auth'], 'prefix' => 'alumnos'], function () {

	Route::get('grupos/agregar_ajax','GroupController@store_ajax')->name('grupos.store_ajax');

});
