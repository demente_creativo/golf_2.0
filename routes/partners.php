<?php

//Partners

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Partners
Route::get('invitados/autocomplete', 'PartnerController@autocomplete')->name('invitados.autocomplete');

Route::get('invitados/alls', 'PartnerController@alls')->name('invitados.alls');

Route::put('invitados/change_status/{user_id}', 'PartnerController@change_status')->name('invitados.change_status');

Route::group([ 'middleware' => ['auth'], 'prefix' => 'admin/{user_id}'], function () {

	Route::resource('invitados', 'PartnerController',
		['parameters' =>['invitados' =>'partner_id']]
	);

	Route::get('/exonerar', 'PartnerController@exonerate')->name('invitados.exonerate');

	Route::get('validar_email_invitado', 'MemberController@validate_email_partner')->name('invitados.validate_email_partner');

	Route::get('validar_cedula_invitado', 'MemberController@validate_identity_card_partner')->name('invitados.validate_identity_card_partner');

});
