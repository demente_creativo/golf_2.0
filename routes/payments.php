<?php

//Students

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Payments

	Route::group(['middleware' => ['auth']], function () {

		Route::resource('pagos', 'PaymentController', 
			['except' => [ 'show' ]],
			['parameters' =>['pagos' =>'payment_id']]
		);

		// Route::get('/','PaymentController@index')->name('all_payment');

		// // confirmar uso
		// Route::get('/create','PaymentController@create_payment')->name('create_payment');

		// // confirmar uso
		// Route::post('/','PaymentController@store_payment')->name('store_payment');

		// // confirmar uso
		// Route::get('/{payment_id}','PaymentController@edit_payment')->name('edit_payment');

		// // confirmar uso
		// Route::put('/{payment_id}','PaymentController@update_payment')->name('update_payment');

		// // confirmar uso
		// Route::delete('/{payment_id}','PaymentController@delete_payment')->name('destroy_payment');

	});
