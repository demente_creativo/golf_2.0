<?php

//Admins

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Admins

Route::get('root/administradores/validar_email_usuario', 'AdminController@validate_email_user')->name('administradores.validate_email_user');
Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource('root/administradores', 'AdminController', 
		['except' => [ 'create' ]],
		['parameters' =>['administradores' =>'user_id']]
	);

	Route::get('root/administradores/{user_id}/habilitar','AdminController@enable')->name('administradores.habilitar');

	Route::get('root/administradores/{user_id}/deshabilitar','AdminController@disable')->name('administradores.deshabilitar');

});