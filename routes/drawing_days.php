<?php

//DrawingDays

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//DrawingDays

Route::group(['prefix' => 'drawing_days/'], function () {

	Route::get('/','DrawingDaysController@index')->name('all_drawing_days');

	// confirmar uso
	Route::get('/create','DrawingDaysController@create_drawing_day')->name('create_drawing_day');

	// confirmar uso
	Route::post('/','DrawingDaysController@store_drawing_day')->name('store_drawing_day');

	// confirmar uso
	Route::get('/{drawing_day_id}','DrawingDaysController@edit_drawing_day')->name('edit_drawing_day');

	// confirmar uso
	Route::put('/{drawing_day_id}','DrawingDaysController@update_drawing_day')->name('update_drawing_day');

	// confirmar uso
	Route::delete('/{drawing_day_id}','DrawingDaysController@delete_drawing_day')->name('destroy_drawing_day');

});
