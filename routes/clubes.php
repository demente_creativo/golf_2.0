<?php

//Clubes

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Clubes


	Route::resource('clubes', 'ClubController', 
		['except' => [ 'show', 'create' ]],
		['parameters' =>['clubes' =>'club_id']]
	);

	Route::get('clubes/post','ClubController@post')->name('clubes.post');
	//->where('club_id', '[0-9]+')	