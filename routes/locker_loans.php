<?php

//LockerLoans

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//LockerLoans

Route::group(['middleware' => ['auth']], function () {

	Route::resource('maletero/{user_id}/prestamo_casilleros', 'LockerLoanController',
		//['except' => ['show','create','edit','store','update','destroy']],
		['parameters' =>['prestamo_casilleros' =>'loan_id']]
	);

});
