<?php

//Students

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Companys

// No sabemos porque no se ejecuta abajo, dejar pendiente
// para la correccion luego.
//

Route::group(['prefix' => 'company'], function () {

	Route::get('/','CompanyController@index')->name('all_company','[0-9]+');

	// confirmar uso
	Route::get('/create','CompanyController@create_company')->name('create_company','[0-9]+');

	// confirmar uso
	Route::post('/','CompanyController@store_company')->name('store_company','[0-9]+');


	// confirmar uso
	Route::get('/{company_id}','CompanyController@edit_company')->name('edit_company','[0-9]+');

	// confirmar uso
	Route::put('/{company_id}','CompanyController@update_company')->name('update_company','[0-9]+');

	// confirmar uso
	Route::delete('/{company_id}','CompanyController@delete_company')->name('destroy_company','[0-9]+');

});
