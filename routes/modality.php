<?php

//Modalities

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Modalities

Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource('modalidades', 'ModalityController',
		['except' => [ 'show', 'create' ]],
		['parameters' =>['modalidades' =>'modality_id']]
	);

		Route::get('modalidades/post','ModalityController@post_modalities')->name('modalities.post');
// 	Route::get('/','ModalityController@index')->name('modalidades.');

// 	Route::get('/crear','ModalityController@create')->name('modalidades.crear');


// 	Route::post('/almacenar','ModalityController@store')->name('modalidades.almacenar');

// 	Route::get('/{reservation_id}','ModalityController@edit')
// 			->name('modalidades.editar')->where('reservation_id', '[0-9]+');

// 	Route::put('/{reservation_id}','ModalityController@update')
// 			->name('modalidades.actualizar')->where('reservation_id', '[0-9]+');

// 	Route::delete('/{reservation_id}','ModalityController@delete')
// 			->name('modalidades.eliminar')->where('reservation_id', '[0-9]+');

});
