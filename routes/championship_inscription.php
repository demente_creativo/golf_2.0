<?php

//ChampionshipInscriptions

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ChampionshipInscriptions

Route::group([ 'middleware' => ['auth'], 'prefix' => 'inscripciones_campeonatos/'], function () {

	Route::resource('inscripciones_campeonatos', 'ChampionshipInscriptionController',
		['except' => [ 'show' ]],
		['parameters' =>['inscripciones_campeonatos' =>'championship_inscription_id']]
	);

	// 	Route::get('/','ChampionshipInscriptionController@index')->name('all_championship_inscription','[0-9]+');

	// 	// confirmar uso
	// 	Route::get('/create','ChampionshipInscriptionController@create_championship_inscription')->name('create_championship_inscription','[0-9]+');

	// 	// confirmar uso
	// 	Route::post('/','ChampionshipInscriptionController@store_championship_inscription')->name('store_championship_inscription','[0-9]+');

	// 	// confirmar uso
	// 	Route::get('/{championship_inscription_id}','ChampionshipInscriptionController@edit_championship_inscription')->name('edit_championship_inscription','[0-9]+');

	// 	// confirmar uso
	// 	Route::put('/{championship_inscription_id}','ChampionshipInscriptionController@update_championship_inscription')->name('update_championship_inscription','[0-9]+');

	// 	// confirmar uso
	// 	Route::delete('/{championship_inscription_id}','ChampionshipInscriptionController@delete_championship_inscription')->name('destroy_championship_inscription','[0-9]+');

});
