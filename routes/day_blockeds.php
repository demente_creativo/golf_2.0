<?php

//Draws

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([ 'middleware' => ['auth'], 'prefix' => 'admin/{user_id}' ], function () {

	Route::resource('dias_bloqueados', 'DayBlockedController',
		['except' => [ 'show', 'create' ]],
		['parameters' =>['dias_bloqueados' =>'day_blocked_id']]
	);

});