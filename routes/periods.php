<?php

//Members

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//periods

Route::group([ 'middleware' => ['auth'], 'prefix' => 'finanzas/{user_id}'], function () {

	Route::resource('periodos', 'PeriodController', 
		['except' => 'destroy'],
		['parameters' =>['periodos' =>'period_id']]
	);

	Route::get('listado_de_salida','FinancierController@departure_list')->name('finanzas.departure_list');

	Route::get('/','PeriodController@index')->name('all_period','[0-9]+');

	// confirmar uso
	Route::get('/create','PeriodController@create_period')->name('create_period','[0-9]+');

	// confirmar uso
	Route::post('/','PeriodController@store_period')->name('store_period','[0-9]+');

	// confirmar uso
	Route::get('{period_id}','PeriodController@edit_period')->name('edit_period','[0-9]+');

	// confirmar uso
	Route::put('/{period_id}','PeriodController@update_period')->name('update_period','[0-9]+');

	// confirmar uso
	Route::delete('/{period_id}','PeriodController@delete_period')->name('destroy_period','[0-9]+');

});
