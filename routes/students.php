<?php

//Students

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Students

// No sabemos porque no se ejecuta abajo, dejar pendiente
// para la correccion luego.
//


	Route::get('/excel','StudentController@excel')->name('excel');
	Route::get('/autocomplete', 'StudentController@autocomplete');
	
Route::group(['middleware' => ['auth'], 'prefix' => 'escuela/{user_id}'], function () {

	Route::get('alumnos/papa','StudentController@student_for_member')->name('alumnos.member');
	Route::resource('alumnos', 'StudentController',
		['parameters' =>['alumnos' =>'student_id']]
	);
	

});

	Route::get('alumnos/validar_cedula_estudiante', 'StudentController@validate_identity_card_student')->name('alumnos.validate_identity_card_student');

Route::group(['middleware' => ['auth'], 'prefix' => 'alumnos'], function () {

	Route::get('excel','StudentController@excel')->name('excel');
	Route::get('pdf','StudentController@pdf')->name('pdf');

	Route::get('agregar/s','StudentController@store_ajax')->name('alumnos.store_ajax');
	Route::get('member/{member_id}','StudentController@student_for_member')->name('alumnos.student_for_member');

	//IMPRIMIR PDF//
// 	Route::get('pdf', function(){ $students = Golf\Student::all();

// 		$pdf = PDF::loadView('student.students_table', ['students' =>$students]);

// 		return $pdf->download('Estudiantes.pdf');
// 	})->name('pdf');

});
