<?php

//Timeframes

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Timeframes

Route::group([ 'middleware' => ['auth'], 'prefix' => 'timeframes'], function () {

	Route::get('/','TimeframeController@index')->name('all_timeframes');

	// confirmar uso
	Route::get('/create','TimeframeController@create_timeframe')->name('create_timeframe');

	// confirmar uso
	Route::post('/','TimeframeController@store_timeframe')->name('store_timeframe');

	// confirmar uso
	Route::get('/{timeframe_id}','TimeframeController@edit_timeframe')->name('edit_timeframe');

	// confirmar uso
	Route::put('/{timeframe_id}','TimeframeController@update_timeframe')->name('update_timeframe');

	// confirmar uso
	Route::delete('/{timeframe_id}','TimeframeController@delete_timeframe')->name('destroy_timeframe');

});
