<?php

//Members

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Members
Route::get('socios/autocomplete', 'MemberController@autocomplete')->name('socios.autocomplete');
Route::get('socios/alls', 'MemberController@alls')->name('socios.alls');

Route::get('socios/validar_email_socio', 'MemberController@validate_email_member')->name('socios.validate_email_member');

Route::get('socios/validar_cedula_socio', 'MemberController@validate_identity_card_member')->name('socios.validate_identity_card_member');

Route::put('socios/change_status/{user_id}', 'MemberController@change_status')->name('socios.change_status');

Route::put('socios/status_inscription/{user_id}', 'MemberController@status_inscription')->name('socios.status_inscription');


Route::group([ 'middleware' => ['auth'], 'prefix' => 'admin/{user_id}'], function () {

	Route::resource('socios', 'MemberController',
		['except' => [ 'create' ]],
		['parameters' =>['socios' =>'member_id']]
	);
	
	Route::get('suscripciones', 'MemberController@registered_member')->name('socios.registered_member');

});
