<?php

//Starters

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Starters

Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource('starters/{start_id}/reservaciones_del_dia', 'StarterController',
		['only' => [ 'index' ]],
		['parameters' => ['starters' =>'start_id']]
	);

	Route::post('starters/{start_id}/cambiar_estado','StarterController@status')->name('reservaciones_del_dia.status');
	Route::post('starters/reservaciones_del_dia','StarterController@reservaciones_del_dia')->name('reservaciones_del_dia.reservaciones_del_dia');

	Route::post('starters/cambiar_hora','StarterController@change_hour')->name('reservaciones_del_dia.hour');
	Route::post('starters/cambiar_participantes','StarterController@change_participant')->name('reservaciones_del_dia.change_participant');
	Route::get('starters/{start_id}/listado_de_salida','StarterController@departure_list')->name('starters.departure_list');
	Route::post('starters/comentario','StarterController@add_comment')->name('reservaciones_del_dia.add_comment');
	Route::post('starters/agregar_participante','StarterController@add_participant')->name('reservaciones_del_dia.add_participant');
	Route::delete('starters/eliminar_participante','StarterController@delete_participant')->name('reservaciones_del_dia.delete_participant');
});
