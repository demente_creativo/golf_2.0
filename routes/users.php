<?php

//Users

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Users

Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource('usuarios', 'UserController', 
		['except' => [ 'show', 'create' ]],
		['parameters' =>['usuarios' =>'user_id']]
	);

	// 	Route::get('/','UserController@index')->name('usuarios.');

	// 	Route::get('/crear','UserController@create')->name('usuarios.crear');


	// 	Route::post('/almacenar','UserController@store')->name('usuarios.almacenar');

	// 	Route::get('/{reservation_id}','UserController@edit')
	// 			->name('usuarios.editar')->where('reservation_id', '[0-9]+');

	// 	Route::put('/{reservation_id}','UserController@update')
	// 			->name('usuarios.actualizar')->where('reservation_id', '[0-9]+');

	// 	Route::delete('/{reservation_id}','UserController@delete')
	// 			->name('usuarios.eliminar')->where('reservation_id', '[0-9]+');

});