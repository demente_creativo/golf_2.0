<?php

//Lockers

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Lockers

Route::group(['middleware' => ['auth']], function () {

	Route::resource('maletero/{user_id}/casilleros', 'LockerController',
		['except' => ['show','create']],
		['parameters' =>['casilleros' =>'draw_id']]
	);

	Route::get('maletero/{user_id}/listado_de_salida','LockerController@departure_list')->name('maletero.departure_list');

});
