<?php

//ChampionshipInscriptions

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ChampionshipInscriptions

Route::group([ 'middleware' => ['auth'], 'prefix' => 'torneos/{tournament_id}'], function () {

	Route::resource('inscripcion_torneos', 'TournamentInscriptionController',
		
		['parameters' =>['inscripcion_torneos' =>'tournament_inscription_id']]

	);

});

Route::group([ 'middleware' => ['auth'], 'prefix' => 'torneos/inscripciones'], function () {

	Route::get('validar_disponibilidad', 'TournamentInscriptionController@validate_disponibility')
		->name('torneos.validate_disponibility');

	Route::get('listado_inscripcion_torneo','TournamentInscriptionController@list_tournament_inscriptions')
		->name('torneos.list_tournament_inscriptions');
	Route::get('listado_inscripcion_torneo/{id}','TournamentInscriptionController@list_tournament_inscriptions_ajax')
		->name('torneos.list_tournament_inscriptions_ajax');
	Route::get('pdf_inscrpcion_torneo','TournamentInscriptionController@pdf_report_create')
		->name('torneos.pdf_report_create_tournament_inscription');
	
	Route::get('xls_inscrpcion_torneo','TournamentInscriptionController@xls_report_create')
		->name('torneos.xls_report_create_tournament_inscription');

});
