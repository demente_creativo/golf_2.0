<?php

//DrawReservations

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//DrawReservations

Route::group([ 'middleware' => ['auth'], 'prefix' => 'draw_reservations/'], function () {

	Route::resource('sorteo_reservaciones', 'DrawReservationController', 
		['except' => [ 'show' ]],
		['parameters' =>['sorteo_reservaciones' =>'draw_reservation_id']]
	);

	// 	Route::get('/','DrawReservationController@index')->name('all__drawreservations');

	// 	// confirmar uso
	// 	Route::get('/create','DrawReservationController@crea_te_drawreservation')->name('crea_te_drawreservation');

	// 	// confirmar uso
	// 	Route::post('/','DrawReservationController@stor_e_drawreservation')->name('stor_e_drawreservation');

	// 	// confirmar uso
	// 	Route::get('/{draw_reservation_id}','DrawReservationController@edit__drawreservation')->name('edit__drawreservation');

	// 	// confirmar uso
	// 	Route::put('/{draw_reservation_id}','DrawReservationController@upda_te_drawreservation')->name('upda_te_drawreservation');

	// 	// confirmar uso
	// 	Route::delete('/{draw_reservation_id}','DrawReservationController@dele_te_drawreservation')->name('dest_roy_drawreservation');

});
