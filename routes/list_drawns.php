<?php

//ListDrawns

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//ListDrawns

Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource('sorteos/lista_reservaciones', 'ListDrawnController', 
		['except' => [ 'show' ]],
		['parameters' =>['sorteos/lista_reservaciones' =>'draw_reservation_id']]
	);

	// 	Route::get('/','ListDrawnController@index')->name('all__drawreservations');

	// 	// confirmar uso
	// 	Route::get('/create','ListDrawnController@crea_te_drawreservation')->name('crea_te_drawreservation');

	// 	// confirmar uso
	// 	Route::post('/','ListDrawnController@stor_e_drawreservation')->name('stor_e_drawreservation');

	// 	// confirmar uso
	// 	Route::get('/{draw_reservation_id}','ListDrawnController@edit__drawreservation')->name('edit__drawreservation');

	// 	// confirmar uso
	// 	Route::put('/{draw_reservation_id}','ListDrawnController@upda_te_drawreservation')->name('upda_te_drawreservation');

	// 	// confirmar uso
	// 	Route::delete('/{draw_reservation_id}','ListDrawnController@dele_te_drawreservation')->name('dest_roy_drawreservation');

});
