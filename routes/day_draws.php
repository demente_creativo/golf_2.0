<?php

//DayDraws

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//DayDraws

Route::group([ 'middleware' => ['auth'] ], function () {

	Route::resource('administrador/{user_id}/dias_sorteables', 'DayDrawController', 
		['except' => [ 'show','create' ]],
		['parameters' =>['dias_sorteables' =>'day_draw_id']]
	);

	// 	Route::get('/','DayDrawController@index')->name('all_day_draws');

	// 	// confirmar uso
	// 	Route::get('/create','DayDrawController@create_day_draw')->name('create_day_draw');

	// 	// confirmar uso
	// 	Route::post('/','DayDrawController@store_day_draw')->name('store_day_draw');

	// 	// confirmar uso
	// 	Route::get('/{day_draw_id}','DayDrawController@edit_day_draw')->name('edit_day_draw');

	// 	// confirmar uso
	// 	Route::put('/{day_draw_id}','DayDrawController@update_day_draw')->name('update_day_draw');

	// 	// confirmar uso
	// 	Route::delete('/{day_draw_id}','DayDrawController@delete_day_draw')->name('destroy_day_draw');

});
