<?php

//Reservations

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Reservations

Route::group([ 'middleware' => ['auth'], 'prefix' => 'admin/{user_id}' ], function () {

	// 	Route::get('/','ReservationController@index')->name('all_reservations');

	Route::resource('/reservaciones', 'ReservationController',
		['except' => [ 'show']],
		['parameters' =>['reservaciones' =>'reservation_id']]
	);

	//Route::get('/check_reservation','ReservationController@check_reservation')
	//	->name('check_reservation');

	Route::get('/check_reservation','ReservationController@check_reservation')
		->name('check_reservation');

	Route::get('reservaciones/obtener_horas_disponibles','ReservationController@get_reservations')
		->name('reservaciones.get_reservations')->where('user_id', '[0-9]+');
	Route::put('reservacion/comentario','ReservationController@update_comment')->name('comment.update');
	Route::delete('reservaciones/comentario', 'ReservationController@delete_comment');
	Route::get('reservaciones/listado','ReservationController@list_reservations')->name('reservaciones.list_reservations');

	Route::get('reservaciones/listado_de_salida','ReservationController@departure_list')->name('reservaciones.departure_list');

	Route::get('reservaciones/pdf','ReservationController@pdf_report_create')->name('reservaciones.pdf_report_create_reservation');

	Route::get('reservaciones/xls','ReservationController@xls_report_create')->name('reservaciones.xls_report_create_reservation');

	Route::get('reservaciones/comentarios','ReservationController@comments')->name('reservaciones.comments');

});
