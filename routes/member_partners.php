<?php

//MemberPartners

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//MemberPartners
Route::get('socios/{user_id}/partner/autocomplete', 'MemberPartnerController@autocomplete')
		->name('s_invitados.autocomplete');

Route::get('s_invitados/alls', 'MemberPartnerController@alls')->name('s_invitados.alls');


Route::group(['middleware' => ['auth'], 'prefix' => 'socios/{user_id}'], function () {

	Route::resource('s_invitados', 'MemberPartnerController',
		['except' => ['create']],
		['parameters' =>['s_invitados' =>'partner_id']]
	);

	// Route::get('/','MemberPartnerController@index')->name('all_partners');

	// // confirmar uso
	// Route::get('/create','MemberPartnerController@create_partner')->name('create_partner');

	// // confirmar uso
	// Route::post('/','MemberPartnerController@store_partner')->name('store_partner');

	// // confirmar uso
	// Route::get('/{partner_id}','MemberPartnerController@edit_partner')->name('edit_partner');

	// // confirmar uso
	// Route::put('/{partner_id}','MemberPartnerController@update_partner')->name('update_partner');

	// // confirmar uso
	// Route::delete('/{partner_id}','MemberPartnerController@delete_partner')->name('destroy_partner');

});
