<?php

//Profile

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Profile



Route::group([ 'middleware' => ['auth'], 'prefix' => 'perfil'], function () {

Route::get('/password', 'ProfileController@index')->name('perfil.index');
Route::post('/reset', 'ProfileController@update_pass')->name('perfil.update');
Route::get('/datos', 'ProfileController@profile')->name('perfil.updateProfile');
Route::post('/datos', 'ProfileController@update_profile')->name('perfil.updateProfile');

});

