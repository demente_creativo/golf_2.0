<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class ReservationDefinitionsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		DB::table('reservation_definitions')->insert([
			'name' => 'lagunita',
			'green_fee' => $faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 99),
			'created_at' => Carbon::now(),
		]);
	}
}
