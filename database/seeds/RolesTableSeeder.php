<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('roles')->insert([
			'name' => 'Root',
			'created_at' => Carbon::now(),
		]);
		DB::table('roles')->insert([
			'name' => 'Administrador de Reservaciones',
			'created_at' => Carbon::now(),
		]);
		DB::table('roles')->insert([
			'name' => 'Administrador Financiero',
			'created_at' => Carbon::now(),
		]);
		DB::table('roles')->insert([
			'name' => 'Maletero',
			'created_at' => Carbon::now(),
		]);
		DB::table('roles')->insert([
			'name' => 'Starter',
			'created_at' => Carbon::now(),
		]);
		DB::table('roles')->insert([
			'name' => 'Socio',
			'created_at' => Carbon::now(),
		]);
    }
}
