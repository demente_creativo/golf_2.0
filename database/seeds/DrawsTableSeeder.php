<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DrawsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$start_date = Carbon::parse('next tuesday');
		$draw_date = Carbon::parse('next thursday');
		$date_saturday = Carbon::parse('next saturday');
		$date_sunday = Carbon::parse('next sunday');

		$times = [
			[
				"start_time" => "06:00:00",
				"end_time" => "06:50:00"
			],
			[
				"start_time" => "07:00:00",
				"end_time" => "07:50:00"
			],
			[
				"start_time" => "08:00:00",
				"end_time" => "08:50:00"
			],
			[
				"start_time" => "09:00:00",
				"end_time" => "09:50:00"
			],
			[
				"start_time" => "10:00:00",
				"end_time" => "10:50:00"
			],
			[
				"start_time" => "11:00:00",
				"end_time" => "11:50:00"
			],
			[
				"start_time" => "12:00:00",
				"end_time" => "12:50:00"
			],
			[
				"start_time" => "13:00:00",
				"end_time" => "13:50:00"
			],
			[
				"start_time" => "14:00:00",
				"end_time" => "14:50:00"
			],
			[
				"start_time" => "15:00:00",
				"end_time" => "15:50:00"
			],
			[
				"start_time" => "16:00:00",
				"end_time" => "16:50:00"
			],
			[
				"start_time" => "17:00:00",
				"end_time" => "17:50:00"
			],
		];

		$first = true;

		do {
			if ($first) {
				$start =$start_date;
				$end = $draw_date;
			} else {
				$start =$start_date->addDays(7);
				$end = $draw_date->addDays(7);
			}

			// Los martes inicia y los jueves se sortea
			$draw = DB::table('draws')->insertGetId(
				[
					'start_date' => $start,
					'draw_date' =>  $end,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				]
			);
	
			// Sábado
			$day_draw_sat = DB::table('day_draws')->insertGetId(
				[
					'date' => $date_saturday->addDays(7),
					'day_draw_status' => 0,
					'enabled' => 1,
					'draw_id' => $draw,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				]
			);
			
			foreach ($times as $key => $time) {
				$time_draw = DB::table('time_draws')->insertGetId(
					[ 
						'start_time' => $time["start_time"],
						'end_time' => $time["end_time"],
						'day_draw_id' => $day_draw_sat,
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now(),
					]
				);
			}

			// Domingo
			$day_draw_sun = DB::table('day_draws')->insertGetId(
				[
					'date' => $date_sunday->addDays(7),
					'day_draw_status' => 0,
					'enabled' => 1,
					'draw_id' => $draw,
					'created_at' => Carbon::now(),
					'updated_at' => Carbon::now(),
				]
			);
			
			foreach ($times as $key => $time) {
				$time_draw = DB::table('time_draws')->insertGetId(
					[ 
						'start_time' => $time["start_time"],
						'end_time' => $time["end_time"],
						'day_draw_id' => $day_draw_sun,
						'created_at' => Carbon::now(),
						'updated_at' => Carbon::now(),
					]
				);
			}

			$first =false;

		} while ($start_date <= '2022-12-27');
	}
}
