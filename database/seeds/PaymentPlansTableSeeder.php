<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PaymentPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create('es_ES');
		$deleted_at =  [Carbon::now(), null];

		for ($i=0; $i < 6; $i++) {
			DB::table('payment_plans')->insert([
				'name' => $faker->realText(50),
				'description' => $faker->realText(400),
				'rate' => $faker->randomFloat($nbMaxDecimals = 2, $min = 30, $max = 100),
				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
				'deleted_at' => $faker->randomElement($deleted_at),
			]);
		}
    }
}
