<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class ParticipantTournamentCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create('es_ES');

		$categories = Golf\Category::get();
		$categories_array = array();
		foreach ($categories as $key => $value) {
			$categories_array[$key] = $value->id;
		}

		$partners = Golf\Partner::take(5)->get();
		$partners_array = array();
		foreach ($partners as $key => $value) {
			$partners_array[$key] = $value->id;
		}

		$members = Golf\Member::take(5)->get();
		$members_array = array();
		foreach ($members as $key => $value) {
			$members_array[$key] = $value->id;
		}

		$status = array(
					'Aprobado',
					'En espera',
					'Excluido',
				);

		for ($i=0; $i < 20; $i++) {

			DB::table('participant_tournament_categories')->insert([
				'order' => rand(1,30),
				'status' => $status[rand(0,2)],

				//'reservation_definition_id' =>  $faker->randomElement($reservation_definitions_array),
				'category_id' =>  $faker->randomElement($categories_array),

				//'reservation_definition_id' =>  $faker->randomElement($reservation_definitions_array),
				'participant_id' =>  $faker->randomElement($partners_array),
				'created_at' => Carbon::now(),
			]);

			DB::table('participant_tournament_categories')->insert([
				'order' => rand(1,30),
				'status' => $status[rand(0,2)],

				//'reservation_definition_id' =>  $faker->randomElement($reservation_definitions_array),
				'category_id' =>  $faker->randomElement($categories_array),

				//'reservation_definition_id' =>  $faker->randomElement($reservation_definitions_array),
				'participant_id' =>  $faker->randomElement($members_array),
				'created_at' => Carbon::now(),
			]);
		}
    }
}
