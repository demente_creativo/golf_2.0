<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DayBlockedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    { 
        $dt = Carbon::parse('next friday');

        do {
            $day_blocked = DB::table('day_blockeds')->insertGetId([ 
                'date' => $dt->addDays(7),
                'observation' => 'Bloqueo diario del día viernes.',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ]);

            $time_blocked = DB::table('time_blockeds')->insert([ 
                'start_time' => '10:00:00',
                'end_time' => '14:30:00',
                'day_blocked_id' => $day_blocked,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
           
        } while ( $dt <= '2022-12-30' );
    }
}
