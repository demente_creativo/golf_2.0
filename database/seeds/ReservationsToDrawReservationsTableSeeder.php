	<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class ReservationsToDrawReservationsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$time_draws = Golf\TimeDraw::select('id')->orderBy("day_draw_id","asc")->get();

		$time_draws_array = array();
		foreach ($time_draws as $key => $value) {
			$time_draws_array[$key] = $value->id;
		}

		$reservations = Golf\Reservation::select('id')->get();

		$reservations_array = array();
		foreach ($reservations as $key => $value) {
			$reservations_array[$key] = $value->id;
		}

		for ($i=0; $i < 35; $i++) {
			DB::table('draw_reservations')->insert([
				'date' => $faker->dateTime('now',date_default_timezone_get()),
				'time' => $faker->time('H:i'),
				'time_draw_id' =>  $faker->randomElement($time_draws_array),
				'reservation_id' =>  $faker->randomElement($reservations_array),
				'created_at' => Carbon::now(),
			]);
		}
	}
}
