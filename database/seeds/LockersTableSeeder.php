<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class LockersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker::create('es_ES');
		$status = [
				 'Disponible',
				 'Prestamo',
				 'Indisponible',
				];

		for ($i=0; $i < 20; $i++) {

			DB::table('lockers')->insert([
				'name' => $faker->realText(50),
				'status' => $status[rand(0,2)],

				'created_at' => Carbon::now(),
				'updated_at' => Carbon::now(),
			]);
		}
    }
}
