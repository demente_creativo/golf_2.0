<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class CategoriesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$sex = [
				 'Masculino',
				 'Femenino',
				 'Mixto',
				];

		$tournaments = Golf\Tournament::get();
		$tournaments_array = array();
		foreach ($tournaments as $key => $value) {
			$tournaments_array[$key] = $value->id;
		}

		for ($i=0; $i < 10; $i++) {
			$handicap_min = rand(1,20);

			DB::table('categories')->insert([
				'name' => $faker->realText(50),
				'sex' =>$sex[rand(0,2)],
				'created_at' => Carbon::now(),
				'number_participants' => rand(5,30),
				'departure_order' => $i+1,
				'handicap_min' => $handicap_min,
				'handicap_max' => rand($handicap_min, 36),
				'tournament_id' => $faker->randomElement($tournaments_array),
			]);
		}
	}
}
