<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class ProductsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$products=[
				'Reservacion' => 'reservations',
				'Inscripción Torneo' => 'categories',
				'Inscripción Escuela' => '',
				'Mensualidad socio' => ''
			];

		foreach ($products as $key => $product) {

			DB::table('products')->insert([
				'name' =>  $key,
				'table' =>  $product,
				'created_at' => Carbon::now()
			]);
		}
	}
}
