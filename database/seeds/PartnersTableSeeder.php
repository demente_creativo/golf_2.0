<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Golf\User;
use Golf\Club;


class PartnersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');
		$users = User::whereIn('role_id',[2,6])->get();

		$users_array = array();

		foreach ($users as $key => $user) {
			$users_array[$key] = $user->id;
		}

		$clubs = Club::get();

		$clubs_array = array();

		foreach ($clubs as $key => $club) {
			$clubs_array[$key] = $club->id;
		}

		$sex =  ['Femenino', 'Masculino'];
		$exonerated =  ['Parcial','Total','Reservacion'];

		for ($i=0; $i < 10; $i++) {
			DB::table('partners')->insert([
				'name' => $faker->firstname,
				'last_name' => $faker->lastname,
				//	randomNumber($NumDig)
				'identity_card' => $faker->randomNumber(8),
				'email' => $faker->email,
				'phone' => $faker->phoneNumber,
				'handicap' => $faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 36),
				'enabled' => 1,
				'user_id' => $faker->randomElement($users_array),
				'club_id' => $faker->randomElement($clubs_array),
				'sex' => $faker->randomElement($sex),
				'exonerated' => $faker->randomElement($exonerated),
				'created_at' => Carbon::now(),
			]);
			DB::table('partners')->insert([
				'name' => $faker->firstname,
				'last_name' => $faker->lastname,
				'identity_card' => $faker->randomNumber(8),
				'email' => $faker->email,
				'phone' => $faker->phoneNumber,
				'handicap' => $faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 36),
				'enabled' => 1,
				'user_id' => $faker->randomElement($users_array),
				'club_id' => $faker->randomElement($clubs_array),
				'sex' => $faker->randomElement($sex),
				'exonerated' => $faker->randomElement($exonerated),
				'created_at' => Carbon::now(),
			]);
		}
	}
}
