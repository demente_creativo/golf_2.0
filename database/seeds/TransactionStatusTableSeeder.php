<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class TransactionStatusTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		$status=[
				'Aprobado' => 'Aprobado',
				'En proceso' => 'En proceso',
				'Pendiente' => 'Pendiente',
				'Cancelado' => 'Cancelado',
			];

		foreach ($status as $name => $description) {

			DB::table('transaction_status')->insert([
				'name' =>  $name,
				'description' =>  $description,
				'created_at' => Carbon::now(),
			]);
		}
	}
}
