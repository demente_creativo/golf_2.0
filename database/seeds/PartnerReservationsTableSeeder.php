<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class PartnerReservationsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$reservations = Golf\Reservation::select('id')->get();
		$partners = Golf\Partner::select('id')->get();

		$reservations_array = array();

		foreach ($reservations as $key => $value) {
			$reservations_array[$key] = $value->id;
		}

		$partners_array = array();

		foreach ($partners as $key => $value) {
			$partners_array[$key] = $value->id;
		}

		for ($i=0; $i < 30; $i++) {

			DB::table('partner_reservations')->insert([
				'partner_id' => $faker->randomElement($partners_array),
				'reservation_id' => $faker->randomElement($reservations_array),
				'created_at' => Carbon::now(),
			]);
		}
	}
}
