<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class MembersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$faker = Faker\Factory::create('es_ES');

		$users = Golf\User::where('role_id', '=', 6)->select('id')->get();

		$sex = ['Femenino','Masculino'];

		foreach ($users as $key => $value) {
			DB::table('members')->insert([
				'name' => $faker->firstname,
				'last_name' => $faker->lastname,
				'identity_card' => $faker->randomNumber(8),
				'sex' => $sex[rand(0,1)],
				'number_action' => $faker->randomNumber(8),
				'phone' => $faker->phoneNumber,
				'handicap' => $faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 36),
				'user_id' => $value->id,
				'created_at' => Carbon::now(),
			]);
		}
	}
}
