<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentPlansTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('payment_plans', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->text('description');
			$table->float('rate', 8, 2);

			$table->timestamps();
			$table->softDeletes();
		});
		DB::statement("ALTER TABLE `payment_plans` comment 'Planes de pago para estudiantes'");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('payment_plans');
	}
}
