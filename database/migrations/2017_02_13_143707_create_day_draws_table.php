<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDayDrawsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('day_draws', function(Blueprint $table){

			$table->increments('id');
			$table->date('date');
			$table->tinyInteger('enabled')->default(1);

			// $table->integer('draw_id')->unsigned()->index();
			// $table->foreign('draw_id')
			// 	 ->references('id')->on('draws')
			// 	 ->onUpdate('cascade')
			// 	 ->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('day_draws');
	}
}