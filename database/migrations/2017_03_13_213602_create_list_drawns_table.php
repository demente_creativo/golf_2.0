<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListDrawnsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('list_drawns', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('day_draw_id')->unsigned()->index();
			$table->foreign('day_draw_id')
				  ->references('id')->on('day_draws')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->integer('reservation_id')->unsigned()->index();
			$table->foreign('reservation_id')
				  ->references('id')->on('reservations')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
			
			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('list_drawns');
	}
}