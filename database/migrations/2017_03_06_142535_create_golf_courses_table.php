<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGolfCoursesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('golf_courses', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('interval')->unsigned();
			$table->time('start_time');
			$table->time('end_time');
			$table->tinyInteger('status')->default(1)->comment('Field to indicate if the golf course is closed or open');
			$table->tinyInteger('enabled')->default(1);
			
			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('golf_courses');
	}
}