<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkReservationDefinitionsReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('reservations', function (Blueprint $table) {

			$table->integer('reservation_definition_id')->unsigned()->nullable()->after('member_id')->index();
			$table->foreign('reservation_definition_id')
				 ->references('id')->on('reservation_definitions')
				 ->onUpdate('set null')
				 ->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('reservations', function (Blueprint $table) {
			$table->dropForeign('reservations_reservation_definition_id_foreign');
			$table->dropColumn('reservation_definition_id');
		});
	}
}