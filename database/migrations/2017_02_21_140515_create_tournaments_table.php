<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournaments', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name', 50);
			$table->date('start_date_inscription');
			$table->date('end_date_inscription');
			$table->date('start_date');
			$table->date('end_date');
			$table->time('start_time');
			$table->time('end_time');
			$table->float('inscription_fee', 50);
			$table->string('conditions_file',100)->nullable();
			$table->tinyInteger('enabled')->default(1);

			$table->integer('modality_id')->unsigned();
			$table->foreign('modality_id')->references('id')->on('modalities')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tournaments');
	}
}