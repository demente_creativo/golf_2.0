<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students', function(Blueprint $table){

			$table->increments('id');
			$table->string('identity_card');
			$table->string('name');
			$table->string('last_name');
			$table->string('phone');
			$table->string('birthdate');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('member_id')->unsigned()->nullable()->index();
			$table->foreign('member_id')
				  ->references('id')->on('members')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();

			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('students');
	}
}