<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLockerLoansTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locker_loans', function (Blueprint $table) {
			$table->increments('id');
			$table->text('observations')->nullable();

			$table->integer('locker_id')->unsigned()->index();
			$table->foreign('locker_id')
				  ->references('id')->on('lockers')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->integer('member_id')->unsigned()->index();
			$table->foreign('member_id')
				  ->references('id')->on('members')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->timestamp('finalized_at')->nullable();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('locker_loans');
	}
}
