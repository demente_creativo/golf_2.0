<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('member_reservations', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('reservation_id')->unsigned()->index();
			$table->foreign('reservation_id')
					->references('id')->on('reservations')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('member_id')->unsigned()->index();
			$table->foreign('member_id')
					->references('id')->on('members')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('member_reservations');
	}
}