<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTournamentCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('tournament_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
        Schema::table('tournament_categories', function($table){
            $table->integer('tournament_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->foreign('tournament_id')->references('id')->on('tournaments');

            $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
