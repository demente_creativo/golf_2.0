<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_categories', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('category_id')->unsigned()->index();
			$table->foreign('category_id')
				  ->references('id')->on('categories')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->integer('tournament_id')->unsigned()->index();
			$table->foreign('tournament_id')
				  ->references('id')->on('tournaments')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->integer('start_time_id')->unsigned()->index();
			$table->foreign('start_time_id')
				  ->references('id')->on('start_times')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tournament_categories');
	}
}