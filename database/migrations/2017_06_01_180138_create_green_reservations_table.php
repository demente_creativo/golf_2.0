<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGreenReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('green_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });
        Schema::table('green_reservations', function($table){
            $table->integer('green_id')->unsigned();
            $table->integer('reservation_id')->unsigned();

            $table->foreign('green_id')->references('id')->on('reservation_definitions');

            $table->foreign('reservation_id')->references('id')->on('reservations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('green_reservations');
    }
}
