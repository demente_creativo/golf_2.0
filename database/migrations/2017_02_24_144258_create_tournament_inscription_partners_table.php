<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTournamentInscriptionPartnersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tournament_inscription_partner', function (Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('partner_id')->unsigned()->index();
			$table->foreign('partner_id')
					->references('id')->on('partners')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('category_id')->unsigned()->index();
			$table->foreign('category_id')
					->references('id')->on('categories')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('tournament_inscription_partner');
	}
}