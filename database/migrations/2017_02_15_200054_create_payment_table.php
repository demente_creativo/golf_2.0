<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('payments', function(Blueprint $table){

			$table->increments('id');
			$table->string('concept');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('period_id')->unsigned()->index();
			$table->foreign('period_id')
				  ->references('id')->on('periods')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->integer('monthly_payment_id')->unsigned()->index();
			$table->foreign('monthly_payment_id')
				  ->references('id')->on('monthly_payments')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('payments');
	}
}