<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTournamentsCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('tournament_categories', function (Blueprint $table) {

			$table->time('interval')->nullable();
			$table->integer('rank')->nullable();
			$table->integer('handicap')->nullable();
			$table->integer('exit_order')->nullable();
			$table->integer('number_participants')->nullable();
			
			$table->dropForeign('tournament_categories_start_time_id_foreign');
			$table->dropColumn('start_time_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('tournament_categories', function (Blueprint $table) {
			$table->dropColumn('interval')->nullable();
			$table->dropColumn('rank')->nullable();
			$table->dropColumn('handicap')->nullable();
			$table->dropColumn('exit_order')->nullable();
			$table->dropColumn('number_participants')->nullable();
			
			$table->integer('start_time_id')->unsigned()->index();
			$table->foreign('start_time_id')
				  ->references('id')->on('start_times')
				  ->onUpdate('cascade')
				  ->onDelete('cascade');
		});
	}
}