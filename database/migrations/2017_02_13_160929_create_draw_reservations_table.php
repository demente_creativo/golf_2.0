<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDrawReservationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('draw_reservations', function (Blueprint $table) {
			$table->increments('id');
			$table->date('date');
			$table->time('time');
			$table->tinyInteger('enabled')->default(1);

			$table->integer('time_draw_id')->unsigned()->index();
			$table->foreign('time_draw_id')
					->references('id')->on('time_draws')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->integer('reservation_id')->unsigned()->index();
			$table->foreign('reservation_id')
					->references('id')->on('reservations')
					->onUpdate('cascade')
					->onDelete('cascade');

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('draw_reservations');
	}
}