// var members = [];

$( "#date_reservaciones_del_dia_starter" ).datepicker({
	dateFormat: 'yy-mm-dd',
	onSelect: function(date){
		console.log('aksd');
		reservaciones_del_dia(date);
		var date_string = new Date(date.replace(/-/g, '/')).toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});
		$('#fecha').html(date_string);
		setTimeout(function(){
			$('#date_reservaciones_del_dia_starter').val('');
		}, 10);
	}
	// minDate: new Date(),

});


$(document).ready(function(){
	// $('#tipo_participante').select2();
	$('#participante_id').select2();
	// llenar_members();

	// $('#tipo_participante').on('select2:select', function (evt) {
	// 	if (parseInt(evt.params.data.id) === 1) {
	// 		if (members.length === 0) {
	// 			$.ajax({
	// 				type: "GET",
	// 				url: '/socios/alls',
	// 				success: function(respuesta) {
	// 					members = respuesta.response;
	// 					llenar_members();
	// 				},
	// 			});
	// 		}else {
	// 			llenar_members();
	// 		}
	// 	}else {
	// 		function llenar_partners() {
	// 			var option = '<option disabled selected>Selecionar invitado</option>';
	// 			var partner_reservation = [];
	// 			$('.starter .listas_starter li ul li.personas#'+reservation_id+' .participantes li').filter('[tipo="partner"]').each(function(){
	// 				partner_reservation.push(parseInt($(this).attr('id')))
	// 			});
	// 			partners.forEach(function(partner){
	// 				if (partner_reservation.indexOf(partner.id) === -1) {
	// 					option += '<option value="'+partner.id+'">'+partner.name+' '+partner.last_name+'</option>';
	// 				}
	// 			});
	// 			$('#participante_id').html(option);
	// 			$('#select_participant').fadeIn();
	// 		}
	// 		if (partners.length === 0) {
	// 			$.ajax({
	// 				type: "GET",
	// 				url: '/invitados/alls',
	// 				success: function(respuesta) {
	// 					partners = respuesta.response;
	// 					llenar_partners();
	// 				},
	// 			});
	// 		}else {
	// 			llenar_partners();
	// 		}
	// 	}
	// });
});

$('#agregar_participantes .boton .cancelar').click(function(){
	agregar_participantes(false);
});
$('#form_add_participant').submit(function(e) {
	e.preventDefault();
	var reservation_id = parseInt($('#add_reservation_id').val());
	var type = parseInt($('#tipo_participante').val());
	var id = parseInt($('#participante_id').val());
	// console.log(reservation_id,type,id);
	// return;
	if ($('#participante_id').val() !== null) {
		$('#participante_id').parent().removeClass('error');
		$.ajax({
			type: "POST",
			url: '/starters/agregar_participante',
			data: $('#form_add_participant').serialize(),
			success: function(respuesta) {
				if (respuesta.status === true) {
					member = members.find(function(member) {
						return member.id === id;
					});
					console.log(member);
					var li = '';
					li += '<li id="'+member.id+'" tipo="member" class="ui-sortable-handle">';
					li += '	<strong><span>'+member.name+' '+member.last_name+' (S)</span></strong>';
					li += '	<div class="icon">';
					li += '		<i class="fa fa-arrows"></i>';
					li += '	</div>';
					li += '	<a href="#">';
					li += '		<div class="icon">';
					li += '			<i class="fa fa-remove"></i>';
					li += '		</div>';
					li += '	</a>';
					li += '	<div class="clear"></div>';
					li += '</li>';
					$('.starter .listas_starter li ul li.personas#'+reservation_id+' .participantes li').filter('[tipo="member"]').filter(':last-child').after(li);
					$('.drop', $('.starter .listas_starter li ul li.personas#'+reservation_id)).css('height', 10 + (26 * $('li', $('.starter .listas_starter li ul li.personas#'+reservation_id+' .participantes')).length)+'px');
					agregar_participantes(false);
				}else {
					error_draw(respuesta.message);
				}
			},
		});

	}else {
		$('#participante_id').parent().addClass('error');
	}
});

$('#eliminar_participantes .boton .cancelar').click(function(){
	eliminar_participantes(false);
});

$('#form_delete_participant').submit(function(e) {
	e.preventDefault();
	var reservation_id = parseInt($('#reservation_id').val());
	var id = parseInt($('#participant_id').val());
	var type = parseInt($('#type_id').val());
	// console.log(reservation_id,id,type);
	$.ajax({
		type: "POST",
		url: '/starters/eliminar_participante',
		data: $('#form_delete_participant').serialize(),
		success: function(respuesta) {
			// console.log(respuesta);
			if (respuesta.status === true) {
				if (type === 1) {
					$('.starter .listas_starter li ul li.personas#'+reservation_id+' .participantes li').filter('[tipo="member"]').filter('[id="'+id+'"]').remove();

				}else {
					$('.starter .listas_starter li ul li.personas#'+reservation_id+' .participantes li').filter('[tipo="partner"]').filter('[id="'+id+'"]').remove();
				}
				$('.drop', $('.starter .listas_starter li ul li.personas#'+reservation_id)).css('height', 10 + (26 * $('li', $('.starter .listas_starter li ul li.personas#'+reservation_id+' .participantes')).length)+'px');
				eliminar_participantes(false);
			}else {
				error_draw(respuesta.message);
			}
		},
	});
});


var black_capa = $('.black_capa');

function starter() {
	$('.add_participant').off('click');
	$('.add_participant').click(function(){
		var reservation_id = parseInt($('+ ul li', $(this).parent()).attr('id'));
		$('#add_reservation_id').val(reservation_id);
		function llenar_members() {
			var member_reservation = [];
				var reservation_id = parseInt($('#add_reservation_id').val());
			$('.starter .listas_starter li ul li.personas#'+reservation_id+' .participantes li').filter('[tipo="member"]').each(function(){
				member_reservation.push(parseInt($(this).attr('id')))
			});
			var option = '<option disabled selected>Selecionar socio</option>';
			members.forEach(function(member){
				if (member_reservation.indexOf(member.id) === -1) {
					option += '<option value="'+member.id+'">'+member.name+' '+member.last_name+'</option>';
				}
			});
			$('#participante_id').html(option);
		}
		llenar_members();
		agregar_participantes(true);
	});

	/* Botones de  */
	$('.activar_starter').off('click');
	$('.activar_starter').click(function(e){
		e.preventDefault();
		activar_starter($(this));
	});
	function activar_starter($this) {
		var li = $this.parent().parent();
		var id_li = '#'+li.attr('id');
		var ul_cont = $this.parent().parent().parent();
		if (li.filter('.active').length < 1) {
			li.addClass('active');
		}else {
			li.removeClass('active');
		}
	}
	$('.starter .listas_starter li .head .select .capa').off('dblclick');
	$('.starter .listas_starter li .head .select .capa').not('.no_accion').dblclick(function(){
		var select = $(this).parent();
		$('li .head .select').not(select).removeClass('active');
		select.addClass('active');
	});
	/*  Cambiar Hora */
	$('.starter .listas_starter li .head .select select').off('change');
	$('.starter .listas_starter li .head .select select').not('.no_accion').change(function(e){
		var li_sender = $(this).parent().parent().parent();
		var sender = $('.conexion', li_sender);
		var item_sender = $('li.personas', sender);
		var reservation_id = item_sender.attr('id');
		var item_sender_cloned = item_sender.clone();
		var select = $(this).parent();
		var value = $(this).val();
		var li_receive = null;
		var receive = null;
		var item_receive = null;
		var item_receive_cloned = null;
		// var receive_id = null;
		var dia = new Date().toLocaleString('es-Es', { year: "numeric", month: "2-digit", day: '2-digit'}).split('/').reverse().join('/');
		var hora = new Date(dia+' '+value);
		var horas = [null, null];
		var li_before = null;
		var li_sender_cloned = li_sender.clone();
		li_sender_cloned.attr('hora', value);
		$('.select', li_sender_cloned).removeClass('active');
		$('.select select option', li_sender_cloned).filter(':selected').prop('selected', false);
		$('.select select option', li_sender_cloned).each(function(){
			if (value == $(this).html()) {
				$(this).prop('selected', true);
			}
		});
		$('.starter .listas_starter#orden_salida li.li_principal').each(function(index){
			var hora_diff = new Date(dia+' '+$(this).attr('hora'));
			if ($(this).attr('hora') == value) {
				li_receive = $(this);
				receive = $('.conexion', li_receive);
				item_receive = $('li.personas', li_receive);
				item_receive_cloned = item_receive.clone();
				// receive_id = item_receive.attr('id');
				return false;
			}else {
				if (hora.getTime() < hora_diff.getTime()) {
					li_before = $(this);
					return false;
				}
			}
		});

		comment_starter(true);

		$('.comment_starter button').click(function() {
			if ($(this).hasClass('aceptar')) {
				$.ajax({
					type: "POST",
					url: '/starters/cambiar_hora',
					data: { start_time: value,  id: reservation_id, comment: $('#comment_hora').val()},
					success: function(respuesta) {
						if (respuesta.status) {
							if (li_receive === null) {
								if (li_before === null) { //Si hay que colocarlo en la ultima posiciion
									$('#orden_salida').append(li_sender_cloned);
								}else {
									li_before.before(li_sender_cloned);
								}
								li_sender.remove();

								var li_create = $('#orden_salida li.personas').filter('[id="'+reservation_id+'"]').parent().parent();
								setTimeout(function(){
									li_create.addClass('marcar').addClass('enviado');
									setTimeout(function(){
										li_create.removeClass('enviado').removeClass('marcar');
									}, 3000);
								}, 10);
							}else {
								item_sender.remove();
								item_receive.remove();
								sender.html(item_receive_cloned);
								receive.html(item_sender_cloned);
								setTimeout(function(){
									li_sender.addClass('marcar').addClass('recibido');
									li_receive.addClass('marcar').addClass('enviado');
									setTimeout(function(){
										li_sender.removeClass('recibido').removeClass('marcar');
										li_receive.removeClass('enviado').removeClass('marcar');
									}, 3000);
								}, 10);
								var value_actual = li_sender.attr('hora');
								$('option', select).filter('[value="'+value_actual+'"]').prop('selected', true);
							}
							starter();
							select.removeClass('active');
							$('#comment_hora').val('');
							comment_starter(false);

						}else{
							error_draw(respuesta.message);
						}
					}
				});
			}else {
				select.removeClass('active');
				$('#comment_hora').val('');
				comment_starter(false);
			}
		});

	});
	$('.starter .listas_starter li .head .select a').off('click');
	$('.starter .listas_starter li .head .select a').click(function(e){
		e.preventDefault();
		var select = $(this).parent();
		select.removeClass('active');
	});
	$( "#orden_salida .conexion" ).sortable({
		connectWith: "#orden_salida .conexion",
		helper: 'clone',
		forcePlaceholderSize: true,
		cancel: 'span, a',
		start:function(event,ui){
			$(ui.item).show();
			clone = $(ui.item).clone();
		},
		receive: function(event, ui){
			var sender = ui.sender;
			var li_sender = sender.parent();
			var item_recibido = ui.item;
			var receive = item_recibido.parent();
			var li_receive = receive.parent();
			var item_actual = $('li.personas', receive).not(item_recibido);
			var item_actual_clonado = item_actual.clone();
			var reservation_id = item_recibido.attr('id');
			var value = receive.parent().attr('hora');


			comment_starter(true);
			$('.comment_starter button').click(function() {
				if ($(this).hasClass('aceptar')) {
					$.ajax({
						type: "POST",
						url: '/starters/cambiar_hora',
						data: { start_time: value,  id: reservation_id, comment: $('#comment_hora').val()},
						success: function(respuesta) {
							if (respuesta.status) {
								item_actual.remove();
								sender.html(item_actual_clonado);
								setTimeout(function(){
									li_sender.addClass('marcar').addClass('recibido');
									li_receive.addClass('marcar').addClass('enviado');
									setTimeout(function(){
										li_sender.removeClass('recibido').removeClass('marcar');
										li_receive.removeClass('enviado').removeClass('marcar');
									}, 3000);
								}, 10);
								starter();
								$('#comment_hora').val('');
								comment_starter(false);
							}else{
								// console.log(sender, item_recibido);
								sender.html(item_recibido);
								error_draw(respuesta.message);
							}
						}
					});
				}else {
					sender.html(item_recibido);
					$('#comment_hora').val('');
					comment_starter(false);
				}
			});

		}
	}).disableSelection();

	$( "#orden_salida .conexion_participantes" ).sortable({
		connectWith: "#orden_salida .conexion_participantes",
		forcePlaceholderSize: true,
		cancel: 'a',
		start:function(event,ui){
			$(ui.item).show();
			clone = $(ui.item).clone();
		},
		receive: function(event, ui){
			var sender = ui.sender;
			var item_sender = ui.item;
			var receive = item_sender.parent();
			var item_receive = null;
			var item_receive_cloned = null;
			var id_reservation_1 = sender.attr('id');
			var id_reservation_2 = receive.attr('id');
			var id_participante_1 = item_sender.attr('id');
			var id_participante_2 = null;
			var tipo_participante_1 = (item_sender.attr('tipo') == 'member') ? 1 : 2;
			var tipo_participante_2 = null;


			var items_receive = $('li', receive).not(item_sender);

			var li = '';
			items_receive.each(function(){
				li += '<li>';
				li += '	 <a href="#" tipo="'+$(this).attr('tipo')+'" id="'+$(this).attr('id')+'">'+$('span', $(this)).html()+'</a>';
				li += '</li>';
			});
			$('.cambiar_participantes ul').html(li);
			cambiar_participantes(true);
			$('.cambiar_participantes ul li a').off('click');
			$('.cambiar_participantes ul li a').click(function(e){
				e.preventDefault();
				var tipo = $(this).attr('tipo');
				id_participante_2 = $(this).attr('id');
				item_receive = $('li', receive).filter('[id="'+id_participante_2+'"]').filter('[tipo="'+tipo+'"]');
				item_receive_cloned = item_receive.clone();
				tipo_participante_2 = (item_receive.attr('tipo') == 'member') ? 1 : 2;
				$.ajax({
					type: "POST",
					url: '/starters/cambiar_participantes',
					data: {
						reservation_id_1: id_reservation_2,
						participante_id_1: id_participante_1,
						tipo_participante_1: tipo_participante_1,
						reservation_id_2: id_reservation_1,
						participante_id_2: id_participante_2,
						tipo_participante_2: tipo_participante_2,
						comment: $('#text_comments').val()
					},
					success: function(respuesta) {
						if (respuesta.status) {
							item_receive.remove();
							sender.append(item_receive_cloned);
							cambiar_participantes(false);
							var item_enviado = $('li', receive).filter('[id="'+id_participante_1+'"]').filter('[tipo="'+((tipo_participante_1 == 1) ? 'member' : 'partner')+'"]');
							var item_recibido = $('li', sender).filter('[id="'+id_participante_2+'"]').filter('[tipo="'+((tipo_participante_2 == 1) ? 'member' : 'partner')+'"]');
							setTimeout(function(){
								item_enviado.addClass('enviado');
								item_recibido.addClass('recibido');
								setTimeout(function(){
									item_enviado.removeClass('enviado');
									item_recibido.removeClass('recibido');
								}, 3000);
							}, 10);
						}else{
							item_sender.remove();
							cambiar_participantes(false);
							error_draw(respuesta.message);
						}
						$('#text_comments').val('');
					}
				});

			});
			$('.cambiar_participantes button.omitir').off('click');
			$('.cambiar_participantes button.omitir').click(function(e){
				e.preventDefault();
				if ($('li', sender).length > 2) {
					if ($('li', receive).length < 5) {
						$.ajax({
							type: "POST",
							url: '/starters/cambiar_participantes',
							data: {
								reservation_id_1: id_reservation_2,
								participante_1: id_participante_1,
								tipo_participante_1: tipo_participante_1,
								reservation_id_2: id_reservation_1,
								comment: $('#text_comments').val()
							},
							success: function(respuesta) {
								if (respuesta.status) {
									$('.drop', sender.parent()).css('height', 10 + (26 * $('li', sender).length)+'px');
									$('.drop', receive.parent()).css('height', 10 + (26 * $('li', receive).length)+'px');
									cambiar_participantes(false);
									var item_enviado = $('li', receive).filter('[id="'+id_participante_1+'"]').filter('[tipo="'+((tipo_participante_1 === 1) ? 'member' : 'partner')+'"]');
									setTimeout(function(){
										item_enviado.addClass('enviado');
										setTimeout(function(){
											item_enviado.removeClass('enviado');
										}, 3000);
									}, 10);
								}else{
									var cloned = item_sender.clone();
									sender.append(cloned);
									item_sender.remove();
									cambiar_participantes(false);
									error_draw(respuesta.message);
								}
								$('#text_comments').val('');
							}
						});
					}else {
						error_draw('Solo son 4 participantes por reservación');
					}
				}else {
					error_draw('Minimo 3 participantes por reservación');

				}

			});
			$('.cambiar_participantes button.cancelar').off('click');
			$('.cambiar_participantes button.cancelar').click(function(e){
				e.preventDefault();
				var cloned = item_sender.clone();
				sender.append(cloned);
				item_sender.remove();
				cambiar_participantes(false);
			});
		}
	}).disableSelection();

	$('.starter_accion').off('click');
	$('.starter_accion').click(function(e){
		e.preventDefault();
		var li_principal = $(this).parent().parent();
		var li_principal_cloned = li_principal.clone();
		var tipo = $(this).attr('type');
		var id = parseInt($('li.personas', li_principal).attr('id'));
		var hora = li_principal.attr('hora');
		var hora_actual = new Date().toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit'});
		if (hora_actual.split(':')[0].length < 2) {
			hora_actual = 0+hora_actual.split(':')[0]+':'+hora_actual.split(':')[1];
		}
		var mensaje = ( tipo == 'salida' ) ? 'Dar salida a la reservación' : 'Cancelar la reservación';
		var status = ( tipo == 'salida' ) ? "Finalizada" : "Cancelada";
		function encontrar_reservation(reservation) {
			return reservation.id === id;
		}
		var reservation = reservation_day.find(encontrar_reservation);
		var datos = '';
		datos += '<input type="hidden" name="reservation_id" value="'+reservation.id+'" id="reservation_id">';
		datos += '<input type="hidden" name="status" value="'+status+'" id="reservation_id">';
		datos += '<input type="hidden" name="start_time" value="'+hora_actual+'" id="reservation_id">';
		datos += '<div class="titulo '+tipo+'">';
		datos += '	<h1> '+mensaje+': </h1>';
		datos += '</div>';
		datos += '<div class="hora">';
		datos += '	<h1> '+hora_actual+' </h1>';
		datos += '</div>';
		datos += '<div class="personas">';
		for (var im = 0; im < reservation.members.length; im++) {
			var member = reservation.members[im];
			datos += '	<h3>'+member.name+' '+member.last_name+'</h3>';
		}
		for (var ip = 0; ip < reservation.partners.length; ip++) {
			var partner = reservation.partners[ip];
			datos += '	<h3>'+partner.name+' '+partner.last_name+'</h3>';
		}
		datos += '</div>';
		datos += '<div class="comentario form-group">';
		datos += '	<label for="">Comentario</label>';
		datos += '	<textarea name="comment" rows="8" cols="80"></textarea>';
		datos += '</div>';
		datos += '<div class="form-group boton">';
		datos += '	<button id="guardar" name="singlebutton" type="button" class="btn btn-primary singlebutton1" onclick="accion_starter(\''+tipo+'\', '+id+', \''+hora_actual+'\'); return false;">Enviar</button>';
		datos += '	<button type="button" onclick="cerrar_black_capa();" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>';
		datos += '</div>';
		$('.datos', black_capa).html(datos);
		black_capa.removeClass('none');
		setTimeout(function(){
			black_capa.addClass('active');
		});
		// return false;
	});
	$('.cerrar_black_capa',  black_capa).off('click');
	$('.cerrar_black_capa',  black_capa).click(function(){
		cerrar_black_capa();
	});

	$('.eliminar_participant').off('click');
	$('.eliminar_participant').click(function(e) {
		e.preventDefault();
		var type = ($(this).parent().attr('tipo') === 'member') ? 1 : 2;
		var reservation_id = parseInt($(this).parent().parent().attr('id'));
		var id = parseInt($(this).parent().attr('id'));

		$('#eliminar_participantes #type_id').val(type);
		$('#eliminar_participantes #reservation_id').val(reservation_id);
		$('#eliminar_participantes #participant_id').val(id);

		eliminar_participantes(true);
	});
}
starter();


function cambiar_participantes(abrir) {
	if (abrir) {
		$('.cambiar_participantes').removeClass('none');
		setTimeout(function(){
			$('.cambiar_participantes').addClass('active');
		}, 10);
	}else {
		$('.cambiar_participantes').removeClass('active');
		setTimeout(function(){
			$('.cambiar_participantes').addClass('none');
		}, 500);

	}
}
function comment_starter(abrir) {
	if (abrir) {
		$('.comment_starter').removeClass('none');
		setTimeout(function(){
			$('.comment_starter').addClass('active');
		}, 10);
	}else {
		$('.comment_starter').removeClass('active');
		setTimeout(function(){
			$('.comment_starter').addClass('none');
		}, 500);

	}
}
function agregar_participantes(abrir) {
	if (abrir) {
		$('#agregar_participantes').removeClass('none');
		setTimeout(function(){
			$('#agregar_participantes').addClass('active');
		}, 10);
	}else {
		$('#agregar_participantes').removeClass('active');
		setTimeout(function(){
			$('#agregar_participantes').addClass('none');
			// $('#agregar_participantes #tipo_participante').html($('#agregar_participantes #tipo_participante').html());
			$('#agregar_participantes textarea').val('');
		}, 500);

	}
}

function eliminar_participantes(abrir) {
	if (abrir) {
		$('#eliminar_participantes').removeClass('none');
		setTimeout(function(){
			$('#eliminar_participantes').addClass('active');
		}, 10);
	}else {
		$('#eliminar_participantes').removeClass('active');
		setTimeout(function(){
			$('#eliminar_participantes').addClass('none');
			$('#eliminar_participantes textarea').val('');
		}, 500);

	}
}


function accion_starter(tipo, id, hora) {
	var li_principal = $('#orden_salida li.personas').filter('[id="'+id+'"]').parent().parent();
	var li_principal_cloned = li_principal.clone();
	var dia = new Date().toLocaleString('es-Es', { year: "numeric", month: "2-digit", day: '2-digit'}).split('/').reverse().join('/');
	var hora_ = new Date(dia+' '+hora);
	li_principal_cloned.attr('hora', hora);
	$('.starter_accion', li_principal_cloned).remove();
	$('.head .select *', li_principal_cloned).addClass('no_accion');
	$('.head .select select', li_principal_cloned).html('<option value="'+hora+'" selected>'+hora+'</option>');
	$('.participantes .icon', li_principal_cloned).remove();
	$('li.personas .drop', li_principal_cloned).remove();

	var contenedor = null;

	if (tipo == 'salida') {
		if ($('#orden_finalizada li.li_principal').length === 0) {
			contenedor = $('#orden_finalizada .reservas');
		}
		$('#orden_finalizada li.li_principal').each(function(index){
			var hora_diff = new Date(dia+' '+$(this).attr('hora'));
			if (hora_.getTime() < hora_diff.getTime()) {
				contenedor = $(this);
				return false;
			}else {
				contenedor = $('#orden_finalizada .reservas');
			}
		});
	}else {
		if ($('#orden_canceladas li.li_principal').length === 0) {
			contenedor = $('#orden_canceladas .reservas');
		}
		$('#orden_canceladas li.li_principal').each(function(index){
			var hora_diff = new Date(dia+' '+$(this).attr('hora'));
			if (hora_.getTime() < hora_diff.getTime()) {
				contenedor = $(this);
				return false;
			}else {
				contenedor = $('#orden_canceladas .reservas');
			}
		});
	}

	$.ajax({
		type: "POST",
		url: $('#form_starter').attr('action'),
		data: $('#form_starter').serialize(),
		success: function(respuesta) {
			if (contenedor.hasClass('li_principal')) {
				contenedor.before(li_principal_cloned);
				starter();
			}else {
				contenedor.append(li_principal_cloned);
				starter();
			}
		}
	});
	li_principal.remove();
	cerrar_black_capa();
}
function cerrar_black_capa() {
	black_capa.removeClass('active');
	setTimeout(function(){
		black_capa.addClass('none');
		$('.datos', black_capa).html('');
	}, 600);
}


function error_draw(mensaje) {
	$('.error_draw p').html(mensaje);
	$('.error_draw').addClass('active');
	setTimeout(function(){
		$('.error_draw').removeClass('active');
	}, 4000);
}
function reservaciones_del_dia(date) {
	$.ajax({
		type: "POST",
		url: '/starters/reservaciones_del_dia',
		data: { date: date },
		success: function(respuesta) {
			var reservations = respuesta;
			var orden_salida = '';
			var orden_finalizada = '';
			var orden_canceladas = '';
			var now = new Date().toLocaleString('es-Es', { year: "numeric", month: "2-digit", day: '2-digit'}).split('/').reverse().join('-');
			var mensaje = '';
			if (reservations.length === 0) {
				mensaje = 'No hay reservaciones para este dia';
			}else {
				reservations.forEach(function(reservation){
					if (reservation.status == 'Aprobada' || reservation.status == 'Pagada') {
						orden_salida += '<li class="li_principal" hora="'+reservation.start_time+'">';
						orden_salida += '	<div class="head">';
						orden_salida += '		<a href="" class="activar_starter" onclick="">';
						orden_salida += '			<span class="icon dropdown">';
						orden_salida += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
						orden_salida += '			</span>';
						orden_salida += '		</a>';
						orden_salida += '		<div class="select">';
						orden_salida += '			<div class="capa"></div>';
						orden_salida += '			<select class="" name="">';
						var start_times = new Date(date+' 06:00 AM');
						var end_times = new Date(date+' 06:00 PM');
						var start_times_string = null;
						for (var i = 0; i < 100; i++) {
							if (i !== 0) {
								if (start_times.getTime() == end_times.getTime()) {
									break;
								}else {
									start_times.setMinutes(start_times.getMinutes() + 10);
								}
							}
							start_times_string = start_times.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
							if (start_times_string.split(':')[0].length < 2) {
								start_times_string = 0+start_times_string.split(':')[0]+':'+start_times_string.split(':')[1];
							}
							orden_salida += '<option value="'+start_times_string+'" '+( (start_times_string == reservation.start_time) ? 'selected' : '' )+'>'+start_times_string+'</option>';
						}
						orden_salida += '			</select>';
						orden_salida += '			<a href="#" class="icon">';
						orden_salida += '				<i class="fa fa-remove"></i>';
						orden_salida += '			</a>';
						orden_salida += '		</div>';
						if (now == date) {
							orden_salida += '		<a class="icon add_participant">';
							orden_salida += '			<i class="fa fa-plus"></i>';
							orden_salida += '		</a>';
							orden_salida += '		<a class="icon check starter_accion" type="salida">';
							orden_salida += '			<i class="fa fa-check"></i>';
							orden_salida += '		</a>';
							orden_salida += '		<a class="icon cancel starter_accion" type="cancelar">';
							orden_salida += '			<i class="fa fa-remove"></i>';
							orden_salida += '		</a>';
						}
						orden_salida += '		<div class="clear"></div>';
						orden_salida += '	</div>';
						orden_salida += '	<ul class="'+((now == date) ? 'conexion' : '')+'">';
						orden_salida += '		<li class="personas" id="'+reservation.id+'">';
						orden_salida += '			<div class="datos">';
						var num_participant = reservation.members.length + reservation.partners.length;
						var height = (num_participant * 26) +10;
						orden_salida += '				<div class="drop" style="height: '+height+'px">';
						orden_salida += '					<i class="fa fa-arrows" aria-hidden="true"></i>';
						orden_salida += '				</div>';
						orden_salida += '				<ul class="participantes conexion_participantes" id="'+reservation.id+'">';
						reservation.members.forEach(function(member) {
							orden_salida += '						<li id="'+member.id+'" tipo="member">';
							orden_salida += '							<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+' <strong>(S)</strong></span>';
							orden_salida += '							<div class="icon">';
							orden_salida += '								<i class="fa fa-arrows"></i>';
							orden_salida += '							</div>';
							if (now == date) {
								orden_salida += '<a href="#" class="eliminar_participant">';
								orden_salida += '	<div class="icon">';
								orden_salida += '		<i class="fa fa-remove"></i>';
								orden_salida += '	</div>';
								orden_salida += '</a>';
							}
							orden_salida += '							<div class="clear"></div>';
							orden_salida += '						</li>';

						});
						reservation.partners.forEach(function(partner) {
							// console.log(partner);
							orden_salida += '						<li id="'+partner.id+'" tipo="partner" class="'+((now == date && partner.payed === 0) ? 'not_payed' : '')+'">';
							orden_salida += '							<span> '+partner.name+' '+partner.last_name+' <b>'+((now == date && partner.payed === 0) ? '(Sin Pago) ' : '')+'</b><strong>(I)</strong></span>';
							orden_salida += '							<div class="icon">';
							orden_salida += '								<i class="fa fa-arrows"></i>';
							orden_salida += '							</div>';
							if (now == date) {
								orden_salida += '<a href="#" class="eliminar_participant">';
								orden_salida += '	<div class="icon">';
								orden_salida += '		<i class="fa fa-remove"></i>';
								orden_salida += '	</div>';
								orden_salida += '</a>';
							}
							orden_salida += '							<div class="clear"></div>';
							orden_salida += '						</li>';

						});
						orden_salida += '				</ul>';
						orden_salida += '				<div class="clear"></div>';
						orden_salida += '			</div>';
						orden_salida += '		</li>';
						orden_salida += '	</ul>';
						orden_salida += '</li>';


					}else if (reservation.status == 'Finalizada') {
						orden_finalizada += '<li class="li_principal" hora="'+reservation.start_time+'">';
						orden_finalizada += '	<div class="head">';
						orden_finalizada += '		<a href="" class="activar_starter" onclick="">';
						orden_finalizada += '			<span class="icon dropdown">';
						orden_finalizada += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
						orden_finalizada += '			</span>';
						orden_finalizada += '		</a>';
						orden_finalizada += '		<div class="select">';
						orden_finalizada += '			<div class="capa no_accion"></div>';
						orden_finalizada += '			<select class="no_accion" name="" >';
						orden_finalizada += '				<option value="">'+reservation.start_time+'</option>';
						orden_finalizada += '			</select>';
						orden_finalizada += '			<a href="#" class="icon">';
						orden_finalizada += '				<i class="fa fa-remove"></i>';
						orden_finalizada += '			</a>';
						orden_finalizada += '		</div>';
						orden_finalizada += '		<div class="clear"></div>';
						orden_finalizada += '	</div>';
						orden_finalizada += '	<ul class="conexion">';
						orden_finalizada += '		<li class="personas" id="'+reservation.id+'">';
						orden_finalizada += '			<div class="datos">';
						orden_finalizada += '				<!-- <div class="drop">';
						orden_finalizada += '					<i class="fa fa-arrows" aria-hidden="true"></i>';
						orden_finalizada += '				</div> -->';
						orden_finalizada += '				<ul class="participantes conexion_participantes">';
						reservation.members.forEach(function(member) {
							orden_finalizada += '<li>';
							orden_finalizada += '	<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+' <strong>(S)</strong></span></span>';
							orden_finalizada += '</li>';

						});
						reservation.partners.forEach(function(partner) {
							orden_finalizada += '<li>';
							orden_finalizada += '	<span> '+partner.name+' '+partner.last_name+' <strong>(I)</strong></span></span>';
							orden_finalizada += '</li>';

						});
						orden_finalizada += '				</ul>';
						orden_finalizada += '				<div class="clear"></div>';
						orden_finalizada += '			</div>';
						orden_finalizada += '		</li>';
						orden_finalizada += '	</ul>';
						orden_finalizada += '</li>';
					}else if(reservation.status == 'Cancelada'){
						orden_canceladas += '<li class="li_principal" hora="'+reservation.start_time+'">';
						orden_canceladas += '	<div class="head">';
						orden_canceladas += '		<a href="" class="activar_starter" onclick="">';
						orden_canceladas += '			<span class="icon dropdown">';
						orden_canceladas += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
						orden_canceladas += '			</span>';
						orden_canceladas += '		</a>';
						orden_canceladas += '		<div class="select">';
						orden_canceladas += '			<div class="capa no_accion"></div>';
						orden_canceladas += '			<select class="no_accion" name="" >';
						orden_canceladas += '				<option value="">'+reservation.start_time+'</option>';
						orden_canceladas += '			</select>';
						orden_canceladas += '			<a href="#" class="icon">';
						orden_canceladas += '				<i class="fa fa-remove"></i>';
						orden_canceladas += '			</a>';
						orden_canceladas += '		</div>';
						orden_canceladas += '		<div class="clear"></div>';
						orden_canceladas += '	</div>';
						orden_canceladas += '	<ul class="conexion">';
						orden_canceladas += '		<li class="personas" id="'+reservation.id+'">';
						orden_canceladas += '			<div class="datos">';
						orden_canceladas += '				<!-- <div class="drop">';
						orden_canceladas += '					<i class="fa fa-arrows" aria-hidden="true"></i>';
						orden_canceladas += '				</div> -->';
						orden_canceladas += '				<ul class="participantes conexion_participantes">';
						reservation.members.forEach(function(member) {
							orden_canceladas += '<li>';
							orden_canceladas += '	<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+' <strong>(S)</strong></span></span>';
							orden_canceladas += '</li>';

						});
						reservation.partners.forEach(function(partner) {
							orden_canceladas += '<li>';
							orden_canceladas += '	<span> '+partner.name+' '+partner.last_name+' <strong>(I)</strong></span></span>';
							orden_canceladas += '</li>';

						});
						orden_canceladas += '				</ul>';
						orden_canceladas += '				<div class="clear"></div>';
						orden_canceladas += '			</div>';
						orden_canceladas += '		</li>';
						orden_canceladas += '	</ul>';
						orden_canceladas += '</li>';

					}
				});

			}


			$('#orden_salida .reservas').html(orden_salida);
			$('#orden_canceladas .reservas').html(orden_canceladas);
			$('#orden_finalizada .reservas').html(orden_finalizada);
			$('#starters .mensaje p').html(mensaje);
			starter();

		}
	});
}
