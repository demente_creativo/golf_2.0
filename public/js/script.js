$.datepicker.regional['es'] = {
	closeText: 'Cerrar',
	prevText: '< Ant',
	nextText: 'Sig >',
	currentText: 'Hoy',
	monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	weekHeader: 'Sm',
	dateFormat: 'dd/mm/yy',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['es']);

$(document).ready(function(){
	modalDelete();
	/************************************    Inicialiacion de Pluggins    ************************************/
	$(".carousel").carousel({  // Torneos y Home
		interval: 5000
	});
	$(".collapse").collapse({
		toggle: false
	});
	/************************************    Inicialiacion de Funciones Generales    ************************************/
	mostrar_alertas(); /* Alertas */

	/***************************************     Data Table     ***********************************************/
	$('.table').DataTable({
		"paging": true,
		"lengthChange": true,
		"searching": true,
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"lengthMenu": [[8, 20, 50, -1], [8, 20, 50, "Todo"]],
		"language": {
			"emptyTable":     "No hay registros disponibles",
			"info":           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
			"infoEmpty":      "Mostrando 0 de 0 en 0 entradas",
			"infoFiltered":   "(filtrando _MAX_ de total de entradas)",
			"lengthMenu":     "Mostrar _MENU_ entradas",
			"loadingRecords": "Cargando...",
			"processing":     "Procesando...",
			"search":         '<i class="fa fa-search" aria-hidden="true"></i>',
			"zeroRecords":    "No se encontraron registros",
			"paginate": {
				"first":      "Primera",
				"last":       "Última",
				"next":       ">>",
				"previous":   "<<"
			},
		}
	});

	var table = $('#example').DataTable();

	/*****************************************      Date Pickers      *****************************************/

	 /* DatePicker COntodas las fechas y cambio de mes y año */
	 $( ".datepicker_change_month" ).datepicker({
	 		dateformat: 'dd-mm-yy',
			changemonth: true,
			changeyear: true,
	 });
	 /* Datepicker Con todas Las Fechas*/
	 $( ".datepicker_alls" ).datepicker({
		   dateFormat: 'dd-mm-yy',
	 });
	 function end_datepciker_alls() {
		   $('.end_datepciker_alls').datepicker( "destroy" );
		   $( ".end_datepciker_alls" ).datepicker({
				   dateFormat: 'dd-mm-yy',
				   minDate: $('.datepicker_alls').val(),
		   });
	 }
	 $(document).ready(end_datepciker_alls);

	 $('.datepicker_alls').change(function(){
	   $('.end_datepciker_alls').val('');
	   end_datepciker_alls();
	 });

	 /* DatePicker COn las fehcas a partir de hoy*/
	 $( ".datepicker_min" ).datepicker({
	 		dateFormat: 'dd-mm-yy',
	 		minDate: new Date(),
	 });
	 function datepicker_min() {
	 		$('.end_datepicker_min').datepicker( "destroy" );
	 		$( ".end_datepicker_min" ).datepicker({
	 				dateFormat: 'dd-mm-yy',
	 				minDate: $('.datepicker_min').val(),

	 		});
	 }
	 $(document).ready(datepicker_min);

	 $('.datepicker_min').change(function(){
	 	$('.end_datepicker_min').val('');
	 	datepicker_min();
	 });

	 /* DatePicker con Marcas disabled*/
	 $( ".datepicker_mark_disabled" ).datepicker({
	 		dateFormat: 'dd-mm-yy',
	 		minDate: new Date(),
	 		beforeShowDay: function( date ) {
				var tipo_dia = beforeShowDay(date);
				if( tipo_dia.dias_bloqueados ) {
					 return [true, "dia_bloqueado", 'Dia Bloqueado'];
				}else if( tipo_dia.sorteable ) {
					 return [false, "sorteable", 'Dia Sorteable'];
				}else if (tipo_dia.torneo) {
					return [false, "torneo", 'Dia de torneo'];
				}else {
					return [true, '', ''];
				}
	 	    }
	 });
	 function datepicker_mark_disabled() {
	 		$('.end_datepicker_mark_disabled').datepicker( "destroy" );
	 		$( ".end_datepicker_mark_disabled" ).datepicker({
	 				dateFormat: 'dd-mm-yy',
	 				minDate: $('.datepicker_mark_disabled').val(),
	 				beforeShowDay: function( date ) {
						var tipo_dia = beforeShowDay(date);
						if( tipo_dia.dias_bloqueados ) {
							 return [true, "dia_bloqueado", 'Dia Bloqueado'];
						}else if( tipo_dia.sorteable ) {
							 return [false, "sorteable", 'Dia Sorteable'];
						}else if (tipo_dia.torneo) {
							return [false, "torneo", 'Dia de torneo'];
						}else {
							return [true, '', ''];
						}
	 			    }
	 		});
	 }
	 $(document).ready(datepicker_mark_disabled);
	 $('.datepicker_mark_disabled').change(function(){
	 	$('.end_datepicker_mark_disabled').val('');
	 	datepicker_mark_disabled();
		datepicker_mark_disabled_2_1();
	 });
	 function datepicker_mark_disabled_2_1() {
		 $('.datepicker_mark_disabled_2').val();
		 $('.end_datepicker_mark_disabled_2').val();
		 /* DatePicker con Marcas disabled 2*/
		 $('.datepicker_mark_disabled_2').datepicker( "destroy" );
		 $( ".datepicker_mark_disabled_2" ).datepicker({
			 dateFormat: 'dd-mm-yy',
			 minDate: new Date(),
			 maxDate: $(".datepicker_mark_disabled").val(),
			 beforeShowDay: function( date ) {
				 var tipo_dia = beforeShowDay(date);
				 if( tipo_dia.sorteable ) {
					 return [false, "sorteable", 'Dia Sorteable'];
				 }else if (tipo_dia.torneo) {
					 return [false, "torneo", 'Dia de torneo'];
				 }else {
					 return [true, '', ''];
				 }
			 }
		 });
		 function datepicker_mark_disabled_2() {
			 $('.end_datepicker_mark_disabled_2').datepicker( "destroy" );
			 $( ".end_datepicker_mark_disabled_2" ).datepicker({
				 dateFormat: 'dd-mm-yy',
				 minDate: $('.datepicker_mark_disabled_2').val(),
				 maxDate: $(".datepicker_mark_disabled").val(),
				 beforeShowDay: function( date ) {
					 var tipo_dia = beforeShowDay(date);

					 if( tipo_dia.sorteable ) {
						 return [false, "sorteable", 'Dia Sorteable'];
					 }else if (tipo_dia.torneo) {
						 return [false, "torneo", 'Dia de torneo'];
					 }else {
						 return [true, '', ''];
					 }
				 }
			 });
		 }
		 $(document).ready(datepicker_mark_disabled_2);
		 $('.datepicker_mark_disabled_2').change(function(){
			 $('.end_datepicker_mark_disabled_2').val('');
			 datepicker_mark_disabled_2();
		 });
	 }

	 /* DatePicker con Marcas disabled*/
	 $( ".datepicker_mark_disabled_edit" ).datepicker({
	 		dateFormat: 'dd-mm-yy',
	 		minDate: new Date(),
	 		beforeShowDay: function( date ) {
				var tipo_dia = beforeShowDay(date);
				if( tipo_dia.sorteable ) {
					 return [false, "sorteable", 'Dia Sorteable'];
				}else if (tipo_dia.torneo) {
					return [true, "torneo", 'Dia de torneo'];
				}else {
					return [true, '', ''];
				}
	 	    }
	 });
	 function datepicker_mark_disabled_edit() {
	 		$('.end_datepicker_mark_disabled_edit').datepicker( "destroy" );
	 		$( ".end_datepicker_mark_disabled_edit" ).datepicker({
	 				dateFormat: 'dd-mm-yy',
	 				minDate: $('.datepicker_mark_disabled_edit').val(),
	 				beforeShowDay: function( date ) {
						var tipo_dia = beforeShowDay(date);

						if( tipo_dia.sorteable ) {
							 return [false, "sorteable", 'Dia Sorteable'];
						}else if (tipo_dia.torneo) {
							return [true, "torneo", 'Dia de torneo'];
						}else {
							return [true, '', ''];
						}
	 			    }
	 		});
	 }
	 $(document).ready(datepicker_mark_disabled_edit);
	 $('.datepicker_mark_disabled_edit').change(function(){
	 	$('.end_datepicker_mark_disabled_edit').val('');
	 	datepicker_mark_disabled_edit();
	 });

	 /* DatePicker con Marcas disabled 2*/
	 $( ".datepicker_mark_disabled_edit_2" ).datepicker({
			dateFormat: 'dd-mm-yy',
			minDate: new Date(),
			beforeShowDay: function( date ) {
				var tipo_dia = beforeShowDay(date);
				if( tipo_dia.sorteable ) {
					 return [false, "sorteable", 'Dia Sorteable'];
				}else if (tipo_dia.torneo) {
					return [false, "torneo", 'Dia de torneo'];
				}else {
					return [true, '', ''];
				}
			}
	 });
	 function datepicker_mark_disabled_edit_2() {
			$('.end_datepicker_mark_disabled_edit_2').datepicker( "destroy" );
			$( ".end_datepicker_mark_disabled_edit_2" ).datepicker({
					dateFormat: 'dd-mm-yy',
					minDate: $('.datepicker_mark_disabled_edit_2').val(),
					beforeShowDay: function( date ) {
						var tipo_dia = beforeShowDay(date);

						if( tipo_dia.sorteable ) {
							 return [false, "sorteable", 'Dia Sorteable'];
						}else if (tipo_dia.torneo) {
							return [false, "torneo", 'Dia de torneo'];
						}else {
							return [true, '', ''];
						}
					}
			});
	 }
	 $(document).ready(datepicker_mark_disabled_edit_2);
	 $('.datepicker_mark_disabled_edit_2').change(function(){
		$('.end_datepicker_mark_disabled_edit_2').val('');
		datepicker_mark_disabled_2();
	 });







	/*****************************************      Mask      *****************************************/
	$('.money').mask('000000.00', {reverse: true}); //se cambio la mascara eliminando un cero para que se ajustara a la longitud en la base de datos
	$('.money_green_feed').mask('0.000.000,00', {reverse: true});
	$('.numParticiped').mask('000');
	$('.date').mask('00-00-0000');
	$('.phone').mask('0000 000-00-00');
	$('.cedula').mask('00000000');
	$('.number_action').mask('00000000000000000000')
	$('.input_time').mask('00:00 AA');
	$('.3_digit').mask('000');
	$('.2_digit').mask('00');
	$('.handican').mask('00.0');
	$('.rate').mask('000000');
	$('.amount').mask('000000');
	$('.handicap_patter').mask('0.0');





	/********************************     Modals Create, Delete, Show     *******************************/
	/* Modal Delete*/
	function modalDelete() {
		$(".modaldelete").off('click');
		$(".modaldelete").click(function(){
			var ruta = $(this).attr('ruta');
			$('#modaldelete form').attr('action', ruta );
			$("#modaldelete").modal();
		});
	/* Modal Delete*/

	$(".modalcreate").click(function(){
		$("#modalcreate").modal();
	});
	/* Modal Validatios*/
	$(".modal_validation").click(function(){
		var url = $(this).attr('href');
		var route = $(this).attr('route');
		$.ajax({
			type: "GET",
			url: route,
			//data: {id: id,exonerated: exonerated},
			success: function(respuesta) {
				if (respuesta.status == true) {
					window.location.href = url;
				} else {
					$("#form_modal_validate").attr('action', url);
					$("#modal_validation").modal();
				}
			}
		});
		return false;
	});

	$(".addgroup").click(function(){
		$("#addgroup").modal();
	});

	$(".addperiod").click(function(){
		$("#addperiod").modal();
	});
	/* Show Category */
	$(".category_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			success: function(respuesta) {
				var category = respuesta.category;

				$('#d_name').html(category.name);
				$('#d_number_participants').html(category.number_participants);
				$('#d_genre').html(category.sex);
				$('#d_handicap_min').html(category.handicap_min);
				$('#d_handicap_max').html(category.handicap_max);
				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Partner */
	$(".partner_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			success: function(respuesta) {
				var partner = respuesta.partner;
				var club = partner.club;

				$('#d_name').html(partner.name);
				$('#d_last_name').html(partner.last_name);
				$('#d_identity_card').html(partner.identity_card);
				$('#d_sex').html(partner.sex);
				$('#d_phone').html(partner.phone);
				$('#d_email').html(partner.email);
				$('#d_handicap').html(partner.handicap);
				$('#d_exonerated').html(partner.exonerated);
				$('#d_club').html(club.name);

				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Members */
	$(".member_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			success: function(respuesta) {
				var member = respuesta.member;
				var user = member.user;
				$('#d_name').html(member.name);
				$('#d_last_name').html(member.last_name);
				$('#d_identity_card').html(member.identity_card);
				$('#d_sex').html(member.sex);
				$('#d_number_action').html(member.number_action);
				$('#d_handicap').html(member.handicap ?  member.handicap : 'No Indicado');
				$('#d_phone').html(member.phone);
				$('#d_email').html(user.email);
				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Paymen_plans */

	$(".payment_plan_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			//data: {id: id,exonerated: exonerated},
			success: function(respuesta) {
				var payment_plan = respuesta.payment_plan;

				$('#d_name').html(payment_plan.name);
				$('#d_description').html(payment_plan.description);
				$('#d_rate').html(payment_plan.rate.toLocaleString('de-DE', { style: 'currency', currency: 'BsF' }));

				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Registration */
	$(".registration_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			success: function(respuesta) {
				var registration = respuesta.registration;
				var student = registration.student;
				var member = registration.student.member;
				var group = registration.group;
				var payment_plan = registration.payment_plan;
				
				$('#d_member').html(registration.student.member.name+' '+registration.student.member.last_name);	
				$('#d_student').html(registration.student.name+' '+registration.student.last_name);
				$('#d_group').html(registration.group.name);
				$('#d_payment_plan').html(registration.payment_plan.name);
				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Teachers */
	$(".teacher_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			//data: {id: id,exonerated: exonerated},
			success: function(respuesta) {
				var teacher = respuesta.teacher;

				$('#d_name').html(teacher.name);
				$('#d_last_name').html(teacher.last_name);
				$('#d_phone').html(teacher.phone);
				$('#d_email').html(teacher.email);

				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Group */
	$(".group_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			//data: {id: id,exonerated: exonerated},
			success: function(respuesta) {
				var group = respuesta.group;
				var teacher = group.teacher;

				$('#d_name').html(group.name);
				$('#d_teacher').html(teacher.name+' '+teacher.last_name);
				$('#d_email').html(teacher.email);

				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Period */
	$(".period_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			//data: {id: id,exonerated: exonerated},
			success: function(respuesta) {
				var period = respuesta.period;
				var group = period.group;

				$('#d_name').html(period.name);
				$('#d_start_date').html(period.date_start);
				$('#d_end_date').html(period.date_end);
				$('#d_price_inscription').html(period.price_inscription.toLocaleString('de-DE', { style: 'currency', currency: 'BsF' }));
				$('#d_group').html(group.name);

				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Student */
	$(".student_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			success: function(respuesta) {
				var student = respuesta.student;
				var member = student.member;

				$('#d_name').html(student.name);
				$('#d_last_name').html(student.last_name);
				$('#d_identity_card').html(student.identity_card);
				$('#d_phone').html(student.phone);
				$('#d_birthdate').html(student.birthdate);
				$('#d_member').html(member.name + ' ' +  member.last_name);

				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Admin */
	$(".admin_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			success: function(respuesta) {
				var user = respuesta.user;
				var role = user.role;

				$('#d_name').html(user.name);
				$('#d_email').html(user.email);
				$('#d_role').html(role.name);

				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	/* Show Monthly Payment */
	$(".monthly_payment_show").click(function(){
		var url = $(this).attr('href');
		$.ajax({
			type: "GET",
			url: url,
			success: function(respuesta) {
				var monthly_payment = respuesta.monthly_payment;
				var member = monthly_payment.member;
				var student = monthly_payment.student;
				var period = monthly_payment.period;

				$('#d_amount').html(monthly_payment.amount.toLocaleString('de-DE', { style: 'currency', currency: 'BsF' }));
				$('#d_date_payment').html(monthly_payment.date_payment);
				$('#d_method_payment').html(monthly_payment.method_payment);
				$('#d_number_receipt').html(monthly_payment.number_receipt);
				$('#d_observation').html(monthly_payment.observation);
				$('#d_period').html(period.name);
				$('#d_student').html(student.name + ' ' +  student.last_name);
				$('#d_member').html(member.name + ' ' +  member.last_name);

				$("#modaldetalles").modal();
			}
		});
		return false;
	});
	}
	/* Show Turnament */
	// $(".boton_ver_detalle").click(function(){
	// 	var url = $(this).attr('href');
	// 	var id = $(this).attr('id');
	// 	$.ajax({
	// 		type: "GET",
	// 		url: url,
	// 		data: {id: id},
	// 		success: function(torneo) {
	// 			$('#ver_detalles_torneo #name').val(torneo.name);
	// 			$('#ver_detalles_torneo #start_date_tournament').val(torneo.start_date);
	// 			$('#ver_detalles_torneo #end_date_tournament').val(torneo.end_date);
	// 			$('#ver_detalles_torneo #inscription_fee').val(torneo.inscription_fee);
	// 			$('#ver_detalles_torneo #modality').html("<option value="+torneo.modality.id+" disabled selected>"+torneo.modality.name+"</option>");
	// 			html = "";
	// 			for (var i = torneo.categories.length - 1; i >= 0; i--) {
	// 				html += "<option value="+torneo.categories[i].id+" disabled selected>"+torneo.categories[i].name+"</option>";
	// 			}
	// 			$('#ver_detalles_torneo #category').html(html);
	// 			$('#ver_detalles_torneo #start_time').val(torneo.start_time);
	// 			$('#ver_detalles_torneo #end_time').val(torneo.end_time);
	// 			$('#ver_detalles_torneo #start_date_inscription').val(torneo.start_date_inscription);
	// 			$('#ver_detalles_torneo #end_date_inscription').val(torneo.end_date_inscription);
	// 			$("#file_tournament_detail").fileinput('destroy');
	// 			$("#file_tournament_detail").fileinput({
	// 				initialPreview: [
	// 					$('#file_tournament_detail').attr('asset')+torneo.conditions_file
	// 				],
	// 				initialPreviewAsData: true,
	// 				initialPreviewConfig: [
	// 					{type: "pdf", caption: torneo.conditions_file, showDelete: false},
	// 				],
	//
	// 				showUpload: false,
	// 				showRemove: false,
	// 				showBrowse: false,
	// 				showClose: false,
	// 				showCaption: false,
	// 				// showCancel: fasle,
	// 				// showUploadedThumbs: false,
	// 				// 'previewFileType': 'any'
	// 				maxFileCount: 1,
	// 				msgZoomModalHeading: 'Vista previa',
	// 				allowedFileExtensions: ['pdf', 'docx'],
	// 				previewFileIconSettings: {
	// 					'doc': '<i class="fa fa-file-word-o text-primary"></i>',
	// 					'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
	// 				},
	// 				// showBrowse: false
	// 			});
	// 		}
	// 	});
	// 	$("#ver_detalles_torneo").modal();
	// 	return false;
	// });






	/********** *** * ** * * ** ** * * ***********/
	/********** *** * ** * * ** ** * * ***********/
	/********** *** * ** * * ** ** * * ***********/
	/********** *** * ** * * ** ** * * ***********/
	$("#myBtn").click(function(){
			$("#myModal").modal();
			$('.hiden').fadeIn();
			$('#myBtn1').prop('disabled',false);
			$('#myBtn3').prop('disabled',false);
	});
	$("#myBtn3").click(function(){
			$("#myModal3").modal();
	});
	$("#myBtn1").click(function(){
			$("#myModal1").modal();
	});


	/***************************   Deshabilitar botones   ***************************/
	$(".inhabilitar").submit(function(){
		$('button[type="submit"]', this).prop("disabled",true);
	});
	/***************************************   Menu    ***************************************/
	/******************************************************/
	/****************** Abrir Sub_menus *******************/
	/******************************************************/
	$('.sb_mn').click(function(){
		var li = $(this).parent();
		var sub_menu = $('.sub_menu', li);
		if (li.filter('.active').length <= 0) {
				$('.sub_menu').removeClass('active');
				li.addClass('active');
		}else {
				li.removeClass('active');
		}
		return false;
	});



	/******************************************************/
	/******************* Cerrar Alertas *******************/
	/******************************************************/
	$('.alerta .alert button').click(function(){
			var alerta = $(this).parent().parent();
			alerta.removeClass('mostrar');
			setTimeout(function(){
					alerta.remove();
			},1000);
	});
	function mostrar_alertas() {
		var alerta = $('.alerta');
		setTimeout(function(){
			if (alerta.length >= 1) {
				alerta.addClass('mostrar');
			}
			setTimeout(function(){
				alerta.removeClass('mostrar');
			},7000);
		}, 1000);

	}
	/* Mostrar Alertas*/






	$("a#crear_reporte_pdf,a#crear_reporte_xls").click(function(e) { //List_Report
		e.preventDefault();
		parametros = '';
		if ($("select#status").length > 0) {
			status_val = $("select#status").val();
			parametros = "?status="+status_val;
		}
		if ($("select#tournament_id").length > 0) {
			tournament_val = $("select#tournament_id").val();
			if (parametros.length == 0) {
				parametros = "?tournament_id="+tournament_val;
			} else {
				parametros = "&tournament_id="+tournament_val;
			}
		}
		if ($("select#group_id").length > 0) {
			group_val = $("select#group_id").val();
			if (parametros.length == 0) {
				parametros = "?group_id="+group_val;
			} else {
				parametros = "&group_id="+group_val;
			}
		}
		start_date_val = $("input#start_date").val();
		end_date_val = $("input#end_date").val();
		location.href = $(this).attr("href")+parametros+"&start_date="+start_date_val+"&end_date="+end_date_val;
	});
	// List Report

	$("#myBtn").click(function(){
		$("#myModal").modal();
	});

		$("#insc").click(function(){
		$("#newModal").modal();
	});

	$("#capture").submit(function(e){

		e.preventDefault();
		e.stopPropagation();
		var form= $('#capture').serializeArray();
		var array = {
		  name: $('#capture #name').val(),
		  number_participants: $('#capture #number_participants').val(),
		  sex: $('#capture #sex').val(),
		  handicap_min: $('#capture #handicap_min').val(),
		  handicap_max: $('#capture #handicap_max').val(),
		};
		var formtosend= JSON.stringify(array);
		var a= "<option value='"+formtosend+"' selected>"+$('#capture #name').val()+"</option>";
		$('#capture')[0].reset();
		$('#myModal').modal('hide');
		$('#category').append(a);

	});

	$("div.table.report li a").click(function (e) {
		e.stopPropagation();
		e.preventDefault();
		status_val = $("select#status").val();
		start_date_val = $("input#start_date").val();
		end_date_val = $("input#end_date").val();
		location.href = $(this).attr("href")+"&+status="+status_val+"&start_date="+start_date_val+"&end_date="+end_date_val;
	 });
});

$('.select_categories').select2();



$("#enviar").click(function(){
	Login();
});






/* Crear Categoria */

$('#form_category').submit(function(){
	var ruta = $(this).attr('ruta');
	$.ajax({
		type: "POST",
		url: ruta,
		data: $('#form_category').serialize(),
		success: function(respuesta) {
			if (respuesta.type == 'success') {
				var option = '<option value="'+respuesta.category.id+'" selected> '+ respuesta.category.name +'</option>';
				$('#category').append(option);
				$('#myModal').modal('hide');
				$('#form_category')[0].reset();
			}
		}
	});
	return false;
});






/* ************************************************ Maletero ************************************************ */

// $('.maletero li a').click(function(e){
// 	e.preventDefault();
// 	e.stopPropagation();
// 	if ($(this).filter('.accion').length > 0) {
// 		maletero($(this));
// 	}
// });
// var capa_maletero = $('.capa_maletero');
// function maletero($this) {
// 	var li = $this.parent();
// 	var locker = parseInt($this.attr('href').split('#')[1]);
// 	$('#form_maletero_prestamo').attr('locker_id', locker);
// 	var disponible = (li.filter('.Prestamo').length <= 0) ? true : false;
// 	for (var i = 0; i < lockers.length; i++) {
// 		var lock = lockers[i];
// 		if (lock.id === locker) {
// 			locker = lockers[i];
// 		}
// 	}
// 	var observation_name = (!disponible) ? 'observations_out' : 'observations_in';
// 	var mensaje = (disponible) ? 'Dar salida a equipo' : 'Dar entrada a equipo';
// 	var input = '';
// 	if (!disponible) {
// 		input += '<input type="hidden" name="_method" value="PUT">';
// 	}
// 	input += '		<input type="hidden" name="locker_id" value="'+locker.id+'" id="">';
// 	input += '		<div class="titulo">';
// 	input += '			<h1> '+mensaje+': </h1>';
// 	input += '		</div>';
// 	input += '		<div class="hora">';
// 	input += '			<h1> '+ hora() +'</h1>';
// 	input += '		</div>';
// 	input += '		<div class="add_socios add_">';
// 	if (disponible) {
// 		input += '			<div class="form-group">';
// 		input += '				<label class="col-4 col-md-center control-label" for="receipt_payment">Buscar Socios</label>';
// 		input += '				<div class="col-6 col-md-center" >';
// 		input += '					<input id="add_members_maletero" type="text" class="search form-control input-md" placeholder="Buscar Socios" name="search_socio" ruta="'+autocomplete_socios+'">';
// 		input += '				</div>';
// 		input += '				<div class="mensaje">';
// 		input += '				</div>';
// 		input += '			</div>';
// 	}
// 	input += '			<ul class="">';
// 	if (!disponible) {
// 		var member = locker.locker_loans[0].member;
// 		input += '<li id="member_'+member.id+'" class="member_reservation" id-="'+member.id+'">';
// 		input += '	<input type="hidden" class="member_id" name="member_id" value="'+member.id+'">';
// 		input += '	<div class="nombre">'+member.name+' '+ member.last_name+'</div>';
// 		input += '	<div>C.I: '+ number_format(member.identity_card, 0, ',', '.') +'</div><br>';
// 		input += '	<div>N° Acción: '+ member.number_action+'</div>';
// 		input += '</li>';
// 	}
// 	input += '			</ul>';
// 	input += '		</div>';
// 	input += '		<div class="form-group textarea">';
// 	input += '			<label class="col-4 col-md-center control-label" for="receipt_payment">Observaciones</label>';
// 	input += '			<div class="col-6 col-md-center" >';
// 	input += '				<textarea name="'+observation_name+'" rows="8" cols="80"></textarea>';
// 	input += '			</div>';
// 	input += '		</div>';
// 	input += '		<div class="form-group boton">';
// 	input += '			<button id="guardar" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Enviar</button>';
// 	input += '			<button type="button" onclick="cerrar_capa_maletero();" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>';
// 	input += '		</div>';
// 	input = $.parseHTML(input);
//
// 	setInterval(function(){
// 		$('.capa_maletero .datos .hora h1').html(hora());
// 	}, 1000);
//
// 	if (!disponible) {
// 		$('.capa_maletero .add_ .form-group').remove();
// 		capa_maletero.addClass('Prestamo');
// 	}else{
// 		capa_maletero.removeClass('Prestamo');
// 	}
// 	$('.datos', capa_maletero).html(input);
// 	capa_maletero.removeClass('none');
// 	setTimeout(function(){
// 		capa_maletero.addClass('active');
// 	});
//
// 	$( "#add_members_maletero" ).autocomplete({
// 		source: $('#add_members_maletero').attr('ruta'),
// 		minLength: 1,
// 		select: function(event, ui) {
// 			var mensaje = $('.mensaje', $('#add_members_maletero').parent().parent());
// 			var li, text;
// 			li =	'<li id="member_'+ui.item.id+'" class="member_reservation" id-="'+ui.item.id+'">';
// 			li +=		'<input type="hidden" class="member_id" name="member_id" value="'+ui.item.id+'">';
// 			li +=		'<div class="nombre">';
// 			li +=			ui.item.value;
// 			li +=		'</div>';
// 			li +=		'<div>';
// 			li +=			'C.I: '+ number_format(ui.item.identity_card, 0, ',', '.');
// 			li +=		'</div>';
// 			li +=		'<div>';
// 			li +=			'N° Acción: '+ ui.item.number_action;
// 			li +=		'</div>';
// 			li +=		'<div class="cerrar" onclick="cerrar_add_maletero(\'#member_'+ui.item.id+'\'); return false;">';
// 			li +=			'<a href="#" class="cerrar_add">';
// 			li +=				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
// 			li +=			'</a>';
// 			li +=		'</div>';
// 			li +=	'</li>';
// 			if ($('#member_'+ui.item.id).length <= 0) {
// 				$('#socio_pagador').append('<option id="option_member_'+ui.item.id+'" value="'+ui.item.id+'">'+ui.item.value+'</option>');
// 				$('#member_id_prestamo').val();
// 				$('.add_socios ul').not('.socio_auth').append(li);
// 			}else {
// 				$( "#add_members_maletero" ).val('');
// 				text = '<p> El socio '+ui.item.value+' esta en la agregado  </p>';
// 				mensaje.html(text);
// 				mensaje.addClass('mostrar');
// 				$('#member_'+ui.item.id).addClass('marcar');
//
// 				setTimeout(function(){
// 					$('#member_'+ui.item.id).removeClass('marcar');
// 					mensaje.removeClass('mostrar');
// 				}, 5000);
// 			}
// 			setTimeout(function(){
// 				$( "#add_members" ).val('');
// 				if ($('.capa_maletero .add_ ul li').length !== 0) {
// 					$('#add_members_maletero').prop('disabled', true);
// 				}
// 			},20);
// 		}
// 	});
// 	$('.cerrar_capa_maletero',  capa_maletero).click(function(){
// 		if (cerrar_capa_maletero()) {
// 			return false;
// 		}
// 	});
// }
//
// function remove_locker(id) {
// 	var locker;
// 	for (var i = 0; i < lockers.length; i++) {
// 		var lock = lockers[i];
// 		if (lock.id === id) {
// 			locker = lockers[i];
// 			indice_locker = i;
// 		}
// 	}
// 	var action = locker.remove;
// 	$('#modaldelete form').attr('action', action );
// 	$("#modaldelete").modal();
// }
// function edit_locker(id) {
// 	var locker;
// 	for (var i = 0; i < lockers.length; i++) {
// 		var lock = lockers[i];
// 		if (lock.id === id) {
// 			locker = lockers[i];
// 			indice_locker = i;
// 		}
// 	}
//
// 	var input = '';
// 	input += '<div class="form-group">';
// 	input += '	<label class="col-md-4 control-label" for="textinput">Número</label>';
// 	input += '	<div class="col-md-6">';
// 	input += '		<input id="textinput" name="name" type="text" placeholder="Número" class="form-control input-md" value="'+locker.name+'" required>';
// 	input += '	</div>';
// 	input += '</div>';
// 	input += '<div class="form-group">';
// 	input += '	<label class="col-md-4 control-label" for="name">Estado</label>';
// 	input += '	<div class="col-md-6">';
// 	input += '		<select required name="status" class="form-control input-md" required>';
// 	input += '			<option value="Disponible" selected>Disponible</option>';
// 	input += '			<option value="Indisponible">No Disponible</option>';
// 	input += '		</select>';
// 	input += '	</div>';
// 	input += '</div>';
// 	input = $.parseHTML(input);
// 	var action = locker.remove;
// 	$('select option', input).each(function(){
// 		if ($(this).attr('value') == locker.status) {
// 			$(this).prop('selected', true);
// 		}else {
// 			$(this).prop('selected', false);
// 		}
// 	});
// 	$('#modaledit fieldset').html(input);
// 	$('#modaledit form').attr('action', action );
// 	$("#modaledit").modal();
//
// }
//
// $('#form_locker_delete').submit(function(e){
// 	e.preventDefault();
// 	var action = $(this).attr('action');
// 	var form_serialize = $(this).serialize();
// 	$.ajax({
// 		type: "POST",
// 		url: action,
// 		data: form_serialize,
// 		success: function(respuesta) {
// 			var locker = respuesta.response;
// 			$('#locker_'+locker.id).fadeOut();
// 			$('#modaldelete').modal('hide');
// 		}
// 	});
// });
//
//
// $('#form_lockers_edit').submit(function(e){
// 	e.preventDefault();
// 	var action = $(this).attr('action');
// 	var form_serialize = $(this).serialize();
// 	$.ajax({
// 		type: "POST",
// 		url: action,
// 		data: form_serialize,
// 		success: function(respuesta) {
// 			var locker = respuesta.response;
// 			for (var i = 0; i < lockers.length; i++) {
// 				var lock = lockers[i];
// 				if (lock.id === locker.id) {
// 					lockers[i] = locker;
// 				}
// 			}
//
// 			var status = (locker.status == 'Indisponible') ? 'No Disponible' : locker.status;
// 			$('#locker_'+locker.id).removeClass('Prestamo').removeClass('Disponible').removeClass('Indisponible').addClass(locker.status);
// 			if (locker.status == 'Indisponible') {
// 				$('#locker_'+locker.id+' .aaa').removeClass('accion');
// 			}else {
// 				$('#locker_'+locker.id+' .aaa').addClass('accion');
//
// 			}
// 			$('#locker_'+locker.id+' h1').html(locker.name);
// 			$('#locker_'+locker.id+' .text span').html(status);
//
// 			$('#modaledit').modal('hide');
// 		}
// 	});
// });
//
//
// $('#form_maletero_prestamo').submit(function(e){
// 	e.preventDefault();
// 	var form_serialize = $(this).serialize();
// 	var locker = parseInt($(this).attr('locker_id'));
// 	var indice_locker = 0;
// 	for (var i = 0; i < lockers.length; i++) {
// 		var lock = lockers[i];
// 		if (lock.id === locker) {
// 			locker = lockers[i];
// 			indice_locker = i;
// 		}
// 	}
// 	var disponible = (locker.status == 'Disponible') ? true : false;
// 	var action = (disponible) ? locker.in : locker.out;
// 	$.ajax({
// 		type: "POST",
// 		url: action,
// 		data: form_serialize,
// 		success: function(respuesta) {
// 			var locker_response = respuesta.response;
// 			var li_locker = $('#locker_'+locker.id);
//
// 			lockers[indice_locker] = locker_response;
//
// 			if (disponible) {
// 				li_locker.removeClass('Disponible').addClass(locker_response.status);
// 				$('.header', li_locker).fadeOut();
// 			}else {
// 				li_locker.removeClass('Prestamo').addClass(locker_response.status);
// 				$('.header', li_locker).fadeIn();
// 			}
//
//
// 			$('.text span', li_locker).html(locker_response.status);
// 			cerrar_capa_maletero();
//
// 		}
// 	});
// });
//
//
// function cerrar_capa_maletero() {
// 	var retorno = true;
// 	try {
// 		capa_maletero.removeClass('active');
// 		setTimeout(function(){
// 			capa_maletero.addClass('none');
// 		}, 600);
// 	} catch (e) {
// 		retorno = false;
// 	}
// 	return retorno;
// }
//
// // function autocomplete_socios() {
// // }
// function cerrar_add_maletero(cerrar) {
// 		$(cerrar).fadeOut(400);
// 		setTimeout(function(){
// 				$(cerrar).remove();
// 				if ($('.capa_maletero .add_ ul li').length === 0) {
// 					$('#add_members_maletero').prop('disabled', false);
// 				}
// 				$('#add_members_maletero').val('');
//
// 		},400);
//
// }
// $('#form_lockers_create').submit(function(e){
// 	e.preventDefault();
//
// 	var form_serialize = $(this).serialize();
// 	var action = $(this).attr('action');
// 	$.ajax({
// 		type: "POST",
// 		url: action,
// 		data: form_serialize,
// 		success: function(respuesta) {
// 			var locker = respuesta.response.locker;
// 			var success = (respuesta.type == 'success' ? true : false);
// 			var status = (locker.status == 'Indisponible') ? 'No Disponible' : locker.status;
// 			if (success) {
// 				lockers.push(locker);
// 				var li = '';
// 				li += '<li class="'+locker.status+'" id="locker_'+locker.id+'">';
// 				li += '<ul class="header">';
// 				li += '	<li>';
// 				li += '		<a href="" onclick="edit_locker('+locker.id+'); return false;">';
// 				li += '			<span class="glyphicon glyphicon-edit"></span>';
// 				li += '		</a>';
// 				li += '	</li>';
// 				li += '	<li>';
// 				li += '		<a href="" onclick="remove_locker('+locker.id+'); return false;">';
// 				li += '			<span class="glyphicon glyphicon-remove"></span>';
// 				li += '		</a>';
// 				li += '	</li>';
// 				li += '</ul>';
// 				li += '	<a href="#'+locker.id+'" class="accion aaa" onclick="maletero($(this)); return false;">';
// 				li += '		<div class="text">';
// 				li += '			<h1> '+locker.name+' </h1>';
// 				li += '			<span>'+status+'</span>';
// 				li += '		</div>';
// 				li += '	</a>';
// 				li += '</li>';
// 				li = $.parseHTML(li);
// 				$('.maletero').append(li);
// 			}
// 			$('#modalcreate').modal('hide');
// 		}
// 	});
// 	//
// });
//
// function hora() {
// 	var fecha_ = new Date();
// 	var hora = fecha_.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit', second: '2-digit' });
// 	return hora;
// }

/* ************************************************ End Maletero ************************************************ */



$('.chequear_cedula').on('keyup change blur', function(){
	var value = $(this).val();
	var ruta = $(this).attr('checkear');
	var $this = $(this);
	var data = {
		identity_card: value,
		// if (window.socio_id) {
		// 	member_id: value
		// }
	};
	if (window.socio_id) {
		data.member_id = socio_id;
	}
	if (window.partner_id) {
		data.partner_id = partner_id;
	}
	if (window.student_id) {
		data.student_id = student_id;
	}
	// if (window.user_id) {
	// 	data.user_id = partner_id;
	// }
	var form = $('.form_validation');
	var chequear = $('.chequear', $this.parent());
	if (value.length >= 4) {
		validate_datos(ruta, data, value, form, $this, chequear);
	}else {
		$('.error .mensaje p', chequear).html('La cédula ingresada debe ser mayor a 4 caracteres');
		$('.icon', chequear).not('.error').removeClass('active');
		$('.error', chequear).addClass('active');
		$this.addClass('border_error');
		form.addClass('form_error');
	}
});

$('.chequear_cedula_registro').on('keyup change blur', function(){
	var value = $(this).val();
	var ruta = $(this).attr('checkear');
	var $this = $(this);
	var data = {
		identity_card: value,
		// if (window.socio_id) {
		// 	member_id: value
		// }
	};
	if (window.socio_id) {
		data.member_id = socio_id;
	}

	var form = $('.form_validation');
	var chequear = $('.chequear', $this.parent());
	if (value.length >= 8) {
		validate_datos(ruta, data, value, form, $this, chequear);
	}else {
		$('.error .mensaje p', chequear).html('La cédula debe tener como máximo 8 caracteres');
		$('.icon', chequear).not('.error').removeClass('active');
		$('.error', chequear).addClass('active');
		$this.addClass('border_error');
		form.addClass('form_error');
	}
});

$('.chequear_correo').on('keyup change blur', function(){
	var value = $(this).val();
	var ruta = $(this).attr('checkear');
	var $this = $(this);
	var data = {
		email: value
	};
	if (window.socio_id) {
		data.member_id = socio_id;
	}
	if (window.partner_id) {
		data.partner_id = partner_id;
	}
	if (window.teacher_id) {
		data.teacher_id = teacher_id;
	}
	var form = $('.form_validation');
	var chequear = $('.chequear', $this.parent());

	var formato = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	if (formato.test(value)) {
		validate_datos(ruta, data, value, form, $this, chequear);
	}else {
		$('.error .mensaje p', chequear).html('Debe ingresar un formato de correo real. Ej: lagunita@golf.com');
		$('.icon', chequear).not('.error').removeClass('active');
		$('.error', chequear).addClass('active');
		$this.addClass('border_error');
		form.addClass('form_error');
	}
});

function validate_datos(ruta, data, value, form, $this, chequear) {
	$.ajax({
		type: "GET",
		url: ruta,
		data: data,
		beforeSend: function(){
			$('.icon', chequear).not('.cargando').removeClass('active');
			$('.cargando', chequear).addClass('active');
			$this.removeClass('border_error');
			form.addClass('form_error');

		},
		success: function(validacion) {
			if (validacion.code == 200) {
				$('.icon', chequear).not('.check').removeClass('active');
				$('.check', chequear).addClass('active');
				$this.removeClass('border_error');
				if ($('.chequear .error.active').length === 0) {
					form.removeClass('form_error');
				}
			}else {
				$('.error .mensaje p', chequear).html(validacion.status);
				$('.icon', chequear).not('.error').removeClass('active');
				$('.error', chequear).addClass('active');
				$this.addClass('border_error');
				form.addClass('form_error');
			}
		}
	});
}
$('.form_validation').submit(function(){
	if ($(this).filter('.form_error').length <= 0) {
		$(this)[0].submit();
	}else {
		return false;
	}
});



// var fecha_inscripcion = new Date('2017/04/07');


$('checkbox_payment_plan').click(function(e){
		e.preventDefault();

});


$('#create_student').click(function(e){
		e.preventDefault();
		$('#modal_create_student').modal();
});
$('#create_group').click(function(e){
		e.preventDefault();
		$('#modal_create_group').modal();
});



$('#form_create_student').submit(function(e){
	e.preventDefault();
	var action = $(this).attr('action');
	var form_serialize = $(this).serialize();
	$.ajax({
		type: "GET",
		url: action,
		data: form_serialize,
		success: function(respuesta) {
			var student = respuesta.response;
			var option = '<option value="'+student.id+'" selected>'+student.name+' '+student.last_name+'</option>';
			option = $.parseHTML(option);

			$('#student_select').append(option);
			$('#modal_create_student').modal('hide');
			$('#form_create_student')[0].reset();

		}
	});
});

$('#form_create_group').submit(function(e){
	e.preventDefault();
	var action = $(this).attr('action');
	var form_serialize = $(this).serialize();
	$.ajax({
		type: "GET",
		url: action,
		data: form_serialize,
		success: function(respuesta) {
			var group = respuesta.response;
			var option = '<option value="'+group.id+'" selected>'+group.name+'</option>';
			option = $.parseHTML(option);

			$('#group_select').append(option);
			$('#modal_create_group').modal('hide');
			$('#form_create_group')[0].reset();

		}
	});
});


$( "#add_member_registration" ).autocomplete({
	source: $('#add_member_registration').attr('ruta'),
	minLength: 1,
	select: function(event, ui) {
		$('#member_id_student').val(ui.item.id);
		$('#student_select, #create_student').prop('disabled', false);
		var action = 'http://localhost:8000/alumnos/member/'+ui.item.id;

		$.ajax({
			type: "GET",
			url: action,
			data: { member_id: ui.item.id },
			success: function(respuesta) {
				if (respuesta.type == 'success') {

					var students = respuesta.response;
					// foreach
					var options = '';
					for (var i = 0; i < students.length; i++) {
						var student = students[i];
						options += '<option value="'+student.id+'">'+student.name+' '+student.last_name+'</option>';
					}
					$('#student_select').html(options);
				}
			}
		});
	}
});
// Change status of members and partners
$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');
    if ($(this).find('.btn-primary').size()>0) {
    	$(this).find('.btn').toggleClass('btn-primary');
    }
    $(this).find('.btn').toggleClass('btn-default');
});

function change_status( id, ruta )
{
	$.ajax({
		type: "PUT",
		url: ruta+'/change_status/'+id,
		success: function(validacion) {
			if ( validacion.changed ) // ojo negar la validacion para que se muestre solo por error
			{

			}
		}
	});
	return false;

}
/*--------------------------------------------change status with toggle------------------*/
$('.toggle-event').change(function() {
	var e = $(this);
	$.ajax({
	    type: "PUT",
	    url: $(this).attr('data-route') + $(this).attr('data-item'),
	    success: function(validacion) {
			if ( e.attr('data-route') == "/socios/status_inscription/")
			{
				/* Se busca la descendencia que contiene el elemento que se va a remover de la BD*/
				var ele = $(e).context.parentElement.parentElement.parentElement;
				setTimeout(function(){
					$(ele).fadeOut( "slow", function() {
						$(ele).remove();
					});
				}, 700);

			}
	    }
  });
})
/*--------------------------------------------change status with toggle------------------*/

/*--------------------------------------- change status court Home ----------------------*/

$('.payment-inscription').change(function(){
	if($(this)[0].id == '1'){
		document.getElementById("2").checked = false;
	}
	else{
		document.getElementById("1").checked = false;
	}
});
$('#tournament_id').change(function(){
	var value = $(this).val();
	if(value){
		$.get('/torneos/inscripciones/listado_inscripcion_torneo/'+value)
		.then(function(response){
			$("#ins_tbody").empty();
			var inscriptions = response.data;

			for(var i in inscriptions){
				var inscription = inscriptions[i],
				 tr = '',
				 categoryName = inscription.category ? inscription.category.name : '-',
				 status = inscription.waiting==0 ? 'Inscrito' : 'Lista de espera' ;
				tr += '<tr>';
				tr += '<td>'+inscription.tournament.name + '</td>';
				tr += '<td>'+inscription.member.name + '</td>';
				tr += '<td>'+ categoryName + '</td>';
				tr += '<td>'+inscription.created_at + '</td>';
				tr += '<td>'+inscription.tournament.start_date + '</td>';
				tr += '<td>'+ status + '</td>';
				tr += '</tr>';
				$("#ins_tbody").append(tr);
			}

		});

	}
});


/*********************************************************/
/*********************************************************/
/*********************************************************/




/*************************************      FunCIones Reutilizables      *************************************/
function number_format (number, decimals, dec_point, thousands_sep) {
	// Strip all characters but numerical ones.
	number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
	var n = !isFinite(+number) ? 0 : +number,
	prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
	sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
	dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
	s = '',
	toFixedFix = function (n, prec) {
		var k = Math.pow(10, prec);
		return '' + Math.round(n * k) / k;
	};
	// Fix for IE parseFloat(0.55).toFixed(0) = 0;
	s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
	if (s[0].length > 3) {
		s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
	}
	if ((s[1] || '').length < prec) {
		s[1] = s[1] || '';
		s[1] += new Array(prec - s[1].length + 1).join('0');
	}
	return s.join(dec);
}
function log(string) {
	console.log(string);
}
function input_error(selector, mensaje, estado) {
	var contenedor = selector.parent();
	var input_error = $('.input_error', contenedor);
	if (estado) {
		$('p', input_error).html(mensaje);
		input_error.removeClass('none');
		setTimeout(function(){
			contenedor.addClass('error');
		}, 10);

	}else {
		contenedor.removeClass('error');
		setTimeout(function(){
			input_error.addClass('none');
			$('p', input_error).html('');
		}, 400);

	}
}
/**/
function beforeShowDay(date) {
    var torneo = false;
    var sorteable = false;
    var días_bloqueados = null;
    var object = {
        torneo: null,
        días_bloqueados: null,
        sorteable: null,
        normal: null
    };
    for (var i = 0; i < tournaments.length; i++) {
        var tournament = tournaments[i];
        var start_date = new Date(tournament.start_date.split('-')[2]+'/'+tournament.start_date.split('-')[1]+'/'+tournament.start_date.split('-')[0]);
        var end_date = new Date(tournament.end_date.split('-')[2]+'/'+tournament.end_date.split('-')[1]+'/'+tournament.end_date.split('-')[0]);
        if (start_date.getTime() <= date.getTime() && end_date.getTime() >= date.getTime()) {
		    object.torneo = true;
        }
    }
    draws.forEach(function(draw){
        draw.day_draws.forEach(function(draw_day){
            // console.log(draw_day);
            draw_date = new Date(draw_day.date.replace(/-/g, '/'));
            if (draw_date.getTime() == date.getTime()) {
			    object.sorteable = true;
            }
        });
    });
    day_blockeds.forEach(function(day_blocked){
        day_blocked_date = new Date(day_blocked.date.replace(/-/g, '/'));
        if (day_blocked_date.getTime() == date.getTime()) {
		    object.dias_bloqueados = true;
        }
    });
    return object;
}


function validar_formato_fecha(dateString){
    // revisar el patrón
    if(!/^\d{1,2}\-\d{1,2}\-\d{4}$/.test(dateString))
        return 1; //formato malo : formato permitido dd-mm-yyyy

    // convertir los numeros a enteros
    var parts = dateString.split("-");
    var day = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var year = parseInt(parts[2], 10);

    // Revisar los rangos de año y mes
    if( (year < 1000) || (year > 3000) || (month === 0) || (month > 12) ){
		return 2; //fecha no real ejemplo 40-03-1200

	}

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Ajustar para los años bisiestos
    if(year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)){
		monthLength[1] = 29;
	}

    // Revisar el rango del dia
    return day > 0 && day <= monthLength[month - 1]; //formato correcto


}
// function registro_id(registro) {
// 	return registro.date === date;
// }
//-------------------------------------------Messages-------------------------------------------
if (userRole ==2) {
	function getMessages() {
		$.get('/mensajes/').then(function(response){
			if(response.quantity != 0){
				$('#messages a').html(response.quantity);
				$('#messages a').attr('id', JSON.stringify(response.id));
				$('#messages').addClass('active');
			}else {
				$('#messages a').html('');
				$('#messages a').attr('id', '');
				$('#messages').removeClass('active');
			}
		});
	}
	getMessages();
	setInterval(function(){
		getMessages();
	},10000);

	$('#messages a').click(function(e){
		e.preventDefault();
		$.post('/mensajes/',
		{
			'ids' : JSON.parse($(this).attr('id')),
			'csrf_token': Laravel.csrfToken
		})
		.then(function(response){
			console.log(response)
		});
	});
}


//-------------------------------------------Messages-------------------------------------------
