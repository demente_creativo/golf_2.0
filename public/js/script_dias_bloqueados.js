$(document).ready(function(){
	dias_bloqueados_calendario();
	$('.intervalo_sorteo').select2();
	elminar_intervalos();
	agregar_intervalos();

});
var date_select = [];
var end_date_draw;
var intervalos_agregados = [];
var estado = 0;
var route = '/admin/'+user_id+'/dias_bloqueados/';





$('#cancelar_day_blocked').click(function(e){
	e.preventDefault();
	cancelar_dias_bloqueados();
});

function cancelar_dias_bloqueados() {
	date_select = [];
	intervalos_agregados = [];
	estado = 0;
	$('#destroy').attr('ruta', '');
	$('#destroy').fadeOut();
	$('#form_dias_bloqueados').attr('action', route);
	$('#add_method').html('');
	// edit = false;
	$('.drawing ul.dias').html('');
	dias_bloqueados_calendario();

}

function dias_bloqueados_calendario() {
	$( ".dias_bloqueados_calendario" ).datepicker('destroy');
	$( ".dias_bloqueados_calendario" ).datepicker({
			minDate: new Date(),
			dateFormat: 'yy-mm-dd',
			onSelect: function(date){
				function encontrar_dia_bloqueado(day_blocked) {
					return day_blocked.date === date;
				}
				var day_blocked = day_blockeds.find(encontrar_dia_bloqueado);

				var date_object = new Date(date.replace(/-/g, '/'));
				var hora_inicio = new Date(date);
				var hora_final = new Date(date);

				hora_inicio.setHours(06,00,00);
				hora_final.setHours(17,00,00);
				var minutos_totales = ((hora_final.getHours() - hora_inicio.getHours()) + 1) * 6;
				var dia_completo = date_object.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});


				var options = '';
				for (var i = 0; i < minutos_totales; i++) {
					if (i !== 0) {
						hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
					}
					var minuto = hora_inicio;
					minuto = minuto.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
					if (minuto.split(':')[0].length < 2) {
						minuto = 0+minuto.split(':')[0]+':'+minuto.split(':')[1];
					}
					options += '<option value="'+minuto+'">'+minuto+'</option>';
				}

				var html = '';
				html += '<li id="'+date.replace(/-/g, '_')+'" class="li_dias '+ ( (day_blocked !== undefined) ? 'active' : '' ) +'" date="'+date+'">';
				html += '	<div class="head">';
				html += '		<div class="icon">';
				html += '			<a href="#" class="select_date" onclick="select_date($(this)); return false;">';
				html += '				<i class="glyphicon glyphicon-menu-right"></i>';
				html += '			</a>';
				html += '		</div>';
				html += '		<span>'+dia_completo+'</span>';
				html += '		<div class="remove">';
				html += '			<a href="#" class="remove_date" id="date" name"date" onclick="remove_date($(this)); return false;" date="'+date+'"> <i class="glyphicon glyphicon-remove"></i> </a>';
				html += '		</div>';
				html += '	</div>';
				html += '	<div class="cont">';
				html += '		<div class="agregar">';
				html += '			<div class="input">';
				html += '				<textarea date="'+date+'" id="observation" name="observation" rows="8" cols="80" placeholder="Agregar Observación" pattern=".{3,255}" title="de 3 a 255 caracteres" required> '+ ((day_blocked !== undefined) ? day_blocked.observation : '') +' </textarea>';
				html += '			</div>';
				html += '			<div class="sub_titulo">';
				html += '				<h5>Agregar Intervalos</h5>';
				html += '			</div>';
				html += '			<div class="inputs">';
				html += '				<div class="inpt">';
				html += '					<label for=""> Desde </label>';
				html += '					<select required class="intervalo_sorteo" name="start_time" id="start_time">';
				html += options;
				html += '					</select>';
				html += '				</div>';
				html += '				<div class="inpt">';
				html += '					<label for=""> Hasta </label>';
				html += '					<select required class="intervalo_sorteo" name="end_time" id="end_time">';
				html += options;
				html += '					</select>';
				html += '				</div>';
				html += '				<div class="boton">';
				html += '					<a href="" date="'+date+'">';
				html += '						<i class="fa fa-plus"></i>';
				html += '					</a>';
				html += '				</div>';
				html += '			</div>';
				html += '		</div>';
				html += '		<div class="intervalos">';
				html += '			<div class="sub_titulo">';
				html += '				<h5>Intervalos</h5>';
				html += '			</div>';
				html += '			<ul>';
				html += '			</ul>';
				html += '			<div class="advertencia">';
				html += '			</div>';
				html += '		</div>';
				html += '	</div>';
				html += '</li>';
				html = $.parseHTML(html);

				$('.cont .agregar .inputs .inpt select', html).select2();
				if (day_blocked === undefined) {
					if (estado === 0 || estado === 1) {
						if ($('#'+date.replace(/-/g, '_')).length < 1) { //Si ya esta agregada la fecha seleccionada
							$('.drawing ul.dias').append(html);
							agregar_intervalos();
							date_select.push(date);
						}else {
							error_draw('La fecha seleccionada ya está agregada');
						}
						estado = 1;
					}
				}else {
					if (estado === 0) {
						var route_edit = '/admin/'+user_id+'/dias_bloqueados/'+day_blocked.id;
						var route_destroy = '/admin/'+user_id+'/dias_bloqueados/'+day_blocked.id;
						var method_put = '<input type="hidden" name="_method" value="PUT" id="method_put">';

						$('#destroy').attr('ruta', route_destroy);
						$('#destroy').fadeIn();
						$('#form_dias_bloqueados').attr('action', route_edit);
						$('#add_method').html(method_put);

						day_blocked.time_blockeds.forEach(function(time_blocked){
							var start_time = new Date(date+' '+ time_blocked.start_time);
							var end_time = new Date(date+' '+ time_blocked.end_time);
							var format_hora_inicio = start_time.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
							var format_hora_final = end_time.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
							var id = start_time.getTime()+end_time.getTime();
							var li = '';
							li += '<li id="'+id+'">';
							li += '	<div class="text">Desde: '+format_hora_inicio+'</div>';
							li += '	<div class="text">Hasta: '+format_hora_final+'</div>';
							li += '	<div class="icon">';
							li += '		<a href="" id="'+id+'">';
							li += '			<i class="fa fa-remove"></i>';
							li += '		</a>';
							li += '	</div>';
							li += '</li>';
							li = $.parseHTML(li);
							$('.intervalos ul', html).append(li);
							intervalos_agregados.push({ date: date, id:id, hora_inicio: start_time, hora_final: end_time });
							$('.drawing ul.dias').append(html);
						});
						elminar_intervalos();

						estado = 2;
					}
				}
			},
			beforeShowDay: function( date ) {
				var tipo_dia = beforeShowDay(date);
				var dia_selecionado = false;
				for (var i = 0; i < date_select.length; i++) {
					var date_selec = new Date(date_select[i].replace(/-/g, '/'));
					if (date.getTime() == date_selec.getTime()) {
						dia_selecionado = true;
					}
				}
				if (dia_selecionado) {
					return [true, "dia_selecionado", 'Día selecionado'];
				}else if(tipo_dia.dias_bloqueados){
					return [true, "dia_bloqueado", 'Día Bloqueado'];
				}else if( tipo_dia.sorteable ) {
					 return [true, "sorteable", 'Día Sorteable'];
				}else if (tipo_dia.torneo) {
					return [true, "torneo", 'Día de torneo'];
				}else {
					return [true, '', ''];
				}
			}
	});
}


function select_date($this) {
	var id_li = '#'+$this.parent().parent().parent().attr('id');
	var li = $this.parent().parent().parent();
	if (li.filter('.active').length <= 0 ) {
			$('.drawing ul.dias li:not('+id_li+'):not(.li_hour)').removeClass('active');
			setTimeout(function(){
					li.addClass('active');
			},500);
	}else {
			li.removeClass('active');
	}
}
function remove_date($this) {
	// console.log($this);
	var id_li = '#'+$this.parent().parent().parent().attr('id');
	var li = $this.parent().parent().parent();
	var date = $this.attr('date');
	$('.cont ul li', li).each(function(){
		var id_intervalo = parseInt($(this).attr('id'));
		for (var i = 0; i < intervalos_agregados.length; i++) {
			var intervalo = intervalos_agregados[i];
			if (id_intervalo === intervalo.id) {
				intervalos_agregados.splice(i, 1);
			}
		}
	});
	date_select.splice(date_select.indexOf(date), 1);
	if (date_select.length === 0) {
		cancelar_dias_bloqueados();
	}
	dias_bloqueados_calendario();

	li.addClass('mostrar');
	setTimeout(function(){
			li.remove();
	},500);
}



function agregar_intervalos() {
	$('.drawing ul.dias li .cont .agregar .inputs .boton a').off('click');
	$('.drawing ul.dias li .cont .agregar .inputs .boton a').click(function(e){
		e.preventDefault();
		// var draws = draws
		// console.log(draws);
		var inputs = $(this).parent().parent();
		var date = $(this).attr('date');
		var start_time = $('#start_time', inputs);
		var end_time = $('#end_time', inputs);

		var hora_inicio = new Date(date.replace(/-/g, '/')+' '+ start_time.val());
		var hora_final = new Date(date.replace(/-/g, '/')+' '+ end_time.val());
		var format_hora_inicio = hora_inicio.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
		var format_hora_final = hora_final.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
		var date_long = hora_inicio.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});
		var id = hora_inicio.getTime()+hora_final.getTime();
		var li = '';
		var cont = $(this).parent().parent().parent().parent();
		var intervalos = $('.intervalos ul', cont);
		function buscar_day_draws(day_draw) {
			return day_draw.date === date;
		}
		var day_draw = day_draws.find(buscar_day_draws);

		li += '<li id="'+id+'">';
		li += '	<div class="text">Desde: '+format_hora_inicio+'</div>';
		li += '	<div class="text">Hasta: '+format_hora_final+'</div>';
		li += '	<div class="icon">';
		li += '		<a href="" id="'+id+'">';
		li += '			<i class="fa fa-remove"></i>';
		li += '		</a>';
		li += '	</div>';
		li += '</li>';
		li = $.parseHTML(li);
		var error_day_draw = {
			error: false,
			mensaje: null,
			time_draws: []
		};
		if (day_draw !== undefined) {

			day_draw.time_draws.forEach(function(time_draw){
				var start_time_draw = new Date(date+' '+time_draw.start_time);
				var end_time_draw = new Date(date+' '+time_draw.end_time);
				if (hora_inicio >= start_time_draw && hora_inicio <= end_time_draw) {
					error_day_draw.error = true;
					// error_day_draw.mensaje = 'Hay un intervalo que esta comprendido en un intervalo de sorteo el dia: ';
					error_day_draw.time_draws.push({
						start_time: start_time_draw.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' }),
						end_time: end_time_draw.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' }),
					});
				}else if (hora_final >= start_time_draw && hora_final <= end_time_draw) {
					error_day_draw.error = true;
					// error_day_draw.mensaje = 'Hay un intervalo que esta comprendido en un intervalo de sorteo el dia: ';
					error_day_draw.time_draws.push({
						start_time: start_time_draw.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' }),
						end_time: end_time_draw.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' }),
					});
				}else if (hora_inicio <= start_time_draw && hora_final >= end_time_draw) {
					error_day_draw.error = true;
					// error_day_draw.mensaje = 'Hay un intervalo que esta comprendido en un intervalo de sorteo el dia: ';
					error_day_draw.time_draws.push({
						start_time: start_time_draw.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' }),
						end_time: end_time_draw.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' }),
					});
				}
			});
		}
		// console.log(error_day_draw.time_draws);
		// console.log();

		function error_intervalo_agregados() {
			var error = null;
			for (var i = 0; i < intervalos_agregados.length; i++) {
				var intervalo = intervalos_agregados[i];
				if (intervalo.date == date) {
					if (hora_inicio >= intervalo.hora_inicio && hora_inicio <= intervalo.hora_final) {
						error_draw('La hora de inicio ya existe en otro intervalo');
						error = intervalo.id;
						break;
					}else if (hora_final >= intervalo.hora_inicio && hora_final <= intervalo.hora_final) {
						error_draw('La hora final ya existe en otro intervalo');
						error = intervalo.id;
						break;
					}else if (hora_inicio <= intervalo.hora_inicio && hora_final >= intervalo.hora_final) {
						error_draw('Hay un intervalo que esta comprendido en el intervalo selecionado');
						error = intervalo.id;
						break;
					}
				}
			}
			return error;
		}

		var error = error_intervalo_agregados();
		if (hora_inicio.getTime() <= hora_final.getTime()) {
			if (error_day_draw.error === false) {
				if (hora_inicio.getTime() !== hora_final.getTime()) {

					if (error === null) {
						intervalos_agregados.push({ date: date, id:id, hora_inicio: hora_inicio, hora_final: hora_final });
						intervalos.append(li);
					}else {
						$('li', intervalos).each(function(){
							if (parseInt($(this).attr('id')) == error) {
								$('li', intervalos).not($(this)).removeClass('marca');
								$(this).addClass('marca');
								setTimeout(function(){
									$('li', intervalos).removeClass('marca');
								}, 3000);
							}
						});
					}
				}else {
					error_draw('Los intervalos no pueden ser iguales');
				}
			}else {
				if (error === null) {
					var advertencia = '';
					advertencia += '<div class="msj">';
					advertencia += '	<p>El intervalo seleccionado pertenece a un sorteo de reservaciones el día:</p>';
					advertencia += '</div>';
					advertencia += '<div class="fecha">';
					advertencia += '	<p><strong>'+date_long+'</strong></p>';
					advertencia += '</div>';
					advertencia += '<div class="horas">';
					error_day_draw.time_draws.forEach(function(time_draw){
						advertencia += '<p>De <strong>'+time_draw.start_time+'</strong> A <strong>'+time_draw.end_time+'</strong></p>';
					});
					advertencia += '</div>';
					advertencia += '<div class="boton">';
					advertencia += '	<button type="button" class="btn btn-primary singlebutton1 aceptar">Aceptar</button>';
					advertencia += '	<button type="button" class="btn btn-primary singlebutton1 cancelar">Cancelar</button>';
					advertencia += '</div>';

					$('.advertencia', cont).html(advertencia);

					$('.advertencia .boton button', cont).off('click');
					$('.advertencia .boton button', cont).click(function(){
						if ($(this).hasClass('aceptar')) {
							intervalos_agregados.push({ date: date, id:id, hora_inicio: hora_inicio, hora_final: hora_final });
							intervalos.append(li);
							$('.advertencia', cont).html('');
						}else {
							$('.advertencia', cont).html('');
						}
					});
				}else {
					$('li', intervalos).each(function(){
						if (parseInt($(this).attr('id')) == error) {
							$('li', intervalos).not($(this)).removeClass('marca');
							$(this).addClass('marca');
							setTimeout(function(){
								$('li', intervalos).removeClass('marca');
							}, 3000);
						}
					});
				}
			}
		}else {
			error_draw('La hora de inicio no puedes ser mayor a la hora final');
		}
		elminar_intervalos();
	});
}
function elminar_intervalos() {
	$('.drawing ul.dias li .cont .intervalos ul li .icon a').off('click');
	$('.drawing ul.dias li .cont .intervalos ul li .icon a').click(function(e){
		e.preventDefault();
		var id = parseInt($(this).attr('id'));
		var li = $(this).parent().parent();

		for (var i = 0; i < intervalos_agregados.length; i++) {
			var intervalo = intervalos_agregados[i];
			if (intervalo.id == id) {
				intervalos_agregados.splice(i, 1);
				li.remove();
			}
		}
	});
}
$('#form_dias_bloqueados').submit(function(e){
	e.preventDefault();

	var error = false;
	date_select.forEach(function(date_selec){
		function encontrar_intervalo_agregado(intervalos_agregado) {
			return intervalos_agregado.date === date_selec;
		}
		var intervalos_agregado = intervalos_agregados.find(encontrar_intervalo_agregado);
		if (intervalos_agregado === undefined) {
			error = true;
		}
	});
	if (error) {
		error_draw('Falta agregar el/los intervalo(s)');
	}else {
		parametros_array_dias_bloqueados();
		setTimeout(function(){
			$('#form_dias_bloqueados')[0].submit();
		}, 10);
	}

});
function parametros_array_dias_bloqueados() {
		parametros = {};
		var day_blockeds = [];

		for (var i = 0; i < intervalos_agregados.length; i++) {
			var intervalos_agregado = intervalos_agregados[i];
			var times = [];
			var hora_inicio = intervalos_agregado.hora_inicio;

			hora_inicio = intervalos_agregado.hora_inicio.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
			if (hora_inicio.split(':')[0].length < 2) {
				hora_inicio = 0+hora_inicio.split(':')[0]+':'+hora_inicio.split(':')[1];
			}
			hora_final = intervalos_agregado.hora_final.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
			if (hora_final.split(':')[0].length < 2) {
				hora_final = 0+hora_final.split(':')[0]+':'+hora_final.split(':')[1];
			}

			function encontrar_day_blocked(date) {
			    return date.date === intervalos_agregado.date;
			}
			var day_blocked = day_blockeds.find(encontrar_day_blocked);

			if (day_blocked === undefined) {
				day_blockeds.push({
					date: intervalos_agregado.date,
					times: [{ start_time: hora_inicio, end_time: hora_final }],
					comment: $('.drawing ul.dias li .cont .agregar .input textarea').filter('[date="'+intervalos_agregado.date+'"]').val()
				});
			}else {
				day_blocked.times.push({ start_time: hora_inicio, end_time: hora_final });
			}
		}
		$('#days_blocked').val(JSON.stringify(day_blockeds));
}
function error_draw(mensaje) {
	$('.error_draw p').html(mensaje);
	$('.error_draw').addClass('active');
	setTimeout(function(){
		$('.error_draw').removeClass('active');
	}, 4000);
}
