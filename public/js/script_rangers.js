$(document).ready(function(){
	comments();
	activar_starter();
});
$( "#date_reservaciones_del_dia" ).datepicker({
	dateFormat: 'yy-mm-dd',
	onSelect: function(date){
		reservaciones_del_dia(date, false);
		var date_string = new Date(date.replace(/-/g, '/')).toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});
		$('#fecha').html(date_string);
		setTimeout(function(){
			$('#date_reservaciones_del_dia').val('');
		}, 10);
	}
});
function activar_starter() {
	/* Botones de  */
	$('.activar_starter').off('click');
	$('.activar_starter').click(function(e){
		e.preventDefault();
		var li = $(this).parent().parent();
		var id_li = '#'+li.attr('id');
		var ul_cont = $(this).parent().parent().parent();
		if (li.filter('.active').length < 1) {
			li.addClass('active');
		}else {
			li.removeClass('active');
		}
	});
}
function remove_comments() {
	$('#listar_comentarios ul li button.remove').off('click');
	$('#listar_comentarios ul li button.remove').click(function(e){
		var id = parseInt($(this).attr('id'));
		var reservation_id = parseInt($(this).attr('reservation'));
		var li = $(this).parent();
		$.ajax({
			type: "POST",
			url: '/rangers/comentario/'+id,
			data: {
				comment_id: id,
				'_method': 'DELETE',
				'_token': tokem
			},
			success: function(respuesta) {
				if (respuesta) {
					function encontrar_reservation(reservation) {
						return reservation.id === reservation_id;
					}
					var reservation = reservations.find(encontrar_reservation);
					reservation.comments.forEach(function(comment, index){
						if (id === comment.id) {
							reservation.comments.splice(index, 1);
							li.remove();
						}
					});

				}
			}
		});
	});

}


$('#form_agregar_comentario').submit(function(e){
	e.preventDefault();
	var data = $('#form_agregar_comentario').serializeArray();
	if(data[4].value == ''){
		error_draw('el comentario no debe estar vacio');
		return false;
	}
	$.ajax({
		type: "POST",
		url: '/rangers/comentario',
		data: $('#form_agregar_comentario').serialize(),
		success: function(respuesta) {
			if (respuesta.status) {
				function encontrar_reservation(reservation) {
					return reservation.id === parseInt(respuesta.comment.reservation_id);
				}
				var reservation = reservations.find(encontrar_reservation);
				reservation.comments.push(respuesta.comment);
				$('#agregar_comentario').modal('hide');
				$('#form_agregar_comentario')[0].reset();
			}
		}
	});
});

function comments() {
	$('.agregar_comentario').off('click');
	$('.agregar_comentario').click(function(e){
		e.preventDefault();
		var now = new Date();
		var hora = now.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
		var fecha = now.toLocaleString('es-Es', { day: "2-digit", year: "2-digit", month: "2-digit"}).split('/').join('-');
		$('#time').val(hora);
		$('#date').val(fecha);
		$('#reservation_id').val($(this).attr('reservation'));
		$('#agregar_comentario').modal();
	});
	$('.listar_comentario').off('click');
	$('.listar_comentario').click(function(e){
		e.preventDefault();
		var id = parseInt($(this).attr('reservation'));
		function encontrar_reservation(reservation) {
			return reservation.id === id;
		}
		var reservation = reservations.find(encontrar_reservation);
		li = '';
		if (reservation.comments.length > 0) {
			reservation.comments.forEach(function(comment, index){
				li += '<li>';
				li += '	<div class="tiempo">';
				li += '		<div class="item fecha">';
				if (index === 0) {
					li += '			<span><strong>Fecha:</strong> '+comment.date.split('-').reverse().join('-')+'</span>';
				}
				li += '		</div>';
				li += '		<div class="item hora">';
				li += '			<span><strong>Hora:</strong> '+comment.time+'</span>';
				li += '		</div>';
				li += '		<div class="clear"></div>';
				li += '	</div>';
				li += '	<div class="comentario">';
				li += '		<p><strong>Comentario:</strong> '+comment.comment+'</p>';
				li += '	</div>';
				li += '	<button type="button" name="button" class="remove" id="'+comment.id+'" reservation="'+comment.reservation_id+'">&times;</button>';
				li += '</li>';
			});
		}else {
			li += '<li><h4>No hay comentarios para esta reservación</h4></li>';
		}
		$('#listar_comentarios ul').html(li);
		$('#listar_comentarios').modal();
		remove_comments();
	});

}





function mostrar_reservaciones(reservations) {
	var orden_salida = '';
	var orden_finalizada = '';
	var orden_canceladas = '';
	var now = new Date().toLocaleString('es-Es', { year: "numeric", month: "2-digit", day: '2-digit'}).split('/').reverse().join('-');
	var mensaje = '';
	if (reservations.length === 0) {
		mensaje = 'No hay reservaciones para este dia';
	}else {
		reservations.forEach(function(reservation){
			if (reservation.status == 'Aprobada') {
					orden_salida += '<li class="li_principal active" hora="'+reservation.start_time+'">';
					orden_salida += '	<div class="head">';
					orden_salida += '		<a href="" class="activar_starter" onclick="">';
					orden_salida += '			<span class="icon dropdown">';
					orden_salida += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
					orden_salida += '			</span>';
					orden_salida += '		</a>';
					orden_salida += '		<div class="select">';
					orden_salida += '			<div class="capa no_accion"></div>';
					orden_salida += '			<select class="no_accion" name="">';
					orden_salida += '				<option value="" selected>'+reservation.start_time+'</option>';
					orden_salida += '			</select>';
					orden_salida += '			<a href="#" class="icon">';
					orden_salida += '				<i class="fa fa-remove"></i>';
					orden_salida += '			</a>';
					orden_salida += '		</div>';
					orden_salida += '		<a class="icon check starter_accion agregar_comentario" type="salida" reservation="'+reservation.id+'">';
					orden_salida += '			<i class="fa fa-commenting-o" aria-hidden="true"></i>';
					orden_salida += '		</a>';
					orden_salida += '		<a class="icon cancel starter_accion listar_comentario" type="cancelar" reservation="'+reservation.id+'">';
					orden_salida += '			<i class="fa fa-comments"></i>';
					orden_salida += '		</a>';

					orden_salida += '		<div class="clear"></div>';
					orden_salida += '	</div>';
					orden_salida += '	<ul class="">';
					orden_salida += '		<li class="personas" id="'+reservation.id+'">';
					orden_salida += '			<div class="datos">';
					orden_salida += '				<?php';
					var num_participant = reservation.members.length + reservation.partners.length;
					var height = (num_participant * 26) +10;
					orden_salida += '				?>';
					orden_salida += '				<ul class="participantes" id="'+reservation.id+'">';
					reservation.members.forEach(function(member) {
						orden_salida += '<li>';
						orden_salida += '	<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+'</span>';
						orden_salida += '</li>';

					});
					reservation.partners.forEach(function(partner) {
						orden_salida += '<li>';
						orden_salida += '	<span> '+partner.name+' '+partner.last_name+'</span>';
						orden_salida += '</li>';

					});
					orden_salida += '				</ul>';
					orden_salida += '				<div class="clear"></div>';
					orden_salida += '			</div>';
					orden_salida += '		</li>';
					orden_salida += '	</ul>';
					orden_salida += '</li>';


			}else if (reservation.status == 'Finalizada') {
				orden_finalizada += '<li class="li_principal" hora="'+reservation.start_time+'">';
				orden_finalizada += '	<div class="head">';
				orden_finalizada += '		<a href="" class="activar_starter" onclick="">';
				orden_finalizada += '			<span class="icon dropdown">';
				orden_finalizada += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
				orden_finalizada += '			</span>';
				orden_finalizada += '		</a>';
				orden_finalizada += '		<div class="select">';
				orden_finalizada += '			<div class="capa no_accion"></div>';
				orden_finalizada += '			<select class="no_accion" name="" >';
				orden_finalizada += '				<option value="">'+reservation.start_time+'</option>';
				orden_finalizada += '			</select>';
				orden_finalizada += '			<a href="#" class="icon">';
				orden_finalizada += '				<i class="fa fa-remove"></i>';
				orden_finalizada += '			</a>';
				orden_finalizada += '		</div>';
				orden_finalizada += '		<a class="icon check starter_accion agregar_comentario" type="salida" reservation="'+reservation.id+'">';
				orden_finalizada += '			<i class="fa fa-commenting-o" aria-hidden="true"></i>';
				orden_finalizada += '		</a>';
				orden_finalizada += '		<a class="icon cancel starter_accion listar_comentario" type="cancelar" reservation="'+reservation.id+'">';
				orden_finalizada += '			<i class="fa fa-comments"></i>';
				orden_finalizada += '		</a>';
				orden_finalizada += '		<div class="clear"></div>';
				orden_finalizada += '	</div>';
				orden_finalizada += '	<ul class="conexion">';
				orden_finalizada += '		<li class="personas" id="'+reservation.id+'">';
				orden_finalizada += '			<div class="datos">';
				orden_finalizada += '				<!-- <div class="drop">';
				orden_finalizada += '					<i class="fa fa-arrows" aria-hidden="true"></i>';
				orden_finalizada += '				</div> -->';
				orden_finalizada += '				<ul class="participantes conexion_participantes">';
				reservation.members.forEach(function(member) {
					orden_finalizada += '<li>';
					orden_finalizada += '	<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+'</span>';
					orden_finalizada += '</li>';

				});
				reservation.partners.forEach(function(partner) {
					orden_finalizada += '<li>';
					orden_finalizada += '	<span> '+partner.name+' '+partner.last_name+'</span>';
					orden_finalizada += '</li>';

				});
				orden_finalizada += '				</ul>';
				orden_finalizada += '				<div class="clear"></div>';
				orden_finalizada += '			</div>';
				orden_finalizada += '		</li>';
				orden_finalizada += '	</ul>';
				orden_finalizada += '</li>';
			}else if(reservation.status == 'Cancelada'){
				orden_canceladas += '<li class="li_principal" hora="'+reservation.start_time+'">';
				orden_canceladas += '	<div class="head">';
				orden_canceladas += '		<a href="" class="activar_starter" onclick="">';
				orden_canceladas += '			<span class="icon dropdown">';
				orden_canceladas += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
				orden_canceladas += '			</span>';
				orden_canceladas += '		</a>';
				orden_canceladas += '		<div class="select">';
				orden_canceladas += '			<div class="capa no_accion"></div>';
				orden_canceladas += '			<select class="no_accion" name="" >';
				orden_canceladas += '				<option value="">'+reservation.start_time+'</option>';
				orden_canceladas += '			</select>';
				orden_canceladas += '			<a href="#" class="icon">';
				orden_canceladas += '				<i class="fa fa-remove"></i>';
				orden_canceladas += '			</a>';
				orden_canceladas += '		</div>';
				orden_canceladas += '		<a class="icon check starter_accion agregar_comentario" type="salida" reservation="'+reservation.id+'">';
				orden_canceladas += '			<i class="fa fa-commenting-o" aria-hidden="true"></i>';
				orden_canceladas += '		</a>';
				orden_canceladas += '		<a class="icon cancel starter_accion listar_comentario" type="cancelar" reservation="'+reservation.id+'">';
				orden_canceladas += '			<i class="fa fa-comments"></i>';
				orden_canceladas += '		</a>';
				orden_canceladas += '		<div class="clear"></div>';
				orden_canceladas += '	</div>';
				orden_canceladas += '	<ul class="conexion">';
				orden_canceladas += '		<li class="personas" id="'+reservation.id+'">';
				orden_canceladas += '			<div class="datos">';
				orden_canceladas += '				<!-- <div class="drop">';
				orden_canceladas += '					<i class="fa fa-arrows" aria-hidden="true"></i>';
				orden_canceladas += '				</div> -->';
				orden_canceladas += '				<ul class="participantes conexion_participantes">';
				reservation.members.forEach(function(member) {
					orden_canceladas += '<li>';
					orden_canceladas += '	<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+'</span>';
					orden_canceladas += '</li>';
				});
				reservation.partners.forEach(function(partner) {
					orden_canceladas += '<li>';
					orden_canceladas += '	<span> '+partner.name+' '+partner.last_name+'</span>';
					orden_canceladas += '</li>';
				});
				orden_canceladas += '				</ul>';
				orden_canceladas += '				<div class="clear"></div>';
				orden_canceladas += '			</div>';
				orden_canceladas += '		</li>';
				orden_canceladas += '	</ul>';
				orden_canceladas += '</li>';
			}
		});
	}
	$('#orden_salida .reservas').html(orden_salida);
	$('#orden_canceladas .reservas').html(orden_canceladas);
	$('#orden_finalizada .reservas').html(orden_finalizada);
	comments();
	activar_starter();
}


$('#search_participant').keyup(function(){
	var value = $(this).val().toLowerCase();
	var result_reservations = [];
	reservations.forEach(function(reservation){
		var status = false;
		reservation.members.forEach(function(member){
			var nombre = member.name+' '+member.last_name;
			if (nombre.toLowerCase().indexOf(value) !== -1) {
				status = true;
			}
		});
		reservation.partners.forEach(function(partner){
			var nombre = partner.name+' '+partner.last_name;
			if (nombre.toLowerCase().indexOf(value) !== -1) {
				status = true;
			}
		});
		if (status) {
			result_reservations.push(reservation);
		}
	});
	mostrar_reservaciones(result_reservations);
	// console.log('Jose Miguel Urbina'.indexOf('Miguel'));
});

function error_draw(mensaje) {
	$('.error_draw p').html(mensaje);
	$('.error_draw').addClass('active');
	setTimeout(function(){
		$('.error_draw').removeClass('active');
	}, 4000);
}
