$( "#date_reservaciones_del_dia" ).datepicker({
	dateFormat: 'yy-mm-dd',
	onSelect: function(date){
		reservaciones_del_dia(date, false);
		var date_string = new Date(date.replace(/-/g, '/')).toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});
		$('#fecha').html(date_string);
		setTimeout(function(){
			$('#date_reservaciones_del_dia').val('');
		}, 10);
	}
	// minDate: new Date(),

});


function activar_starter() {
	/* Botones de  */
	$('.activar_starter').off('click');
	$('.activar_starter').click(function(e){
		e.preventDefault();
		// activar_starter($(this));
		var li = $(this).parent().parent();
		var id_li = '#'+li.attr('id');
		var ul_cont = $(this).parent().parent().parent();
		if (li.filter('.active').length < 1) {
			li.addClass('active');
		}else {
			li.removeClass('active');
		}
	});
	// function activar_starter($this) {
	// }
}
activar_starter();

function reservaciones_del_dia(date, starter__) {
	$.ajax({
		type: "POST",
		url: '/starters/reservaciones_del_dia',
		data: { date: date },
		success: function(respuesta) {
			var reservations = respuesta;
			var orden_salida = '';
			var orden_finalizada = '';
			var orden_canceladas = '';
			var now = new Date().toLocaleString('es-Es', { year: "numeric", month: "2-digit", day: '2-digit'}).split('/').reverse().join('-');
			var mensaje = '';
			if (reservations.length === 0) {
				mensaje = 'No hay reservaciones para este dia';
			}else {
				reservations.forEach(function(reservation){
					if (reservation.status == 'Aprobada') {
						if (starter__ === true) {
							orden_salida += '<li class="li_principal active" hora="'+reservation.start_time+'">';
							orden_salida += '	<div class="head">';
							orden_salida += '		<a href="" class="activar_starter" onclick="">';
							orden_salida += '			<span class="icon dropdown">';
							orden_salida += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
							orden_salida += '			</span>';
							orden_salida += '		</a>';
							orden_salida += '		<div class="select">';
							orden_salida += '			<div class="capa"></div>';
							orden_salida += '			<select class="" name="">';
							var start_times = new Date(date+' 06:00 AM');
							var end_times = new Date(date+' 06:00 PM');
							var start_times_string = null;
							for (var i = 0; i < 100; i++) {
								if (i !== 0) {
									if (start_times.getTime() == end_times.getTime()) {
										break;
									}else {
										start_times.setMinutes(start_times.getMinutes() + 10);
									}
								}
								start_times_string = start_times.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
								if (start_times_string.split(':')[0].length < 2) {
									start_times_string = 0+start_times_string.split(':')[0]+':'+start_times_string.split(':')[1];
								}
								orden_salida += '<option value="'+start_times_string+'" '+( (start_times_string == reservation.start_time) ? 'selected' : '' )+'>'+start_times_string+'</option>';
							}
							orden_salida += '			</select>';
							orden_salida += '			<a href="#" class="icon">';
							orden_salida += '				<i class="fa fa-remove"></i>';
							orden_salida += '			</a>';
							orden_salida += '		</div>';
							if (now == date) {
								orden_salida += '		<a class="icon check starter_accion" type="salida">';
								orden_salida += '			<i class="fa fa-check"></i>';
								orden_salida += '		</a>';
								orden_salida += '		<a class="icon cancel starter_accion" type="cancelar">';
								orden_salida += '			<i class="fa fa-remove"></i>';
								orden_salida += '		</a>';
							}
							orden_salida += '		<div class="clear"></div>';
							orden_salida += '	</div>';
							orden_salida += '	<ul class="conexion">';
							orden_salida += '		<li class="personas" id="'+reservation.id+'">';
							orden_salida += '			<div class="datos">';
							var num_participant = reservation.members.length + reservation.partners.length;
							var height = (num_participant * 26) +10;
							orden_salida += '				<div class="drop" style="height: '+height+'px">';
							orden_salida += '					<i class="fa fa-arrows" aria-hidden="true"></i>';
							orden_salida += '				</div>';
							orden_salida += '				<ul class="participantes conexion_participantes" id="'+reservation.id+'">';
							reservation.members.forEach(function(member) {
								orden_salida += '						<li id="'+member.id+'" tipo="member">';
								orden_salida += '							<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+'</span>';
								orden_salida += '							<div class="icon">';
								orden_salida += '								<i class="fa fa-arrows"></i>';
								orden_salida += '							</div>';
								orden_salida += '							<div class="clear"></div>';
								orden_salida += '						</li>';

							});
							reservation.partners.forEach(function(partner) {
								orden_salida += '						<li id="'+partner.id+'" tipo="partner">';
								orden_salida += '							<span> '+partner.name+' '+partner.last_name+'</span>';
								orden_salida += '							<div class="icon">';
								orden_salida += '								<i class="fa fa-arrows"></i>';
								orden_salida += '							</div>';
								orden_salida += '							<div class="clear"></div>';
								orden_salida += '						</li>';

							});
							orden_salida += '				</ul>';
							orden_salida += '				<div class="clear"></div>';
							orden_salida += '			</div>';
							orden_salida += '		</li>';
							orden_salida += '	</ul>';
							orden_salida += '</li>';

						}else {
							orden_salida += '<li class="li_principal active" hora="'+reservation.start_time+'">';
							orden_salida += '	<div class="head">';
							orden_salida += '		<a href="" class="activar_starter" onclick="">';
							orden_salida += '			<span class="icon dropdown">';
							orden_salida += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
							orden_salida += '			</span>';
							orden_salida += '		</a>';
							orden_salida += '		<div class="select">';
							orden_salida += '			<div class="capa no_accion"></div>';
							orden_salida += '			<select class="no_accion" name="">';
							orden_salida += '				<option value="" selected>'+reservation.start_time+'</option>';
							orden_salida += '			</select>';
							orden_salida += '			<a href="#" class="icon">';
							orden_salida += '				<i class="fa fa-remove"></i>';
							orden_salida += '			</a>';
							orden_salida += '		</div>';
							orden_salida += '		<div class="clear"></div>';
							orden_salida += '	</div>';
							orden_salida += '	<ul class="">';
							orden_salida += '		<li class="personas" id="'+reservation.id+'">';
							orden_salida += '			<div class="datos">';
							orden_salida += '				<?php';
							var num_participant = reservation.members.length + reservation.partners.length;
							var height = (num_participant * 26) +10;
							orden_salida += '				?>';
							orden_salida += '				<ul class="participantes" id="'+reservation.id+'">';
							reservation.members.forEach(function(member) {
								orden_salida += '<li>';
								orden_salida += '	<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+' (S)</span>';
								orden_salida += '</li>';

							});
							reservation.partners.forEach(function(partner) {
								orden_salida += '<li>';
								orden_salida += '	<span> '+partner.name+' '+partner.last_name+' <strong>(I)</strong></span>';
								orden_salida += '</li>';

							});
							orden_salida += '				</ul>';
							orden_salida += '				<div class="clear"></div>';
							orden_salida += '			</div>';
							orden_salida += '		</li>';
							orden_salida += '	</ul>';
							orden_salida += '</li>';

						}

					}else if (reservation.status == 'Finalizada') {
						orden_finalizada += '<li class="li_principal" hora="'+reservation.start_time+'">';
						orden_finalizada += '	<div class="head">';
						orden_finalizada += '		<a href="" class="activar_starter" onclick="">';
						orden_finalizada += '			<span class="icon dropdown">';
						orden_finalizada += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
						orden_finalizada += '			</span>';
						orden_finalizada += '		</a>';
						orden_finalizada += '		<div class="select">';
						orden_finalizada += '			<div class="capa no_accion"></div>';
						orden_finalizada += '			<select class="no_accion" name="" >';
						orden_finalizada += '				<option value="">'+reservation.start_time+'</option>';
						orden_finalizada += '			</select>';
						orden_finalizada += '			<a href="#" class="icon">';
						orden_finalizada += '				<i class="fa fa-remove"></i>';
						orden_finalizada += '			</a>';
						orden_finalizada += '		</div>';
						orden_finalizada += '		<div class="clear"></div>';
						orden_finalizada += '	</div>';
						orden_finalizada += '	<ul class="conexion">';
						orden_finalizada += '		<li class="personas" id="'+reservation.id+'">';
						orden_finalizada += '			<div class="datos">';
						orden_finalizada += '				<!-- <div class="drop">';
						orden_finalizada += '					<i class="fa fa-arrows" aria-hidden="true"></i>';
						orden_finalizada += '				</div> -->';
						orden_finalizada += '				<ul class="participantes conexion_participantes">';
						reservation.members.forEach(function(member) {
							orden_finalizada += '<li>';
							orden_finalizada += '	<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+' (S)</span>';
							orden_finalizada += '</li>';

						});
						reservation.partners.forEach(function(partner) {
							orden_finalizada += '<li>';
							orden_finalizada += '	<span> '+partner.name+' '+partner.last_name+' <strong>(I)</strong></span>';
							orden_finalizada += '</li>';

						});
						orden_finalizada += '				</ul>';
						orden_finalizada += '				<div class="clear"></div>';
						orden_finalizada += '			</div>';
						orden_finalizada += '		</li>';
						orden_finalizada += '	</ul>';
						orden_finalizada += '</li>';
					}else if(reservation.status == 'Cancelada'){
						orden_canceladas += '<li class="li_principal" hora="'+reservation.start_time+'">';
						orden_canceladas += '	<div class="head">';
						orden_canceladas += '		<a href="" class="activar_starter" onclick="">';
						orden_canceladas += '			<span class="icon dropdown">';
						orden_canceladas += '				<i class="glyphicon glyphicon-triangle-bottom"></i>';
						orden_canceladas += '			</span>';
						orden_canceladas += '		</a>';
						orden_canceladas += '		<div class="select">';
						orden_canceladas += '			<div class="capa no_accion"></div>';
						orden_canceladas += '			<select class="no_accion" name="" >';
						orden_canceladas += '				<option value="">'+reservation.start_time+'</option>';
						orden_canceladas += '			</select>';
						orden_canceladas += '			<a href="#" class="icon">';
						orden_canceladas += '				<i class="fa fa-remove"></i>';
						orden_canceladas += '			</a>';
						orden_canceladas += '		</div>';
						orden_canceladas += '		<div class="clear"></div>';
						orden_canceladas += '	</div>';
						orden_canceladas += '	<ul class="conexion">';
						orden_canceladas += '		<li class="personas" id="'+reservation.id+'">';
						orden_canceladas += '			<div class="datos">';
						orden_canceladas += '				<!-- <div class="drop">';
						orden_canceladas += '					<i class="fa fa-arrows" aria-hidden="true"></i>';
						orden_canceladas += '				</div> -->';
						orden_canceladas += '				<ul class="participantes conexion_participantes">';
						reservation.members.forEach(function(member) {
							orden_canceladas += '<li>';
							orden_canceladas += '	<span style="font-weight: bolder;"> '+member.name+' '+member.last_name+' (S)</span>';
							orden_canceladas += '</li>';

						});
						reservation.partners.forEach(function(partner) {
							orden_canceladas += '<li>';
							orden_canceladas += '	<span> '+partner.name+' '+partner.last_name+' <strong>(I)</strong></span>';
							orden_canceladas += '</li>';

						});
						orden_canceladas += '				</ul>';
						orden_canceladas += '				<div class="clear"></div>';
						orden_canceladas += '			</div>';
						orden_canceladas += '		</li>';
						orden_canceladas += '	</ul>';
						orden_canceladas += '</li>';

					}
				});

			}


			$('#orden_salida .reservas').html(orden_salida);
			$('#orden_canceladas .reservas').html(orden_canceladas);
			$('#orden_finalizada .reservas').html(orden_finalizada);
			$('#starters .mensaje p').html(mensaje);
			activar_starter();

		}
	});
}
