var members = null, partners = null, date_reservation_selected = '', dias_de_sorteo_select = null;
var members_agregados = [];
var partners_agregados = [];
var partners_green_fee = [];
var green_fee_mounts = [];
var date_selected;
var hoy = new Date();




$(document).ready(function(){
    buscar_invitados_reservacion();
    buscar_socios_reservacion();
    mostrar_minutos(reservation_edit.date);
    date_reservation();
    setTimeout(function(){
        agregar_participantes_edit();
    }, 1000);
});

function agregar_participantes_edit(){
    var members = '';
    var partners = '';
    reservation_edit.members.forEach(function(member){
        if (member.user_id !== user_id) {
            members += '<li id="member_'+member.id+'" class="member_reservation" id-="'+member.id+'">';
            members += '    <input type="hidden" class="member_id" name="member_id[]" value="'+member.id+'">';
            members += '    <div class="nombre">'+member.name+' '+member.last_name+'</div>';
            members += '    <div>N° Accion: '+member.number_action+'</div>';
            members += '    <div class="cerrar" onclick="cerrar_add(\'#member_'+member.id+'\'); return false;">';
            members += '        <a href="#" class="cerrar_add">';
            members += '            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
            members += '        </a>';
            members += '    </div>';
            members += '</li>';
            $('#socio_pagador').append('<option id="option_member_'+member.id+'" value="'+member.id+'">'+member.nombre+'</option>');
            members_agregados.push(member.id);

        }
    });
    reservation_edit.partners.forEach(function(partner){
        partners += '<li id="partner_'+partner.id+'" exonerado="" class="select_green_fee">';
        partners += '    <input type="hidden" class="partner_id" name="partner_id[]" value="'+partner.id+'">';
        partners += '    <div>'+partner.name+' '+partner.last_name+'</div>';
        partners += '    <div>C.I: '+number_format(partner.identity_card, 0, ',', '.')+'</div>';
        partners += '    <div class="green_fee">';
        partners += '        <select name="reservation_definition_id[]" id="" class="form-control input-md" onchange="select_green_fee($(this), '+partner.id+');">';
        partners += '            <option value="" selected="" disabled="">Seleccionar gren fee</option>';
        green_fees.forEach(function(green_fee){
            partners += '<option value="'+green_fee.id+'">'+green_fee.name+'</option>';
        });
        partners += '        </select>';
        partners += '        <div class="input_error none" style="top: calc(100% + 20px); transform: translateY(0);">';
        partners += '            <p></p>';
        partners += '        </div>';
        partners += '    </div>';
        partners += '    <div class="cerrar" onclick="cerrar_add(\'#partner_'+partner.id+'\'); return false;">';
        partners += '        <a href="#" class="cerrar_add">';
        partners += '            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
        partners += '        </a>';
        partners += '    </div>';
        partners += '</li>';
        partners_agregados.push(partner.id);
        partners_green_fee.push(partner.id);
        if (partner.exonerated === null) {
            $('.reserv .inputs .boton').addClass('pagar');
        }
    });
    $('.add_invitados ul').append(partners);
    $('.add_socios ul').not('.socio_auth').append(members);
}

/* Funcion que consulta todos los members de la DB y lo pasa a la variable members*/
function buscar_socios_reservacion() {
	$.ajax({
		type: "GET",
		url: '/socios/alls',
		success: function(respuesta) {
			if (respuesta.status === true) {
				members = respuesta.response;
			}
		}
	});
}
/* Funcion que consulta todos los Partners de la DB y lo pasa a la variable partners*/
function buscar_invitados_reservacion() {
	var url = ($('.type_member').length < 1) ? '/invitados/alls' : '/s_invitados/alls';
	var data = ($('.type_member').length < 1) ? {} : {user_id: 17};
	$.ajax({
		type: "GET",
		url: url,
		data: data,
		success: function(respuesta) {
			if (respuesta.status === true) {
				partners = respuesta.response;
			}
		}
	});
}
// Funcion que agrega todos los members y partners a select2 de reservacion y inicializa el mismo
function select2() {
	var option_partner = '';
	option_partner = '<option value="" disabled selected>Seleccionar Invitado</option>';
	for (var i = 0; i < partners.length; i++) {
		var partner = partners[i];
		// var nombre = partner.name + ' ' + partner.last_name;
		var id = partner.id;
		option_partner += '<option value="'+id+'">C.I.: '+(number_format(partner.identity_card, 0, ',', '.'))+' '+ partner.nombre +' </option>';
	}
	$('#add_partners').html(option_partner);
	var option_members = '';
	option_members = '<option value="selected" disabled selected>Seleccionar Socio</option>';
	for (var i = 0; i < members.length; i++) {
		var member = members[i];
		// var nombre = member.name + ' ' + member.last_name;
		var id = member.id;
		option_members += '<option value="'+id+'">C.I.: '+(number_format(member.identity_card, 0, ',', '.'))+' '+ member.nombre +' </option>';
	}
	$('#add_members').html(option_members);
	$(".select2").select2();
}
// Funcion que muestra todos los consaje de reservacion
function mensaje_reservation($this, mensaje_, tipo, usuario) {
	var mensaje = $('.mensaje', $this.parent().parent());
	text = '<p> '+ mensaje_ +' </p>';
	mensaje.html(text);
	mensaje.addClass('mostrar');
	$('#'+tipo+'_'+usuario.id).addClass('marcar');
	setTimeout(function(){
		$('#'+tipo+'_'+usuario.id).removeClass('marcar');
		mensaje.removeClass('mostrar');
	}, 5000);

}

// Funcion que seleciona las horas de reservacion
function select_hour_reservation() {
	$('.select_hour_reservation').off('click');
	$('.select_hour_reservation').click(function(){
		$('#time_reservacion').val($(this).val());
		if ($(this).hasClass('intervalo')) {
			var check = $(this).prop('checked');
			var id = parseInt($(this).attr('id'));
			var checks = $('.intervalo_'+id);
			var icons = checks.parent();
			var icons_not = $('.edit .horas_reservacion ul li .minutos ul li .icon').not(icons);
			var no_checks = $('input', icons_not).prop('checked', false);
			var draw_id = parseInt($(this).attr('draw-id'));
			function encontrar_draw(draw) {
			    return draw.id === draw_id;
			}
			var draw = draws.find(encontrar_draw);
			var start_date = new Date(draw.start_date.replace(/-/g, '/'));
			var draw_date = new Date(draw.draw_date.replace(/-/g, '/'));
			draw_date.setHours(23);
			draw_date.setMinutes(59);
			var now = new Date();
			if (now.getTime() >= start_date.getTime() && now.getTime() <= draw_date.getTime()) {
				if (dias_de_sorteo_select === null) {
					$(this).prop('checked', true);
					no_checks.prop('checked', false);
					icons_not.removeClass('selected');
					icons.addClass('selected');
					dias_de_sorteo_select = id;
				}else {
					if (dias_de_sorteo_select === id) {
						icons.removeClass('selected');
						dias_de_sorteo_select = null;
					}else {
						no_checks.prop('checked', false);
						icons_not.removeClass('selected');
						icons.addClass('selected');
						dias_de_sorteo_select = id;
					}
				}
				input_error($('.horas_reservacion ul'), null, false);
			}else {
				var start_date_string = start_date.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});
				var draw_date_string = draw_date.toLocaleString('es-Es', { weekday: "long", year: "numeric", month: "long", day: "numeric",});

				input_error($('.horas_reservacion ul'), 'Este sorteo solo estara disponible <br> desde:  <strong>'+start_date_string+'</strong> <br> hasta: <strong>'+draw_date_string+'</strong>', true);
			}
		}else {
			var check = $(this).prop('checked');
			$('#time_reservacion').val($(this).val());
			var icon = $(this).parent();
			var icon_not = $('.edit .horas_reservacion ul li .minutos ul li .icon').not(icon);
			if (check === true) {
				$('input', icon_not).prop('checked', false);
				$(this).prop('checked', true);
				icon_not.removeClass('selected');
				icon.addClass('selected');
				dias_de_sorteo_select = null;
			}else {
				icon.removeClass('selected');
				$(this).prop('checked', false);
			}
			input_error($('.horas_reservacion ul'), null, false);

		}
	});
}
// Funcion que exonera a partners durante reeservacion
function exonerar(id_selector) {
	var id = parseInt(id_selector.split('_')[1]);
	if (partners_green_fee.indexOf(id) == -1) {
		partners_green_fee.push(id);
		$(id_selector).addClass('select_green_fee');
		if ($('.green_fee select', $(id_selector)).val() !== '') {
			green_fee_mounts.push({ id: id, id_green_fee: parseInt($('.green_fee select', $(id_selector)).val()) });
		}
	}else {
		partners_green_fee.splice(partners_green_fee.indexOf(id), 1);
		$(id_selector).removeClass('select_green_fee');
		for (var i = 0; i < green_fee_mounts.length; i++) {
			var green_fee_mount = green_fee_mounts[i];
			if (green_fee_mount.id == id) {
				green_fee_mounts.splice(i, 1);
			}
		}
	}
	if (partners_green_fee.length < 1) {
		$('.reserv .inputs .boton').removeClass('pagar');
	}else {
		$('.reserv .inputs .boton').addClass('pagar');
	}
}

/* Funcion que remueve participantes de lista */
function cerrar_add(cerrar) {
	var tipo = cerrar.split('_')[0].split('#')[1];
	var id = parseInt(cerrar.split('_')[1]);
	if (tipo == 'partner') {
		if (partners_green_fee.indexOf(id) != -1) {
			partners_green_fee.splice(partners_green_fee.indexOf(id), 1);
		}
		partners_agregados.splice(partners_agregados.indexOf(id), 1);
	}else {
		var pagador = '#option_member_'+cerrar.split('_')[1];
		$(pagador).remove();
		members_agregados.splice(members_agregados.indexOf(id), 1);
	}
	for (var i = 0; i < green_fee_mounts.length; i++) {
		var green_fee_mount = green_fee_mounts[i];
		if (green_fee_mount.id == id) {
			green_fee_mounts.splice(i, 1);
		}
	}
	if (partners_green_fee.length < 1) {
		$('.reserv .inputs .boton').removeClass('pagar');
	}
	$(cerrar).fadeOut(400);
	setTimeout(function(){
		$(cerrar).remove();
	},400);
}

/* Funcion que seleciona el green fee de cada partner*/
function select_green_fee(selector, id) {
	input_error(selector, null, false);
	var object = { id: id, id_green_fee: parseInt(selector.val()) };
	var index;
	if (green_fee_mounts.length > 0) {
		for (var i = 0; i < green_fee_mounts.length; i++) {
			var green_fee_mount = green_fee_mounts[i];
			if (green_fee_mount.id == id) {
				index = i;
			}
		}
		if (index === undefined) {
			green_fee_mounts.push(object);
		}else {
			green_fee_mounts.splice(index, 1, object);
		}
	}else {
		green_fee_mounts.push(object);
	}
}

/* Funcion que abre el modal de procesar pago */
function mostrar_procesar(estado, tipo_pago) {
	var monto = 0;
	for (var igfm = 0; igfm < green_fee_mounts.length; igfm++) {
		var green_fee_mount = green_fee_mounts[igfm];
		for (var igf = 0; igf < green_fees.length; igf++) {
			var green_fee = green_fees[igf];
			if (green_fee_mount.id_green_fee == green_fee.id) {
				monto += green_fee.green_fee;
			}
		}
	}
	var procesar = $('.procesar');
	if (estado) {
		var tipo_pago_nombre = tipo_pago.split('-')[0];
		var tipo_pago_id = tipo_pago.split('-')[1];
		var html = '';
		html += '<div class="titulo">';
		html += '	<h1> Procesar Pago </h1>';
		html += '</div>';
		html += '<ul class="tipo_pago">';
		html += '	<li>';
		html += '		<p> '+ ( (tipo_pago_nombre == 'pago_online') ? 'Online' : 'En el club' ) +' </p>';
		html += '	</li>';
		html += '	<li>';
		html += '		<img src="/img/icons/'+tipo_pago_nombre+'.png" alt="">';
		html += '		<input type="hidden" name="type_payment_id" value="'+tipo_pago_id+'">';
		html += '	</li>';
		html += '</ul>';
		if ($('.type_member').length < 1) {
			html += '<div class="inpt socio_pagador">';
			html += '	<label class="" for="receipt_payment">Buscar socio a cancelar la reserva</label>';
			html += '	<select class="js-example-basic-single form-control input-md" id="socio_pagador" name="payer_id">';
			html += '			<option value="" disabled selected>Seleccionar socio</option>';
								for (var i = 0; i < members.length; i++) {
									var member = members[i];
									if (members_agregados.indexOf(member.id) != -1) {
										html += '<option value="'+member.id+'">'+member.nombre+'</option>';
									}
								}
			html += '	</select>';
			html += '	<div class="input_error none">';
			html += '		<p></p>';
			html += '	</div>';
			html += '</div>';
		}
		html += '<div class="monto">';
		html += '	<h1>'+ number_format(monto, 2, ',', '.') +' BsF</h1>';
		html += '</div>';
		html += '<div class="condiciones">';
		html += '	<div class="inpt condiciones">';
		html += '		<input type="checkbox" name="condiciones" value="true" id="condiciones">';
		html += '		<label for="">Acepto las <a href="#">Condiciones de pago</a></label>';
		html += '		<div class="input_error none">';
		html += '			<p></p>';
		html += '		</div>';
		html += '	</div>';
		html += '</div>';
		html += '<div class="form-group boton">';
		html += '	<button type="button" class="btn btn-primary singlebutton1 submit">Procesar</button>';
		html += '	<button type="button" class="btn btn-primary singlebutton1" onclick="mostrar_procesar(false);">Cancelar</button>';
		html += '</div>';
		html = $.parseHTML(html);
		if ($('.type_member').length > 0) {
			$('.socio_pagador', html).remove();
		}
		$('.procesar .form').html(html);
		$('#socio_pagador').select2();
		procesar.removeClass('none');
		setTimeout(function(){
			procesar.addClass('active');
		}, 10);
		$('.procesar .form .boton button').filter('.submit').click(function(){
			var enviar = false;
			if ( $('#socio_pagador').val() === '' || $('#socio_pagador').val() === null ) {
				input_error($('#socio_pagador'), 'Debes seleccionar el socio a cancelar la reservacion', true);
			}else if ($('#condiciones').prop('checked') === false) {
				input_error($('#condiciones'), 'Indicar si esta de acuerdo con las condiciones de pago', true);
			}else {
				enviar = true;
			}
			if (enviar) {
				$('#form_reservacion')[0].submit();
			}
		});
		$('#socio_pagador').on('select2:select', function (evt) {
			input_error($('#socio_pagador'), null, false);
		});
		$('#condiciones').change(function (evt) {
			input_error($('#condiciones'), null, false);
		});
	}else {
		procesar.removeClass('active');
		setTimeout(function(){
			$('.procesar .form').html('');
			procesar.addClass('none');
		}, 700);
	}
}



$('#add_members').on('select2:select', function (evt) {
	var $this = $(this);
	var id = parseInt(evt.params.data.id);
	var member;
	for (var i = 0; i < members.length; i++) {
		if (members[i].id == id) {
			member = members[i];
		}
	}
	var li, text;
	li =	'<li id="member_'+member.id+'" class="member_reservation" id-="'+member.id+'">';
	li +=		'<input type="hidden" class="member_id" name="member_id[]" value="'+member.id+'">';
	li +=		'<div class="nombre">';
	li +=			member.nombre;
	li +=		'</div>';
	li +=		'<div>';
	if (member.user == 2) {
		li +=			'C.I: '+ number_format(member.identity_card, 0, ',', '.');
	}else{
		li +=			'N° Accion: '+ member.number_action;
	}
	li +=		'</div>';
	li +=		'<div class="cerrar" onclick="cerrar_add(\'#member_'+member.id+'\'); return false;">';
	li +=			'<a href="#" class="cerrar_add">';
	li +=				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
	li +=			'</a>';
	li +=		'</div>';
	li +=	'</li>';
	li = $.parseHTML(li);
    function append_member() {
        $('.add_socios ul').not('.socio_auth').append(li);
        members_agregados.push(member.id);
    }

	if ($('.type_member').length <= 0) { //Verifico si es tipo admin
		if ($('#member_'+member.id).length <= 0) {
			$.ajax({
				type: "GET",
				url: '/socios/'+user_id+'/validar_reservacion_socio',
				data: {
					member_id: member.id,
					date: $('#date_reservation').val(),
				},
				success: function(respuesta) {
					if (respuesta.validation === true) {
						$('.participante_dia label span', $this.parent()).html('');
						reservacion_en_el_dia($this, false);

						$('#socio_pagador').append('<option id="option_member_'+member.id+'" value="'+member.id+'">'+member.nombre+'</option>');
						input_error($('#add_members'), null, false);
                        // $('.add_socios ul').not('.socio_auth').append(li);
						// members_agregados.push(member.id);
                        append_member();
					} else {
						input_error($('#add_members'), null, false);
						$('.participante_dia label span', $this.parent()).html(respuesta.message);
						reservacion_en_el_dia($this, true);
						$('.participante_dia button', $this.parent()).off('click');
						/* Si Hace clicl en continuar*/
						$('.participante_dia button', $this.parent()).filter('.continuar').click(function(){
							$('#socio_pagador').append('<option id="option_member_'+member.id+'" value="'+member.id+'">'+member.nombre+'</option>');
							// $('.add_socios ul').not('.socio_auth').append(li);
							// members_agregados.push(member.id);
                            append_member();
							reservacion_en_el_dia($this, false);
						});

						/* Si Hace clicl en cancelar*/
						$('.participante_dia button', $this.parent()).filter('.cancelar').click(function(){
							$('.participante_dia label span', $this.parent()).html('');
							reservacion_en_el_dia($this, false);
						});


					}
				}
			});
		}else {
			mensaje_reservation($this, 'El socio '+member.nombre+' esta en la reservacion', 'member', member);
		}
	}else { //sino se agregan validaciones para tipos miembros
		$.ajax({
			type: "GET",
			url: '/socios/'+user_id+'/validar_reservacion_socio',
			data: {
				member_id: member.id,
				date: $('#date_reservation').val(),
			},
			success: function(respuesta) {
				if (respuesta.validation === true || (respuesta.validation === false && reservation_edit.date == $('#date_reservation').val())) {
					var total = partners_agregados.length + members_agregados.length;
					if (total < 3) {
						if ($('#member_'+member.id).length <= 0) {
							// $('.add_socios ul').not('.socio_auth').append(li);
							// members_agregados.push(member.id);
                            append_member();
						}else {
							mensaje_reservation($this, 'El socio '+member.nombre+' esta en la reservacion', 'member', member);
						}
					}else {
						mensaje_reservation($this,'Ya la reservacion esta completa', 'member', member);
					}
				} else {
					mensaje_reservation($this, respuesta.message, 'member', member);

				}
			}
		});
	}
	select2();
});


$('#add_partners').on('select2:select', function (evt) {
	var $this = $(this);
	var id = parseInt(evt.params.data.id);
	var partner;
	for (var i = 0; i < partners.length; i++) {
		if (partners[i].id == id) {
			partner = partners[i];
		}
	}
	 var mensaje = $('.mensaje', $('#add_partners').parent().parent());
	 var li, text;
	 li =	'<li id="partner_'+partner.id+'" exonerado="" class="'+( ((partner.exonerated === null)) ? 'select_green_fee' : '' )+'">';
	 li +=		'<input type="hidden" class="partner_id" name="partner_id[]" value="'+partner.id+'">';
	 li +=		'<div>';
	 li +=			partner.nombre;
	 li +=		'</div>';
	 li +=		'<div>';
	 li +=			'C.I: '+ number_format(partner.identity_card, 0, ',', '.');
	 li +=		'</div>';
	 li += 		'<div class="green_fee">';
	 li += 			'<select name="reservation_definition_id[]" id="" class="form-control input-md" onchange="select_green_fee($(this), '+partner.id+');">';
	 li += 			'<option value="" selected disabled>Seleccionar gren fee</option>';
					for (var igf = 0; igf < green_fees.length; igf++) {
						var green_fee = green_fees[igf];
						li += '<option value="'+green_fee.id+'">'+green_fee.name+'</option>';
					}
	 li += 			'</select>';
	 li += 			'<div class="input_error none">';
	 li += 			'	<p></p>';
	 li += 			'</div>';
	 li += 		'</div>';
	 li +=		'<div class="cerrar" onclick="cerrar_add(\'#partner_'+partner.id+'\'); return false;">';
	 li +=			'<a href="#" class="cerrar_add">';
	 li +=				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
	 li +=			'</a>';
	 li +=		'</div>';

	 li +=	'</li>';
	//  li = $.parseHTML(li);
// || (respuesta.validation === false && reservation_edit.date == $('#date_reservation').val())

	if ($('.type_member').length <= 0) { //Verifico si es tipo admin

		if (members_agregados.length > 0) {
			if ($('#partner_'+partner.id).length <= 0) {
				var exonerated =	'<div class="check_exonerated">';
				exonerated +=		'<input type="checkbox" name="exonerated[]" value="'+partner.id+'" onclick="exonerar(\'#partner_'+partner.id+'\')">';
				exonerated +=	'</div>';
				var exo = $.parseHTML(exonerated);
				var html = $.parseHTML(li);
				$.ajax({
					type: "GET",
					url: '/socios/'+user_id+'/validar_reservacion_invitado',
					// url: 'http://dementecreativo.com/lagunita/golf/socios/'+user_id+'/validar_reservacion_invitado',
					data: {
						partner_id: partner.id,
						date: $('#date_reservation').val(),
					},
					success: function(respuesta) {
						if (respuesta.validation === true) {
							$('.participante_dia label span', $this.parent()).html('');
							reservacion_en_el_dia($this, false);


							$('.cerrar', html).prev().after(exo);
							$('.add_invitados ul').append(html);
							partners_agregados.push(partner.id);

							if (partner.exonerated === null) {
								$('.reserv .inputs .boton').addClass('pagar');
								partners_green_fee.push(partner.id);
							}else {
								$('.check_exonerated input', html).prop('checked', true);
								mensaje_reservation($this, 'El invitado '+partner.nombre+' es exonerado', 'partner', partner);
							}

						}else {
							input_error($('#add_members'), null, false);
							$('.participante_dia label span', $this.parent()).html(respuesta.message);
							reservacion_en_el_dia($this, true);
							$('.participante_dia button', $this.parent()).off('click');
							/* Si Hace clicl en continuar*/
							$('.participante_dia button', $this.parent()).filter('.continuar').click(function(){
								$('.cerrar', html).prev().after(exo);
								$('.add_invitados ul').append(html);
								partners_agregados.push(partner.id);

								if (partner.exonerated === null) {
									$('.reserv .inputs .boton').addClass('pagar');
									partners_green_fee.push(partner.id);
								}else {
									$('.check_exonerated input', html).prop('checked', true);
									mensaje_reservation($this, 'El invitado '+partner.nombre+' es exonerado', 'partner', partner);
								}

								reservacion_en_el_dia($this, false);
							});

							/* Si Hace clicl en cancelar*/
							$('.participante_dia button', $this.parent()).filter('.cancelar').click(function(){
								$('.participante_dia label span', $this.parent()).html('');
								reservacion_en_el_dia($this, false);
							});

							// mensaje_reservation($this, respuesta.message, partner);
						}
					}
				});

			}else {
					mensaje_reservation($this, 'El invitado '+partner.nombre+' esta en la reservacion', 'partner', partner);
			}
		}else {
			mensaje_reservation($this, 'Debes seleccionar un socio para la reservacion', 'partner', partner);
		}
	}else { //sino se agregan validaciones para tipos miembros
		$.ajax({
			type: "GET",
			url: '/socios/'+user_id+'/validar_reservacion_invitado',
			// url: 'http://dementecreativo.com/lagunita/golf/socios/'+user_id+'/validar_reservacion_invitado',
			data: {
				partner_id: partner.id,
				date: $('#date_reservation').val(),
			},
			success: function(respuesta) {
				if (respuesta.validation === true || (respuesta.validation === false && reservation_edit.date == $('#date_reservation').val())) {
					var total = partners_agregados.length + members_agregados.length;
					 if (total < 3) {
							 if (partners_agregados.length < 2) {
									 if ($('#partner_'+partner.id).length <= 0) {
											 $('.add_invitados ul').append(li);
											 partners_agregados.push(partner.id);
											if (partner.exonerated === null) {
												$('.reserv .inputs .boton').addClass('pagar');
												partners_green_fee.push(partner.id);
											}else {
												mensaje_reservation($this, 'El invitado '+partner.nombre+' es exonerado', 'partner', partner);
											}
									 }else {
										 mensaje_reservation($this, 'El invitado '+partner.nombre+' esta en la reservacion', 'partner', partner);
									 }
							 }else {
									 mensaje_reservation($this, 'Ya hay 2 invitados en la reservacion', 'partner', partner);
							 }
					 }else {
							 mensaje_reservation($this, 'Ya la reservacion esta completa', 'partner', partner);
					 }
				}else {
					mensaje_reservation($this, respuesta.message, partner);
				}
			}
		});
	}
	select2();
});

function reservacion_en_el_dia($this, abrir) {
	var participante_dia = $('.participante_dia', $this.parent());
	if (abrir) {
		participante_dia.removeClass('none');
		setTimeout(function(){
			participante_dia.addClass('active');
		}, 10);
	}else {
		participante_dia.removeClass('active');
		setTimeout(function(){
			participante_dia.addClass('none');
		}, 500);
	}
}

$('.reserv .inputs .boton a').click(function(event){
	event.preventDefault();
	var form = $('#form_reservacion');
	var num_members = members_agregados.length;
	var num_times = $('input', $('.edit .horas_reservacion ul li .minutos ul li .icon.selected')).length;
	var estado = false;
	var green_fee = ($('.green_fee').filter('.active').length > 0) ? true : false;
	var procesar = $('.procesar', form);


	if ($('.type_member').length < 1) {
		if (num_members < 1) {
			input_error($('#add_members'), 'Debes agregar un socio', true);
		}
	}
	if (num_times < 1) {
		input_error($('.horas_reservacion ul'), 'Debes seleccionar una hora para la reserva', true);
	}

	if ($('.type_member').length < 1) {
		if (num_members > 0 && num_times > 0) {
			estado = true;
		}
	}else {
		if (num_times > 0) {
			estado = true;
		}

	}

	if (estado === true) {
		var date = $(".date_reservation").datepicker( "getDate" ).toLocaleString('es-ES', { day: '2-digit', month: '2-digit', year: 'numeric' }).replace(/\//g, '-');
		var time = $('input', $('.edit .horas_reservacion ul li .minutos ul li .icon.selected')).val();
		$($('#date_reservacion').val(date));
		$($('#time_reservacion').val(time));

		if (partners_green_fee.length < 1) {
			form[0].submit();
		}else {
			var tipo_pago = $(this).attr('tipo-pago');
			var enviar = false;
			var num = 0;
			$('.add_.add_invitados ul li.select_green_fee select').each(function(index, element){
				if (num === 0) {
					if ($(this).val() === '' || $(this).val() === null) {
						input_error($(this), 'Debes seleccionar el green fee de los invitados selecionados', true);
						num++;
					}
				}
			});
			if (num === 0) {
				mostrar_procesar(true, tipo_pago);
			}
		}
	}
});

function this_remove(element) {
	element.remove();
}
function mostrar_minutos(date) {
	$($('#date_reservacion').val(date));
	$.ajax({
		type: "GET",
		url: (window.url_consultar_reserv) ? url_consultar_reserv : '',
		data: {date: date},
		autoSize: true,
		beforeSend: function (){
			$('.edit.reserv .form .carga').removeClass('none');
			setTimeout(function(){
				$('.edit.reserv .form').addClass('cargando');
			}, 10);
		},
		success: function(respuesta) {
			var status = respuesta.response;
			var mensaje = respuesta.message;
			var code = respuesta.code;
			if (status) {
				var reservations = respuesta.reservations;
				var tournament = respuesta.tournaments[0];
				var time_draws = (respuesta.day_draws.length !== 0 && (respuesta.day_draws[0].day_draw_status === 0  )) ? respuesta.day_draws[0].time_draws : '';
				var date_format = date.split('-')[2]+'/'+date.split('-')[1]+'/'+date.split('-')[0];
                var date_format2 = date.split('-').reverse().join('-');
				var objeto_fecha = new Date(date_format);
                var reservation_in_day = (reservation_edit.date == date) ? false : respuesta.reservation_in_day;
				var hora_inicio = new Date(date_format);
				var hora_final = new Date(date_format);
				hora_inicio.setHours(06,00,00);
				hora_final.setHours(17,00,00);
				var horas_totales = hora_final.getHours() - hora_inicio.getHours();
				li = '';
				li += '<li>';
				li += '	<div class="horas tiempo">';
				li += '		<div class="hora">';
				li += '			<h4>Salidas</h4>';
				li += '		</div>';
				li += '	</div>';
				li += '	<div class="minutos tiempo">';
				li += '		<ul>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>1era</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>2da</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>3era</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>4ta</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>5ta</span>';
				li += '				</div>';
				li += '			</li>';
				li += '			<li>';
				li += '				<div class="icon">';
				li += '					<span>6ta</span>';
				li += '				</div>';
				li += '			</li>';
				li += '		</ul>';
				li += '	</div>';
				li += '</li>';
				for (var i = 0; i < horas_totales+1; i++) {
					if (i !== 0) {
						hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
					}
					var hora = hora_inicio.toLocaleString('en-Us', { hour: '2-digit', hour12: true });
					li += '<li>';
					li += '    <div class="horas tiempo">';
					li += '        <div class="hora"> <span> '+hora+' </span> </div>';
					li += '    </div>';
					li += '    <div class="minutos tiempo">';
					li += '        <ul>';
					for (var im = 0; im < 6; im++) {
						var hora_pasada = false;
						var reservacion = null;
						if (im !== 0) {
							hora_inicio.setMinutes(hora_inicio.getMinutes() + 10);
						}
						var minuto = hora_inicio;
						minuto = minuto.toLocaleString('en-Us', { hour: '2-digit', hour12: true, minute: '2-digit' });
						if (minuto.split(':')[0].length < 2) {
							minuto = 0+minuto.split(':')[0]+':'+minuto.split(':')[1];
						}

						if (hora_inicio.getTime() < hoy.getTime()) {
							hora_pasada = true;
						}
						for (var ir = 0; ir < reservations.length; ir++) {
							var reservation = reservations[ir];
							if (reservation.start_time == minuto) {
								reservacion = reservation;
							}
						}
						var torneo = null;
						if (tournament !== undefined && tournament !== null) {
							var tournament_start_time = new Date(date_format+' '+tournament.start_time);
							var tournament_end_time = new Date(date_format+' '+tournament.end_time);
							if (hora_inicio.getTime() >= tournament_start_time.getTime() && hora_inicio.getTime() <= tournament_end_time.getTime()) {
								torneo = tournament;
							}
						}

						var sorteo = false;
						var intervalo_id = null;
						var draw_id = null;
						draws.forEach(function(draw){
							draw.day_draws.forEach(function(day_draw){
								if (date_format2 == day_draw.date) {
									draw_id = day_draw.draw_id;
									day_draw.time_draws.forEach(function(time_draw){
										var draw_start_time = new Date(date_format+' '+time_draw.start_time);
										var draw_end_time = new Date(date_format+' '+time_draw.end_time);
										if (hora_inicio.getTime() >= draw_start_time.getTime() && hora_inicio.getTime() <= draw_end_time.getTime()) {
											sorteo = true;
											intervalo_id = time_draw.id;
										}
									});
								}
							});
						});
                        var hora_bloqueada = false;
                        function encontrar_dia_bloqueado(day_blocked) {
                            return day_blocked.date === date_format2;
                        }
                        var day_blocked = day_blockeds.find(encontrar_dia_bloqueado);
                        if (day_blocked !== undefined) {
                            day_blocked.time_blockeds.forEach(function(time_blocked){
                                var start_time = new Date(date_format+' '+time_blocked.start_time);
                                var end_time = new Date(date_format+' '+time_blocked.end_time);
                                if (hora_inicio.getTime() >= start_time.getTime() && hora_inicio.getTime() <= end_time.getTime()) {
                                    hora_bloqueada = true;
                                }
                            });
                        }



						li += '            <li class="">';
						li += '                <div class="icon '+ ( (hora_pasada || reservation_in_day) ? 'filter' : '' ) +'"> ';
						if (!reservation_in_day) {
							if (!hora_pasada) {
                                if (!hora_bloqueada) {
                                    if (torneo === null) {
                                        if (reservacion === null || sorteo) {
                                            li += '                    <input type="checkbox" class="select_hour_reservation '+( (sorteo) ? 'intervalo intervalo_'+intervalo_id : '' )+'" name="time" value="'+minuto+'" id="'+( (sorteo) ? intervalo_id : '' )+'" draw-id="'+( (sorteo) ? draw_id : '' )+'"> ';
                                        }
                                    }
                                }
							}
						}
                        if (!hora_bloqueada) {
                            if (reservacion === null || torneo !== null) {
                                if (torneo !== null) {
                                    li += '    				   <img class="no_disponible" src="'+ public_asset +'img/icons/torneo.png" alt="" >';
                                }else{
                                    if (sorteo) {
                                        li += '                    <img class="disponible" src="'+ public_asset +'img/icons/sorteable.png" alt="" > ';
                                        li += '                    <img class="selected" src="'+ public_asset +'img/icons/selected_sorteable.png" alt="" >';
                                    }else {
                                        li += '                    <img class="disponible" src="'+ public_asset +'img/icons/disponible.png" alt="" > ';
                                        li += '                    <img class="selected" src="'+ public_asset +'img/icons/selected.png" alt="" >';
                                    }
                                }
                            }else {
                                if (sorteo) {
                                    li += '                    <img class="disponible" src="'+ public_asset +'img/icons/sorteable.png" alt="" > ';
                                    li += '                    <img class="selected" src="'+ public_asset +'img/icons/selected_sorteable.png" alt="" >';
                                }else {
                                    li += '				   	   <img class="no_disponible" src="'+ public_asset +'img/icons/no_disponible.png" alt="" >';
                                }
                            }

                        }else {
                            li += '                    <img class="no_disponible" src="'+ public_asset +'img/icons/hora_bloqueada.png" alt="" style="height: 27.5px;"> ';
                        }
						li += '                    <div class="ayuda">';
						li += '                        <h6>'+minuto+'</h6>';
                        if (hora_bloqueada) {
							li += '                        <p>Hora Bloqueada</p>';
						}else if (torneo !== null) {
							li += '                        <p>'+torneo.name+'</p>';
						}else if (hora_pasada || reservacion !== null) {
							if (hora_pasada) {
								li += '                        <p>Hora no disponible</p>';
							}else if (sorteo) {
								li += '                        <p>Hora de sorteo</p>';
							}else {
								li += '                        <p>Hora no disponible</p>';
							}
						}else if (reservacion === null && torneo !== null) {
							li += '                        <p>'+torneo.name+'</p>';
						}else if (sorteo) {
							li += '                        <p>Hora de sorteo</p>';
						}else if (reservacion === null) {
							li += '                        <p>Disponible</p>';
						}
						li += '                    </div>';
						li += '                </div>';
						li += '            </li>';
					}
					li += '        </ul>';
					li += '    </div>';
					li += '</li>';
				}
				if (reservation_in_day) {
					$('.reserv .inputs').fadeOut();
					$('.reserv .reservacion_en_el_dia').fadeIn();
				}else {
					$('.reserv .inputs').fadeIn();
					$('.reserv .reservacion_en_el_dia').fadeOut();
				}


				$('.add_ ul').not('.socio_auth').html('');
				partners_agregados = [];
				members_agregados = [];
				partners_green_fee = [];
				green_fee_mounts = [];

				if (partners_green_fee.length < 1) {
					$('.reserv .inputs .boton').removeClass('pagar');
				}else {
					$('.reserv .inputs .boton').addClass('pagar');
				}


				$('.edit .horas_reservacion ul').html(li);
				select_hour_reservation();
				$('.edit.reserv .form.cargando').removeClass('cargando');
				setTimeout(function(){
					$('.edit.reserv .form .carga').addClass('none');
				},700);
				// $('.procesar').addClass('active');
			}else {
				// $('.procesar').removeClass('active');
				$('.consultar .mensaje p span').removeClass('warning');
				$('.consultar .mensaje p span').html(mensaje);
				$('.consultar .mensaje').addClass('mostrar');
				setTimeout(function(){
						$('.consultar .mensaje').removeClass('mostrar');
				},5000);
			}
            select2();
            // console.log('/******************************/');

		}
	});

}
/* Datepicker Reservacion */
function date_reservation() {
	$( ".date_reservation" ).datepicker({
	    minDate: new Date(),
	    dateFormat: 'dd-mm-yy',
        defaultDate: reservation_edit.date,
	    beforeShowDay: function( date ) {
			var tipo_dia = beforeShowDay(date);
            if( tipo_dia.dias_bloqueados ) {
				 return [true, "dia_bloqueado", 'Dia Bloqueado'];
			}else if( tipo_dia.sorteable ) {
				 return [true, "sorteable", 'Dia Sorteable'];
			}else if (tipo_dia.torneo) {
				return [true, "torneo", 'Dia de torneo'];
			}else {
				return [true, '', ''];
			}
	    },
		onSelect: function(date){
			input_error($('.horas_reservacion ul'), null, false);
			input_error($('#add_members'), null, false);
			mostrar_minutos(date);
		}
	});
}
