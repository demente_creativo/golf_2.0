<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    |'El following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'El :attribute debe ser accepted.',
    'active_url'           => 'El :attribute no es una URL válida.',
    'after'                => 'El :attribute debe ser una fecha después de :date.',
    'after_or_equal'       => 'El :attribute debe ser una fecha igual a :date.',
    'alpha'                => 'El :attribute puede contener sólo letras.',
    'alpha_dash'           => 'El :attribute puede contener sólo letras, números, y guiones.',
    'alpha_num'            => 'El :attribute puede contener sólo letras y números.',
    'array'                => 'El :attribute debe ser un arreglo.',
    'before'               => 'El :attribute debe ser una fecha antes de :date.',
    'before_or_equal'      => 'El :attribute debe ser una fecha antes o igual que :date.',
    'between'              => [
        'numeric' => 'El :attribute debe estar entre :min y :max.',
        'file'    => 'El :attribute debe estar entre :min y :max kilobytes.',
        'string'  => 'El :attribute debe estar entre :min y :max caracteres.',
        'array'   => 'El :attribute must have between :min y :max items.',
    ],
    'boolean'              => 'El :attribute field debe ser true or false.',
    'confirmed'            => 'El :attribute la confirmación no coincide.',
    'date'                 => 'El :attribute no es una fecha válida.',
    'date_format'          => 'La :attribute no cumple con el formato requerido :format.',
    'different'            => 'El :attribute y :other debe ser different.',
    'digits'               => 'El :attribute debe ser de :digits digitos.',
    'digits_between'       => 'El :attribute debe estar entre :min y :max digitos.',
    'dimensions'           => 'La :attribute no posee las dimensiones requeridas.',
    'distinct'             => 'El :attribute debe ser único.',
    'Elmail'                => 'El :attribute debe ser una dirección de correo válida.',
    'Elxists'               => 'El campo :attribute seleccionado es invalido.',
    'file'                 => 'El :attribute debe ser un archivo.',
    'filled'               => 'El :attribute campo requerido.',
    'image'                => 'El :attribute debe ser an image.',
    'in'                   => 'La selección no es válida :attribute.',
    'in_array'             => 'El :attribute campo no existe en :other.',
    'integer'              => 'El :attribute debe ser un entero.',
    'ip'                   => 'El :attribute debe ser ua IP valida.',
    'json'                 => 'El :attribute debe ser un JSON valido.',
    'max'                  => [
        'numeric' => 'El :attribute puede no ser mayor de :max.',
        'file'    => 'El :attribute puede no ser mayor de :max kilobytes.',
        'string'  => 'El :attribute puede no ser mayor de :max caracteres.',
        'array'   => 'El :attribute no puede contener mas de :max items.',
    ],
    'mimes'                => 'El :attribute debe ser un archivo tipo: :values.',
    'mimetypes'            => 'El :attribute debe ser un archivo tipo: :values.',
    'min'                  => [
        'numeric' => 'El :attribute debe ser menor :min.',
        'file'    => 'El :attribute debe ser menor :min kilobytes.',
        'string'  => 'El :attribute debe ser menor :min characters.',
        'array'   => 'El :attribute puede contener menos de :min items.',
    ],
    'not_in'               => 'El selected :attribute es invalido.',
    'numeric'              => 'El :attribute debe ser a numbero.',
    'present'              => 'El :attribute campo debe estar presente.',
    'regex'                => 'El :attribute el formato es invalido.',
    'required'             => 'El :attribute el campo es requerido.',
    'required_if'          => 'El :attribute el campo es requerido cuando :other es :value.',
    'required_unless'      => 'El :attribute el campo es requerido puede no ser mayor :other es en :values.',
    'required_with'        => 'El :attribute el campo es requerido cuando :values esta presente presente.',
    'required_with_all'    => 'El :attribute el campo es requerido cuando :values esta presente presente.',
    'required_without'     => 'El :attribute el campo es requerido cuando :values no esta presente.',
    'required_without_all' => 'El :attribute el campo es requerido cuando  ninguno de :values estan presentes.',
    'same'                 => 'El :attribute y :other debe coincidir.',
    'size'                 => [
        'numeric' => 'El :attribute debe ser :size.',
        'file'    => 'El :attribute debe ser :size kilobytes.',
        'string'  => 'El :attribute debe ser :size characters.',
        'array'   => 'El :attribute debe contener :size items.',
    ],
    'string'               => 'El :attribute debe ser un string.',
    'timezone'             => 'El :attribute debe ser una zona válida.',
    'unique'               => 'El :attribute ya se ha tomado.',
    'uploaded'             => 'El :attribute falló al abrir.',
    'url'                  => 'El :attribute formato inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    |'El following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
