@extends('layouts.app')
@section('content')
<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Categoria</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('categorias.update', $category->id) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<fieldset>
                    @if ($errors->has('name'))
                        <div class="input_error">
                            <span>{{ $errors->first('name') }}</span>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Nombre</label>
                        <div class="col-md-6">
                            <input id="textinput" name="name" type="text" placeholder="Nombre" class="form-control input-md" value="{{$category->name}}">
                        </div>
                    </div>
					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

						<a href="{{route('categorias.index')}}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
 @endsection
