@extends('layouts.app')
@section('content')
<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Categorías</h2>
        </div>
        @if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
        <div class="table-responsive success tabla">
            <div class="agregar_registro">
                <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Cantidad de participantes</th>
                        <th>Género</th>
                        <th>Handicap min</th>
                        <th>Handicap Max</th>
                        <th class="opciones">Opciones</th>
                        <th>Estatus</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $categories as $category)
                        <tr>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->number_participants }}</td>
                            <td>{{ $category->sex }}</td>
                            <td>{{ $category->handicap_min }}</td>
                            <td>{{ $category->handicap_max }}</td>
                            <td>
                                <a href="{{'/categoria/'.Auth::user()->id.'/'.$category->id}}" class="btn btn-primary singlebutton1 category_show" style="padding-right: 10px;">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                </a>
                                <a href="{{'/categoria/'.Auth::user()->id.'/'.$category->id}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{'/categoria/'.$category->id}}" style="padding-right: 10px;">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </td>
                            <td>
                                <input type="checkbox" {{ ($category->enabled == 1) ? 'checked' : '' }} class="toggle-event" data-item="{{$category->id}}"  data-toggle="toggle" data-route="/categoria/" data-on="Activo" data-off="Inactivo">
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7">

                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Categoría</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" 	method="POST" action="{{route('categorias.store')}}" enctype="multipart/form-data">
						{{ csrf_field() }}
						<fieldset>
							@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Nombre</label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" required pattern="[A-Za-z0-9 ./*,-]{3,50}" maxlength="50" title="de 3 a 50 caracteres">
								</div>
							</div>

							@if ($errors->has('number_participants'))
							<div class="input_error">
								<span>{{ $errors->first('number_participants') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Cantidad de Participantes</label>
								<div class="col-md-6">
									<input id="number_participants" name="number_participants" type="text" placeholder="Cantidad de Participantes" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3" title="de 1 a 3 caracteres">
								</div>
							</div>

							@if ($errors->has('genre'))
							<div class="input_error">
								<span>{{ $errors->first('genre') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Género</label>
								<div class="col-md-6">
									<select required class="form-control input-md" name="genre" id="sex">
										<option value="" disabled selected>Seleccione Género</option>
										<option value="Masculino" >Masculino</option>
										<option value="Femenino" >Femenino</option>
										<option value="Mixto" >Mixto</option>
									</select>
								</div>
							</div>
							@if ($errors->has('handicap_min'))
							<div class="input_error">
								<span>{{ $errors->first('handicap_min') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Handicap Mínimo</label>
								<div class="col-md-6">
									<input id="handicap_min" name="handicap_min" type="text" placeholder="Handicap Mínimo" class="form-control input-md handican" required maxlength="3">
								</div>
							</div>
							@if ($errors->has('handicap_max'))
							<div class="input_error">
								<span>{{ $errors->first('handicap_max') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Handicap Máximo</label>
								<div class="col-md-6">
									<input id="handicap_max" name="handicap_max" type="text" placeholder="Handicap Máximo" class="form-control input-md handican" required maxlength="3">
								</div>
							</div>
							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary ">Enviar</button>
								<button type="button" class="btn btn-primary " data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar esta categoria?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <fieldset>
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detalle de Categoría</h4>
            </div>
            <div class="modal-body form">
                <fieldset>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Nombre</label>
                        <div class="col-md-6">
                            <p id="d_name"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Cantidad de Participantes</label>
                        <div class="col-md-6">
                            <p id="d_number_participants"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Género</label>
                        <div class="col-md-6">
                            <p id="d_genre"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Handicap Mínimo</label>
                        <div class="col-md-6">
                            <p id="d_handicap_min"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="name">Handicap Máximo</label>
                        <div class="col-md-6">
                            <p id="d_handicap_max"></p>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</div>
@endsection
