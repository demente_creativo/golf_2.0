<ul class="maletero">
    @foreach($lockers as $locker)
        <li class="{{ $locker->status }}" id="locker_{{ $locker->id }}">
            <ul class="header" style="{{ ($locker->status == 'Prestamo') ? 'display: none' : '' }}">
                <li>
                    <a href="" onclick="edit_locker({{ $locker->id }}); return false;">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                </li>
                <li>
                    <a href="" onclick="remove_locker({{ $locker->id }}); return false;">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </li>
            </ul>
            <a href="#{{ $locker->id }}" class="aaa {{ ($locker->status != 'Indisponible') ? 'accion' : '' }}">
                <div class="text">
                    <h1> {{ $locker->name }} </h1>
                    <span>{{ ($locker->status == 'Indisponible') ? 'No Disponible' : $locker->status }}</span>
                </div>
            </a>
        </li>
    @endforeach

</ul>
