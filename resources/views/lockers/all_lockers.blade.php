@extends('layouts.app')
@section('content')

<div class="container" id="maletero">
	<div class="tabla_">
		<div class="titulo">
			<!-- <h1 style="text-transform: capitalize;">{{ Carbon\Carbon::now()->formatLocalized('%A, %d de %B del %Y') }}</h1> -->
			<h2 class="">Casilleros </h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="table-responsive success tabla">
			<div class="lockers">
				<div class="arriba">
					<div class="casilleros _1_74 horizontal">
						1 al 74
						<div class="prueba"></div>
					</div>
				</div>

				<div class="entrada">
					<div class="content">
						<div class="casilleros _1042_1063 vertical">
							1042 al 1063
						</div>
						<div class="casilleros _1026_1041 vertical">
							1026 al 1041
						</div>
						<div class="casilleros _1010_1025 vertical">
							1010 al 1025
						</div>
						<div class="casilleros _1064_1079 horizontal">
							1064 al 1079
						</div>
						<div class="clear"></div>
					</div>
					<div class="content">
						<div class="espacio">
							<div class="casilleros _1080_1084 vertical">
								1080 al 1084
							</div>
							<div class="clear"></div>
						</div>
						<div class="espacio">
							<div class="casilleros _903_958 vertical">
								903 al 958
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="centro">
					<div class="casilleros _823_902 vertical">
						823 al 902
					</div>
					<div class="casilleros _743_822 vertical">
						743 al 822
					</div>
					<div class="casilleros _663_742 vertical">
						663 al 742
					</div>
					<div class="casilleros _583_662 vertical">
						583 al 662
					</div>
					<div class="casilleros _498_582 vertical">
						498 al 582
					</div>
					<div class="casilleros _430_497 vertical">
						430 al 497
					</div>
					<div class="casilleros _348_429 vertical">
						348 al 429
					</div>
					<div class="casilleros _265_347 vertical">
						265 al 347
					</div>
					<div class="clear"></div>

				</div>
				<div class="salida">
					<div class="casilleros _215_264 vertical">
						215 al 264
					</div>
					<div class="casilleros _145_214 vertical">
						145 al 214
					</div>
					<div class="casilleros _75_144 vertical">
						75 al 144
					</div>

					<div class="clear"></div>
				</div>
				<div class="final">
					<div class="abajo">
						<div class="casilleros _959_998 horizontal">
							959 al 998
						</div>
						<div class="clear"></div>

					</div>
					<div class="derecha">
						<div class="casilleros _999_1009 vertical">
							999 al 1009
						</div>

						<div class="clear"></div>

					</div>

					<div class="clear"></div>

				</div>
				<div class="clear"></div>


			</div>
		</div>
	</div>

</div>


	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Casillero</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" action="{{ route('casilleros.store', array('user_id' => Auth::user()->id)) }}" method="POST" id="form_lockers_create">
						{{ csrf_field() }}
						<fieldset>

							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{ $errors->first('name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Número</label>
								<div class="col-md-6">
									<input id="textinput" name="name" type="text" placeholder="Número" class="form-control input-md" required="">
								</div>
							</div>

							@if ($errors->has('status'))
							<div class="input_error">
								<span>{{ $errors->first('status') }}</span>
							</div>
							@endif

							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Estado</label>
								<div class="col-md-6">
									<select required name="status" class="form-control input-md" required>
										<option value="Disponible" selected>Disponible</option>
										<option value="Indisponible">No Disponible</option>
									</select>
								</div>
							</div>


							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar este casillero?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST" id="form_locker_delete">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modaledit" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Editar Casillero</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" action="{{ route('casilleros.store', array('user_id' => Auth::user()->id)) }}" method="POST" id="form_lockers_edit">
						{{ csrf_field() }}
						{{ method_field('PUT') }}

						<fieldset>
							<!-- Form Name -->
							




							<!-- Button -->
						</fieldset>
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


<div class="capa_maletero none">
	<div class="cerrar_capa_maletero"></div>
	<form action="#" method="POST" id="form_maletero_prestamo">
		{{ csrf_field() }}
		<div class="datos">

		</div>
	</form>
</div>
<script type="text/javascript">
	var autocomplete_socios = '{{ route('socios.autocomplete') }}';
	var lockers = {!! $lockers !!};
</script>
@endsection
