@extends('layouts.app')
@section('content')
<div class="container">



    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Horarios de salida</h2>
        </div>
        @if (session('status'))
            <div class="alerta">
                <div class="alert alert-{{ session('type') }}">
                    {{session('status')}}.
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
            </div>
        @endif

        <div class="tabla_head">
            <div class="tabla_divider add">
                <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
            </div>
            <div class="tabla_divider search">
                <div class="buscar">
                    <!-- <form class="" action="" method="post">
                        <button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
                        <input id="search_category" name="q" type="text" placeholder="" class="form-control input-md">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </form> -->
                </div>
            </div>
        </div>
        <div class="table-responsive success tabla">
            {!! $table_start_times->render() !!}

        </div>
    </div>


    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar horario de salida</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" action="{{ route('horario_salidas.store') }}" method="POST">
                        {{ csrf_field() }}
                        <fieldset>
                            <!-- Form Name -->
                            

                            @if ($errors->has('start_time'))
                                <div class="input_error">
                                    <span>{{ $errors->first('start_time') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Hora de inicio</label>
                                <div class="col-md-6">
                                    <input type="text" class="time_start_time form-control input-md" placeholder="Hora de inicio:" name="start_time" id="start_start_time" date="" value="">
                                </div>
                            </div>

                            @if ($errors->has('end_time'))
                                <div class="input_error">
                                    <span>{{ $errors->first('end_time') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Hora de salida</label>
                                <div class="col-md-6">
                                    <input type="text" class="form-control input-md" placeholder="Hora de salida:" name="end_time" id="end_start_time" date="" value="">
                                </div>
                            </div>



                            <!-- Button -->
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar este horario de salida?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <fieldset>
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
