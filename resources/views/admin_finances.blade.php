@extends('layouts.app')

@section('content')


<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Usuario Admin Finances</h3>
			@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
			@endif
		</div>
	</div>
</div>@endsection
