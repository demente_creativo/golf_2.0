@extends('layouts.app')

@section('content')
<div id="contenido">
				
		<div class="reservas division">
			<center>
				<div class="division_cont">
					<div class="estilo panel panel-default">					  
					    Reservar
					</div>
					<div class="reservas-cont">
						<div class="contenidoSeleccion" id="contenidoSeleccion">
							
							<div class="detalle-reservas-cont" id="contDetalle">
						<div class="col-md-5 detalle">
							<div class="item">
								<center>
									<div class="titulo">
										<div class="icon">
											<img src="../img/icons/descripcion.png" alt="" />
											<div class="text">
												<span class="tot_">Fecha</span>
											</div>
											<input type="datetimepicker" class="datetimepicker1 textbox" placeholder="Fecha:" name="">
											<input type="datetimepicker" class="datetimepicker2 textbox" placeholder="Hora:" name="">
										</div>
										<div class="texto" id="detalle">

										</div>
										<div id="script">

										</div>


									</div>
								</center>
							</div>
						</div>
						<div class="col-md-7 total">
							<div class="total_">
								<div class="icono">
									<img src="../img/icons/factura.png" alt="" />

								</div>
								<div class="texto">
									<div class="text">
										<span>Sub-total:</span> <span><b id="sub-total"></b> Bsf</span>
									</div>
									<div class="text">
										<span>Iva:</span> <span><b id="iva"></b> Bsf</span>
									</div>
									<div class="text">
										<span class="tot_">total:</span> <span><b id="total"></b> Bsf</span>
									</div>
								</div>
							</div>
							<form action="" id="form_reservar">
								<div class="muestra" id="muestra">
									
										<div class="muestra_" id="muestra_">
											<div class="titulo">
												<div class="icono">
													<img src="../img/icons/descripcion.png" alt="" />
												</div>
												<div class="text">
												</div>
											</div>
											<table>
												<thead>
													<tr>
														<th>
															<img src="../img/icons/fecha.png" alt="" />
														</th>
														<th>
															<img src="../img/icons/reloj.png" alt="" />
														</th>
														<th>
															<img src="../img/icons/factura.png" alt="" />
														</th>
													</tr>
												</thead>
												<tbody id="detalleCompra">
													
														<tr id="">
															<td><input type="text" name="fecha" class="fech" id="fecha" value="" readonly="readonly"></td>
															<td><input type="text" name="hora[]" class="hr" value="" readonly="readonly"></td>
															<td class="precio">
																<input type="text" name="precio[]" class="tl" value="" readonly="readonly">
																<input type="hidden" name="nombre[]" class="nmb" id="nombre" value="" readonly="readonly">
																<input type="hidden" name="idDepartamento[]" class="cd" id="idDepartamento" value="" readonly="readonly">
																<input type="hidden" name="idCategoria[]" class="ct" value="" readonly="readonly">
																<input type="hidden" id="idUser" name="idUser" value="" readonly="readonly">
															</td>
														</tr>
													
												</tbody>
											</table>
										</div>
									

								</div>
						</form>
						</div>
						</div>
						<form method="POST" action="inicio">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<button style="float: left;" type="submit" class="btn btn-primary">Volver</button>
						</form>
			</center>


		</div>
		<div>
			<h1>Horario no disponible</h1>
		</div>
@endsection