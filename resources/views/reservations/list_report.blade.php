@extends('layouts.app')

@section('content')

<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Listado de Reservaciones</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="tabla_head head_reportes">
			<form class="" action="{{route('reservaciones.list_reservations', ['user_id' => Auth::user()->id])}}" method="get">
				<div class="inputs">
					<label class="" for="status">Status</label>
					<select id="status" name="status" class="form-control input-md" required>
						<option value="null" {{ (empty($input) || (empty($input['status'])) || ($input['status'] == 'null')) ? 'selected' : '' }}>status</option>
						<option value="En proceso" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'En proceso')) ? 'selected' : '' }}>En proceso</option>
						<option value="Sorteable" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Sorteable')) ? 'selected' : '' }}>Sorteable</option>
						<option value="Aprobada" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Aprobada')) ? 'selected' : '' }}>Aprobada</option>
						<option value="Cancelada" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Cancelada')) ? 'selected' : '' }}>Cancelada</option>
						<option value="Cerrada" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Cerrada')) ? 'selected' : '' }}>Cerrada</option>
						<option value="Finalizada" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Finalizada')) ? 'selected' : '' }}>Finalizada</option>
					</select>
				</div>

				@if ($errors->has('start_date'))
				<div class="input_error">
					<span>{{ $errors->first('start_date') }}</span>
				</div>
				@endif
				<div class="inputs">
					<label class="" for="start_date">Fecha de inicio</label>
					<input type="text" class="datepicker_alls date report form-control input-md" placeholder="Fecha de inicio:" name="start_date" id="start_date" date="" value="{{(isset($input['start_date'])) ? $input['start_date'] : ''}}">

				</div>

				@if ($errors->has('end_date'))
				<div class="input_error">
					<span>{{ $errors->first('end_date') }}</span>
				</div>
				@endif
				<div class="inputs">
					<label class="" for="end_date">Fecha de cierre</label>
					<input type="text" class="end_datepciker_alls date report form-control input-md" placeholder="Fecha de cierre:" name="end_date" id="end_date" date="" value="{{(isset($input['end_date'])) ? $input['end_date'] : ''}}">
				</div>

				<div class="boton">
					<button type="submit" class="btn btn-primary singlebutton1">
						<span>Buscar</span>
						<i class="glyphicon glyphicon-search" aria-hidden="true"></i>
					</button>
				</div>
			</form>
		</div>
		<div class="margin_none table-responsive success tabla report {{ ((Auth::user()->role_id == 5) | (Auth::user()->role_id == 2)) ? 'menos_2' : '' }}">
			<table class="table">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Hora</th>
						<th>Creado por</th>
						<th>Participantes</th>
						<th>Estatus</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $reservations as $reservation)
						<tr>
							<td>{{ $reservation->date }}</td>
							<td>{{ $reservation->start_time }}</td>
							<td>{{ $reservation->user->email }}</td>
							<td>
								<strong>Socios</strong><br>
								@foreach($reservation->members as $member)
									{{$member->name}} {{$member->last_name}} C.I: {{$member->identity_card}} <br>
								@endforeach
								@if( count($reservation->partners) > 0 )
									<strong>Invitados</strong><br>
									@foreach($reservation->partners as $partner)
										{{$partner->name}} {{$partner->last_name}} C.I: {{$partner->identity_card}}<br>
									@endforeach
								@endif
							</td>
							<td>{{$reservation->status}}</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">

						</td>
					</tr>
				</tfoot>

			</table>

		</div>
		@if(count($reservations) >= 1)
			<div class="bajar_reporte">
				<a id="crear_reporte_pdf" type="button" href="{{route('reservaciones.pdf_report_create_reservation',['user_id' => Auth::user()->id])}}">
					<span class="icon">
						<img src="{{asset('img/icons/pdf.png')}}" alt="">
					</span>
				</a>
				<a id="crear_reporte_xls" type="button" href="{{route('reservaciones.xls_report_create_reservation',['user_id' => Auth::user()->id])}}">
					<span class="icon">
						<img src="{{asset('img/icons/xls.png')}}" alt="">
					</span>
				</a>
			</div>
		@endif
	</div>

</div>

@if( Auth::user()->role_id == 2 )
@endif


@endsection
