@extends('layouts.app')

@section('content')


<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Reservación</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('reservaciones.update', array('user_id' => Auth::user()->id, 'reservation_id' => $reservation->id)) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}


				<fieldset>
					<!-- Form Name -->
					

                    @if ($errors->has('receipt_payment'))
					<div class="input_error">
						<span>{{ $errors->first('receipt_payment') }}</span>
					</div>
					@endif
					<div class="form-group consultar">
						<label class="col-4 col-md-center control-label" for="receipt_payment">Seleccione Fecha</label>
						<div class="col-6 col-md-center" >
							<input type="hidden" name="reservation_id" value="{{ $reservation->id }}" id="reservation_id">
                            <input type="date_reservation" value="{{ $reservation->date }}" date="{{ $reservation->date }}" class="date_reservation textbox" placeholder="Fecha:" name="date" id="date">
            				<input type="time_reservation" value="{{ $reservation->start_time }}" starttime="{{ $reservation->start_time }}" class="time_reservation textbox" placeholder="Hora:" name="start_time" id="start_time">
						</div>
						<div class="mensaje">
							<p>
								<span>

								</span>
							</p>
						</div>
                        <div class="consultar_fecha">
							<a href="{{ route('check_reservation', array( 'user_id', Auth::user()->id ) ) }}" id="consultar" class="btn btn-primary singlebutton1">
								Consultar
							</a>
                        </div>
					</div>


					<div class="procesar active">
						<div class="add_invitados add_">
							<div class="form-group">
								<label class="col-4 col-md-center control-label" for="receipt_payment">Buscar Invitados</label>
								<div class="col-6 col-md-center" >
									<input id="add_partners" type="text" class="search" placeholder="Buscar Invitados" name="search_invite" ruta="{{ route('invitados.autocomplete', array( 'user_id', Auth::user()->id ) ) }}">
								</div>
							</div>
							<ul class="">
								@foreach( $reservation->partners as $partner )

									<li id="partner_{{ $partner->id }}">
										<input type="hidden" name="partner_id[]" value="{{ $partner->id }}">
										<div>{{ $partner->name }} {{ $partner->last_name }}</div>
										<div>C.I: {{ $partner->identity_card }}</div>
										<div class="cerrar" onclick="cerrar_add('#partner_{{ $partner->id }}'); return false;">
											<a href="#" class="cerrar_add">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
											</a>
										</div>
									</li>
								@endforeach
							</ul>
						</div>

						<div class="add_socios add_">
							<div class="form-group">
								<label class="col-4 col-md-center control-label" for="receipt_payment">Buscar Socios</label>
								<div class="col-6 col-md-center" >
									<input id="add_members" type="text" class="search" placeholder="Buscar Socios" name="search_socio" ruta="{{ route('socios.autocomplete', array( 'user_id', Auth::user()->id ) ) }}">
								</div>
							</div>
							<ul class="">
								@foreach( $reservation->members as $member )
									<li id="member_{{ $member->id }}">
										<input type="hidden" name="member_id[]" value="{{ $member->id }}">
										<div>{{ $member->name }} {{ $member->last_name }}</div>
										<div>C.I: {{ $member->identity_card }}</div>
										<div class="cerrar" onclick="cerrar_add('#member_{{ $member->id }}'); return false;">
											<a href="#" class="cerrar_add">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
											</a>
										</div>
									</li>
								@endforeach
							</ul>
						</div>

						<div class="green_fee {{ ( !empty($reservation_definition->green_fee) ) ? 'active' : '' }}">
							@if ($errors->has('total_green_fee'))
								<div class="input_error">
									<span>{{ $errors->first('total_green_fee') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-6 control-label" for="receipt_payment">Green Fee: </label>
								<div class="col-md-3" >
									<span>Bsf</span> <input id="green_fee" green-fee="{{ $reservation_definition->green_fee }}" readonly type="text" class="search form-control input-md money" name="total_green_fee" value="{{ $reservation->total_green_fee }}" >
									<input type="hidden" name="reservation_definition_id" value="{{ $reservation_definition->id }}">
								</div>
							</div>
						</div>

						@if ($errors->has('status'))
							<div class="input_error">
								<span>{{ $errors->first('status') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-4 col-md-center control-label" for="textinput">Estado
							</label>
							<div class="col-6 col-md-center">
								<select name="status" class="form-control input-md">
									<option value="" disabled selected>Seleccione un estado</option>
									<option value="En proceso" @if($reservation->status == 'En proceso') {{  'selected' }} @endif >En proceso</option>
									<option value="Sorteable" @if($reservation->status == 'Sorteable') {{ 'selected' }} @endif >Sorteable</option>
									<option value="Aprobada" @if($reservation->status == 'Aprobada') {{ 'selected' }} @endif >Aprobada</option>
									<option value="Cancelada" @if($reservation->status == 'Cancelada') {{ 'selected' }} @endif >Cancelada</option>
									<option value="Cerrada" @if($reservation->status == 'Cerrada') {{ 'selected' }} @endif >Cerrada</option>
									<option value="Finalizada" @if($reservation->status == 'Finalizada') {{ 'selected' }} @endif >Finalizada</option>
								</select>
							</div>
						</div>


						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Procesar</button>
							<a href="{{route('reservaciones.index', array( 'user_id' => Auth::user()->id ))}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</div>



				</fieldset>
			</form>
		</div>
	</div>
</div>@endsection
