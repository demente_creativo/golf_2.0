@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Administradores</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<div class="table-responsive success tabla">
		<div class="agregar_registro">
                 <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
             </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Correo</th>
						<th>Rol</th>
                        <th class="opciones">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $users as $user)
                        <tr>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email}}</td>
							<td>{{ $user->role->name}}</td>
                            <td>
								<a href="{{route('administradores.show', array( $user->id, 'user_id' => Auth::user()->id ))}}" class="btn btn-primary singlebutton1 admin_show" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
								</a>
                                <a href="{{ route('administradores.edit', array( $user->id, 'user_id' => Auth::user()->id )) }}" class="btn btn-primary singlebutton1">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('administradores.destroy', array( $user->id, 'user_id' => Auth::user()->id ))}}" style="padding-right: 10px;">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4">

                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
	</div>
	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Administrador</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" action="{{ route('administradores.store') }}" method="POST">
						{{ csrf_field() }}

						<fieldset>
							<!-- Form Name -->
							

							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{ $errors->first('name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
								</div>
							</div>

							@if ($errors->has('email'))
								<div class="input_error">
									<span>{{ $errors->first('email') }}</span>
								</div>
							@endif

							<div class="form-group">
								<label class="col-md-4 control-label" for="email">Correo</label>
								<div class="col-md-6 inpt">
									<input id="email" name="email" type="email" placeholder="Correo" class="form-control input-md chequear_correo" checkear="{{ route('administradores.validate_email_user') }}" required pattern=".{3,50}" title="de 3 a 50 caracteres">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
											<div class="mensaje">
												<p>
												</p>
											</div>
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>

							@if ($errors->has('password'))
								<div class="input_error">
									<span>{{ $errors->first('password') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="password">Contraseña</label>
								<div class="col-md-6">
									<input id="password" name="password" type="password" placeholder="Password" class="form-control input-md" pattern=".{6,25}" title="de 6 a 25 caracteres" required>
								</div>
							</div>

							@if ($errors->has('password_confirmation'))
								<div class="input_error">
									<span>{{ $errors->first('password_confirmation') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="password_confirmation">Confirmar Contraseña</label>
								<div class="col-md-6">
									<input id="password_confirmation" name="password_confirmation" type="password" placeholder="Confirmación Password" class="form-control input-md" required>
								</div>
							</div>

							@if ($errors->has('role_id'))
							<div class="input_error">
								<span>{{ $errors->first('role_id') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Roles</label>
								<div class="col-md-6">
									<select required name="role_id" class="form-control input-md" required>
										<option value="" disabled selected>Seleccione Rol</option>
										@foreach($roles as $role)
											@if ( $role->id != 6 )
												<option value="{{ $role->id }}">{{ $role->name }}</option>
											@endif
										@endforeach
									</select>
								</div>
							</div>



							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Administrador</h4>
				</div>
					<div class="modal-body form">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<p id="d_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Correo</label>
								<div class="col-md-6">
									<p id="d_email"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Rol</label>
								<div class="col-md-6">
									<p id="d_role"></p>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar este socio?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
