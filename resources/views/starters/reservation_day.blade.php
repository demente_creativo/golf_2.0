<script type="text/javascript">
	var reservation_day = {!! $reservations !!};
	console.log(reservation_day);
</script>

@php

use Carbon\Carbon;
$fecha = new Carbon(date('Y-m-d'));
@endphp
@extends('layouts.app')

@section('content')
<div class="error_draw">
	<p></p>
</div>
<div class="container" id="starters">
	<div class="tabla_" style=" position: static;">
		<div class="titulo">
			<h1 style="text-transform: capitalize;" id="fecha">{{ $fecha->formatLocalized('%A, %d de %B del %Y') }}</h1>
			<h2 class="">Reservaciones </h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="table-responsive success tabla {{ (Auth::user()->role_id == 6) ? 'menos_2' : '' }}" style="overflow: visible; position: static;">
			<div class="form-group input_date">
				<label class="control-label" for="textinput">Buscar Fecha</label>
				<div class="">
					<input type="text" class="date form-control input-md" placeholder="Buscar Fecha:" name="" id="date_reservaciones_del_dia_starter" date="">
				</div>
			</div>

            <div class="starter">
				<ul id="orden_salida" class="listas_starter">
					<h3>Partidas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Aprobada' || $reservation->status == 'Pagada')
								<li class="li_principal active" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa"></div>
											<select class="" name="">
												@php
												$start_times = Carbon::parse('2017-04-07 06:00 AM');
												$end_times = Carbon::parse('2017-04-07 05:50 PM');

												for ( $i = 0; $i < 100; $i++){
													if ($i != 0) {
														if ($start_times == $end_times) {
															break;
														}else {
															$start_times->addMinute(10);
														}
													}
													echo '<option value="'.$start_times->format('h:i A').'" '.( ($start_times->format('h:i A') == $reservation->start_time) ? 'selected' : '' ).'>'.$start_times->format('h:i A').'</option>';
												}
												@endphp
											</select>
											<a href="#" class="icon">
												<i class="fa fa-remove"></i>
											</a>
										</div>
										<a class="icon add_participant" reservation-id="{{$reservation->id}}">
											<i class="fa fa-plus"></i>
										</a>
										<a class="icon check starter_accion" type="salida">
											<i class="fa fa-check"></i>
										</a>
										<a class="icon cancel starter_accion" type="cancelar">
											<i class="fa fa-remove"></i>
										</a>
										<div class="clear"></div>
									</div>
									<ul class="conexion">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<?php
												$num_participantes = count($reservation->members) +  count($reservation->partners);
												$height = ($num_participantes * 26) + 10;
												?>
												<div class="drop" style="height: {{$height}}px">
													<i class="fa fa-arrows" aria-hidden="true"></i>
												</div>
												<ul class="participantes conexion_participantes" id="{{$reservation->id}}">
													@foreach( $reservation->members as $member )
														<li id="{{$member->id}}" tipo="member">
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
															<div class="icon">
																<i class="fa fa-arrows"></i>
															</div>
															<a href="#" class="eliminar_participant">
																<div class="icon">
																	<i class="fa fa-remove"></i>
																</div>
															</a>
															<div class="clear"></div>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li id="{{$partner->id}}" tipo="partner" class="{{($partner->payed === 0) ? 'not_payed' : ''}}">
															<span> {{ $partner->name }} {{ $partner->last_name }} <b>{{($partner->payed === 0) ? '(Sin Pago)' : ''}}</b> <strong>(I)</strong></span>
															<div class="icon">
																<i class="fa fa-arrows"></i>
															</div>
															<a href="#" class="eliminar_participant">
																<div class="icon">
																	<i class="fa fa-remove"></i>
																</div>
															</a>
															<div class="clear"></div>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
						@endforeach
					</div>
				</ul>
				<ul id="orden_finalizada" class="listas_starter">
					<h3>Salidas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Finalizada')
								<li class="li_principal" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa no_accion"></div>
											<select class="no_accion" name="" >
												<option value="">{{ $reservation->start_time }}</option>
											</select>
											<a href="#" class="icon">
												<i class="fa fa-remove"></i>
											</a>
										</div>
										<div class="clear"></div>
									</div>
									<ul class="conexion">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<ul class="participantes conexion_participantes">
													@foreach( $reservation->members as $member )
														<li>
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li>
															<span> {{ $partner->name }} {{ $partner->last_name }} <strong>(I)</strong></span>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
						@endforeach

					</div>
				</ul>
				<ul id="orden_canceladas" class="listas_starter">
					<h3>Canceladas</h3>
					<div class="reservas">
						@foreach( $reservations as $reservation )
							@if ($reservation->status == 'Cancelada')
								<li class="li_principal" hora="{{ $reservation->start_time }}">
									<div class="head">
										<a href="" class="activar_starter" onclick="">
											<span class="icon dropdown">
												<i class="glyphicon glyphicon-triangle-bottom"></i>
											</span>
										</a>
										<div class="select">
											<div class="capa no_accion"></div>
											<select class="no_accion" name="" >
												<option value="">{{ $reservation->start_time }}</option>
											</select>
											<a href="#" class="icon">
												<i class="fa fa-remove"></i>
											</a>
										</div>
										<div class="clear"></div>
									</div>
									<ul class="conexion">
										<li class="personas" id="{{$reservation->id}}">
											<div class="datos">
												<ul class="participantes conexion_participantes">
													@foreach( $reservation->members as $member )
														<li>
															<strong><span>{{ $member->name }} {{ $member->last_name }} (S)</span></strong>
														</li>
													@endforeach
													@foreach( $reservation->partners as $partner )
														<li>
															<span> {{ $partner->name }} {{ $partner->last_name }} <strong>(I)</strong></span>
														</li>
													@endforeach
												</ul>
												<div class="clear"></div>
											</div>
										</li>
									</ul>
								</li>
							@endif
						@endforeach
					</div>
				</ul>
				<div class="clear"></div>
				<div class="mensaje" style="display: {{ (count($reservations) == 0) ? 'block' : '' }}">
					<p>{{ (count($reservations) == 0) ? 'No hay reservaciones para este dia' : '' }}</p>
				</div>
            </div>
		</div>
	</div>
</div>
<div class="black_capa none">
	<div class="cerrar_black_capa"></div>
	<form action="{{ route('reservaciones_del_dia.status', array( 'start_id' => Auth::user()->id )) }}" method="POST" id="form_starter">
		{{ csrf_field() }}
		<div class="datos">
		</div>
	</form>
</div>
<div class="cambiar_participantes none">
	<div class="contenedor">
		<h1> Seleccione intercambio </h1>
		<ul>
		</ul>
		<div class="clear"></div>
		<div class="form-group">
			<label for="" class="control-label">Comentario</label>
			<textarea name="name" rows="8" cols="80" class="form-control input-md" id="text_comments"></textarea>
		</div>
		<div class="boton">
			<button type="button" class="omitir btn btn-primary singlebutton1">
				Solo mover
			</button>
			<button type="button" class="cancelar btn btn-primary singlebutton1">
				cancelar
			</button>
		</div>
	</div>
</div>
<div class="manager_participantes none" id="agregar_participantes">
	<div class="contenedor">
		<h1> Agregar socio </h1>
		<div class="clear"></div>
		<form class="" action="" method="post" id="form_add_participant">
			{{ csrf_field() }}
			<input type="hidden" name="reservation_id" value="" id="add_reservation_id">
			<input type="hidden" name="type_id" value="1" id="tipo_participante">
			<!-- <div class="form-group">
				<label for="" class="control-label">Selecionar participante</label>
				<select class="" name="type_id" id="tipo_participante">
					<option disabled selected>Selecionar tipo de participante</option>
					<option value="1">Socio</option>
					<option value="2">Invitado</option>
				</select>
				<div class="error">
					<p>Debe seleccionar un tipo de participante</p>
				</div>
			</div> -->
			<div class="form-group" id="select_participant">
				<label for="" class="control-label">Selecionar socio</label>
				<select class="" name="participant_id" id="participante_id">
					<!-- <option disabled selected>Selecionar socio</option>
					@foreach($members as $member)
						<option value="{{$member->id}}">{{$member->name}} {{$member->last_name}}</option>
					@endforeach -->
				</select>
				<div class="error">
					<p>Debe seleccionar un socio</p>
				</div>
			</div>
			<div class="clear"></div>
			<div class="form-group">
				<label for="" class="control-label">Comentario</label>
				<textarea name="comment" rows="8" cols="80" class="form-control input-md"></textarea>
			</div>
			<div class="boton">
				<button type="submit" class="btn btn-primary singlebutton1">
					Aceptar
				</button>
				<button type="button" class="cancelar btn btn-primary singlebutton1">
					cancelar
				</button>
			</div>
		</form>
	</div>
</div>
<div class="manager_participantes none" id="eliminar_participantes">
	<div class="contenedor">
		<h1> Eliminar Participante </h1>
		<div class="clear"></div>
		<form class="" action="" method="post" id="form_delete_participant">
			<input type="hidden" name="_method" value="DELETE">
			<input type="hidden" name="reservation_id" value="" id="reservation_id">
			<input type="hidden" name="participant_id" value="" id="participant_id">
			<input type="hidden" name="type_id" value="" id="type_id">
			<div class="form-group">
				<label for="" class="control-label">Comentario</label>
				<textarea name="comment" rows="8" cols="80" class="form-control input-md"></textarea>
			</div>
			<div class="boton">
				<button type="submit" class="btn btn-primary singlebutton1">
					Aceptar
				</button>
				<button type="button" class="cancelar btn btn-primary singlebutton1">
					cancelar
				</button>
			</div>
		</form>
	</div>
</div>

<div class="comment_starter none">
	<div class="contenedor">
		<h1> Agregar Comentario </h1>
		<div class="clear"></div>
		<div class="form-group">
			<label for="" class="control-label">Comentario</label>
			<textarea name="name" rows="8" cols="80" class="form-control input-md" id="comment_hora"></textarea>
		</div>
		<div class="boton">
			<button type="button" class="aceptar btn btn-primary singlebutton1">
				Aceptar
			</button>
			<button type="button" class="cancelar btn btn-primary singlebutton1">
				cancelar
			</button>
		</div>
	</div>
</div>
<script type="text/javascript">
	var autocomplete_socios = '{{ route('socios.autocomplete') }}';
	var members = {!! $members !!};
</script>
@endsection
@section('script')
	<script src="{{ asset('js/script_starter.js') }}" charset="utf-8"></script>
@endsection
