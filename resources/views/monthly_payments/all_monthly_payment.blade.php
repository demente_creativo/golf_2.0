@extends('layouts.app')
@section('content')
<div class="container">
    <div class="titulo">
        <h2 class="">Mensualidades</h2>
    </div>
    <div class="tabla_">
        <div class="table-responsive success tabla">
            <div class="agregar_registro">
                <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
            </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>Estudiante</th>
                        <th>Monto</th>
                        <th>Periodo</th>
                        <th>Fecha de Pago</th>
                        <th width="100px">Editar</th>
                        <th width="100px">Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($monthly_payments as $mp)
                        <tr id="{{$mp->id}}">
                            <td>Estudiante</td>
                            <td>{{$mp->rode}}</td>
                            <td>{{$mp->period->name}}</td>
                            <td>{{$mp->date_payment}}</td>
                            <td>
                                <a href="{{ route('edit_monthly_payment', $mp->id) }}" class="btn btn-primary singlebutton1">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                            </td>
                            <td>
                            <?php
                            echo "<td onclick= \"modal_activate('".
                            route( "destroy_monthly_payment", $mp->id ).
                            "' , '#modaldelete')\" >";
                            ?>
                            <button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">

                        </td>
                    </tr>
                </tfoot>

            </table>
        </div>

    </div>


<!--MODAL AGREGAR MENSUALIDAD-->
    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Mensualidad</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" action="{{ route('store_monthly_payment') }}" method="POST">
                        {{ csrf_field() }}
                        <fieldset>
                            <!-- Form Name -->
                            

                            @if ($errors->has('number_receipt'))
                                <div class="input_error">
                                    <span>{{ $errors->first('number_receipt') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Numero de Recibo</label>
                                <div class="col-md-4">
                                    <input id="textinput" name="number_receipt" type="number" placeholder="Numero de Recibo" class="form-control input-md">
                                </div>
                            </div>

                            @if ($errors->has('rode'))
                                <div class="input_error">
                                    <span>{{ $errors->first('rode') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Monto</label>
                                <div class="col-md-4">
                                    <input id="textinput" name="rode" type="number" placeholder="Monto" class="form-control input-md">
                                </div>
                            </div>

                            @if ($errors->has('observation'))
                                <div class="input_error">
                                    <span>{{ $errors->first('observation') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Observacion</label>
                                <div class="col-md-4">
                                    <input id="textinput" name="observation" type="text" placeholder="Observacion" class="form-control input-md">
                                </div>
                            </div>

                            @if ($errors->has('date_payment'))
                                <div class="input_error">
                                    <span>{{ $errors->first('date_payment') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Fecha de Pago
                                </label>
                                    <div class="col-md-4">
                                        <input type="datetimepicker" class="datetimepicker1 textbox" name="date_payment" >
                                    </div>
                            </div>


                            @if ($errors->has('method_payment'))
                                <div class="input_error">
                                    <span>{{ $errors->first('method_payment') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Metodo de Pago</label>
                                <div class="col-md-4">
                                    <input id="textinput" name="method_payment" type="text" placeholder="Metodo de Pago" class="form-control input-md">
                                </div>
                            </div>



                            @if ($errors->has('period_id'))
                            <div class="input_error">
                                <span>{{ $errors->first('period_id') }}</span>
                            </div>
                            @endif

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Periodo</label>
                                <div class="col-md-4">
                                    <select name="period_id" class="form-control input-md">
                                        <option value="" disabled selected>Seleccione Periodo</option>
                                            @foreach ($periods as $period)
                                                <option value="{{$period->id}}" >{{$period->name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>



                            <!-- Button -->
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!--MODAL PARA ELIMINAR-->
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar esta Mensualidad?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <fieldset>
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
