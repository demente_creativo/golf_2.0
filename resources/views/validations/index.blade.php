@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Opciones de Reserva</h3>
					@if (session('validations'))
						<div class="alerta">
							<div class="alert alert-{{ session('type') }}">
								@foreach (session('validations') as $m)
									{{$m}}<br/>
								@endforeach
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
						</div>
					@endif
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<div class="form">
				<form class="form-horizontal" action="/opciones" method="POST">
					{{ csrf_field() }}
					{{ method_field('POST') }}

					<fieldset>
						<!-- Form Name -->


						<div class="form-group">
                            <div class="col-md-6">
                                
                                <label class="col-md-12  text-center" for="old">Reserva Directa</label>
                                <div class="col-md-12">
                                    
                                    <div class="col-md-6">
										<label class="col-md-4 control-label" for="">Min</label>
                                      <div class="col-md-8">
										  <input id="" name="min_participants_direct" type="number" value="{{$validation->min_participants_direct}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
									  </div>
                                    </div>
									<div class="col-md-6">
									  <label class="col-md-4 control-label" for="">Máx</label>
									  <div class="col-md-8">
										  <input id="" name="max_participants_direct" type="number" value="{{$validation->max_participants_direct}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
									  </div>
                                      
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                
                                <label class="col-md-12  text-center" for="old">Reserva Sorteable</label>
                                <div class="col-md-12">
									 <div class="col-md-6">
										<label class="col-md-4 control-label" for="">Min</label>
                                      <div class="col-md-8">
										  <input id="" name="min_participants_draw" type="number" value="{{$validation->min_participants_draw}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
									  </div>
                                    </div>
                                    <div class="col-md-6">
									  <label class="col-md-4 control-label" data-toggle="tooltip" title="Hooray!" for="">Máx</label>
									  <div class="col-md-8">
										  <input id="" name="max_participants_draws" type="number" value="{{$validation->max_participants_draws}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
									  </div>
                                      
                                    </div>

                                </div>
                                
                            </div>

						</div>


                        <div class="form-group">
							<div class="col-md-6">
								<label class="col-md-6 control-label" for="old">Invitados por socio</label>
								<div class="col-md-6">
									<input id="" name="max_partners_member" type="number" value="{{$validation->max_partners_member}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
								</div>
							</div>
							<div class="col-md-6">
								<label class="col-md-6 control-label" for="old">Socios por partida</label>
								<div class="col-md-6">
									<input id="" name="min_member_reservation" type="number" value="{{$validation->min_member_reservation}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
								</div>
							</div>

						</div>

						<div class="form-group">
							<div class="col-md-6">
								<label class="col-md-6 control-label" for="old">Reservaciones por mes (Invitado)</label>
								<div class="col-md-6">
									<input id="" name="max_reservation_month_partner" type="number" value="{{$validation->max_reservation_month_partner}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
								</div>
							</div>
							<div class="col-md-6">
								<label class="col-md-6 control-label" for="old">Reservaciones por día (Socio)</label>
								<div class="col-md-6">
									<input id="" name="max_reservation_day_member" type="number" value="{{$validation->max_reservation_day_member}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
								</div>
							</div>

						</div>
	
						<div class="form-group">
							<div class="row">
								
								<div class="col-md-8 center-block" style= "float:none;">
									<label class="col-md-4 col-md-offset-1 control-label" for="old">Días habilitados para reserva</label>
									<div class="col-md-5">
										<input id="" name="enabled_reservation_days" type="number" value="{{$validation->enabled_reservation_days}}" class="form-control input-md" autocomplete="off" minlength="1" maxlength="2" required>
									</div>
								</div>
							</div>
							
						</div>
						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="/"  class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
