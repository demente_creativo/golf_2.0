@extends('layouts.app')

@section('content')


<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Agregar Reservación</h3>
			@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('reservaciones.store', array( 'member_id' => Auth::user()->member->id ) ) }}" method="POST">
				{{ csrf_field() }}

				<fieldset>
					<!-- Form Name -->
					

                    @if ($errors->has('receipt_payment'))
					<div class="input_error">
						<span>{{ $errors->first('receipt_payment') }}</span>
					</div>
					@endif
					<div class="form-group consultar">
						<label class="col-4 col-md-center control-label" for="receipt_payment">Seleccione Fecha</label>
						<div class="col-6 col-md-center" >
                            <input type="text" class="date_reservation form-control input-md" placeholder="Fecha:" name="date" id="date" date="">
            				<input type="text" class="time_reservation form-control input-md" placeholder="Hora:" name="start_time" id="start_time" starttime="">
						</div>
						<div class="mensaje">
							<p>
								<span>

								</span>
							</p>
						</div>
                        <div class="consultar_fecha">
							<a href="{{ route( 's_check_reservation', array('member_id' => Auth::user()->member->id ) ) }}" id="consultar" class="btn btn-primary singlebutton1">
								Consultar
							</a>
						</div>
					</div>


					<div class="procesar">
						<div class="add_invitados add_">
							<div class="form-group">
								<label class="col-4 col-md-center control-label" for="receipt_payment">Buscar Invitados</label>
								<div class="col-6 col-md-center" >
									<input id="add_partners" type="text" class="search form-control input-md" placeholder="Buscar Invitados" name="search_invite">
								</div>
								<div class="mensaje">

								</div>
							</div>
							<ul class="">

							</ul>

						</div>

						<div class="add_socios add_">
							<div class="form-group">
								<label class="col-4 col-md-center control-label" for="receipt_payment">Buscar Socios</label>
								<div class="col-6 col-md-center" >
									<input id="add_members" type="text" class="search form-control input-md" placeholder="Buscar Socios" name="search_socio">
								</div>
								<div class="mensaje">

								</div>
							</div>
							<ul class="">

							</ul>
						</div>



						<div class="green_fee">
							@if ($errors->has('total_green_fee'))
								<div class="input_error">
									<span>{{ $errors->first('total_green_fee') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-6 control-label" for="receipt_payment">Green Fee: </label>
								<div class="col-md-3" >
									<span>Bsf</span> <input id="green_fee" green-fee="{{ $reservation_definition->green_fee }}" readonly type="text" class="search form-control input-md money" name="total_green_fee" >
									<input type="hidden" name="reservation_definition_id" value="{{ $reservation_definition->id }}">
								</div>
							</div>
						</div>

						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Procesar</button>
						</div>
					</div>



				</fieldset>
			</form>
		</div>
	</div>
</div>
@endsection
