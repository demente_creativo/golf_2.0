@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Periodo</h3>
			</div>
			<div class="form">
				<form class="form-horizontal inhabilitar" action="{{ route('periodos.update', array('user_id' => Auth::user()->id, 'period_id' => $period->id))}}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset>
						<!-- Form Name -->


						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Nombre</label>
							<div class="col-md-6">
								<input id="textinput" required name="name" type="text" value="{{ $period->name }}" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
							</div>
						</div>

						@if ($errors->has('date_start'))
							<div class="input_error">
								<span>{{ $errors->first('date_start') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Fecha Inicial</label>
							<div class="col-md-6">
							<input type="text"  placeholder="Fecha Inicial" class="date form-control input-md datepicker_min" name="date_start" id="start_date_periods" required  value="{{$period->date_start}}">
							</div>
						</div>

						@if ($errors->has('date_end'))
							<div class="input_error">
								<span>{{ $errors->first('date_end') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Fecha Final</label>
							<div class="col-md-6">
							<input type="text"  placeholder="Fecha Final" class="date form-control input-md" name="date_end" id="end_datepicker_min" date=" " required  value="{{$period->date_end}}">
							</div>
						</div>

						@if ($errors->has('group_id'))
							<div class="input_error">
								<span>{{ $errors->first('group_id') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Grupo</label>
							<div class="col-md-6">
								<select name="group_id" required class="form-control input-md">

                                 	 <option value="{{ $period->group_id}}" disabled selected>Seleccione Grupo</option>
                                        @foreach ($groups as $group)
                                             <option value="{{$group->id}}" {{ ($period->group_id == $group->id) ? 'selected' : '' }}>{{$group->name}}</option>
                                        @endforeach
                                </select>
                            </div>
						</div>


						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('periodos.index', array('user_id' => Auth::user()->id))}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
