<script type="text/javascript">
	var socio_id = {{ $member->id }};
</script>
@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Socio</h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<div class="form">
				<form class="form-horizontal form_validation" action="{{ route('socios.update', array('user_id' => Auth::user()->id, 'member_id' => $member->id ) ) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}
					<fieldset>
						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{ $errors->first('name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<input id="name" name="name" type="text" value="{{ $member->name }}" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
							</div>
						</div>
						@if ($errors->has('last_name'))
							<div class="input_error">
								<span>{{ $errors->first('last_name') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="last_name">Apellido</label>
							<div class="col-md-6">
								<input id="last_name" name="last_name" type="text" value="{{ $member->last_name }}" placeholder="Apellido" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
							</div>
						</div>
						@if ($errors->has('identity_card'))
							<div class="input_error">
								<span>{{ $errors->first('identity_card') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="identity_card">Cédula</label>
							<div class="col-md-6 inpt">
								<input id="identity_card" name="identity_card" type="text" value="{{ $member->identity_card }}" placeholder="Cedula" class="cedula form-control input-md chequear_cedula" required checkear="{{ route('socios.validate_identity_card_member') }}" pattern=".{6,8}" title="de 6 a 8 caracteres">
								<div class="chequear">
									<div class="icon check">
										<img src="{{ asset('img/icons/check.png') }}" alt="">
									</div>
									<div class="icon error">
										<img src="{{ asset('img/icons/error.png') }}" alt="">
										<div class="mensaje">
											<p></p>
										</div>
									</div>
									<div class="icon cargando">
										<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
									</div>
								</div>
							</div>
						</div>
						@if ($errors->has('gender'))
						<div class="input_error">
							<span>{{ $errors->first('gender') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Sexo</label>
							<div class="col-md-6">
								<select required name="sex" class="form-control input-md" id="sex">
									<option value="" disabled>Seleccione {{ $member->sex }} </option>
									<option value="1" {{ ($member->sex == 'Femenino') ? 'selected' : '' }}>Femenino</option>
									<option value="2" {{ ($member->sex == 'Masculino') ? 'selected' : '' }}>Masculino</option>
								</select>
							</div>
						</div>
						@if ($errors->has('number_action'))
							<div class="input_error">
								<span>{{ $errors->first('number_action') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Nº Acción</label>
							<div class="col-md-6">
								<input id="number_action" name="number_action" type="text" value="{{ $member->number_action }}" placeholder="Nº Acción" class="form-control input-md number_action" pattern=".{5,20}" title="de 5 a 20 caracteres" required>
							</div>
						</div>
						@if ($errors->has('handicap'))
								<div class="input_error">
									<span>{{ $errors->first('handicap') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="handicap">Handicap</label>
								<div class="col-md-6">
									<input id="handicap" name="handicap" type="text" placeholder="Handicap" class="handican form-control input-md handicap_patter" title="" value="{{$member->handicap}}">
								</div>
							</div>
						@if ($errors->has('phone'))
							<div class="input_error">
								<span>{{ $errors->first('phone') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="phone">Teléfono</label>
							<div class="col-md-6">
								<input id="phone" name="phone" type="text" placeholder="Teléfono" class="phone form-control input-md" required pattern=".{5,14}" title="de 5 a 14 caracteres" value="{{ $member->phone }}"> 
							</div>
						</div>
						@if ($errors->has('email'))
							<div class="input_error">
								<span>{{ $errors->first('email') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="email">Correo</label>
							<div class="col-md-6 inpt">
								<input id="email" name="email" type="email" value="{{ $member->user->email }}" placeholder="Correo" class="form-control input-md chequear_correo" checkear="{{ route('socios.validate_email_member') }}" required pattern=".{3,50}" title="de 3 a 50 caracteres">
								<div class="chequear">
									<div class="icon check">
										<img src="{{ asset('img/icons/check.png') }}" alt="">
									</div>
									<div class="icon error">
										<img src="{{ asset('img/icons/error.png') }}" alt="">
										<div class="mensaje">
											<p>

											</p>
										</div>
									</div>
									<div class="icon cargando">
										<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
									</div>
								</div>
							</div>
						</div>
						@if ($errors->has('password'))
							<div class="input_error">
								<span>{{ $errors->first('password') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="password">Contraseña</label>
							<div class="col-md-6">
								<input id="password" name="password" type="password" placeholder="Contraseña" class="form-control input-md" pattern=".{6,25}" title="de 6 a 25 caracteres">
							</div>
						</div>
						@if ($errors->has('password_confirmation'))
							<div class="input_error">
								<span>{{ $errors->first('password_confirmation') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="password_confirmation">Confirmar Contraseña</label>
							<div class="col-md-6">
								<input id="password_confirmation" name="password_confirmation" type="password" placeholder="Confirmar Contraseña" class="form-control input-md">
							</div>
						</div>						
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
							<a href="{{route('socios.index', array('user_id' => Auth::user()->id) )}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
