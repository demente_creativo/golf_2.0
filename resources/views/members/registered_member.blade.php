@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Solicitud de Suscripciones</h2>
		</div>
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<div class="table-responsive success tabla margin_none">
			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Cédula</th>
						<th>Nº acción</th>
						<th>Teléfono</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $members as $member)
						
							
								<tr>
										<td>{{ $member->name }}</td>
										<td>{{ $member->last_name }}</td>
										<td>{{ $member->identity_card }}</td>
										<td>{{ $member->number_action }}</td>
										<td>{{ $member->phone }}</td>
										<td>
											<input type="checkbox" {{ (!$member->deleted_at) ? 'checked' : '' }} class="toggle-event" data-item="{{$member->user_id}}"  data-toggle="toggle" data-route="/socios/status_inscription/" data-on="Aprobar" data-off="Declinar">
										</td>
								</tr>
							
						

					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6">

						</td>
					</tr>
				</tfoot>
			</table>


		</div>
	</div>
</div>
@endsection
