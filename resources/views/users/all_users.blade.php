@extends('layouts.app')
@section('content')
<div class="container">



	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Usuarios</h2>
		</div>
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif

		<div class="tabla_head">
			<div class="tabla_divider add">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
			</div>
			<div class="tabla_divider search">
				<div class="buscar">
					<!-- <form class="" action="" method="post">
						<button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
						<input id="search" name="q" type="text" placeholder="" class="form-control input-md">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
					</form> -->
				</div>
			</div>
		</div>
		<div class="table-responsive success tabla">
			{!! $table_users->render() !!}
		</div>
	</div>


	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Usuario</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" action="{{ route('usuarios.store') }}" method="POST">
						{{ csrf_field() }}

						<fieldset>
							<!-- Form Name -->
							

							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{ $errors->first('name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md">
								</div>
							</div>

							@if ($errors->has('email'))
								<div class="input_error">
									<span>{{ $errors->first('email') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="email">Correo</label>
								<div class="col-md-6">
									<input id="email" name="email" type="email" placeholder="Correo" class="form-control input-md">
								</div>
							</div>

							@if ($errors->has('email'))
								<div class="input_error">
									<span>{{ $errors->first('email') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="password">Password</label>
								<div class="col-md-6">
									<input id="password" name="password" type="password" placeholder="Password" class="form-control input-md">
								</div>
							</div>

							@if ($errors->has('role_id'))
								<div class="input_error">
									<span>{{ $errors->first('role_id') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-4 col-md-center control-label" for="role_id">Rol</label>
								<div class="col-6">
									<select required name="role_id" class="form-control input-md">
										<option value="" disabled selected>Seleccione Rol</option>
										@foreach($roles as $role)
											<option value="{{ $role->id }}">{{ $role->name }}</option>
										@endforeach
									</select>
								</div>
							</div>


							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

								<a href="{{route('usuarios.index')}}" class="btn btn-primary singlebutton1">
									Cancelar
								</a>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar este socio?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
