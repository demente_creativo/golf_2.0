@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Estudiante</h3>
			</div>
			<div class="form">
				<form class="form-horizontal inhabilitar" action="{{route('alumnos.update', array('user_id' => Auth::user()->id, 'student_id' => $student->id))}}" method="POST">
					{{csrf_field()}}
					{{method_field('PUT')}}

					<fieldset>
						<!-- Form Name -->


						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{$errors->first('name')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Nombre</label>
							<div class="col-md-6">
								<input id="textinput" required name="name" type="text" value="{{$student->name}}" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
							</div>
						</div>

						@if ($errors->has('last_name'))
							<div class="input_error">
								<span>{{$errors->first('last_name')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Apellido</label>
							<div class="col-md-6">
								<input id="textinput" required name="last_name" type="text" value="{{$student->last_name}}" placeholder="Apellido" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
							</div>
						</div>

						@if ($errors->has('identity_card'))
							<div class="input_error">
								<span>{{$errors->first('identity_card')}}</span>
							</div>
						@endif
						<div class="form-group">
						<label class="col-md-4 control-label" for="identity_card">Cédula</label>
							<div class="col-md-6 inpt">
								<input id="identity_card" name="identity_card" type="text" placeholder="Cédula" value="{{$student->identity_card}}" class="cedula form-control input-md chequear_cedula" required checkear="{{ route('alumnos.validate_identity_card_student') }}" pattern=".{6,8}" title="de 6 a 8 caracteres">
								<div class="chequear">
									<div class="icon check">
										<img src="{{ asset('img/icons/check.png') }}" alt="">
									</div>
									<div class="icon error">
										<img src="{{ asset('img/icons/error.png') }}" alt="">
										<div class="mensaje">
											<p></p>
										</div>
									</div>
									<div class="icon cargando">
										<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
									</div>
								</div>
							</div>
						</div>

						@if ($errors->has('phone'))
							<div class="input_error">
								<span>{{$errors->first('phone')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Teléfono</label>
							<div class="col-md-6">
								<input id="textinput" required name="phone" type="text" value="{{$student->phone}}" pattern=".{5,14}" title="de 5 a 14 caracteres" placeholder="Teléfono" class="form-control input-md phone" title="Formato: 0414 888-88-88">
							</div>
						</div>

						@if ($errors->has('birthdate'))
							<div class="input_error">
								<span>{{$errors->first('birthdate')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Fecha de Nacimiento</label>
							<div class="col-md-6">
								<input type="text" required class="datepicker_change_month form-control input-md" name="birthdate" value="{{$student->birthdate}}" >
							</div>
						</div>


						@if(Auth::user()->role_id == 3)
							@if ($errors->has('member_id'))
								<div class="input_error">
									<span>{{$errors->first('member_id')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Socio
								</label>
									<div class="col-md-6">
										<select name="member_id" class="form-control input-md" required>
											<option value="{{$student->member_id}}" disabled selected>Seleccione un Miembro</option>
												@foreach ($members as $member)
										   <option value="{{$member->id}}" {{($student->member_id == $member->id) ? 'selected' : ''}}>{{$member->name.' '.$member->last_name}}</option>
												@endforeach
										</select>
									</div>
							</div>
						@endif

						<!-- Button -->
						<div class="form-group boton">
							<button id="" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('alumnos.index', array('user_id' => Auth::user()->id))}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
<script type="text/javascript">
	var student_id = {{ $student->id }};
</script>
