@extends('layouts.app')

@section('content')

	<div id="contenido" class="container white">
		<form class="form-horizontal inhabilitar" action="{{route('alumnos.store', array('user_id' => Auth::user()->id))}}" method="POST">
			{{csrf_field()}}

			<fieldset>
				<!-- Form Name -->
				

				@if ($errors->has('name'))
					<div class="input_error">
						<span>{{$errors->first('name')}}</span>
					</div>
				@endif
				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Nombre</label>
					<div class="col-md-4">
						<input id="textinput" required name="name" type="text" placeholder="Nombre" class="form-control input-md">
					</div>
				</div>

				@if ($errors->has('last_name'))
					<div class="input_error">
						<span>{{$errors->first('last_name')}}</span>
					</div>
				@endif
				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Apellido</label>
					<div class="col-md-4">
						<input id="textinput" required name="last_name" type="text" placeholder="Apellido" class="form-control input-md">
					</div>
				</div>

				@if ($errors->has('identity_card'))
					<div class="input_error">
						<span>{{$errors->first('identity_card')}}</span>
					</div>
				@endif
				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Cédula</label>
					<div class="col-md-4">
						<input id="textinput" required name="identity_card" type="text" placeholder="Cédula" class="form-control input-md cedula" min="7">
					</div>
				</div>

				@if ($errors->has('phone'))
					<div class="input_error">
						<span>{{$errors->first('phone')}}</span>
					</div>
				@endif
				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Teléfono</label>
					<div class="col-md-4">
						<input id="textinput" required name="phone" type="text" placeholder="Teléfono" class="form-control input-md phone" min="11">
					</div>
				</div>

				@if ($errors->has('birthdate'))
					<div class="input_error">
						<span>{{$errors->first('birthdate')}}</span>
					</div>
				@endif
				<div class="form-group">
					<label class="col-md-4 control-label" for="textinput">Fecha de Nacimiento
					</label>
						<div class="col-md-4">
							<input type="datetimepicker" id="datetimepicker" required class="datetimepicker3 form-control input-md" name="birthdate" >
						</div>
				</div>

				@if(Auth::user()->role_id == 3)
					@if ($errors->has('member_id'))
						<div class="input_error">
							<span>{{$errors->first('member_id')}}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="textinput">Miembro
						</label>
							<div class="col-md-4">
								<select name="member_id" class="form-control input-md">
									<option value="" disabled selected>Seleccione un Miembro</option>
										@foreach ($members as $member)
									<option value="{{$member->id}}">{{$member->name}}</option>
										@endforeach
								</select>
							</div>
					</div>
				@endif

				<!-- Button -->
				<div class="form-group boton">

					<button id="guardar" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Enviar</button>

					<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
				</div>
			</fieldset>
		</form>
</div>

@endsection