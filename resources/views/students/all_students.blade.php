@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Estudiantes</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<div class="table-responsive success tabla">
			<div class="agregar_registro">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Cédula</th>
						<th>Teléfono</th>
						<th class="opciones" id="kdkd">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $students as $student)
						<tr>
							<td>{{ $student->name }}</td>
							<td>{{ $student->last_name }}</td>
							<td>{{ $student->identity_card }}</td>
							<td>{{ $student->phone }}</td>
							<td>
								<a href="{{route('alumnos.show', array('user_id' =>  Auth::user()->id, 'student_id' =>  $student->id))}}" class="btn btn-primary singlebutton1 student_show" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
								</a>
								<a href="{{route('alumnos.edit', array('user_id' => Auth::user()->id, 'student_id' => $student->id))}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								@if(Auth::user()->role_id == 3)
									<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('alumnos.destroy', array('user_id' => Auth::user()->id, 'student_id' => $student->id))}}">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">

						</td>
					</tr>
				</tfoot>
			</table>

		</div>
		@if(Auth::user()->role_id == 3)
			@if(count($students) >= 1)
				<div class="bajar_reporte">
					<a id="crear_reporte_pdf" type="button" href="{{route('pdf',['user_id' => Auth::user()->id])}}">
						<span class="icon">
							<img src="{{asset('img/icons/pdf.png')}}" alt="">
						</span>
					</a>
					<a id="crear_reporte_xls" type="button" href="{{route('excel',['user_id' => Auth::user()->id])}}">
						<span class="icon">
							<img src="{{asset('img/icons/xls.png')}}" alt="">
						</span>
					</a>
				</div>
			@endif
		@endif
	</div>


<!--MODAL AGREGAR ESTUDIANTE-->

@if(Auth::user()->role_id == 3)
	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Estudiante</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal inhabilitar" action="{{route('alumnos.store', array('user_id' => Auth::user()->id))}}" method="POST">
						{{csrf_field()}}

						<fieldset>
							<!-- Form Name -->


							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{$errors->first('name')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Nombre</label>
								<div class="col-md-6">
									<input id="textinput" required name="name" type="text" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
								</div>
							</div>

							@if ($errors->has('last_name'))
								<div class="input_error">
									<span>{{$errors->first('last_name')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Apellido</label>
								<div class="col-md-6">
									<input id="textinput" required name="last_name" type="text" placeholder="Apellido" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres">
								</div>
							</div>

							@if ($errors->has('identity_card'))
								<div class="input_error">
									<span>{{$errors->first('identity_card')}}</span>
								</div>
							@endif

							<div class="form-group">
							<label class="col-md-4 control-label" for="identity_card">Cédula</label>
								<div class="col-md-6 inpt">
									<input id="identity_card" name="identity_card" type="text" placeholder="Cédula" class="cedula form-control input-md chequear_cedula" required checkear="{{ route('alumnos.validate_identity_card_student') }}" pattern=".{6,8}" title="de 6 a 8 caracteres">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
											<div class="mensaje">
												<p></p>
											</div>
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>

							@if ($errors->has('phone'))
								<div class="input_error">
									<span>{{$errors->first('phone')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Teléfono</label>
								<div class="col-md-6">
									<input id="textinput" required name="phone" type="text" placeholder="Teléfono" pattern=".{5,14}" title="de 5 a 14 caracteres" class="phone form-control input-md" required title="Formato: 0414 888-88-88">
								</div>
							</div>

							@if ($errors->has('birthdate'))
								<div class="input_error">
									<span>{{$errors->first('birthdate')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Fecha de Nacimiento
								</label>
								<div class="col-md-6">
									<input type="text" id="datetimepicker" required class="datepicker_change_month form-control input-md" name="birthdate" placeholder="Fecha de Nacimiento">
								</div>
							</div>

							@if(Auth::user()->role_id == 3)
								@if ($errors->has('member_id'))
									<div class="input_error">
										<span>{{$errors->first('member_id')}}</span>
									</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Socio</label>
										<div class="col-md-6">
											<select name="member_id" class="form-control input-md" required>
												<option value="" disabled selected>Seleccione un Socio</option>
													@foreach ($members as $member)
												<option value="{{$member->id}}">{{$member->name.' '.$member->last_name}}</option>
													@endforeach
											</select>
										</div>
								</div>
							@endif

							<!-- Button -->
							<div class="form-group boton">

								<button id="guardar" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Enviar</button>

								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!--FIN DEL MODAL AGREGAR ESTUDIANTE-->

		<!--MODAL ELIMINAR ESTUDIANTE-->
		<div class="modal" id="modaldelete" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">¿Seguro que desea eliminar este Estudiante?</h4>
					</div>
					<div class="modal-body form">
						<form class="form-horizontal inhabilitar" method="POST">
							{{csrf_field()}}
							{{method_field('DELETE')}}
							<fieldset>
								<div class="form-group boton">
									<button id="eliminar" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!--MODAL DETALLE ESTUDIANTE-->
		<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Estudiante</h4>
				</div>
					<div class="modal-body form">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<p id="d_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Apellido</label>
								<div class="col-md-6">
									<p id="d_last_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Cédula</label>
								<div class="col-md-6">
									<p id="d_identity_card"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Teléfono</label>
								<div class="col-md-6">
									<p id="d_phone"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Fecha de Nacimiento</label>
								<div class="col-md-6">
									<p id="d_birthdate"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Socio</label>
								<div class="col-md-6">
									<p id="d_member"></p>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection
