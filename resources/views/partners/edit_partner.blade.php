<script type="text/javascript">
	var user_id = {{ $partner->user_id }};
	var partner_id = {{ $partner->id }};
</script>
@extends('layouts.app')

@section('content')

<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Invitado</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal form_validation" action="{{ route( 'invitados.update', array( 'user_id' =>  Auth::user()->id, 'partner_id' =>  $partner->id ) ) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}

				<fieldset>
					<!-- Form Name -->
					

					@if ($errors->has('name'))
						<div class="input_error">
							<span>{{ $errors->first('name') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="name">Nombre</label>
						<div class="col-md-6">
							<input id="name" name="name" type="text" value="{{ $partner->name }}" placeholder="Nombre" class="form-control input-md" required  pattern=".{3,25}" title="de 3 a 25 caracteres">
						</div>
					</div>

					@if ($errors->has('last_name'))
						<div class="input_error">
							<span>{{ $errors->first('last_name') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="last_name">Apellido</label>
						<div class="col-md-6">
							<input id="last_name" name="last_name" type="text" value="{{ $partner->last_name }}" placeholder="Apellido" class="form-control input-md" required  pattern=".{3,25}" title="de 3 a 25 caracteres">
						</div>
					</div>

					@if ($errors->has('identity_card'))
						<div class="input_error">
							<span>{{ $errors->first('identity_card') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="identity_card">Cédula</label>
						<div class="col-md-6 inpt">
							<input id="identity_card" name="identity_card" type="text" value="{{ $partner->identity_card }}" placeholder="Cédula" class="cedula form-control input-md chequear_cedula" required checkear="{{ route('socios.validate_identity_card_member') }}"  pattern=".{6,8}" title="de 6 a 8 caracteres">
							<div class="chequear">
								<div class="icon check">
									<img src="{{ asset('img/icons/check.png') }}" alt="">
								</div>
								<div class="icon error">
									<img src="{{ asset('img/icons/error.png') }}" alt="">
									<div class="mensaje">
										<p>

										</p>
									</div>
								</div>
								<div class="icon cargando">
									<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
								</div>
							</div>
						</div>
					</div>

					@if ($errors->has('sex'))
						<div class="input_error">
							<span>{{ $errors->first('sex') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="sex">Sexo</label>
						<div class="col-md-6">
							<select required name="sex" class="form-control input-md" id="sex">
								<option value="" disabled>Seleccione {{ $partner->sex }} </option>
								<option value="1" {{ ($partner->sex == 'Femenino') ? 'selected' : '' }}>Femenino</option>
								<option value="2" {{ ($partner->sex == 'Masculino') ? 'selected' : '' }}>Masculino</option>
							</select>
						</div>
					</div>

					@if ($errors->has('phone'))
						<div class="input_error">
							<span>{{ $errors->first('phone') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="phone">Teléfono</label>
						<div class="col-md-6">
							<input id="phone" name="phone" type="text" value="{{ $partner->phone }}" placeholder="Teléfono" class="phone form-control input-md" pattern=".{5,14}" title="de 5 a 11 caracteres" required title="Formato: 0414 888-88-88">
						</div>
					</div>
					@if ($errors->has('email'))
						<div class="input_error">
							<span>{{ $errors->first('email') }}</span>
						</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="email">Correo</label>
						<div class="col-md-6 inpt">
							<input id="email" name="email" type="text" value="{{ $partner->email }}" placeholder="Correo" class="form-control input-md chequear_correo" required checkear="{{ route('socios.validate_email_member') }}"  pattern=".{3,50}" title="de 3 a 50 caracteres" >
							<div class="chequear">
								<div class="icon check">
									<img src="{{ asset('img/icons/check.png') }}" alt="">
								</div>
								<div class="icon error">
									<img src="{{ asset('img/icons/error.png') }}" alt="">
									<div class="mensaje">
										<p>

										</p>
									</div>
								</div>
								<div class="icon cargando">
									<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
								</div>
							</div>
						</div>
					</div>

					<!-- Solo si es administrador -->
					@if(Auth::user()->role_id == 2)
						@if ($errors->has('exonerated'))
							<div class="input_error">
								<span>{{ $errors->first('exonerated') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="exonerated">Exonerado</label>
							<div class="col-md-6">
								<select name="exonerated" class="form-control input-md" required>
									<option value="" disabled>Seleccione</option>
									<option value="0" {{ (is_null($partner->exonerated)) ? 'selected' : '' }}>No Exonerado</option>
									<option value="1" {{ ("Parcial" == $partner->exonerated) ? 'selected' : '' }}>Parcial</option>
									<option value="2" {{ ("Total" == $partner->exonerated) ? 'selected' : '' }}>Total</option>
									<option value="3" {{ ("Reservacion" == $partner->exonerated) ? 'selected' : '' }}>Por reservación</option>
								</select>
							</div>
						</div>

						@if ($errors->has('club_id'))
						<div class="input_error">
							<span>{{ $errors->first('club_id') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="club_id">Club</label>
							<div class="col-md-6">
								<select name="club_id" class="form-control input-md" required>
									<option value="" disabled {{ (NULL == $partner->club_id) ? 'selected' : '' }}>Seleccione Club</option>
									@foreach($clubes as $club)
										<option value="{{ $club->id }}" {{ ($club->id == $partner->club_id) ? 'selected' : '' }}>{{ $club->name }}</option>
									@endforeach
								</select>
							</div>
						</div>
					@endif

					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

						<a href="{{route( 'invitados.index', array( 'user_id' =>  Auth::user()->id ) ) }}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>


@endsection
