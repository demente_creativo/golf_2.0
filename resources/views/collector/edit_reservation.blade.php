<script type="text/javascript">
	var draws = {!! $draws !!};
	var tournaments = {!! $tournaments !!};
	var not_available = {};
	var user_id = {{ Auth::user()->id }};
	var url_consultar_reserv = '{{route('check_reservation', array( 'user_id', Auth::user()->id ) )}}';
	var green_fees = {!! $reservation_definition !!};
	var day_blockeds = {!! $day_blockeds !!};
	var reservation_edit = null;
	var transaction = {!! $transaction !!}
	var reservation_id = {{ $transaction->reservation_id }};
</script>


@extends('layouts.app')
@section('content')

<div class="container" id="reservacion">
	<div class="edit reserv">
		<div class="form">
			<div class="modal-header">
				<h3 class="modal-title">Agregar Reservación</h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<div class="carga none">
				<div class="icon">
					<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
				</div>
			</div>
			<form class="form-horizontal" method="POST" action="{{ route('cobranza.update')}}" id="form_reservacion">
				{{ csrf_field() }}
				<input type="hidden" name="transaction_id" value="" id="transaction_id">

				<fieldset>
                    @if ($errors->has('receipt_payment'))
					<div class="input_error">
						<span>{{ $errors->first('receipt_payment') }}</span>
					</div>
					@endif
					<div class="form-group consultar divider">
						<div class="fecha_reservacion" >
							<input type="text" name="date" value="" id="date_reservacion" class="input_tiempo_reserv" required>
                            <div class="date_reservation" id="date_reservation" date="">
							</div>
							<div class="leyenda">
								<div class="item torneo">
									<div class="icon"></div>
									<div class="text">
										<p>
											Dia de torneo
										</p>
									</div>
								</div>
								<div class="item sorteo">
									<div class="icon"></div>
									<div class="text">
										<p>
											Dia de sorteos
										</p>
									</div>
								</div>
								<div class="item bloqueado">
									<div class="icon"></div>
									<div class="text">
										<p>
											Dia bloqueado
										</p>
									</div>
								</div>
							</div>
						</div>
						<div class="mensaje">
							<p>
								<span>

								</span>
							</p>

						</div>
						<div class="inputs">
							<h2>Participantes</h2>
							<div class="add_socios add_">
								<div class="inpt">
									<select class="js-example-basic-single select2_ form-control input-md" id="add_members">
										<option value="" disabled selected>Selecionar Socio</option>
									</select>
									<div class="mensaje"></div>
									<div class="input_error none">
										<p></p>
									</div>
									<div class="participante_dia none">
										<label for=""><span></span> <br> <strong>¿Desea Continuar?</strong></label>
										<button type="button" name="button" class="btn btn-primary singlebutton1 continuar">Continuar</button>
										<button type="button" name="button" class="btn btn-primary singlebutton1 cancelar">Cancelar</button>
									</div>
								</div>
								<ul class="">

								</ul>
							</div>
							<div class="add_invitados add_">
								<div class="inpt">
									<select class="js-example-basic-single select2_ form-control input-md" id="add_partners">
										<option value="" disabled selected>Selecionar Invitado</option>
									</select>
									<div class="mensaje">
									</div>
									<div class="participante_dia none">
										<label for=""><span></span> <br> <strong>¿Desea Continuar?</strong></label>
										<button type="button" name="button" class="btn btn-primary singlebutton1 continuar">Continuar</button>
										<button type="button" name="button" class="btn btn-primary singlebutton1 cancelar">Cancelar</button>
									</div>

								</div>
								<ul class="">
								</ul>
							</div>
							<div class="boton">
								<div class="avanzar division">
									<div class="item">
										<a href="#">
											<button type="button" class="btn btn-primary singlebutton1">Procesar</button>
										</a>
									</div>
								</div>
								<div class="tipo_pago division">
									<a href="#">
										<button type="button" class="btn btn-primary singlebutton1">Procesar</button>
									</a>
								</div>
							</div>

						</div>
					</div>


					<div class="divider">
						<input type="text" name="start_time" value="" id="time_reservacion" class="input_tiempo_reserv">
						<div class="muestra interaction">
							<div class="horas_reservacion">
								<ul>
								</ul>
								<div class="input_error none">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
								</div>
							</div>
						</div>
					</div>


				</fieldset>
				<div class="procesar none">
					<div class="form">
					</div>
				</div>
				<div class="modal fade modaldetalles modal_transaccion" id="modaldetalles" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Detalle de Estudiante</h4>
							</div>
							<div class="modal-body form">
								<fieldset>
									<!-- <form action="{{ route('cobranza.store') }}" method="POST"> -->
										{{ csrf_field() }}
										<div class="fechas">
											<div class="inpt divider">
												<label for="">Fecha</label>
												<span id="fecha">19-51-5211</span>
											</div>
											<div class="inpt divider">
												<label for="">Hora</label>
												<span id="hora">12:24 AM</span>
											</div>
											<div class="clear"></div>
										</div>
										<div class="participants">
											<div class="socios participantes divider">
												<div class="titulo">
													<h4>Socios</h4>
												</div>
												<ul>
													<li>
														<div class="name">
															Jose Miguel Urbina
														</div>
														<div class="icon">
															<i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
														</div>
														<div class="clear"></div>
													</li>
												</ul>
											</div>
											<div class="invitados participantes divider">
												<div class="titulo">
													<h4>Invitados</h4>
												</div>
												<ul>
													<li>
														<div class="name">
															Jose Miguel Urbina
														</div>
														<div class="icon">
															<i class="glyphicon glyphicon-remove" aria-hidden="true"></i>
														</div>
														<div class="clear"></div>
													</li>
												</ul>

											</div>
											<div class="clear"></div>
										</div>
										<div class="total">
											<div class="divider">
												<span class="label">Total:</span>
											</div>
											<div class="divider">
												<span class="monto">100.00</span> BsF
											</div>
											<div class="clear"></div>
										</div>
										<div class="pago">
											<h5>Seleccione Metodo de pago</h5>
											<select class="" name="payment_method_id" id="method_pago">
												<option selected disabled>Seleccionar metodo de pago</option>
													@foreach( $payment_methods as $payment)
														<option value= {{ $payment->id}}>{{ $payment->name}}</option>
													@endforeach
											</select>
										</div>
										<div class="boton">
											<button type="submit" class="btn btn-primary singlebutton1">
												<span>Pagar</span>
											</button>
										</div>

									<!-- </form> -->
								</fieldset>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


@endsection

@section('script')
	<script src="{{ asset('js/script_collector.js') }}" charset="utf-8"></script>
@endsection
