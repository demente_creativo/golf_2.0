@extends('layouts.app')
@section('content')
<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Préstamo</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('prestamo_casilleros.update', array('user_id' => Auth::user()->id,'locker_loan_id' => $locker_loan->id)) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}
				<fieldset>
				<input name="locker_id" type="hidden" value="{{$locker_loan->locker_id}}">
                    @if ($errors->has('observations_in'))
                        <div class="input_error">
                            <span>{{ $errors->first('observations_in') }}</span>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="observations_in">Observaciones de entrada</label>
                        <div class="col-md-6">
                            <input id="observations_in" name="observations_in" type="text" placeholder="Observaciones de entrada" class="form-control input-md" value="{{$locker_loan->observations_in}}">
                        </div>
                    </div>

					@if ($errors->has('member_id'))
					<div class="input_error">
						<span>{{ $errors->first('member_id') }}</span>
					</div>
					@endif

					<div class="form-group">
						<label class="col-md-4 control-label" for="name">Socio</label>
						<div class="col-md-6">
							<select required name="member_id" class="form-control input-md" required>
								<option value="" disabled selected>Seleccione Rol</option>
								@foreach($members as $member)
									<option value="{{ $member->id }}" {{ ($member->id == $locker_loan->member_id) ? 'selected' : '' }}>{{ $member->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

						<a href="{{route('prestamo_casilleros.index', array('user_id' => Auth::user()->id))}}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
 @endsection
