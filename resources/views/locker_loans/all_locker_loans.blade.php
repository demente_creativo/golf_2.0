@extends('layouts.app')
@section('content')
<div class="container">



	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Prestamos</h2>
		</div>
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif

		<div class="tabla_head">
			<div class="tabla_divider add">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
			</div>
			<div class="tabla_divider search">
				<div class="buscar">
				   <!--  <form class="" action="" method="post">
						<button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
						<input id="search_locker_loan" name="q" type="text" placeholder="" class="form-control input-md">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
					</form> -->
				</div>
			</div>
		</div>
		<div class="table-responsive success tabla">
			{!! $table_locker_loans->render() !!}

		</div>
	</div>


	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Casillero</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" action="{{ route('prestamo_casilleros.store', array('user_id' => Auth::user()->id)) }}" method="POST">
						{{ csrf_field() }}
						<fieldset>
							<input name="locker_id" type="hidden" value="{{$locker->id}}">
							@if ($errors->has('observations_in'))
								<div class="input_error">
									<span>{{ $errors->first('observations_in') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="observations_out">Observaciones de entrada</label>
								<div class="col-md-6">
									<input id="observations_out" name="observations_out" type="text" placeholder="Observaciones de entrada" class="form-control input-md" value="">
								</div>
							</div>

							@if ($errors->has('member_id'))
							<div class="input_error">
								<span>{{ $errors->first('member_id') }}</span>
							</div>
							@endif

							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Socio</label>
								<div class="col-md-6">
									<select required name="member_id" class="form-control input-md" required>
										<option value="" disabled selected>Seleccione Rol</option>
										@foreach($members as $member)
											<option value="{{ $member->id }}">{{ $member->name }}</option>
										@endforeach
									</select>
								</div>
							</div>

							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar esta prestamo?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
