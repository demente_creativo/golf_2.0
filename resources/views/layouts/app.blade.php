<?php
	$current_route = explode('.', Route::currentRouteName())[0];


	if (Route::currentRouteName() == 'torneos.edit') {
		$document = $tournament->conditions_file;
		$document_name = explode('/', $document)[count(explode('/', $document))-1];
	}

	///dd(route('s_invitados.index', array('user_id' => Auth::user()->id) ));
	ini_set('memory_limit', '-1');
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{csrf_token()}}">

	<title>{{config('app.name')}}</title>

	<link rel="shortcut icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">
	<link rel="icon" href="{{asset('img/favicon.png')}}" type="image/x-icon">

	<link href="{{asset('css/app.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap-select.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/fileinput.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/select2.min.css')}}" rel="stylesheet">

	<link href="{{asset('css/owl.carousel.css')}}" rel="stylesheet">
	<link href="{{asset('css/owl.theme.default.css')}}" rel="stylesheet">
	<link href="{{asset('css/jquery-ui.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/bootstrap-theme.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
	<link href="{{asset('css/animate.css')}}" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/hint.css/2.4.0/hint.min.css" rel="stylesheet" type='text/css'>
	<link href="{{asset('css/bootstrap2-toggle.min.css')}}" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{asset('css/jquery.timepicker.css')}}"/ >

	<link href="{{asset('css/style.css')}}" rel="stylesheet">

	<!-- Datatables -->
    <link href="{{asset('js/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('js/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('js/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('js/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('js/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">

	<script>
		window.Laravel = {!! json_encode([
			'csrfToken' => csrf_token(),

		]) !!};
		var tokem = {!! json_encode([
			'tokem' => csrf_token(),
		]) !!};

	</script>

</head>

<body class="{{ ($current_route != 'maletero') ? $current_route : '' }}">
	<div class="rotate valign-wrapper">
		<center>
			<img src="{{asset('img/icons/rotate_movil.gif')}}" alt=""  class="valign"/>
		</center>
	</div>

	@if( !Auth::guest() )
		<nav class="navbar navbar-default menu_" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Desplegar navegación</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				@if(Auth::user()->role_id == 1)
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-left ul_left">
							<li class="hover {{($current_route == 'administradores.index') ? 'actual' : ''}}">
								<a href="{{route('home')}}">
									<span>Inicio</span>
								</a>
							</li>
						</ul>
						<ul class="logo">
							<li>
								<a href="{{route('home')}}">
									<img src="{{asset('img/logo.png')}}" alt="">
								</a>
							</li>
						</ul>


						<ul class="nav navbar-nav navbar-right ul_right">
							<li class="hover {{($current_route == 'administradores.index') ? 'actual' : ''}}">
								<a href="{{route('administradores.index')}}">
									<span>Administradores</span>
								</a>
							</li>
							<li class="hover">
								<a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span>Salir</span>
								</a>
								<form id="logout-form" action="{{url('logout')}}" method="POST" style="display: none;">
								 {{csrf_field()}}
							 </form>
							</li>
						</ul>
					</div>
				@endif
				@if(Auth::user()->role_id == 2)
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-left ul_left">
							<li style="width: 157px" class="hover sub_menu {{($current_route == 'reservaciones' OR $current_route == 'definicion_reservas' OR $current_route == 'sorteos') ? 'actual' : ''}}">
								<a href="#" class="sb_mn">
									<span>Reservaciones</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{(Route::currentRouteName() == 'reservaciones.create') ? 'actual' : ''}}">
										<a href="{{route('reservaciones.create', array( 'user_id' => Auth::user()->id ))}}">
											<span>Reservar</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'reservaciones.index') ? 'actual' : ''}}">
										<a href="{{route('reservaciones.index', array( 'user_id' => Auth::user()->id ))}}">
											<span>Hist. Reservas</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'definicion_reservas.index') ? 'actual' : ''}}">
										<a href="{{route('definicion_reservas.index', array( 'user_id' => Auth::user()->id ))}}">
											<span>Green Fee</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'sorteos.index') ? 'actual' : ''}}">
										<a href="{{route('sorteos.index', array('user_id' => Auth::user()->id ))}}">
											<span>Sorteos</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'sorteos.history') ? 'actual' : ''}} ">
										<a href="{{route('sorteos.history', array('user_id' => Auth::user()->id ))}}">
											<span>Hist. Sorteos</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'reservaciones.list_reservations') ? 'actual' : ''}} ">
										<a href="{{route('reservaciones.list_reservations', array('user_id' => Auth::user()->id ))}}">
											<span>Reportes</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'reservaciones.departure_list') ? 'actual' : ''}}">
										<a href="{{route('reservaciones.departure_list', array('user_id' => Auth::user()->id ))}}">
											<span>Horario de Salida</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'dias_bloqueados.index') ? 'actual' : ''}}">
										<a href="{{route('dias_bloqueados.index', array('user_id' => Auth::user()->id ))}}">
											<span>Días Bloqueados</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'reservaciones.comments') ? 'actual' : ''}}">
										<a href="{{route('reservaciones.comments', array('user_id' => Auth::user()->id ))}}">
											<span>Comentarios</span>
										</a>
									</li>
								</ul>
							</li>

							<li class="hover {{($current_route == 'invitados') ? 'actual' : ''}}">
								<a href="{{route('invitados.index', array( 'user_id' => Auth::user()->id ) )}}">
									<span>Invitados</span>
								</a>
							</li>


							<li class="hover campeonato sub_menu {{($current_route == 'socios') ? 'actual' : ''}}" style=" width: 120px;">
								<a href="#" class="sb_mn">
									<span>Socios</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{($current_route == 'socios.registered_member') ? 'actual' : ''}}" style="height: 45px;">
										<a href="{{route('socios.registered_member', array('user_id' => Auth::user()->id) )}}">
											<span>Suscripciones</span>
										</a>
									</li>
									<li class="{{($current_route == 'socios') ? 'actual' : ''}}" style="height: 45px;">
										<a href="{{route('socios.index', array('user_id' => Auth::user()->id) )}}">
											<span>Socios</span>
										</a>
									</li>
								</ul>
							</li>

							<li class="hover {{($current_route == 'home') ? 'actual' : ''}}">
								<a href="{{route('home')}}">
									<span>Inicio</span>
								</a>
							</li>
						</ul>
						<ul class="logo">
							<li>
								<a href="{{route('reservaciones.index', array( 'user_id' => Auth::user()->id ) )}}">
									<img src="{{asset('img/logo.png')}}" alt="">
								</a>
							</li>
						</ul>

						<ul class="nav navbar-nav navbar-right ul_right">
							<li style="width: 135px" class="hover campeonato sub_menu {{($current_route == 'torneos' or $current_route == 'categorias' or $current_route == 'horario_salidas') ? 'actual' : ''}}">
								<a href="#" class="sb_mn">
									<span>Torneos</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{(Route::currentRouteName() == 'torneos.index') ? 'actual' : ''}}">
										<a href="{{route('torneos.index')}}">
											<span>Torneos</span>
										</a>
									</li>
									<li class="dos_lineas {{(Route::currentRouteName() == 'torneos.list_tournament_inscriptions') ? 'actual' : ''}} ">
										<a href="{{route('torneos.list_tournament_inscriptions')}}">
											<span>Reportes de Inscripcion</span>
										</a>
									</li>
									<li class="{{($current_route == 'categorias') ? 'actual' : ''}}">
										<a href="{{route('categorias')}}">
											<span>Categorias</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="hover campeonato sub_menu {{($current_route == 'perfil') ? 'actual' : ''}}" style=" width: 120px;">
								<a href="#" class="sb_mn">
									<span>Perfil</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{($current_route == 'perfil.index') ? 'actual' : ''}}" style="height: 45px;">
										<a href="{{route('perfil.index')}}">
											<span>Contraseña</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="hover">
								<a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span>Salir</span>
								</a>
								<form id="logout-form" action="{{url('logout')}}" method="POST" style="display: none;">
								 {{csrf_field()}}
							 </form>
							</li>
						</ul>
						<div class="messages" id="messages">
							<a href="" id="">

							</a>
						</div>

					</div>
				@endif
				@if(Auth::user()->role_id == 3)
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-left ul_left">
							<li class="hover escuela sub_menu {{($current_route == 'planes.index' or $current_route == 'alumnos.index' or $current_route == 'profesores.index' or $current_route == 'grupos.index' or $current_route == 'pagos.index' or $current_route == 'periodos.index' or $current_route == 'all_monthly_payment') ? 'actual' : ''}}">
								<a href="#" class="sb_mn">
									<span>Escuela</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{($current_route == 'planes.index') ? 'actual' : ''}}">
										<a href="{{route('planes.index', array('user_id' => Auth::user()->id))}}">
											<span>Planes</span>
										</a>
									</li>
									<li class="{{($current_route == 'profesores.index') ? 'actual' : ''}}">
										<a href="{{route('profesores.index', array('user_id' => Auth::user()->id))}}">
											<span>Profesores</span>
										</a>
									</li>
									<li class="{{($current_route == 'grupos.index') ? 'actual' : ''}}">
										<a href="{{route('grupos.index', array('user_id' => Auth::user()->id))}}">
											<span>Grupos</span>
										</a>
									</li>
									<li class="{{($current_route == 'alumnos.index') ? 'actual' : ''}}">
										<a href="{{route('alumnos.index', array('user_id' => Auth::user()->id))}}">
											<span>Alumnos</span>
										</a>
									</li>
									<li class="{{($current_route == 'inscripciones.index') ? 'actual' : ''}}">
										<a href="{{route('inscripciones.index', array('user_id' => Auth::user()->id))}}">
											<span>Inscripciones</span>
										</a>
									</li>
									<li class="{{($current_route == 'periodos.index') ? 'actual' : ''}}">
										<a href="{{route('periodos.index', array('user_id' => Auth::user()->id))}}">
											<span>Períodos</span>
										</a>
									</li>
									<li class="{{($current_route == 'mensualidades.index') ? 'actual' : ''}}">
										<a href="{{route('mensualidades.index', array('user_id' => Auth::user()->id))}}">
											<span>Mensualidades</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'alumnos.list_reservations') ? 'actual' : ''}} ">
										<a href="{{route('alumnos.list_registrations', array('user_id' => Auth::user()->id))}}">
											<span>Reportes</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="hover {{($current_route == 'home') ? 'actual' : ''}}">
								<a href="{{route('home')}}">
									<span>Inicio</span>
								</a>
							</li>
						</ul>
						<ul class="logo">
							<li>
								<a href="{{route('home')}}">
									<img src="{{asset('img/logo.png')}}" alt="">
								</a>
							</li>
						</ul>


						<ul class="nav navbar-nav navbar-right ul_right">

							<li style="width: 157px" class="hover campeonato sub_menu">
								<a href="#" class="sb_mn">
									<span>Reservaciones</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{(Route::currentRouteName() == 'finanzas.departure_list') ? 'actual' : ''}}">
										<a href="{{route('finanzas.departure_list', array('user_id' => Auth::user()->id ))}}">
											<span>Horario de Salida</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="hover">
								<a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span>Salir</span>
								</a>
								<form id="logout-form" action="{{url('logout')}}" method="POST" style="display: none;">
								 {{csrf_field()}}
							 </form>
							</li>
						</ul>
					</div>
				@endif
				@if(Auth::user()->role_id == 4)
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-left ul_left">
							<li class="hover {{($current_route == 'casilleros') ? 'actual' : ''}}" >
								<a href="{{route('casilleros.index', array( 'user_id' => Auth::user()->id))}}">
									<span>Casilleros</span>
								</a>
							</li>
							<li class="hover {{($current_route == 'home') ? 'actual' : ''}}">
								<a href="{{route('home')}}">
									<span>Inicio</span>
								</a>
							</li>
						</ul>
						<ul class="logo">
							<li>
								<a href="{{route('home')}}">
									<img src="{{asset('img/logo.png')}}" alt="">
								</a>
							</li>
						</ul>

						<ul class="nav navbar-nav navbar-right ul_right">
							<li style="width: 157px" class="hover campeonato sub_menu">
								<a href="#" class="sb_mn">
									<span>Reservaciones</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{(Route::currentRouteName() == 'maletero.departure_list') ? 'actual' : ''}}">
										<a href="{{route('maletero.departure_list', array('user_id' => Auth::user()->id ))}}">
											<span>Horario de Salida</span>
										</a>
									</li>
								</ul>
							</li>

							<li class="hover">
								<a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span>Salir</span>
								</a>
								<form id="logout-form" action="{{url('logout')}}" method="POST" style="display: none;">
								 {{csrf_field()}}
							 </form>
							</li>
						</ul>
					</div>
				@endif
				@if(Auth::user()->role_id == 5)
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-left ul_left">
							<li style="width: 157px" class="hover campeonato sub_menu {{($current_route == 'reservaciones_del_dia') ? 'actual' : ''}}">
								<a href="#" class="sb_mn">
									<span>Reservaciones</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{(Route::currentRouteName() == 'reservaciones.create') ? 'actual' : ''}}">
										<a href="{{route('reservaciones.create', array( 'user_id' => Auth::user()->id ))}}">
											<span>Reservar</span>
										</a>
									</li>
									<li class="{{($current_route == 'starters') ? 'actual' : ''}}">
										<a href="{{route('reservaciones_del_dia.index', array( 'user_id' => Auth::user()->id ) )}}">
											<span>Horario de Salida</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="hover {{($current_route == 'home') ? 'actual' : ''}}">
								<a href="{{route('home')}}">
									<span>Inicio</span>
								</a>
							</li>
						</ul>
						<ul class="logo">
							<li>
								<a href="{{route('home')}}">
									<img src="{{asset('img/logo.png')}}" alt="">
								</a>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right ul_right">
							<li class="hover">
								<a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span>Salir</span>
								</a>
								<form id="logout-form" action="{{url('logout')}}" method="POST" style="display: none;">
								 {{csrf_field()}}
							 </form>
							</li>
						</ul>
					</div>
				@endif
				@if(Auth::user()->role_id == 6)

					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-left ul_left">
							<li style="width: 157px" class="hover campeonato sub_menu {{($current_route == 's_reservaciones') ? 'actual' : ''}}">
								<a href="#" class="sb_mn">
									<span>Reservaciones</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{($current_route == 's_reservaciones') ? 'actual' : ''}}">
										<a href="{{route('s_reservaciones.create', array( 'user_id' => Auth::user()->id ))}}">
											<span>Reservar</span>
										</a>
									</li>
									<li class="{{($current_route == 's_reservaciones') ? 'actual' : ''}}">
										<a href="{{route('s_reservaciones.index', array( 'user_id' => Auth::user()->id ))}}">
											<span>Hist. Reservas</span>
										</a>
									</li>
									<li class="{{($current_route == 's_reservaciones') ? 'actual' : ''}}">
										<a href="{{route('s_reservaciones.departure_list', array('user_id' => Auth::user()->id ))}}">
											<span>Horario de Salida</span>
										</a>
									</li>

								</ul>
							</li>

							<li class="hover {{($current_route == 's_invitados.index') ? 'actual' : ''}}">
								<a href="{{route('s_invitados.index', array('user_id' => Auth::user()->id) )}}">
									<span>Invitados</span>
								</a>
							</li>
							<li class="hover {{($current_route == 'home') ? 'actual' : ''}}">
								<a href="{{route('home')}}">

									<span>Inicio</span>
								</a>
							</li>
						</ul>
						<ul class="logo">
							<li>
								<a href="{{route('home')}}">
									<img src="{{asset('img/logo.png')}}" alt="">
								</a>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right ul_right">
							<li style="width: 157px" class="hover campeonato sub_menu {{($current_route == 'inscripciones') ? 'actual' : ''}}">
								<a href="#" class="sb_mn">
									<span>Escuela</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{($current_route == 'inscripciones.index') ? 'actual' : ''}}">
										<a href="{{route('inscripciones.index', array( 'user_id' => Auth::user()->id ))}}">
											<span>Inscripciones</span>
										</a>
									</li>
									<li class="hover {{($current_route == 'alumnos.index') ? 'actual' : ''}}">
										<a href="{{route('alumnos.index', array('user_id' => Auth::user()->id) )}}">
											<span>Alumnos</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="hover campeonato sub_menu {{($current_route == 'perfil') ? 'actual' : ''}}" style=" width: 120px;">
								<a href="#" class="sb_mn">
									<span>Perfil</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{($current_route == 'perfil.index') ? 'actual' : ''}}" style="height: 45px;">
										<a href="{{route('perfil.index')}}">
											<span>Contraseña</span>
										</a>
									</li>
									<li class="{{($current_route == 'perfil.updateProfile') ? 'actual' : ''}}" style="height: 45px;">
										<a href="{{route('perfil.updateProfile')}}">
											<span>Datos</span>
										</a>
									</li>
								</ul>
							</li>

							<li class="hover">

								<a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span>Salir</span>
								</a>
								<form id="logout-form" action="{{url('logout')}}" method="POST" style="display: none;">
								 {{csrf_field()}}
							 </form>
							</li>

						</ul>
					</div>
				@endif
				@if(Auth::user()->role_id == 7) <!-- Ranger -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-left ul_left">
							<li style="width: 157px" class="hover campeonato sub_menu {{($current_route == 'reservaciones_del_dia') ? 'actual' : ''}}">
								<a href="#" class="sb_mn">
									<span>Reservaciones</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{(Route::currentRouteName() == 'rangers.departure_list') ? 'actual' : ''}}">
										<a href="{{route('rangers.departure_list', array('user_id' => Auth::user()->id ))}}">
											<span>Horario de Salida</span>
										</a>
									</li>

								</ul>
							</li>
							<li class="hover {{($current_route == 'home') ? 'actual' : ''}}">
								<a href="{{route('home')}}">
									<span>Inicio</span>
								</a>
							</li>
						</ul>
						<ul class="logo">
							<li>
								<a href="{{route('home')}}">
									<img src="{{asset('img/logo.png')}}" alt="">
								</a>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right ul_right">
							<li class="hover">
								<a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span>Salir</span>
								</a>
								<form id="logout-form" action="{{url('logout')}}" method="POST" style="display: none;">
								 {{csrf_field()}}
							 </form>
							</li>
						</ul>
					</div>
				@endif
				@if(Auth::user()->role_id == 8)<!-- Cobrador -->
					<div class="collapse navbar-collapse navbar-ex1-collapse">
						<ul class="nav navbar-nav navbar-left ul_left">
							<li style="width: 157px" class="hover campeonato sub_menu {{($current_route == 'reservaciones_del_dia') ? 'actual' : ''}}">
								<a href="#" class="sb_mn">
									<span>Reservaciones</span>
									<span class="glyphicon glyphicon-menu-down icon"></span>
								</a>
								<ul class="">
									<li class="{{($current_route == 'cobranza') ? 'actual' : ''}}">
										<a href="/cobranza">
											<span>Reservaciones</span>
										</a>
									</li>
									<li class="{{(Route::currentRouteName() == 'starters.departure_list') ? 'actual' : ''}}">
										<a href="{{route('starters.departure_list', array('user_id' => Auth::user()->id ))}}">
											<span>Horario de Salida</span>
										</a>
									</li>
								</ul>
							</li>
							<li class="hover {{($current_route == 'home') ? 'actual' : ''}}">
								<a href="{{route('home')}}">
									<span>Inicio</span>
								</a>
							</li>
						</ul>
						<ul class="logo">
							<li>
								<a href="{{route('home')}}">
									<img src="{{asset('img/logo.png')}}" alt="">
								</a>
							</li>
						</ul>
						<ul class="nav navbar-nav navbar-right ul_right">
							<li class="hover">
								<a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
									<span>Salir</span>
								</a>
								<form id="logout-form" action="{{url('logout')}}" method="POST" style="display: none;">
								 {{csrf_field()}}
							 </form>
							</li>
						</ul>
					</div>
				@endif
			</div>
		</nav>
	@endif

	@yield('content')


	<footer>
	  <div class="footer">
		<p>
		  © {{date('Y')}} - Todos los derechos reservados. Diseñado por <a href="http://dementecreativo.com/" target="_blank"><img src="{{asset('img/demente.png')}}" alt=""></a>
		</p>
	  </div>
	</footer>

</body>
<script type="text/javascript">
	var public_asset = '{{asset('')}}';
	var userRole = {{(Auth::user())? Auth::user()->role_id: 0}}
</script>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
<script src="{{asset('js/jquery-ui.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.js')}}"></script>
<script src="{{asset('js/select2.full.min.js')}}"></script>
<script src="{{asset('js/jquery.simpleWeather.min.js')}}"></script>
<script src="{{asset('js/fileinput.min.js')}}"></script>
@if($current_route == 'torneos')
@endif

<script src="{{asset('js/collapse.min.js')}}"></script>
<script src="{{asset('js/jquery.mask.js')}}"></script>

<!-- <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script> -->
<script src="{{asset('js/jquery.timepicker.min.js')}}"></script>
<!-- <script src="{{asset('datetime/build/jquery.datetimepicker.full.min.js')}}"></script> -->
<!-- <script src="http://digitalbush.com/wp-content/uploads/2014/10/jquery.maskedinput.js" charset="utf-8"></script> -->

<script type="text/javascript">
	var public_directory = '{{asset('')}}';
</script>
<script src="{{asset('js/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('js/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('js/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('js/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('js/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('js/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('js/datatables.net-scroller/js/datatables.scroller.min.js')}}"></script>
<script src="{{asset('js/bootstrap2-toggle.min.js')}}"></script>
<script src="{{asset('js/wow.min.js')}}"></script>
<script src="{{asset('js/script.js')}}"></script>
@yield('script')


<!-- Datatables -->

<script>new WOW().init();</script>
    @if($current_route == 'torneos')
    	<script type="text/javascript">
    		$("#file_torunament").fileinput({
    			@if(Route::currentRouteName() == 'torneos.edit')
    				initialPreview: [
    					"{{asset(''.$document.'')}}",
    				],
    			@endif
    			initialPreviewAsData: true,
    			initialPreviewConfig: [
    				{type: "pdf", size: 8000, caption: "{{(Route::currentRouteName() == 'torneos.edit') ? $document_name : ''}}"},
    			],

    			showUpload: false,
    			showRemove: false,
    			// 'previewFileType': 'any'
    			maxFileCount: 1,
    			browseLabel: 'Buscar',
    			msgZoomModalHeading: 'Vista previa',
    			allowedFileExtensions: ['pdf', 'docx'],
    			previewFileIconSettings: {
    				'doc': '<i class="fa fa-file-word-o text-primary"></i>',
    				'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
    			},
    			// indicatorNew: '<i class="fa fa-file-word-o text-primary"></i>',
    			// indicatorSuccess: '<i class="fa fa-file-pdf-o text-danger"></i>',
    			msgInvalidFileExtension: 'Error, solo puede agregar documentos "pdf" o "docx"',
    			msgNoFilesSelected: 'No hay archivo seleccionado'
    			// showBrowse: false
    		});

    		@if(Route::currentRouteName() == 'torneos.edit')
    			$('#file_torunament').on('fileclear', function(event) {
    				$('#modified').val('false');
    			});
    			$('#file_torunament').on('fileloaded', function(event, file, previewId, index, reader) {
    				$('#modified').val('true');
    			});
    		@endif
    	</script>
    @endif

</html>
