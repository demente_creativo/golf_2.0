@section('menu_root')
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav navbar-left ul_left">
                        <li class="hover {{ ($current_route == 'reservaciones') ? 'actual' : '' }}" >
                            <a href="{{route('reservaciones.index')}}">
                                <span>Reservaciones</span>
                            </a>
                        </li>
                        <li class="hover {{ ($current_route == 'socios') ? 'actual' : '' }}">
                            <a href="{{route('socios.index', array('user_id' => Auth::user()->id) )}}">
                                <span>Socios</span>
                            </a>
                        </li>
                        <li class="hover {{ ($current_route == 'all_partners') ? 'actual' : '' }}">
                            <a href="{{ route('all_partners') }}">
                                <span>Invitados</span>
                            </a>
                        </li>
                        <li class="hover {{ ($current_route == 'home') ? 'actual' : '' }}">
                            <a href="{{ route('home') }}">
                                <span>Inicio</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="logo">
                        <li>
                            <a href="{{ route('home') }}">
                                <img src="{{asset('img/logo.png')}}" alt="">
                            </a>
                        </li>
                    </ul>


                    <ul class="nav navbar-nav navbar-right ul_right">


                        <li class="hover campeonato sub_menu {{ ($current_route == 'torneos' or $current_route == 'categorias' or $current_route == 'horario_salidas') ? 'actual' : '' }}">
                            <a href="#" class="sb_mn">
                                <span>Campeonatos</span>
                                <span class="glyphicon glyphicon-menu-down icon"></span>
                            </a>
                            <ul class="">
                                <li class="{{ ($current_route == 'torneos') ? 'actual' : '' }}">
                                    <a href="{{ route('torneos.index') }}">
                                        <span>Torneos</span>
                                    </a>
                                </li>
                                <li class="{{ ($current_route == 'categorias') ? 'actual' : '' }}">
                                    <a href="{{ route('categorias.index') }}">
                                        <span>Categorias</span>
                                    </a>
                                </li>
                                <li class="{{ ($current_route == 'horario_salidas') ? 'actual' : '' }}">
                                    <a href="{{ route('horario_salidas.index') }}">
                                        <span>Hora de salidas</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="hover escuela sub_menu {{ ($current_route == 'all_student' or $current_route == 'profesores.index' or $current_route == 'grupos.index' or $current_route == 'all_payment' or $current_route == 'all_period' or $current_route == 'all_monthly_payment') ? 'actual' : '' }}">
                            <a href="#" class="sb_mn">
                                <span>Escuela</span>
                                <span class="glyphicon glyphicon-menu-down icon"></span>
                            </a>
                            <ul class="">
                                <li class="{{ ($current_route == 'all_student') ? 'actual' : '' }}">
                                    <a href="{{ route('all_student') }}">
                                        <span>Estudiantes</span>
                                    </a>
                                </li>
                                <li class="{{ ($current_route == 'profesores.index') ? 'actual' : '' }}">
                                    <a href="{{ route('profesores.index') }}">
                                        <span>Profesores</span>
                                    </a>
                                </li>
                                <li class="{{ ($current_route == 'grupos.index') ? 'actual' : '' }}">
                                    <a href="{{ route('grupos.index') }}">
                                        <span>Grupos</span>
                                    </a>
                                </li>
                                <li class="{{ ($current_route == 'all_payment') ? 'actual' : '' }}">
                                    <a href="{{ route('all_payment') }}">
                                        <span>Pagos</span>
                                    </a>
                                </li>
                                <li class="{{ ($current_route == 'all_period') ? 'actual' : '' }}">
                                    <a href="{{ route('all_period') }}">
                                        <span>Periodos</span>
                                    </a>
                                </li>
                                <li class="{{ ($current_route == 'all_monthly_payment') ? 'actual' : '' }}">
                                    <a href="{{ route('all_monthly_payment') }}">
                                        <span>Mensualidades</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="hover">
                            <a href="/" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span>Salir</span>
                            </a>
                            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                             {{ csrf_field() }}
                         </form>
                        </li>
                    </ul>
                </div>
@endsection('menu_root')