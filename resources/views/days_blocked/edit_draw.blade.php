@extends('layouts.app')

@section('content')

<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Sorteo</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('sorteos.update', array('user_id' => Auth::user()->id, 'draw_id' => $draw->id ) ) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}

				<fieldset>
					@if ($errors->has('start_date'))
					<div class="input_error">
						<span>{{ $errors->first('start_date') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="textinput">Fecha de inicio</label>
						<div class="col-md-6">
							<input type="text" class="date date_draw form-control input-md" placeholder="Fecha de inicio:" name="start_date" id="" date="" value="$draw->start_date">
						</div>
					</div>

					@if ($errors->has('draw_day'))
					<div class="input_error">
						<span>{{ $errors->first('draw_day') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="textinput">Fecha de sorteo</label>
						<div class="col-md-6">
							<input type="text" class="date date_draw form-control input-md" placeholder="Fecha final:" name="draw_date" id="" date="" value="$draw->draw_date">
						</div>
					</div>


					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
						<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
					</div>
				</fieldset>
			</form>

		</div>
	</div>
</div>


@endsection
