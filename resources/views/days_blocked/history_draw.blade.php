@extends('layouts.app')

@section('content')

<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Historial de Sorteos</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif

		<div class="table-responsive success tabla sorteos history">
			<ul>
				<?php

				setlocale(LC_TIME, '');
				$meses = [];
				$mes_esp = [
					'01' => 'Enero',
					'02' => 'Febrero',
					'03' => 'Marzo',
					'04' => 'Abril',
					'05' => 'Mayo',
					'06' => 'Junio',
					'07' => 'Julio',
					'08' => 'Agosto',
					'09' => 'Septiembre',
					'10' => 'Octubre',
					'11' => 'Noviembre',
					'12' => 'Diciembre',
				]
				?>
				@foreach($draws as $mes)
					@if(!in_array(date("m", strtotime($mes->draw_date)), $meses))
						<?php array_push($meses, date("m", strtotime($mes->draw_date))); ?>
						<li class="li_mes">
							<a href="#" class="a_mes" onclick="sorteo_dropdown($(this)); return false;">
								<div class="icon">
									<i class="glyphicon glyphicon-menu-right"></i>
								</div>
								<div class="text">
									<span>
										{{  $mes_esp[date("m", strtotime($mes->draw_date))] }}
									</span>
								</div>
							</a>
							<ul>
								@foreach($draws as $draw)
									@if($meses[count($meses)-1] == date("m", strtotime($draw->draw_date)))
										<li class="li_draw">
											<a href="#" class="a_draw" onclick="sorteo_dropdown($(this)); return false;">
												<div class="icon">
													<i class="glyphicon glyphicon-menu-right"></i>
												</div>
												<div class="text">
													<span>
													{{ Carbon\Carbon::parse($draw->draw_date)->format('d-m-Y') }}
													</span>
												</div>
											</a>
											<ul>
												@foreach($draw->day_draws as $day_draw)
													@if(count($day_draw->list_drawns) != 0)
														<li class="li_date">
															<a href="#" class="a_date" onclick="sorteo_dropdown($(this)); return false;">
																<div class="icon">
																	<i class="glyphicon glyphicon-menu-right"></i>
																</div>
																<div class="text">
																	<span>

																		{{ Carbon\Carbon::parse($day_draw->date)->formatLocalized('%A, %d de %B del %Y') }}
																	</span>
																</div>
															</a>
															<ul>
																@foreach($day_draw->list_drawns as $drawn)
																	<li class="li_hour">
																		<a href="#" class="a_hour" onclick="sorteo_dropdown($(this)); return false;">
																			<div class="icon">
																				<i class="glyphicon glyphicon-menu-right"></i>
																			</div>
																			<div class="text">
																				<span>
																					{{$drawn->reservation->start_time}}
																				</span>
																			</div>
																		</a>
																		<ul class="datos">
																		@foreach($drawn->reservation->members as $member)
																			<li>
																				<span style="font-weight: bolder;">{{$member->name}} {{$member->last_name }}</span>
																			</li>
																		@endforeach
																		@foreach($drawn->reservation->partners as $partner)
																			<li>
																				<span>{{$partner->name}} {{$partner->last_name }}</span>
																			</li>
																		@endforeach
																		</ul>
																	</li>
																@endforeach
															</ul>
														</li>
													@endif
												@endforeach

											</ul>
										</li>

									@endif
								@endforeach

							</ul>
						</li>

					@endif
				@endforeach


				<!-- @foreach($draws as $mes)
					@if(!in_array(date("m", strtotime($mes->draw_date)), $meses))
						<?php array_push($meses, date("m", strtotime($mes->draw_date))); ?>
						<li class="li_mes">
							<a href="#" class="a_mes" onclick="sorteo_dropdown($(this)); return false;">
								<div class="icon">
									<i class="glyphicon glyphicon-menu-right"></i>
								</div>
								<div class="text">
									<span>
										{{  $mes_esp[date("m", strtotime($mes->draw_date))] }}
									</span>
								</div>
							</a>
							<ul>
								@foreach($draws as $draw)
									@if($meses[count($meses)-1] == date("m", strtotime($draw->draw_date)))
										@foreach($draw->day_draws as $day_draw)

											<li class="li_draw">
												<a href="#" class="a_draw" onclick="sorteo_dropdown($(this)); return false;">
													<div class="icon">
														<i class="glyphicon glyphicon-menu-right"></i>
													</div>
													<div class="text">
														<span>

														{{ Carbon\Carbon::parse($day_draw->date)->format('d-m-Y') }}
														</span>
													</div>
												</a>
												<ul>
													<li class="li_date">
														<a href="#" class="a_date" onclick="sorteo_dropdown($(this)); return false;">
															<div class="icon">
																<i class="glyphicon glyphicon-menu-right"></i>
															</div>
															<div class="text">
																<span>
																	{{ Carbon\Carbon::parse($day_draw->date)->format('D d-m-Y') }}
																</span>
															</div>
														</a>
														<ul>
															@foreach($day_draw->list_drawns as $drawn)
																<li class="li_hour">
																	<a href="#" class="a_hour" onclick="sorteo_dropdown($(this)); return false;">
																		<div class="icon">
																			<i class="glyphicon glyphicon-menu-right"></i>
																		</div>
																		<div class="text">
																			<span>
																				{{$drawn->reservation->start_time}}
																			</span>
																		</div>
																	</a>
																	<ul class="datos">
																	@foreach($drawn->reservation->members as $member)
																		<li>
																			<span>{{$member->name}} {{$member->last_name }}</span>
																		</li>
																	@endforeach
																	@foreach($drawn->reservation->partners as $partner)
																		<li>
																			<span>{{$partner->name}} {{$partner->last_name }}</span>
																		</li>
																	@endforeach
																	</ul>
																</li>
															@endforeach
														</ul>
													</li>
												</ul>
											</li>

										@endforeach

									@endif
								@endforeach
							</ul>
						</li>
					@endif
				@endforeach -->
			</ul>

		</div>
	</div>

</div>
@endsection
