@extends('layouts.app')
@section('content')
<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Grupos</h2>
        </div>
        @if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif

         <div class="table-responsive success tabla">
             <div class="agregar_registro">
                 <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
             </div>

            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Profesor</th>
                        <th class="opciones">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach( $groups as $group)
                        <tr>
                            <td>{{ $group->name }}</td>
                            <td>{{ $group->teacher->name.' '.$group->teacher->last_name}}</td>
                            <td>
                                <a href="{{ route('grupos.show', array('user_id' =>  Auth::user()->id, 'group_id' =>  $group->id)) }}" class="btn btn-primary singlebutton1 group_show" style="padding-right: 10px;">
                                    <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                </a>
                                <a href="{{ route('grupos.edit', array( 'group_id' => $group->id, 'user_id' => Auth::user()->id )) }}" class="btn btn-primary singlebutton1">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                </a>
                                <button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('grupos.destroy', array( 'group_id' => $group->id, 'user_id' => Auth::user()->id ))}}" style="padding-right: 10px;">
                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">

                        </td>
                    </tr>
                </tfoot>

            </table>


        </div>
    </div>


<!--MODAL AGREGAR GRUPO-->
    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Grupo</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal inhabilitar" action="{{route('grupos.store', array('user_id' => Auth::user()->id))}}" method="POST">
                        {{csrf_field()}}
                        <fieldset>
                            <!-- Form Name -->
                            

                            @if ($errors->has('name'))
                                <div class="input_error">
                                    <span>{{$errors->first('name')}}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Nombre</label>
                                <div class="col-md-6">
                                    <input id="textinput" required name="name" type="text" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" >
                                </div>
                            </div>


                            @if ($errors->has('teacher_id'))
                            <div class="input_error">
                                <span>{{$errors->first('teacher_id')}}</span>
                            </div>
                            @endif

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Profesor</label>
                                <div class="col-md-6">
                                    <select required name="teacher_id" class="form-control input-md">
                                        <option value="" disabled selected>Seleccione Profesor</option>
                                            @foreach ($teachers as $teacher)
                                                <option value="{{$teacher->id}}" >{{$teacher->name.' '.$teacher->last_name}}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>



                            <!-- Button -->
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <!--MODAL PARA ELIMINAR-->
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar este Grupo?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal inhabilitar" method="POST">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                        <fieldset>
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--MODAL PARA DETALLE-->
    @if(Auth::user()->role_id == 3)
    <div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Grupo</h4>
				</div>
					<div class="modal-body form">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<p id="d_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Profesor</label>
								<div class="col-md-6">
									<p id="d_teacher"></p>
								</div>
							</div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="name">Correo del Profesor</label>
                                <div class="col-md-6">
                                    <p id="d_email"></p>
                                </div>
                            </div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection
