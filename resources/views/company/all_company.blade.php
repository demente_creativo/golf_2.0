@extends('layouts.app')
@section('content')



<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Compañías</h2>
        </div>
        <div class="tabla_head">
            <div class="tabla_divider add">
            
                <button name="" type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>


            </div>
            <div class="tabla_divider search">
                <div class="buscar">
                    <!-- <form class="" action="" method="post">
                        <button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
                        <input id="search" name="q" type="text" placeholder="" class="form-control input-md">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </form> -->
                </div>
            </div>
        </div>
        <div class="table-responsive success tabla">
            {!! $table_companies->render() !!}
        </div>

    </div>


<!--MODAL AGREGAR Compañia-->

    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Compañía</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal inhabilitar" action="{{ route('store_company') }}" method="POST">
                        {{ csrf_field() }}

                        <fieldset>
                            <!-- Form Name -->
                            

                            @if ($errors->has('name'))
                                <div class="input_error">
                                    <span>{{ $errors->first('name') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Nombre</label>
                                <div class="col-md-4">
                                    <input id="textinput" required name="name" type="text" placeholder="Nombre" class="form-control input-md">
                                </div>
                            </div>

                            @if ($errors->has('phone'))
                                <div class="input_error">
                                    <span>{{ $errors->first('phone') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Teléfono</label>
                                <div class="col-md-4">
                                    <input id="textinput" required name="phone" type="text" placeholder="Teléfono" class="form-control input-md phone" min="11">
                                </div>
                            </div>

                            @if ($errors->has('rif'))
                                <div class="input_error">
                                    <span>{{ $errors->first('rif') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Rif</label>
                                <div class="col-md-4">
                                    <input id="textinput" required name="rif" type="text" placeholder="Rif" class="form-control input-md cedula" min="7">
                                </div>
                            </div>


                            @if ($errors->has('address'))
                                <div class="input_error">
                                    <span>{{ $errors->first('address') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Dirección </label>
                                <div class="col-md-4">
                                    <textarea required name="address" placeholder="Dirección " class="form
                                     input-md"></textarea>
                                </div>
                            </div>

                            @if ($errors->has('tax'))
                                <div class="input_error">
                                    <span>{{ $errors->first('tax') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Impuesto</label>
                                <div class="col-md-4">
                                    <input id="textinput" required name="tax" type="text" placeholder="Impuesto" class="form-control input-md cedula" min="7">
                                </div>
                            </div>


                            <!-- Button -->
                            <div class="form-group boton">

                                <button id="guardar" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Enviar</button>

                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--FIN DEL MODAL AGREGAR COMPAÑIA-->


    <!--MODAL ELIMINAR COMPAÑIA-->
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar esta Compañia?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal inhabilitar" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <fieldset>
                            <div class="form-group boton">
                                <button id="eliminar" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
