@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Profesores</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif

		<!--COMIENZO DE LA TABLA-->

		 <div class="table-responsive success tabla">
			 <div class="agregar_registro">
				 <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
			 </div>

			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Teléfono</th>
						<th>Correo</th>
						<th class="opciones">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $teachers as $teacher)
						<tr>
							<td>{{ $teacher->name }}</td>
							<td>{{ $teacher->last_name }}</td>
							<td>{{ $teacher->phone }}</td>
							<td>{{ $teacher->email }}</td>
							<td>

								<a href="{{route('profesores.show', array('user_id' =>  Auth::user()->id, 'teacher_id' =>  $teacher->id))}}" class="btn btn-primary singlebutton1 teacher_show" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
								</a>
								<a href="{{ route('profesores.edit', array('user_id' => Auth::user()->id, 'teacher_id' => $teacher->id)) }}" class="btn btn-primary singlebutton1">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('profesores.destroy', array('user_id' => Auth::user()->id, 'teacher_id' => $teacher->id))}}" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">

						</td>
					</tr>
				</tfoot>

			</table>

		</div>
	</div>


	@if(Auth::user()->role_id == 3)
<!--MODAL AGREGAR PROFESOR-->
	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Profesor</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal inhabilitar" action="{{route('profesores.store', array('user_id' => Auth::user()->id))}}" method="POST">
						{{csrf_field()}}
						<fieldset>
							<!-- Form Name -->
							

							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{$errors->first('name')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Nombre</label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
								</div>
							</div>

							@if ($errors->has('last_name'))
								<div class="input_error">
									<span>{{$errors->first('last_name')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Apellido</label>
								<div class="col-md-6">
									<input id="last_name" name="last_name" type="text" placeholder="Apellido" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
								</div>
							</div>

							@if ($errors->has('phone'))
								<div class="input_error">
									<span>{{$errors->first('phone')}}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Teléfono</label>
								<div class="col-md-6">
									<input id="phone" name="phone" type="text" placeholder="Teléfono" class="phone form-control input-md" required title="Formato: 0414 888-88-88" pattern=".{5,14}" title="de 5 a 14 caracteres">
								</div>
							</div>

							@if ($errors->has('email'))
								<div class="input_error">
									<span>{{$errors->first('email')}}</span>
								</div>
							@endif

							<div class="form-group">
								<label class="col-md-4 control-label" for="email">Correo</label>
								<div class="col-md-6 inpt">
									<input id="email" name="email" type="email" placeholder="Correo" class="form-control input-md chequear_correo" checkear="{{ route('teachers.validate_email_teacher') }}" required pattern=".{3,50}" title="de 3 a 50 caracteres">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
											<div class="mensaje">
												<p>
												</p>
											</div>
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>

							<!-- Button -->
							<div class="form-group boton">
								<button id="" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Enviar</button>
								<button id="" name="singlebutton" type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>


	<!--MODAL PARA ELIMINAR-->
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar este Profesor?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal inhabilitar" method="POST">
						{{csrf_field()}}
						{{method_field('DELETE')}}
						<fieldset>
							<div class="form-group boton">
								<button id="" name="singlebutton" type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button id="" name="singlebutton" type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Detalle de Profesor</h4>
				</div>
					<div class="modal-body form">
						<fieldset>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<p id="d_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Apellido</label>
								<div class="col-md-6">
									<p id="d_last_name"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Teléfono</label>
								<div class="col-md-6">
									<p id="d_phone"></p>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Correo</label>
								<div class="col-md-6">
									<p id="d_email"></p>
								</div>
							</div>
						</fieldset>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection
