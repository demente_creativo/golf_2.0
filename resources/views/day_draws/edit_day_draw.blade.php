@extends('layouts.app')

@section('content')

<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Dia Sorteable</h3>
			@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
			@endif
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('dias_sorteables.update', array('user_id' => Auth::user()->id, 'day_draw_id' => $day_draw->id ) ) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}

				<fieldset>
					<!-- Form Name -->
					

					<div class="drawing">
						<div class="divider">
							<div class="drawing_days_calendar"></div>

						</div>
						<div class="divider">
							<ul class="dias">
								<li id="{{ $day_draw->date }}" class="active">
					                <div class="head">
					                    <div class="icon">
					                        <a href="#" class="select_date" onclick="select_date($(this)); return false;">
					                            <i class="glyphicon glyphicon-menu-right"></i>
					                        </a>
					                    </div>
					                    <span>'+ dia_completo +'</span>
					                    <div class="remove">
					                        <a href="#" class="remove_date" onclick="remove_date($(this)); return false;" date="'+$.datepicker.formatDate('yy.mm.dd', f)+'">
					                            <i class="glyphicon glyphicon-remove"></i>
					                        </a>
					                    </div>
					                </div>
					                <!-- <div class="cont">
					                    <ul class="horas">

					            for (var i = start_time.getHours(); i < end_time.getHours()+1; i++) {
					                var hora = new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }).split(' ')[0];
					                var meridiano = new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }).split(' ')[1];
					                // console.log(meridiano);
					                // array[i]
					                            <li class="li_hour" date="'+date+'">
					                                <div class="hour">
					                                <a href="#" class="select_hour" onclick="select_hour($(this)); return false;" date="'+date+'">
					                                    <span>'+ new Date(date+' '+i+':00:00').toLocaleString('en-US', { hour: 'numeric', hour12: true }) +'</span>
					                                </a>

					                //                     <input type="checkbox" name="hour" value="" id="'+fecha+'_'+hora+'_'+meridiano+'" class="select_hour" onclick="select_hour($(this)); return false;">
					                                </div>
					                                <div class="checkbox_">
					                for (var min = 0; min < 6; min++) {
					                                      <div class="check">
					                                          <input type="checkbox" name="hour[]" value="'+i+':'+min+'0:00" checked date="'+date+'" onclick="select_min($(this));">
					                                          <span>'+hora+':'+min+'0 '+meridiano+'</span>
					                                      </div>
					                }

					                                </div>
					                            </li>
					            }

					                    </ul>
					                </div> -->
					            </li>
							</ul>
						</div>
					</div>
					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
						<a href="{{route('dias_sorteables.index', array('user_id' => Auth::user()->id ) ) }}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>


@endsection
