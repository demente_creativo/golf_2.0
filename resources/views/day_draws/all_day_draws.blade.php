@extends('layouts.app')

@section('content')

<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Dias Sorteables</h2>
        </div>
        @if (session('status'))
        <div class="alerta">
            <div class="alert alert-{{ session('type') }}">
                {{session('status')}}.
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        </div>
        @endif
        <div class="tabla_head">
            <div class="tabla_divider add">
                <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
            </div>
            <div class="tabla_divider search">
                <!-- <div class="buscar">
                    <form class="" action="" method="post">
                        <button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
                        <input id="search_partners" name="q" type="text" placeholder="" class="form-control input-md">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </form>
                </div> -->
            </div>
        </div>
        <div class="table-responsive success tabla">
            {!! $table_day_draws->render() !!}
        </div>
    </div>


    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog" id="modal_draw_add">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Dia Sorteable</h4>
                </div>
                <div class="modal-body form">
                    <!-- <form class="form-horizontal" action="{{ route('dias_sorteables.store', array( 'user_id' => Auth::user()->id ) ) }}" method="POST" id="store_draw"> -->
                        <!-- {{ csrf_field() }} -->
                        <fieldset>
                            <!-- Form Name -->
                            

                            <div class="drawing">
                                <div class="divider">
                                    <div class="drawing_days_calendar"></div>

                                </div>
                                <div class="divider">
                                    <ul class="dias">

                                    </ul>
                                </div>
                            </div>

                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1" id="button_drawing" ruta="{{ route('dias_sorteables.store', array( 'user_id' => Auth::user()->id )) }}">Enviar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalverificar" role="dialog">
        <div class="modal-dialog" id="">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Enviar</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" action="{{ route('dias_sorteables.store', array( 'user_id' => Auth::user()->id ) ) }}" method="POST" id="store_draw">
                        {{ csrf_field() }}
                        <input type="hidden" name="day_draws" value="" id="day_draws" required>
                        <fieldset>
                            <div class="show_dias">

                            </div>
                            <!-- <div class="drawing">
                                <div class="divider">
                                    <div class="drawing_days_calendar"></div>

                                </div>
                                <div class="divider">
                                    <ul class="dias">

                                    </ul>
                                </div>
                            </div> -->

                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1" id="button_drawing_store" ruta="{{ route('dias_sorteables.store', array( 'user_id' => Auth::user()->id )) }}">Enviar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar este dia sorteable?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <fieldset>
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
