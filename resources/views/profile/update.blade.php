@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Actualizar Contraseña</h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<div class="form">
				<form class="form-horizontal" action="/perfil/reset" method="POST">
					{{ csrf_field() }}
					{{ method_field('POST') }}

					<fieldset>
						<!-- Form Name -->
						

						<div class="form-group">
							<label class="col-md-4 control-label" for="old">Contraseña Actual</label>
							<div class="col-md-6">
								<input id="name" name="old" type="password" value="" class="form-control input-md" autocomplete="off" minlength="6" maxlength="15">
							</div>
						</div>

					
						<div class="form-group">
							<label class="col-md-4 control-label" for="new">Contraseña Nueva</label>
							<div class="col-md-6">
								<input id="email" name="new" type="password" value=""  class="form-control input-md" autocomplete="off" minlength="6" maxlength="15">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="conf">Repetir Nueva</label>
							<div class="col-md-6">
								<input id="email" name="conf" type="password" value="" class="form-control input-md" autocomplete="off" minlength="6" maxlength="15" >
							</div>
						</div>

						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('usuarios.index')}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
