@extends('layouts.app')

@section('content')

<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Green Fee</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="table-responsive success tabla menos_1">
			<div class="agregar_registro">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
			</div>

			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Monto</th>
						<th class="opciones">Opciones</th>
						<th>Estatus</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $reservation_definitions as $reservation_definition)
						<tr>
							<td>{{ $reservation_definition->name }}</td>
							<td>{{ number_format($reservation_definition->green_fee, 2, ',', '.') }} BsF</td>
							<td>
								<a href="{{'/green/'.Auth::user()->id.'/'.$reservation_definition->id}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
								<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{'/green/'.$reservation_definition->id}}" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
								</button>
							</td>
							<td>
								<input type="checkbox" {{ ($reservation_definition->enabled == 1) ? 'checked' : '' }} class="toggle-event" data-item="{{$reservation_definition->id}}"  data-toggle="toggle" data-route="/socios/change_status/" data-on="Activo" data-off="Inactivo">
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="4">

						</td>
					</tr>
				</tfoot>

			</table>

		</div>
	</div>

</div>

<div class="modal fade" id="modalcreate" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Agregar Green Fee</h4>
            </div>
            <div class="modal-body form">
                <form class="form-horizontal" action="{{ route('definicion_reservas.store', array('user_id' => Auth::user()->id) ) }}" method="POST">
                    {{ csrf_field() }}
                    <fieldset>
                        <!-- Form Name -->
                        

                        @if ($errors->has('name'))
                            <div class="input_error">
                                <span>{{ $errors->first('name') }}</span>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Nombre</label>
                            <div class="col-md-6">
                                <input id="textinput"  name="name" type="text" required="required" placeholder="Nombre" class="form-control input-md">
                            </div>
                        </div>

                        @if ($errors->has('green_fee'))
                            <div class="input_error">
                                <span>{{ $errors->first('green_fee') }}</span>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Monto</label>
                            <div class="col-md-6">
                                <input id="textinput" required="required" name="green_fee" type="text" placeholder="Monto"  class="form-control input-md money_green_feed">
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group boton">
                            <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                            <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="modaldelete" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">¿Seguro que desea eliminar este Green Fee?</h4>
			</div>
			<div class="modal-body form">
				<form class="form-horizontal"  method="GET">
					{{ csrf_field() }}
					<fieldset>
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection
