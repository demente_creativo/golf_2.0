@extends('layouts.app')
@section('content')

    <form class="form-horizontal" action="{{ route( 'definicion_reservas', array( 'user_id' => Auth::user()->id ) ) }}"  method="POST">
        {{ csrf_field() }}

      @if ($errors->has('name'))
      <div class="input_error">
        <span>{{ $errors->first('name') }}</span>
      </div>
      @endif
      <div class="form-group">
        <label class="col-md-4 control-label" for="textinput">Nombre</label>  
        <div class="col-md-4">
        <input id="textinput" name="name" type="text" placeholder="Nombre" class="form-control input-md">
        </div>
      </div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label" for="singlebutton">Enviar</label>
        <div class="col-md-4">
          <button id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
        </div>
      </div>

      </fieldset>
    </form>

@endsection