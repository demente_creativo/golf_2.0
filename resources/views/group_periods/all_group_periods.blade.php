@extends('layouts.app')
@section('content')
<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Periodos</h2>
        </div>
        <div class="tabla_head">
            <div class="tabla_divider add">
                @if(Auth::user()->role_id == 3)
                    <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
                @endif

            </div>
            <div class="tabla_divider search">
                <div class="buscar">
                    <!-- <form class="" action="" method="post">
                        <button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
                        <input id="search" name="q" type="text" placeholder="" class="form-control input-md">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </form> -->
                </div>
            </div>
        </div>


        <div class="table-responsive success tabla {{ (Auth::user()->role_id == 3 ) ? 'menos_2' : '' }}">
            {!! $table_periods->render() !!}
        </div>

    </div>

    @if(Auth::user()->role_id == 3)

        <!-- MODAL AGREGAR PERIODO -->
        <div class="modal fade" id="modalcreate" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Agregar Periodo</h4>
                    </div>
                    <div class="modal-body form">
                        <form class="form-horizontal inhabilitar" action="{{ route('periodos.store', array('user_id' => Auth::user()->id)) }}" method="POST">
                            {{ csrf_field() }}
                            <fieldset>
                                <!-- Form Name -->
                                

                                @if($errors->has('date_start'))
                                <div class="input_error">
                                    <span>{{ $errors->first('date_start') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Fecha Inicial
                                    </label>
                                    <div class="col-md-4">
                                        <input type="datetimepicker" id="inicial" required class=" textbox" name="date_start" >
                                    </div>
                                </div>

                                @if($errors->has('date_end'))
                                <div class="input_error">
                                    <span>{{ $errors->first('date_end') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Fecha Final
                                    </label>
                                    <div class="col-md-4">
                                        <input type="datetimepicker" id="final" disabled required class=" textbox" name="date_end" >
                                    </div>
                                </div>


                                @if ($errors->has('name'))
                                <div class="input_error">
                                    <span>{{ $errors->first('name') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Nombre</label>
                                    <div class="col-md-4">
                                        <input id="textinput" required name="name" type="text" placeholder="Nombre" class="form-control input-md">
                                    </div>
                                </div>

                                @if ($errors->has('price_inscription'))
                                <div class="input_error">
                                    <span>{{ $errors->first('price_inscription') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Precio de Inscripción</label>
                                    <div class="col-md-4">
                                        <input id="textinput" required name="price_inscription" type="number"  class="form-control input-md">
                                    </div>
                                </div>

                                @if ($errors->has('group_id'))
                                <div class="input_error">
                                    <span>{{ $errors->first('group_id') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Grupos</label>
                                    <div class="col-md-4">
                                        <select required name="group_id" class="form-control input-md">
                                            <option value="" disabled selected>Seleccione Grupo</option>
                                            @foreach ($groups as $group)
                                            <option value="{{$group->id}}" >{{$group->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <!-- Button -->
                                <div class="form-group boton">
                                    <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                                    <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="modaldelete" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">¿Seguro que desea eliminar este Periodo?</h4>
                    </div>
                    <div class="modal-body form">
                        <form class="form-horizontal inhabilitar" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <fieldset>
                                <div class="form-group boton">
                                    <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                    <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@endsection
