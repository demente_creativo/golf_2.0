@extends('layouts.app')

@section('content')

<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Invitados</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<!-- <div class="tabla_head">
			<div class="tabla_divider add">
			</div>
			<div class="tabla_divider search">
				<div class="buscar">
				</div>
			</div>
		</div> -->
		<div class="table-responsive success tabla menos_1">
			<div class="agregar_registro">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Cédula</th>
						<th>Correo Electronico</th>
						<th class="opciones" id="kdkd">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $partners as $partner)
						<tr>
							<td>{{ $partner->name }}</td>
							<td>{{ $partner->last_name }}</td>
							<td>{{ $partner->identity_card }}</td>
							<td>{{ $partner->email }}</td>
							<td>
								<a href="{{route('s_invitados.edit', array('user_id' => Auth::user()->id, 's_invitado' => $partner->id))}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
								</a>
							</td>
						</tr>

					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">

						</td>
					</tr>
				</tfoot>
			</table>

		</div>
	</div>


	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Invitado</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal form_validation" action="{{ route('s_invitados.store', array('user_id' => Auth::user()->id)) }}" method="POST">
						{{ csrf_field() }}
						<fieldset>
							<!-- Form Name -->
							

							@if ($errors->has('name'))
								<div class="input_error">
									<span>{{ $errors->first('name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="name">Nombre</label>
								<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Nombre" class="form-control input-md" required>
								</div>
							</div>

							@if ($errors->has('last_name'))
								<div class="input_error">
									<span>{{ $errors->first('last_name') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="last_name">Apellido</label>
								<div class="col-md-6">
									<input id="last_name" name="last_name" type="text" placeholder="Apellido" class="form-control input-md" required>
								</div>
							</div>

							@if ($errors->has('identity_card'))
								<div class="input_error">
									<span>{{ $errors->first('identity_card') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="identity_card">Cédula</label>
								<div class="col-md-6 inpt">
									<input id="identity_card" name="identity_card" type="text" placeholder="Cédula" class="cedula form-control input-md chequear_cedula" required checkear="{{ route('socios.validate_identity_card_member') }}">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>

							@if ($errors->has('sex'))
								<div class="input_error">
									<span>{{ $errors->first('sex') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="sex">Sexo</label>
								<div class="col-md-6">
									<select name="sex" class="form-control input-md" required>
										<option value="Femenino">Femenino</option>
										<option value="Masculino">Masculino</option>
									</select>
								</div>
							</div>

							@if ($errors->has('phone'))
								<div class="input_error">
									<span>{{ $errors->first('phone') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="textinput">Teléfono</label>
								<div class="col-md-6">
									<input id="textinput" name="phone" type="text" placeholder="Teléfono" class="phone form-control input-md" required>
								</div>
							</div>

							@if ($errors->has('email'))
								<div class="input_error">
									<span>{{ $errors->first('email') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="email">Email</label>
								<div class="col-md-6 inpt">
									<input id="email" name="email" type="email" placeholder="Email" class="form-control input-md chequear_correo" required checkear="{{ route('socios.validate_email_member') }}">
									<div class="chequear">
										<div class="icon check">
											<img src="{{ asset('img/icons/check.png') }}" alt="">
										</div>
										<div class="icon error">
											<img src="{{ asset('img/icons/error.png') }}" alt="">
										</div>
										<div class="icon cargando">
											<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
										</div>
									</div>
								</div>
							</div>

							<!-- @if ($errors->has('handicap'))
								<div class="input_error">
									<span>{{ $errors->first('handicap') }}</span>
								</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="handicap">Handicap</label>
								<div class="col-md-6">
									<input id="handicap" name="handicap" type="text" placeholder="handicap" class="form-control input-md handican">
								</div>
							</div> -->


							@if ($errors->has('club_id'))
							<div class="input_error">
								<span>{{ $errors->first('club_id') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-4 control-label" for="club_id">Club</label>
								<div class="col-md-6">
									<select name="club_id" class="form-control input-md" required>
										<option value="" disabled selected>Seleccione club</option>

										@foreach($clubes as $club)
											<option value="{{ $club->id }}">{{ $club->name }}</option>
										@endforeach
									</select>
								</div>
							</div>

							<!-- Button -->
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar este invitado?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var user_id = {{ Auth::user()->id }};
</script>
@endsection
