@extends('layouts.app')

@section('content')

<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Reservaciones</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="table-responsive success tabla {{ (Auth::user()->role_id == 6) ? 'menos_2' : '' }} margin_none">
			<table class="table">
				<thead>
					<tr>
						<th>Fecha</th>
						<th>Hora</th>
						<th>Participantes</th>
						<th>Estatus</th>
						<th class="opciones">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $reservations as $reservation)
						<tr>
							<td>{{ $reservation->date }}</td>
							<td>{{ $reservation->start_time }}</td>
							<td>
								<strong>Socios</strong><br>
								@foreach($reservation->members as $member)
									{{$member->name}} {{$member->last_name}} C.I: {{$member->identity_card}} <br>
								@endforeach
								@if( count($reservation->partners) > 0 )
									<strong>Invitados</strong><br>
									@foreach($reservation->partners as $partner)
										{{$partner->name}} {{$partner->last_name}} C.I: {{$partner->identity_card}}<br>
									@endforeach
								@endif
							</td>
							<td>{{$reservation->status}}</td>
							<td>
								@if( $reservation->status == 'Aprobada' OR $reservation->status == 'Sorteable' AND count($reservation->created_at) >= Carbon\Carbon::now()->format('Y-m-d'))
									<a href="{{route('s_reservaciones.edit', array('user_id' => Auth::user()->id, 'reservation_id' => $reservation->id))}}" class="btn btn-primary singlebutton1" style="padding-right: 10px;">
										<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
									</a>
								@else
									<a href="#" class="btn btn-primary singlebutton1" style="padding-right: 10px;" disabled>
										<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
									</a>
								@endif
								@if( $reservation->status != 'Pagada')
									<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="{{route('s_reservaciones.destroy', array('user_id' =>  Auth::user()->id, 'reservation_id' =>  $reservation->id))}}" style="padding-right: 10px;">
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								@else
									<button id="" name="" type="button" class="btn btn-primary singlebutton1" style="padding-right: 10px;" disabled>
										<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
									</button>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="5">

						</td>
					</tr>
				</tfoot>
			</table>

		</div>
	</div>

</div>

@if( Auth::user()->role_id == 6 )
	<div class="modal" id="modaldelete" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">¿Seguro que desea eliminar esta reservación?</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" method="POST">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<fieldset>
							<div class="form-group boton">
								<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
								<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
							</div>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
@endif


@endsection
