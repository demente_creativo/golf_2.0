@extends('layouts.app')

@section('content')

<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Sorteos</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
        <div class="tabla_head">
            <div class="tabla_divider add">
                <!-- <form class="" action="" method="post" id="search_date">
                    <input id="date" name="date" type="text" placeholder="" class="form-control input-md datatimepicker1">
                    <button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
                    <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                </form> -->
            </div>
            <div class="tabla_divider search">
                <!-- <div class="buscar">
                    <form class="" action="" method="post">
                        <button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
                        <input id="search_partners" name="q" type="text" placeholder="" class="form-control input-md">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </form>
                </div> -->
            </div>
        </div>
		<div class="table-responsive success tabla" id="drawign">
		</div>
	</div>

</div>

@endsection
