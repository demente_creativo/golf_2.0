@extends('layouts.app')

@section('content')
	<div id="contenido" class="container white">
		<form class="form-horizontal" style="color: white;" action="{{ route('store_reservation') }}" method="POST">
				
			{{ csrf_field() }}
			<fieldset>

			<!-- Form Name -->
			<legend style="color: white;">Reserva</legend>

			@if ($errors->has('name'))
			<div class="input_error">
				<span>{{ $errors->first('resevation_definition_id') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="name">Reservación</label>	
				<div class="col-md-4">
					<select	name="reservation_definition_id" class="form-control input-md">
						<?php if ( isset($reservation_definitions) ) { ?>
							<?php foreach($reservation_definitions as $key => $reservation_definition) { ?>
								<option value="{{$reservation_definition->id}}" @if($key == 0) selected @endif ">
									{{$reservation_definition->name}}
								</option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>

			@if ($errors->has('receipt_payment'))
			<div class="input_error">
				<span>{{ $errors->first('receipt_payment') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="receipt_payment">Recibo de pago</label>	
				<div class="col-md-4">
					<input id="receipt_payment" name="receipt_payment" value="{{ old('receipt_payment') }}" type="text" placeholder="Recibo de pago" class="form-control input-md">
				</div>
			</div>

			@if ($errors->has('detail'))
			<div class="input_error">
				<span>{{ $errors->first('detail') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="detail">Detalle</label>	
				<div class="col-md-4">
					<textarea id="detail" name="detail" value="{{ old('detail') }}" placeholder="Detalle" rows="8" cols="80" class="form-control input-md"></textarea>
				</div>
			</div>

			@if ($errors->has('date'))
			<div class="input_error">
				<span>{{ $errors->first('date') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="date">Fecha</label>	
				<div class="col-md-4">
					<input type="datetimepicker" class="datetimepicker1 textbox" placeholder="Fecha:" name="date" >
					<input type="datetimepicker" class="datetimepicker2 textbox" placeholder="Hora:" name="start_time">
				</div>
			</div>

			@if ($errors->has('processed'))
			<div class="input_error">
				<span>{{ $errors->first('processed') }}</span>
			</div>
			@endif
			<div class="form-group">
				<label class="col-md-4 control-label" for="processed">Procesado</label>	
				<div class="col-md-4">
					<input id="processed" name="processed" type="checkbox" placeholder="Procesado" class="form-control input-md">
				</div>
			</div>
			

			<!-- Button -->
			<div class="form-group">
				<label class="col-md-4 control-label" for="singlebutton">Enviar</label>
				<div class="col-md-4">
					
					<button id="singlebutton" name="singlebutton" class="btn btn-primary">Enviar</button>
				</div>
			</div>

			</fieldset>
		</form>
	</div>
@endsection