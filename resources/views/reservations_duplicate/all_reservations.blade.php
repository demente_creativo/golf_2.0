@extends('layouts.app')

@section('content')

<div class="container">
	<div class="titulo">
		<h2 class="">Reservaciones</h2>
	</div>
	<div class="tabla_">
		<div class="tabla_head">
			<div class="tabla_divider add">
				<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
			</div>
			<div class="tabla_divider search">
				<div class="buscar">
					<!-- <form class="" action="" method="post">
						<button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
						<input id="search" name="q" type="text" placeholder="" class="form-control input-md">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
					</form> -->
				</div>
			</div>
		</div>
		<div class="table-responsive success tabla">
			<table class="table">
				<thead>
					<tr>
						<th data-field="country">Nombre</th>
						<th data-field="country">Detalle</th>
						<th data-field="country">Horario</th>
						<th width="100px">Editar</th>
						<th width="100px">Eliminar</th>
					</tr>
				</thead>
				<tbody>
					@foreach($reservations as $reservation)
					<tr id="{{$reservation->reservation_id}}">
						<td>{{$reservation->reservation_definition->name}}</td>
						<td>{{$reservation->detail}} </td>
						<td>{{$reservation->start_time}} </td>

						<td>
							<a href="{{ route('edit_reservation', $reservation->id) }}" class="btn btn-primary singlebutton1">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
							</a>
						</td>

						<?php
							echo "<td onclick= \"modal_activate('".
							route( "destroy_reservation", $reservation->id ).
							"' , '#modaldelete')\" >";
							?>
							<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete">
								<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
							</button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</div>
<div class="modal fade" id="modalcreate" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Agregar Socio</h4>
			</div>
			<div class="modal-body form">
				<form class="form-horizontal" action="{{ route('store_reservation') }}" method="POST">
					{{ csrf_field() }}
					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('name'))
						<div class="input_error">
							<span>{{ $errors->first('resevation_definition_id') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Reservación</label>
							<div class="col-md-6">
								<select	name="reservation_definition_id" class="form-control input-md">
									<?php if ( isset($reservation_definitions) ) { ?>
										<?php foreach($reservation_definitions as $key => $reservation_definition) { ?>
											<option value="{{$reservation_definition->id}}" @if($key == 0) selected @endif >
												{{$reservation_definition->name}}
											</option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
						</div>

						@if ($errors->has('receipt_payment'))
						<div class="input_error">
							<span>{{ $errors->first('receipt_payment') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="receipt_payment">Recibo de pago</label>
							<div class="col-md-6">
								<input id="receipt_payment" name="receipt_payment" value="{{ old('receipt_payment') }}" type="text" placeholder="Recibo de pago" class="form-control input-md">
							</div>
						</div>

						@if ($errors->has('detail'))
						<div class="input_error">
							<span>{{ $errors->first('detail') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="detail">Detalle</label>
							<div class="col-md-6">
								<textarea id="detail" name="detail" value="{{ old('detail') }}" placeholder="Detalle" rows="8" cols="60" class="form-control input-md" style="resize: none;"></textarea>
							</div>
						</div>

						@if ($errors->has('date'))
						<div class="input_error">
							<span>{{ $errors->first('date') }}</span>
						</div>
						@endif
						<div class="form-group fecha">
							<label class="col-md-4 control-label" for="date">Fecha</label>
							<div class="col-md-6">
								<input type="datetimepicker" class="datetimepicker1 textbox" placeholder="Fecha:" name="date" >
								<input type="datetimepicker" class="datetimepicker2 textbox" placeholder="Hora:" name="start_time">
							</div>
						</div>

						@if ($errors->has('processed'))
						<div class="input_error">
							<span>{{ $errors->first('processed') }}</span>
						</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="processed">Procesado</label>
							<div class="col-md-1">
								<input id="processed" name="processed" type="checkbox" placeholder="Procesado" class="form-control input-md">
							</div>
						</div>


						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="modal" id="modaldelete" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">¿Seguro que desea eliminar este socio?</h4>
			</div>
			<div class="modal-body form">
				<form class="form-horizontal" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<fieldset>
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
							<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection
