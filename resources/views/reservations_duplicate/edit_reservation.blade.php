@extends('layouts.app')

@section('content')

<div class="container">
	<div class="edit">
		<div class="modal-header">
			<h3 class="modal-title">Editar Reservacion</h3>
		</div>
		<div class="form">
			<form class="form-horizontal" action="{{ route('update_reservation', $reservation->id) }}" method="POST">
				{{ csrf_field() }}
				{{ method_field('PUT') }}

				<fieldset>
					<!-- Form Name -->
					

					@if ($errors->has('name'))
					<div class="input_error">
						<span>{{ $errors->first('resevation_definition_id') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="name">Reservación</label>
						<div class="col-md-6">
							<select	name="reservation_definition_id" class="form-control input-md">
								@if(isset($reservation_definitions))
									@foreach($reservation_definitions as $key => $reservation_definition)
										<option value="{{$reservation_definition->id}}" @if( ($reservation->reservation_definition_id == $reservation_definition->id) || ($key == 0) ) echo("select") @endif ">
											{{$reservation_definition->name}}
										</option>
									@endforeach
								@endif
							</select>
						</div>
					</div>
					@if ($errors->has('receipt_payment'))
					<div class="input_error">
						<span>{{ $errors->first('receipt_payment') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="receipt_payment">Recibo de pago</label>
						<div class="col-md-6">
							<input id="receipt_payment" name="receipt_payment" value="{{ $reservation->receipt_payment }}" type="text" placeholder="Recibo de pago" class="form-control input-md">
						</div>
					</div>

					@if ($errors->has('detail'))
					<div class="input_error">
						<span>{{ $errors->first('detail') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="detail">Detalle</label>
						<div class="col-md-6">
							<textarea id="detail" name="detail" value="{{ $reservation->detail }}" placeholder="Detalle" rows="8" cols="80" class="form-control input-md">{{$reservation->detail}}</textarea>
						</div>
					</div>

					@if ($errors->has('date'))
					<div class="input_error">
						<span>{{ $errors->first('date') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="date">Fecha</label>
						<div class="col-md-6">
							<input type="datetimepicker" class="datetimepicker1 textbox" placeholder="Fecha:" name="date" value="{{ $reservation->date}}" >
							<input type="datetimepicker" class="datetimepicker2 textbox" placeholder="Hora:" name="start_time" value="{{ $reservation->start_time}}" >
						</div>
					</div>

					@if ($errors->has('processed'))
					<div class="input_error">
						<span>{{ $errors->first('processed') }}</span>
					</div>
					@endif
					<div class="form-group">
						<label class="col-md-4 control-label" for="processed">Procesado</label>
						<div class="col-md-6">
							<input id="processed" name="processed" type="checkbox" placeholder="Procesado" @if ($reservation->processed === 1) checked @endif class="form-control input-md">
						</div>
					</div>



					<!-- Button -->
					<div class="form-group boton">
						<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

						<a href="{{route('all_reservations')}}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>



@endsection
