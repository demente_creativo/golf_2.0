@extends('layouts.app')


@section('content')
<div class="table-responsive col-sm-12 col-md-10 departamento _1" style="margin-top: 70px; margin-left: 100px;">
  <center>
    <table  class="table table-responsive">
      <tr>
        <th colspan="8" class="title">
          <img src="../img/icons/fecha.png">
          <input type="datetimepicker" class="datetimepicker1 textbox" placeholder="Fecha:" name="" >
        </th>
      </tr>
        <tr class="encabezado">
          <th>
            Codig
          </th>
          <th>
            Fecha
          </th>
          <th>
            Cliente/Responsable
          </th>
          <th>
            Correo
          </th>
          <th>
            Telefono
          </th>
          <th>
            Recibo
          </th>
          <th>
            Procesar
          </th>
          <th>
            Detalle
          </th>
        </tr>
      <tbody id="contReservas">
        
              <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td class="procesar_" id="">
                  <button type="button" class="btn btn-primary">Procesar</button>
                </td>
                <td>
                  <a href="#" >
                    <button type="button" class="btn btn-primary">Buscar</button>
                  </a>
                </td>
              </tr>
      </tbody>
    </table>
  </center>
</div>
@endsection