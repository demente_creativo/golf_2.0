@extends('layouts.app')
@section('content')
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Inscripciones</h2>
		</div>
		@if (session('validations'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					@foreach (session('validations') as $m)
						{{$m}}<br/>
					@endforeach
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif
		<div class="table-responsive success tabla menos_1 {{(Auth::user()->role_id == 3) ? 'menos_1' : ''}}">
			<div class="agregar_registro">
				<a class="btn btn-primary singlebutton1" href="{{route('inscripciones.create', array('user_id' => Auth::user()->id))}}"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
			</div>
			<table class="table">
				<thead>
					<tr>
						<th>Estudiante</th>
						@if(Auth::user()->role_id == 3)
							<th>Socio</th>
						@endif
						<th>Grupo</th>
						<th>Plan de Pago</th>
						<th class="opciones">Opciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach( $registrations as $registration)
						<tr>
							<td>{{ (isset($registration->student)) ? $registration->student->name.' '.$registration->student->last_name : '' }}</td>
							@if(Auth::user()->role_id == 3)
								<td>{{ (isset($registration->student) && isset($registration->student->member)) ? $registration->student->member->name.' '.$registration->student->member->last_name : '-' }}</td>
							@endif
							<td>{{ $registration->group->name }}</td>
							<td> {{ $registration->payment_plan->name }}</td>
							<td>
								<a href="{{route('inscripciones.show', array('user_id' =>  Auth::user()->id, 'registration_id' =>  $registration->id))}}" class="btn btn-primary singlebutton1 registration_show" style="padding-right: 10px;">
									<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="{{ (Auth::user()->role_id == 3) ? 5 : 4 }}">

						</td>
					</tr>
				</tfoot>
			</table>

		</div>

	</div>

	@if(Auth::user()->role_id == 3)

<!--MODAL AGREGAR INSCRIPCION-->
	<div class="modal fade" id="modalcreate" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Agregar Inscripción</h4>
				</div>
				<div class="modal-body form">
					<form class="form-horizontal" action="{{route('inscripciones.store', array('user_id' => Auth::user()->id))}}" method="POST">
						{{csrf_field()}}
						<fieldset>
							<!-- Form Name -->

								@if ($errors->has('payment_plan_id'))
									<div class="input_error">
										<span>{{$errors->first('payment_plan_id')}}</span>
									</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Plan de pago
									</label>
									<div class="col-md-4">
										<select name="payment_plan_id" class="form-control input-md">
											<option value="" disabled selected>Seleccione un Plan de pago</option>
												@foreach ($payment_plans as $payment_plan)
											<option value="{{$payment_plan->id}}">{{$payment_plan->name}}</option>
												@endforeach
										</select>
									</div>
								</div>

								@if ($errors->has('group_id'))
									<div class="input_error">
										<span>{{$errors->first('group_id')}}</span>
									</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Group
									</label>
									<div class="col-md-4">
										<select name="group_id" class="form-control input-md">
											<option value="" disabled selected>Seleccione un Group</option>
												@foreach ($groups as $group)
											<option value="{{$group->id}}">{{$group->name}}</option>
												@endforeach
										</select>
									</div>
								</div>

								@if(Auth::user()->role_id == 3)
									@if ($errors->has('member_id'))
										<div class="input_error">
											<span>{{$errors->first('member_id')}}</span>
										</div>
									@endif
									<div class="form-group">
										<label class="col-md-4 control-label" for="textinput">Socio
										</label>
										<div class="col-md-4">
											<select name="member_id" class="form-control input-md">
												<option value="" disabled selected>Seleccione un Socio</option>
													@foreach ($members as $member)
														<option value="{{$member->id}}">{{$member->name}} {{$member->last_name}}</option>
													@endforeach
											</select>
										</div>
									</div>
								@endif

								@if ($errors->has('student_id'))
									<div class="input_error">
										<span>{{$errors->first('student_id')}}</span>
									</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="student_id">Alumno
									</label>
									<div class="col-md-4">
										<select id="student_id" name="student_id" class="disabled form-control input-md">
											<option value="" disabled selected>Seleccione un Alumno</option>
											<!-- @if(Auth::user()->role_id == 6)
												@foreach ($students as $student)
													<option value="{{$student->id}}">{{$student->name}}</option>
												@endforeach
											@endif
											@if(Auth::user()->role_id == 3)
												@foreach ($members[0]->students as $student)
													<option value="{{$student->id}}">{{$student->name}}</option>
												@endforeach
											@endif -->
												@foreach ($students as $student)
													<option value="{{$student->id}}">{{$student->name}}</option>
												@endforeach
										</select>
									</div>
								</div>

								@if ($errors->has('type_payment_id'))
									<div class="input_error">
										<span>{{ $errors->first('type_payment_id') }}</span>
									</div>
								@endif
								<div class="form-group">
									<label class="col-4 col-md-center control-label" for="type_payment_id">Tipo de pago
									</label>
									<div class="col-6 col-md-center">
										<select name="type_payment_id" class="form-control input-md" id="type_payment_id">
											<option value="" disabled selected>Seleccione tipo de pago</option>
											@foreach( $type_payments as $type_payment )
												<option value="{{ $type_payment->id }}"> {{ $type_payment->name }} </option>
											@endforeach
										</select>
									</div>
								</div>

								<!-- Button -->
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

	 	<!--MODAL PARA DETALLE-->
	    @if(Auth::user()->role_id == 3)
	    <div class="modal fade modaldetalles" id="modaldetalles" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Detalle de Inscripci&oacute;n</h4>
					</div>
						<div class="modal-body form">
							<fieldset>
								<div class="form-group">
									<label class="col-md-4 control-label" for="name">Estudiante</label>
									<div class="col-md-6">
										<p id="d_student"></p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="name">Socio</label>
									<div class="col-md-6">
										<p id="d_member"></p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="name">Grupo</label>
									<div class="col-md-6">
										<p id="d_group"></p>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-4 control-label" for="name">Plan de Pago</label>
									<div class="col-md-6">
										<p id="d_payment_plan"></p>
									</div>
								</div>
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		@endif

		<!--MODAL PARA ELIMINAR-->
		<div class="modal" id="modaldelete" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">¿Seguro que desea eliminar Inscripción?</h4>
					</div>
					<div class="modal-body form">
						<form class="form-horizontal" method="POST">
							{{csrf_field()}}
							{{method_field('DELETE')}}
							<fieldset>
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	@endif
</div>
@endsection
