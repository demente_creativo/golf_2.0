@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Pago</h3>
			</div>
			<div class="form">
				<form class="form-horizontal" action="{{route('planes.update', array('user_id' => Auth::user()->id, 'payment_plan_id' => $payment_plan->id))}}" method="POST">
					{{csrf_field()}}
					{{method_field('PUT')}}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('name'))
							<div class="input_error">
								<span>{{$errors->first('name')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="name">Nombre</label>
							<div class="col-md-6">
								<input id="name" name="name" type="text" value="{{$payment_plan->name}}" pattern=".{3,50}" title="de 3 a 50 caracteres" placeholder="Nombre" class="form-control input-md" required>
							</div>
						</div>

						@if ($errors->has('description'))
							<div class="input_error">
								<span>{{$errors->first('description')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="description">Descripción</label>
							<div class="col-md-6">
								<textarea style="resize: none;" rows="3" cols="1" id="description" name="description" type="text" placeholder="Descripción" class="form-control input-md" pattern=".{3,500}" title="de 3 a 500 caracteres" required>{{$payment_plan->description}}</textarea>
							</div>
						</div>

						@if ($errors->has('rate'))
							<div class="input_error">
								<span>{{$errors->first('rate')}}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="rate">Tarifa</label>
							<div class="col-md-6">
								<input id="rate" name="rate" type="text" value="{{$payment_plan->rate}}" placeholder="Tarifa" class="form-control money input-md" pattern=".{1,9}" title="máximo 9 caracteres" required>
							</div>
						</div>

						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
							<a href="{{route('planes.index', array('user_id' => Auth::user()->id))}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
