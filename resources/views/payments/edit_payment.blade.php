@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Editar Pago</h3>
			</div>
			<div class="form">
				<form class="form-horizontal" action="{{ route('update_payment', $payment->id) }}" method="POST">
					{{ csrf_field() }}
					{{ method_field('PUT') }}

					<fieldset>
						<!-- Form Name -->
						

						@if ($errors->has('concept'))
							<div class="input_error">
								<span>{{ $errors->first('concept') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Concepto</label>
							<div class="col-md-6">
								<input id="textinput" name="concept" type="text" value="{{ $payment->concept }}" placeholder="Concepto" class="form-control input-md">
							</div>
						</div>


						@if ($errors->has('period_id'))
							<div class="input_error">
								<span>{{ $errors->first('period_id') }}</span>
							</div>
						@endif
						<div class="form-group">
							<label class="col-md-4 control-label" for="textinput">Periodo</label>
							<div class="col-md-6">
								<select name="teacher_id" class="form-control input-md">

                                 	 <option value="{{ $payment->period_id}}" disabled selected>Seleccione Periodo</option>
                                        @foreach ($periods as $period)
                                             <option value="{{$period->id}}" {{ ($payment->period_id == $period->id) ? 'selected' : '' }}>{{$period->name}}</option>
                                        @endforeach
                                </select>
                            </div>
						</div>



						<!-- Button -->
						<div class="form-group boton">
							<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>

							<a href="{{route('all_payment')}}" class="btn btn-primary singlebutton1">
								Cancelar
							</a>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

@endsection
