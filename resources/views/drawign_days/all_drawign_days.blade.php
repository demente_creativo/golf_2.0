@extends('layouts.app')

@section('content')

<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Dias Sorteables</h2>
        </div>
        @if (session('status'))
        <div class="alerta">
            <div class="alert alert-{{ session('type') }}">
                {{session('status')}}.
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
        </div>
        @endif
        <div class="tabla_head">
            <div class="tabla_divider add">
                <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
            </div>
            <div class="tabla_divider search">
                <!-- <div class="buscar">
                    <form class="" action="" method="post">
                        <button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
                        <input id="search_partners" name="q" type="text" placeholder="" class="form-control input-md">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </form>
                </div> -->
            </div>
        </div>
        <div class="table-responsive success tabla">
            {!! $table_drawign_days->render() !!}
        </div>
    </div>


    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Dia Sorteable</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" action="{{ route('dias_sorteables.store') }}" method="POST">
                        {{ csrf_field() }}
                        <fieldset>
                            <!-- Form Name -->
                            

                            <div class="drawing">
                                <div class="divider">
                                    <div class="drawing_days_calendar"></div>

                                </div>
                                <div class="divider">
                                    <ul class="dias">
                                        <!-- <li id="Viernes_31_03_2017">
                                            <div class="head">
                                                <div class="icon">
                                                    <i class="glyphicon glyphicon-menu-right"></i>
                                                </div>
                                                <span>Viernes , 31 de Marzo Del 2017</span>
                                            </div>
                                            <div class="cont">
                                                <ul class="horas">
                                                    <li>
                                                        <div class="hour">
                                                            <input type="checkbox" name="hour" value="7:00 AM">
                                                            <span>7:00 AM</span>
                                                        </div>
                                                        <div class="checkbox_">
                                                          <div class="check">
                                                              <input type="checkbox" name="hour[]" value="7:00 AM" checked>
                                                              <span>7:00 AM</span>
                                                          </div>
                                                          <div class="check">
                                                              <input type="checkbox" name="hour[]" value="7:10 AM" checked>
                                                              <span>7:10 AM</span>
                                                          </div>
                                                          <div class="check">
                                                              <input type="checkbox" name="hour[]" value="7:20 AM" checked>
                                                              <span>7:20 AM</span>
                                                          </div>
                                                          <div class="check">
                                                              <input type="checkbox" name="hour[]" value="7:30 AM" checked>
                                                              <span>7:30 AM</span>
                                                          </div>
                                                          <div class="check">
                                                              <input type="checkbox" name="hour[]" value="7:40 AM" checked>
                                                              <span>7:40 AM</span>
                                                          </div>
                                                          <div class="check">
                                                              <input type="checkbox" name="hour[]" value="7:50 AM" checked>
                                                              <span>7:50 AM</span>
                                                          </div>
                                                        </div>
                                                    </li>

                                                </ul>
                                            </div>
                                        </li> -->

                                    </ul>

                                </div>

                            </div>

                            <!-- @if ($errors->has('date'))
                                <div class="input_error">
                                    <span>{{ $errors->first('date') }}</span>
                                </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Fecha</label>
                                <div class="col-md-6">
                                    <input type="text" class="drawing_days form-control input-md" placeholder="Fecha:" name="date" id="date" date="">
                                </div>
                            </div> -->
                            <!-- Button -->
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1" id="button_drawing">Enviar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modaldelete" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">¿Seguro que desea eliminar este dia sorteable?</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <fieldset>
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
