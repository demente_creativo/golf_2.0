@extends('layouts.app')
@section('content')
<div class="container">
    <div class="tabla_">
        <div class="titulo">
            <h2 class="">Pagos</h2>
        </div>
        <div class="tabla_head">
            <div class="tabla_divider add">
                @if( Auth::user()->role_id == 2 )
                    <button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
                @endif
            </div>
            <div class="tabla_divider search">
                <div class="buscar">
                    <form class="" action="" method="post">
                        <button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
                        <input id="search" name="q" type="text" placeholder="" class="form-control input-md">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </form>
                </div>
            </div>
        </div>
        <div class="table-responsive success tabla {{ ( Auth::user()->role_id == 3 ) ? 'menos_2' : '' }}">
            {!! $table_payments->render() !!}
        </div>

    </div>

    @if( Auth::user()->role_id == 2 )

<!--MODAL AGREGAR PAGO-->
    <div class="modal fade" id="modalcreate" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Pago</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" action="{{ route('pagos.store') }}" method="POST">
                        {{ csrf_field() }}
                        <fieldset>
                            <!-- Form Name -->
                            

                                @if ($errors->has('concept'))
                                <div class="input_error">
                                    <span>{{ $errors->first('concept') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Concepto</label>
                                    <div class="col-md-4">
                                        <input id="textinput" name="concept" type="text" placeholder="Concepto" class="form-control input-md">
                                    </div>
                                </div>

                                @if ($errors->has('period_id'))
                                <div class="input_error">
                                    <span>{{ $errors->first('period_id') }}</span>
                                </div>
                                @endif

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Periodo</label>
                                    <div class="col-md-4">
                                        <select name="period_id" class="form-control input-md">
                                            <option value="" disabled selected>Seleccione Periodo</option>
                                            @foreach ($periods as $period)
                                            <option value="{{$period->id}}" >{{$period->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>



                                <!-- Button -->
                                <div class="form-group boton">
                                    <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                                    <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!--MODAL PARA ELIMINAR-->
        <div class="modal" id="modaldelete" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">¿Seguro que desea eliminar este Pago?</h4>
                    </div>
                    <div class="modal-body form">
                        <form class="form-horizontal" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <fieldset>
                                <div class="form-group boton">
                                    <button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
                                    <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

@endsection
