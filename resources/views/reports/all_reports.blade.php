@extends('layouts.app')
@section('content')
	<div class="container">
		<div class="tabla_">
			<div class="titulo">
				<h2 class="">Reportes</h2>
			</div>
			<div class="tabla_head">
				<div class="tabla_divider add">
					@if( Auth::user()->role_id == 2 )
						<button type="button" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Reportes</button>
					@endif

				</div>
				<div class="tabla_divider search">
					<div class="buscar">
						<form class="" action="{{route('reportes.prueba')}}" method="get">
							<button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
							<select name="status" class="form-control input-md" required>
								<option value="null">status</option>
								<option value="En proceso">En proceso</option>
								<option value="Sorteable">Sorteable</option>
								<option value="Aprobada">Aprobada</option>
								<option value="Cancelada">Cancelada</option>
								<option value="Cerrada">Cerrada</option>
								<option value="Finalizada">Finalizada</option>
							</select>
							<span class="glyphicon glyphicon-search" aria-hidden="true"></span>

							@if ($errors->has('start_date'))
							<div class="input_error">
								<span>{{ $errors->first('start_date') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-6 control-label" for="start_date">Fecha de inicio</label>
								<div class="col-md-6">
									<input type="text" class="date date_draw form-control input-md" placeholder="Fecha de inicio:" name="start_date" id="start_date" date="">
								</div>
							</div>

							@if ($errors->has('end_date'))
							<div class="input_error">
								<span>{{ $errors->first('end_date') }}</span>
							</div>
							@endif
							<div class="form-group">
								<label class="col-md-6 control-label" for="end_date">Fecha de cierre</label>
								<div class="col-md-6">
									<input type="text" class="date date_draw form-control input-md" placeholder="Fecha de cierre:" name="end_date" id="end_date" date="">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		@if( Auth::user()->role_id == 2 )
		@endif

		@if( Auth::user()->role_id == 5 )
		@endif
	</div>

@endsection
