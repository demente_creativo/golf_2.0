@extends('layouts.app')

@section('content')

<div class="container" id="reportes">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Reportes</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="tabla_head">
			<div class="buscar">
				<form class="" action="{{route('reportes.prueba')}}" method="get">


					@if ($errors->has('status'))
					<div class="input_error">
						<span>{{ $errors->first('status') }}</span>
					</div>
					@endif
					<div class="form-group col-md-5">
						<label class=" control-label" for="status">Estatus</label>
						<select id="status" name="status" class="form-control input-md" required>
							<option value="null" {{ (empty($input) || (empty($input['status'])) || ($input['status'] == 'null')) ? 'selected' : '' }}>Estatus</option>
							<option value="En proceso" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'En proceso')) ? 'selected' : '' }}>En proceso</option>
							<option value="Sorteable" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Sorteable')) ? 'selected' : '' }}>Sorteable</option>
							<option value="Aprobada" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Aprobada')) ? 'selected' : '' }}>Aprobada</option>
							<option value="Cancelada" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Cancelada')) ? 'selected' : '' }}>Cancelada</option>
							<option value="Cerrada" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Cerrada')) ? 'selected' : '' }}>Cerrada</option>
							<option value="Finalizada" {{ (isset($input) && (isset($input['status'])) && ($input['status'] == 'Finalizada')) ? 'selected' : '' }}>Finalizada</option>
						</select>
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
					</div>

					@if ($errors->has('start_date'))
					<div class="input_error">
						<span>{{ $errors->first('start_date') }}</span>
					</div>
					@endif
					<div class="form-group col-md-3">
						<label class=" control-label" for="start_date">Fecha de inicio</label>
						<div class="">
							<input type="text" class="date date_draw form-control input-md" placeholder="Fecha de inicio:" name="start_date" id="start_date" date="" value="{{(isset($input['start_date'])) ? $input['start_date'] : ''}}">
						</div>
					</div>

					@if ($errors->has('end_date'))
					<div class="input_error">
						<span>{{ $errors->first('end_date') }}</span>
					</div>
					@endif
					<div class="form-group col-md-3">
						<label class=" control-label" for="end_date">Fecha de cierre</label>
						<div class="">
							<input type="text" class="date date_draw form-control input-md" placeholder="Fecha de cierre:" name="end_date" id="end_date" date="" value="{{(isset($input['end_date'])) ? $input['end_date'] : ''}}">
						</div>
					</div>
					<div class="col-md-1">
						<label class=" control-label" for="end_date"></label>

						<button type="submit" class="btn btn-primary singlebutton1" style="margin-top: 4px;">Buscar</button>

					</div>
				</form>
			</div>

		</div>
		<div class="table-responsive success tabla report {{ ((Auth::user()->role_id == 5) | (Auth::user()->role_id == 2)) ? 'menos_2' : '' }}">
			{!! $table_reservations->render() !!}
			<!--<table>
				<thead>
					<tr>
						<th>
							Fecha
						</th>
						<th>
							Hora
						</th>
						<th>
							Estado
						</th>
						<th>
							Participantes
						</th>
					</tr>
				</thead>
				@foreach($reservations as $reservation)
					<tr>
						<td>
							{{$reservation->date}}
						</td>
						<td>
							{{$reservation->start_time}}
						</td>
						<td>
							{{$reservation->status}}
						</td>
						<td>
						<?php
							$invitados = '';
							foreach($reservation->members as $member){

								$invitados .= '<strong>'.$member->name.' '.$member->last_name.' C.I: '.$member->identity_card.'<br></strong>';
							}

							foreach($reservation->partners as $partner){
								$invitados .= $partner->name.' '.$partner->last_name.' C.I: '.$partner->identity_card.'<br>';
							}
							echo($invitados);
						?>
						</td>
					</tr>
				@endforeach
			</table>-->
		</div>
		<!--<div>
			{{$reservations->appends(Illuminate\Support\Facades\Input::except('page'))->links()}}
		</div>-->
		<div>
			<a id="crear_reporte_pdf" type="button" href="{{route('reportes.crear_reporte_pdf')}}">Ver PDF</a>
			<a id="crear_reporte_xls" type="button" href="{{route('reportes.crear_reporte_xls')}}">Ver XML</a>
		</div>
	</div>

</div>

@if( Auth::user()->role_id == 2 )
@endif


@endsection
