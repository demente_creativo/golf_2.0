@php
use Carbon\Carbon;
setlocale(LC_TIME, 'Spanish');
	$start_date = Carbon::parse($tournament->start_date);
	$end_date = Carbon::parse($tournament->end_date);
	$start_date_inscription = Carbon::parse($tournament->start_date_inscription);
	$end_date_inscription = Carbon::parse($tournament->end_date_inscription);
@endphp
@extends('layouts.app')
@section('content')

<div class="container">
	<div class="edit inscripcion_torneos">
		<div class="torneo">
			<h1>
				{{ $tournament->name }}
			</h1>
			<div class="mod_cat">
				
				<div class="">
					<h3>Categoria</h3>
					@foreach( $tournament->categories as $category )
						<h4>* {{ $category->name }}</h4>
					@endforeach

				</div>
			</div>
			<div class="precio">
				<h1>
					Bsf {{ number_format($tournament->inscription_fee, 2,',','.') }}
				</h1>
			</div>
			<div class="fechas">
				<h3>Fechas del torneo</h3>
				<h4>Desde: {{ $start_date->formatLocalized('%A, %d de %B del %Y') }}</h4>
				<h4>Hasta: {{ $end_date->formatLocalized('%A, %d de %B del %Y') }}</h4>
			</div>
		</div>
		<div class="inscripcion">
			<div class="form">
				<form class="form-horizontal" action="{{ route('inscripcion_torneos.store', array( 'tournament_id' => $tournament->id ) ) }}" method="POST">
					{{ csrf_field() }}
					<input type="hidden" name="tournament_id" value="{{ $tournament->id }}">
					<div class="titulo">
						<h1>Datos de pago</h1>
					</div>
					@if ($errors->has('type_payment_id'))
						<div class="input_error">
							<span>{{ $errors->first('type_payment_id') }}</span>
						</div>
					@endif
				
					<div class="form-group">
						<label class="col-4 col-md-center control-label" for="type_payment_id">Tipo de pago
						</label>
						<div class="col-6 col-md-center">
							@foreach( $type_payments as $key => $type_payment )
									<div class="item center-block" style="display: inline-block" >
										<label for="{{$type_payment->id}}">
											<img src="{{asset('img/icons/'.$type_payment->name.'.png')}}" alt="" style="width: 60px">
										</label>
										
										<input class="payment-inscription" style="display: block;margin-left: 45%;" id="{{$type_payment->id}}" type="checkbox" name="payment_type" value="{{$type_payment->id}}"><br>
										<span style="display: block;">{{$type_payment->label}}</span>
									</div>

							@endforeach
						</div>
					</div>
					<div class="boton">
						<button type="submit" class="btn btn-primary singlebutton1">
							<span class="icon">
								<img src="{{ asset('img/icons/inscribirse.png') }}" alt="incribirse">
							</span>
							<span class="text">
								Inscribirse
							</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>




@endsection
