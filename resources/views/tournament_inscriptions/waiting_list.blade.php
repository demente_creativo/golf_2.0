@extends('layouts.app')
@section('content')
<div class="container">



	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Torneos</h2>
		</div>
		@if (session('status'))
			<div class="alerta">
				<div class="alert alert-{{ session('type') }}">
					{{session('status')}}.
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
			</div>
		@endif

		<div class="tabla_head">
			<div class="tabla_divider add">
				@if( Auth::user()->role_id == 2 )
					<button type="button" onclick="hacer_click('{{route('modalities.post')}}')" class="btn btn-primary singlebutton1 modalcreate"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar</button>
				@endif
			</div>
			<div class="tabla_divider search">
				<div class="buscar">
					<!-- <form class="" action="" method="post">
						<button type="submit" class="btn btn-primary singlebutton1">Buscar</button>
						<input id="search_tournament" name="q" type="text" placeholder="" class="form-control input-md">
						<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
					</form> -->
				</div>
			</div>
		</div>
		<div class="table-responsive success tabla tournament {{ (Auth::user()->role_id == 6) ? 'menos_2' : '' }}">
			{!! $table_tournaments->render() !!}
		</div>
	</div>


    @if( Auth::user()->role_id == 2 )
        <div class="modal fade" id="modalcreate" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Agregar Torneo</h4>
                    </div>
                    <div class="modal-body form">
                        <form class="form-horizontal" action="{{ route('torneos.store') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <fieldset>
                                @if ($errors->has('name'))
                                <div class="input_error">
                                    <span>{{ $errors->first('name') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Nombre</label>
                                    <div class="col-md-6">

                                        <input id="textinput" name="name" type="text" placeholder="Nombre" class="form-control input-md" required pattern="[A-Za-z0-9./,*' ]{3,100}" maxlength="100">

                                    </div>
                                </div>

                                @if ($errors->has('start_date'))
                                <div class="input_error">
                                    <span>{{ $errors->first('start_date') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Fecha de inicio</label>
                                    <div class="col-md-6">
                                        <input type="date"  placeholder="Fecha de inicio:" class="form-control input-md date_tournament" name="start_date" id="start_date_tournament" required>
                                    </div>
                                </div>

                                @if ($errors->has('end_date'))
                                <div class="input_error">
                                    <span>{{ $errors->first('end_date') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Fecha final</label>
                                    <div class="col-md-6">
                                        <input type="date"  placeholder="Fecha final:" class="form-control input-md" name="end_date" id="end_date_tournament" date=" " required>
                                    </div>
                                </div>

                                @if ($errors->has('inscription_fee'))
                                <div class="input_error">
                                    <span>{{ $errors->first('inscription_fee') }}</span>
                                </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Cuota de inscripción</label>
                                    <div class="col-md-6">
                                        <input id="" name="inscription_fee" type="text" placeholder="Cuota de inscripción" class="form-control input-md money" required>

                                    </div>
                                </div>

								@if ($errors->has('modality'))
								<div class="input_error">
									<span>{{ $errors->first('modality') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="name">Modalidad</label>
									<div class="col-md-6">
										<select required class="form-control" name="modality">
											<option value="" selected disabled>Seleccione modalidad</option>
                                            @foreach($modalities as $modality)
                                                <option value="{{ $modality->id }}">{{ $modality->name }}</option>
                                            @endforeach
										</select>
									</div>
								</div>

								@if ($errors->has('category_id'))
								<div class="input_error">
									<span>{{ $errors->first('category_id') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="name">Categoria</label>
									<div class="col-md-4">
										<select required multiple name="category_id[]" class="form-control input-md select_categories" id="category">
											<!-- <option value="" disabled selected>Seleccione</option> -->
										</select>
									</div>
									<div class="col-md-2">
										<button type="button" id="myBtn" class="btn btn-primary singlebutton1">Crear</button>
									</div>
								</div>

								@if ($errors->has('start_time'))
								<div class="input_error">
									<span>{{ $errors->first('start_time') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Hora de Inicio</label>
									<div class="col-md-6">
										<input required id="start_time" name="start_time" type="text" placeholder="Hora de Inicio" class="form-control input-md">
									</div>
								</div>

								@if ($errors->has('end_time'))
								<div class="input_error">
									<span>{{ $errors->first('end_time') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Hora Final</label>
									<div class="col-md-6">
										<input required id="end_time" name="end_time" type="text" placeholder="Hora Final" class="form-control input-md">
									</div>
								</div>

								@if ($errors->has('condition_file'))
								<div class="input_error">
									<span>{{ $errors->first('condition_file') }}</span>
								</div>
								@endif
								<div class="form-group">
									<label class="col-md-4 control-label" for="textinput">Condiciones de campeonatos</label>
									<div class="col-md-6">
										<input id="file_torunament" type="file" class="file" name="conditions_file">
									</div>

									<!-- <script type="text/javascript">
									console.log($('#file_torunament'));
								</script> -->

								</div>

                            @if ($errors->has('start_date_inscription'))
                            <div class="input_error">
                                <span>{{ $errors->first('start_date_inscription') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Fecha de inscripcion</label>
                                <div class="col-md-6">
                                    <input type="date"  placeholder="Fecha de inicio:" class="form-control input-md date_inscription" name="start_date_inscription" id="start_date_inscription" required>
                                </div>
                            </div>

                            @if ($errors->has('end_date_inscription'))
                            <div class="input_error">
                                <span>{{ $errors->first('end_date_inscription') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Fecha Tope inscripcion</label>
                                <div class="col-md-6">
                                    <input type="date"  placeholder="Fecha final:" class="form-control input-md" name="end_date_inscription" id="end_date_inscription" date=" " required>
                                </div>
                            </div>


								<!-- Button -->
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="modal" id="modaldelete" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">myBtn
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">¿Seguro que desea eliminar este torneo?</h4>
					</div>
					<div class="modal-body form">
						<form class="form-horizontal" method="POST">
							{{ csrf_field() }}
							{{ method_field('DELETE') }}
							<fieldset>
								<div class="form-group boton">
									<button type="submit" class="btn btn-primary singlebutton1">Eliminar</button>
									<button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	@endif
	<br>2
</div>

<!-- Modal para crear Nueva Categoria -->

	<div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Agregar Categoría</h4>
                </div>
                <div class="modal-body form">
                    <form class="form-horizontal" id="capture"	method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <fieldset>

                            @if ($errors->has('name'))
                            <div class="input_error">
                                <span>{{ $errors->first('name') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Nombre</label>
                                <div class="col-md-6">
                                    <input id="name" name="name" type="text" placeholder="Nombre de la Categoría" class="form-control input-md" required pattern="[A-Za-z0-9 ./*,-]{3,20}" maxlength="20">
                                </div>
                            </div>

                            @if ($errors->has('number_participants'))
                            <div class="input_error">
                                <span>{{ $errors->first('number_participants') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Cantidad de Participantes</label>
                                <div class="col-md-6">
                                    <input id="number_participants" name="number_participants" type="text" placeholder="Número de Participantes" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3">
                                </div>
                            </div>

                            @if ($errors->has('sex'))
                            <div class="input_error">
                                <span>{{ $errors->first('sex') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Género</label>
                                <div class="col-md-6">
                                    <select required class="form-control input-md" name="sex" id="sex">
                                        <option value="" disabled selected>Sexo</option>
                                        <option value="Masculino" >Masculino</option>
                                        <option value="Femenino" >Femenino</option>
                                        <option value="Mixto" >Mixto</option>
                                    </select>
                                </div>
                            </div>

                            @if ($errors->has('handicap_min'))
                            <div class="input_error">
                                <span>{{ $errors->first('handicap_min') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Handicap Mínimo</label>
                                <div class="col-md-6">
                                    <input id="handicap_min" name="handicap_min" type="text" placeholder="Rango Mínimo" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3">
                                </div>
                            </div>

                            @if ($errors->has('handicap_max'))
                            <div class="input_error">
                                <span>{{ $errors->first('handicap_max') }}</span>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="textinput">Handicap Máximo</label>
                                <div class="col-md-6">
                                    <input id="handicap_max" name="handicap_max" type="text" placeholder="Rango Máximo" class="form-control input-md" required pattern="[0-9]{1,3}" maxlength="3">
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group boton">
                                <button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
                                <button type="button" class="btn btn-primary singlebutton1" data-dismiss="modal">Cancelar</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
	</div>


@endsection
