@extends('layouts.app')

@section('content')
@php
use Carbon\Carbon;
setlocale(LC_TIME, 'Spanish');
@endphp
<div class="container">
	<div class="tabla_">
		<div class="titulo">
			<h2 class="">Listado de Inscripciones en Torneos</h2>
		</div>
		@if (session('status'))
		<div class="alerta">
			<div class="alert alert-{{ session('type') }}">
				{{session('status')}}.
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
		</div>
		@endif
		<div class="tabla_head head_reportes" style="">
				<div class="inputs" style=" display:inherit; padding:0;">
					<label class="" for="status">Torneo</label>
					<select id="tournament_id" name="tournament_id" class="form-control input-md" required>
					   <option value="">Seleccione</option>
						@foreach($tournaments as $tournament)
							<option value="{{$tournament->id}}" {{ (isset($input) && (isset($input['tournament_id'])) && ($input['tournament_id'] == $tournament->id)) ? 'selected' : '' }}>{{$tournament->name}}</option>
						@endforeach
					</select>
				</div>
		</div>

		<div class="tabla margin_none">
			<table class="table dataTable no-footer" id="DataTables_Table_0">
				<thead>
				  <tr>
				  	 <td>Torneo</td>
					  <td>Socio</td>

					  <td>Categoria</td>
					  <td>Fecha de inscripción</td>
					  <td>Fecha de torneo</td>
					  <td>Status</td>
				  </tr>

				</thead>
				<tbody id="ins_tbody">

					@foreach($inscriptions as $inscription)

						<tr>
							<td>{{$inscription->tournament->name}}</td>
							<td>{{$inscription->member->name}}</td>
							<td>{{($inscription->category) ? $inscription->category->name:  '-'}}</td>
							<td>{{date('d-m-Y',strtotime($inscription->created_at) )}}</td>
							<td>{{date('d-m-Y',strtotime($inscription->tournament->start_date))}}</td>
							<td>{{($inscription->waiting == 0) ? "Inscrito" : "Lista de espera"}}</td>

						</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td colspan="6">

						</td>
					</tr>
				</tfoot>
			</table>

		</div>
		@if(count($inscriptions) >= 1)
			<div class="bajar_reporte">
				<a id="crear_reporte_pdf" type="button" href="{{route('torneos.pdf_report_create_tournament_inscription')}}">
					<span class="icon">
						<img src="{{asset('img/icons/pdf.png')}}" alt="">
					</span>
				</a>
				<a id="crear_reporte_xls" type="button" href="{{route('torneos.xls_report_create_tournament_inscription')}}">
					<span class="icon">
						<img src="{{asset('img/icons/xls.png')}}" alt="">
					</span>
				</a>
			</div>
		@endif
	</div>

</div>

@if( Auth::user()->role_id == 2 )
@endif


@endsection
