<style type="text/css">
.header{
	width: 100%;
}
.header h2{
	margin: 0;
}
.fechas{
	width: 100%;
	/*background: rgb(69, 224, 45);*/
	text-align: center;
	position: relative;
}
.fecha{
	display: inline-block;
	font-size: 18px;
	margin-right: 15px;
	position: relative;
	/*left: 50%;*/
	/*background: rgb(142, 246, 121);*/
}
.fecha:nth-child(2){
	margin: 0 15px;
}



.table_reportes{
	width: 100%;
	border-spacing: 0px;
	border: none;
}
.table_reportes thead tr th,
.table_reportes tbody tr td{
	border: none;
	border-collapse: none;
}
.table_reportes thead tr th{
	border-left: solid 1px #fff;
	padding-left: 10px;
	text-align: left;
	border-top: solid 1px #000;

}
.table_reportes thead tr th.th_listado{
	border-bottom: 1px solid #fff;
}
.table_reportes thead tr th.th_fecha{
	width: 160px;
}


.table_reportes thead tr th.th_categoria{
	width: 150px;
}
.table_reportes thead tr th.th_socio{
	width: 200px;
}
.table_reportes thead tr th:first-child{
	border-left: solid 1px #000;
}
.table_reportes thead tr th:last-child{
	border-right: solid 1px #000;
}

.table_reportes tbody tr{
	border-bottom: solid 1px #000;
}
.table_reportes tbody tr td{
	border-left: solid 1px #000;
	padding: 5px 0 5px 10px;
    border-bottom: 1px solid #000;
}
.table_reportes tbody tr td:last-child{
	border-right: solid 1px #000;
}
.table_reportes tbody tr td {
	/*border-left: solid 1px #000;*/
}

#estadisticas{
	position: absolute;
	bottom: 0;
}
#footer {
	position: fixed;
	left: 0px;
	bottom: -100px;
	right: 0px;
	height: 100px;
	background-color: white;
	text-align: left;
}
#footer .page:after {
	content: counter(page);

	background-color: white;

}
@page {
	margin-top: 2cm;
	margin-bottom: 2cm;
	margin-left: 1cm;
	margin-right: 1cm;
}
</style>

<table class="header">
	<tr>
		<td>
			<h2>
				Sistema para Alquiler de Canchas de Golf <br>
				Lagunita Country Club <br>
				Listado de Inscripciones en Torneo<br>
				{{$tournament_inscriptions[0]->tournament->name}}
			</h2>
		</td>
		<td align="center">
			<img src="{{public_path('img/logo.png')}}" style="width: 110px;" img align=center alt="">
		</td>
	</tr>
</table>

<!-- <h2 class="title" align="center">
	Sistema para Alquiler de Canchas de Golf <br>
	Lagunita Country Club <br>
	Listado de Inscripciones en Torneo {{$tournament_inscriptions[0]->tournament->name}}<br>
	<?php
		// if (!empty($start_date) && !empty($end_date) ) {
		// 	if ($start_date == $end_date) {
		// 		echo "Fecha : ".$start_date;
		// 	} else {
		// 		echo "Desde : ".$start_date." Hasta : ".$end_date;
		// 	}
		// } else {
		// 	if (!empty($start_date)) {
		// 		echo "Desde : ".$start_date;
		// 	}
		// 	if (!empty($end_date)) {
		// 		echo "Hasta : ".$end_date;
		// 	}
		// }
	?><br>
	<?php
		// if (!empty($status) && $status != "null") {
		// 	echo "Estado : ".$status;
		// }
	?>
</h2> -->
<table class="fechas">

	<?php if (!empty($start_date) OR !empty($end_date) ): ?>
		<tr>
			<td align="center">
				<?php if (!empty($start_date) && !empty($end_date) ): ?>
					<?php if ($start_date == $end_date): ?>
						<div class="fecha">
							<strong>Fecha:</strong> <?php echo $start_date ?>
						</div>
					<?php else: ?>
						<div class="fecha">
							<strong>Desde:</strong> <?php echo $start_date ?>
						</div>
						<div class="fecha">
							<strong>Hasta:</strong> <?php echo $end_date ?>
						</div>
					<?php endif; ?>
				<?php else: ?>
					<?php if (!empty($start_date)): ?>
						<div class="fecha">
							<strong>Desde:</strong> <?php echo $start_date ?>
						</div>
					<?php else: ?>
						<div class="fecha">
							<strong>Hasta:</strong> <?php echo $end_date ?>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			</td>

		</tr>
	<?php endif; ?>
</table>

<table class="table table-striped table-bordered table_reportes" border="1">
	<thead style='background-color: rgb(27, 60, 123); color:#ffffff; text-align: center;'>
		<tr>
			<th colspan="4" style="height: 30px;" class="th_listado">LISTADO DE SOCIOS INSCRITOS</th>
		</tr>
	</thead>
	<thead style='background-color: rgb(27, 60, 123); color:#ffffff; text-align: center;'>
		<tr>
			<th style="height: 30px;" class="th_fecha">FECHA DE INSCRIPCIÓN</th>
			<th style="height: 30px;" class="th_categoria">CATEGORÍA</th>
			<th style="height: 30px;" class="th_socio">SOCIO</th>
			<th style="height: 30px;" class="">ESTADO DE INSCRIPCIÓN</th>
		</tr>
	</thead>
	<tbody style="text-align: left;">
		@foreach($tournament_inscriptions as $tournament_inscription)
			<tr>
				<td style="height: 40px;">{{$tournament_inscription->created_at}}</td>
				<td style="height: 40px;">{{strtoupper($tournament_inscription->category->name)}}</td>
				<td style="height: 40px;">{{strtoupper($tournament_inscription->member->name)}} {{strtoupper($tournament_inscription->member->last_name)}} <br> C.I: {{number_format($tournament_inscription->member->identity_card, 0,',','.')}}</td>
				<td style="height: 40px;">{{($tournament_inscription->waiting == 0) ? "INSCRITO"  : "LISTA DE ESPERA"}}</td>
			</tr>
		@endforeach
	</tbody>
</table>

<table>
	<div id="estadisticas" class="hidden">
		<p>
			<span>Nº TOTAL DE SOCIOS INSCRITOS: {{$totales}}</span><br>
			<span>Nº TOTAL DE SOCIOS INSCRITOS EN LOS TORNEOS: {{$cantidad_inscritos}}</span><br>
			<span>Nº TOTAL DE SOCIOS EN LISTA DE ESPERA DE LOS TORNEOS: {{$cantidad_espera}}</span><br>
		</p>
	</div>

</table>
<div id="footer" class="page">

	<span class="page"></span>

</div>
