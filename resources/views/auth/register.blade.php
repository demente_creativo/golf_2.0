@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Creación de Cuenta</h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
			<form class="form-horizontal" role="form" method="POST">
				{{ csrf_field() }}
				<div class="form-group">
					<label for="name" class="col-md-4 control-label">Nombre</label>
					<div class="col-md-6">
						<input id="name" name="name" placeholder="Nombre" type="text" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
						@if ($errors->has('name'))
							<span class="auth-alerta">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
				<div class="form-group">
					<label for="last_name" class="col-md-4 control-label">Apellido</label>
					<div class="col-md-6">
						<input id="last_name" name="last_name" placeholder="Apellido" type="text" class="form-control input-md" pattern=".{3,25}" title="de 3 a 25 caracteres" required>
						@if ($errors->has('last_name'))
							<span class="auth-alerta">
								<strong>{{ $errors->first('last_name') }}</strong>
							</span>
						@endif
					</div>
				</div>

				<div class="form-group">
				<label class="col-md-4 control-label" for="identity_card">Cédula</label>
					<div class="col-md-6 inpt">
						<input id="identity_card" name="identity_card" type="text" placeholder="Cédula" class="cedula form-control input-md chequear_cedula_registro" required checkear="{{ route('socios.validate_identity_card_member') }}" pattern=".{0,8}" title="máximo 8 caracteres">
						@if ($errors->has('identity_card'))
							<span class="auth-alerta">
								<strong>{{ $errors->first('identity_card') }}</strong>
							</span>
						@endif
						<div class="chequear">
							<div class="icon check">
								<img src="{{ asset('img/icons/check.png') }}" alt="">
							</div>
							<div class="icon error">
								<img src="{{ asset('img/icons/error.png') }}" alt="">
								<div class="mensaje">
									<p></p>
								</div>
							</div>
							<div class="icon cargando">
								<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
							</div>
						</div>
					</div>
				</div>
				
				<div class="form-group">
					<label for="sex" class="col-md-4 control-label">Sexo</label>
					<div class="col-md-6">
						<select required name="sex" class="form-control input-md" id="sex">
							<option value="" disabled selected>Seleccione</option>
							<option value="1">Femenino</option>
							<option value="2">Masculino</option>
						</select>
						@if ($errors->has('sex'))
							<span class="auth-alerta">
								<strong>{{ $errors->first('sex') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
				<div class="form-group">
					<label for="number_action" class="col-md-4 control-label">Nº acción</label>
					<div class="col-md-6">
						<input id="number_action" name="number_action" placeholder="Nº acción" type="text" class="number_action form-control input-md" pattern=".{5,20}" title="de 5 a 20 caracteres" required>
						@if ($errors->has('number_action'))
							<span class="auth-alerta">
								<strong>{{ $errors->first('number_action') }}</strong>
							</span>
						@endif
					</div>
				</div>
				
				<div class="form-group">
					<label for="phone" class="col-md-4 control-label">Teléfono</label>
					<div class="col-md-6">
						<input id="phone" name="phone" type="text" placeholder="Teléfono" class="phone form-control input-md" required title="Formato: 0414 888-88-88" pattern=".{5,14}" title="de 5 a 14 caracteres">
						@if ($errors->has('phone'))
							<span class="auth-alerta">
								<strong>{{ $errors->first('phone') }}</strong>
							</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="email">Correo</label>
					<div class="col-md-6 inpt">
						<input id="email" name="email" type="email" placeholder="Correo" class="form-control input-md chequear_correo" checkear="{{ route('socios.validate_email_member') }}" required pattern=".{3,50}" title="de 3 a 50 caracteres">
						@if ($errors->has('email'))
							<span class="auth-alerta">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
						<div class="chequear">
							<div class="icon check">
								<img src="{{ asset('img/icons/check.png') }}" alt="">
							</div>
							<div class="icon error">
								<img src="{{ asset('img/icons/error.png') }}" alt="">
								<div class="mensaje">
									<p>
									</p>
								</div>
							</div>
							<div class="icon cargando">
								<img src="{{ asset('img/icons/cargando.gif') }}" alt="">
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="password" class="col-md-4 control-label">Contraseña</label>
					<div class="col-md-6">
						<input id="password" type="password" placeholder="Contraseña" class="form-control" name="password" pattern=".{6,25}" title="de 6 a 25 caracteres" required>
						@if ($errors->has('password'))
							<span class="auth-alerta">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					<label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>
					<div class="col-md-6">
						<input id="password-confirm" type="password" placeholder="Confirmar Contraseña" class="form-control" name="password_confirmation" required>
						 @if ($errors->has('password_confirmation'))
                             <span class="auth-alerta">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                             </span>
                         @endif
					</div>
				</div>

				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="btn btn-primary">
							Registrar
						</button>
						<a href="{{route('login')}}" class="btn btn-primary singlebutton1">
							Cancelar
						</a>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
