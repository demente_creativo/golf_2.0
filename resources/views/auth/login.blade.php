@extends('layouts.app')

@section('content')



<div class="container">

  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <section class="login-form">
        <form method="post" action="{{ route('login') }}" role="login">
            {{ csrf_field() }}
              <img src="{{asset('img/logo.png')}}" class="img-responsive" alt="" />
              <!-- <input type="email" name="email" placeholder="Email" required class="form-control input-lg" value="joestudent@gmail.com" /> -->
                @if (session('status'))
                <div class="alerta">
                    <div class="alert alert-{{ session('type') }}">
                        {{session('status')}}.
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                </div>
                @endif
              <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                  <!-- <label for="email" class="col-md-4 control-label">Dirreccion de correo</label> -->

                  <div class="col-md-12">
                      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Correo">
                      @if ($errors->has('email'))
                          <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  <!-- <label for="password" class="col-md-4 control-label">Password</label> -->

                  <div class="col-md-12">
                      <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">

                      @if ($errors->has('password'))
                          <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                          </span>
                      @endif
                  </div>
              </div>
              <!-- <input type="password" class="form-control input-lg" id="password" placeholder="Password" required="" /> -->
              <div class="pwstrength_viewport_progress"></div>
              <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Iniciar Sesión</button>
              <a href="{{ route('forgot_password') }}">¿Olvidó su contraseña?</a></br>
              <a href="{{ route('get_register') }}">Registrarse</a>
              <!-- <div>
                <a href="{{ route('get_register') }}">Create account</a> or <a href="#">reset password</a>
              </div> -->

        </form>
      </section>
      </div>
      <div class="col-md-4"></div>


  </div>
</div>



@endsection
