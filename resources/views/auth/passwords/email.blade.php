@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Recordar Contraseña</h3>
				@if (session('status'))
				<div class="alerta">
					<div class="alert alert-{{ session('type') }}">
						{{session('status')}}.
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				</div>
				@endif
			</div>
          <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
          {{ csrf_field() }}
            <div class="form-group">
                <label for="email" class="col-md-4 control-label">Indicar Correo</label>
                <div class="col-md-6">
                    <input id="email" type="email" placeholder="Indicar Correo" class="form-control" name="email" value="{{ old('email') }}" required>
                    @if ($errors->has('email'))
                        <span class="auth-alerta">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <!-- Button -->
			<div class="form-group boton">
				<button type="submit" class="btn btn-primary singlebutton1">Enviar</button>
				<a href="{{route('login')}}" class="btn btn-primary singlebutton1">
					Cancelar
				</a>
			</div>
        </form>
    </div>
</div>              
@endsection
