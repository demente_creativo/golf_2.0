@extends('layouts.app')

@section('content')
<div class="container">
		<div class="edit">
			<div class="modal-header">
				<h3 class="modal-title">Reestablecer Contraseña</h3>
					@if (session('status'))
						<div class="alerta">
							<div class="alert alert-{{ session('type') }}">
								{{session('status')}}
								<button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
						</div>
						@endif
					</div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Confirmar Correo</label>
                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="Confirmar Correo" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="auth-alerta">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" placeholder="Contraseña" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="auth-alerta">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" placeholder="Confirmar Contraseña" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="auth-alerta">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Reestablecer
                                </button>
                                <a href="{{route('login')}}" class="btn btn-primary singlebutton1">
									Cancelar
								</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
