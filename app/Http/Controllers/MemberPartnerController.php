<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Mail;

use Golf\Club;
use Golf\Member;
use Golf\Partner;
use Golf\User;
use Gbrock\Table\Facades\Table;

/**
 * PartnerController for Member
 */

class MemberPartnerController extends Controller
{

	public function autocomplete(){

		if ( Auth::user()->role_id != 6 ) {
			return response()->json([
				'status' => 'error',
				'message' => 'Unauthorized'
			], 401);
		}

		$term = Input::get('term');

		// dd($term);

		if (empty($partner_ids)) {
			$partner_ids = array();
		}

		$results = array();

		$queries = Partner::where([
								['name', 'LIKE', '%'.$term.'%'],
								['user_id', '=', Auth::user()->id]
							])->orWhere([
								['last_name', 'LIKE', '%'.$term.'%'],
								['user_id', '=', Auth::user()->id]
							])->orWhere([
								['identity_card', 'LIKE', '%'.$term.'%'],
								['user_id', '=', Auth::user()->id]
							])->take(10)->get();

		foreach ($queries as $query)
		{
			$results[] = [
				'id' => $query->id,
				'name' => $query->name,
				'last_name' => $query->last_name,
				'identity_card' => $query->identity_card,
				'exonerated' => $query->exonerated,
				'value' => $query->name.' '.$query->last_name
			];
		}
		return response()->json($results);
	}

	public function index()
	{
		if ( Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}
		// dd()

		$clubes = Club::get();

		$partners = Partner::where([
								['user_id', '=', Auth::user()->id]
							])->sorted()->orderBy('created_at', 'desc')->get();


		return view('member_partners.all_partners', [
			'partners' => $partners,
			'clubes' => $clubes,
			] );
	}

	/**
	 * Display view to create a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		if ( Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}

		$clubes = Club::get();

		return view('member_partners.create_partner', [ 'clubes' => $clubes ] );
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{
		if ( Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}
		DB::beginTransaction();
		try {

			$partner = new Partner();
			$partner->name = ucwords(strtolower($request->name));
			$partner->last_name = ucwords(strtolower($request->last_name));
			$partner->identity_card = $request->identity_card;
			$partner->sex = $request->sex;
			$partner->email = $request->email;
			$partner->phone = $request->phone;
			$partner->user_id = Auth::user()->id;
			$partner->club_id = $request->club_id;
			if (!$partner->save()) {
				DB::rollback();
				return redirect()->route( 's_invitados.index', array('user_id' => Auth::user()->id) )
								->with( [ 'status' => 'Error al registrar invitado ', 'type' => 'success' ] )
								->withInput();
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route( 's_invitados.index', array('user_id' => Auth::user()->id))
							->with( [ 'status' => 'Error al registrar invitado 3', 'type' => 'error' ] )
							->withInput();
		}
		DB::commit();

		
		// Se retorna al listado de invitados con los valores guardados
		$parametros = array();
		$nombre_destinatario = ucwords($partner->name.' '.$partner->last_name);
		$email_destinatario = $partner->email;
		

		
			$parametros = [
				'nombre_socio' => ucwords(Auth::user()->member->name.' '.Auth::user()->member->last_name),
				'correo_socio' => Auth::user()->member->user->email,
				'nombre_destinatario' => $nombre_destinatario,
				'email_destinatario' => $email_destinatario
			];
		
		
		Mail::send('emails.registry_partner', $parametros, function ( $m ) use ( $email_destinatario, $nombre_destinatario  ) {

			$m->from(env('MAIL_USERNAME'), 'Reservación Lagunita');
			$m->to($email_destinatario, $nombre_destinatario)->subject('Invitación - Lagunita Contry Club');
		});
		return redirect()->route( 's_invitados.index', array('user_id' => Auth::user()->id) )
						->with( [ 'status' => 'Invitado se ha creado exitosamente', 'type' => 'success' ] );
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $user_id, $partner_id)
	{
		if ( Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}
		try {
			$partner = Partner::where('user_id', $user_id)->find($partner_id);
			$club = Club::find($partner->club_id);
			if ($partner->exonerated == null) {
				$partner->exonerated = 'No exonerado';
			}elseif ($partner->exonerated == 'Reservacion') {
				$partner->exonerated = 'Por reservación';
			}

			$partner->club = $club->name;
			//dd($partner);
			if (empty($partner)) {
				throw new \Exception("Error al obtener el recurso", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getCode();
			$response['line'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['partner' => $partner,
								'type' => 'success']);
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $user_id, $partner_id )
	{
		if ( Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}

		$clubes = Club::get();

		$partner = Partner::where([
							[ 'id', '=', $partner_id ]
						])->first();

		return view('member_partners.edit_partner', ['partner' => $partner, 'clubes' => $clubes ]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $partner_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $user_id, $partner_id)
	{
		if ( Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$partner = Partner::where([
								[ 'id', '=', $partner_id ]
							])->first();

			$partner->name = ucwords(strtolower($request->name));
			$partner->last_name = ucwords(strtolower($request->last_name));
			$partner->identity_card = $request->identity_card;
			$partner->sex = $request->sex;
			$partner->email = $request->email;
			$partner->phone = $request->phone;

			$name = $partner->name.' '.$partner->last_name;

			if (!$partner->save()) {
				return redirect()->route('edit_partner')
					->with(['status' => 'Error al actualizar los datos de:'.$name, 'type' => 'error'])
					->withInput();
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);

			return redirect()->route( 's_invitados.index', array('user_id' => Auth::user()->id))
							->with( [ 'status' => 'Error al actualizar invitado 3', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route('s_invitados.index', array('user_id' => Auth::user()->id))
			->with(['status' => 'Se ha actualizado satisfactoriamente de:'.$name, 'type' => 'success']);
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function delete( $user_id, $partner_id )
	{
		if ( Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}


		DB::beginTransaction();

		try {

			$partner = Partner::where([
								[ 'id', '=', $partner_id ]
							])->first();

			$name = $partner->name.' '.$partner->last_name;

			if (!$partner->delete()) {
				return redirect()->route('s_invitados.index', array('user_id' => Auth::user()->id))
					->with(['status' => 'Error al eliminar el invitado:'.$name, 'type' => 'error']);
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);

			return redirect()->route( 's_invitados.index', array('user_id' => Auth::user()->id))
							->with( [ 'status' => 'Error al eliminar invitado 3', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();

		return redirect()->route('s_invitados.index', array('user_id' => Auth::user()->id))
			->with(['status' => 'Se ha eliminado satisfactoriamente:'.$name, 'type' => 'success']);
	}


	public function alls(Request $request)
	{
		try
		{

			$partners = Partner::where([
					['user_id', '=', Auth::user()->id]
				])->get();

			foreach ($partners as $partner) {
				$reservation_quantity = $partner->reservations()->whereMonth('date','=', date('m'))->get(['date'])->toArray();
				$partner->reservated_dates = $reservation_quantity;
				$partner->authReservation = (count($reservation_quantity) >= 2 ) ? false : true;
				$partner->user = Auth::user()->role_id; 
				$partner->nombre = $partner->name. ' ' .$partner->last_name;
			}
		}
		catch(\Exception $e)
		{
			return response()->json( ['status' => false, 'message' => $e->getMessage() ] );
		}
		return response()->json( ['status' => true, 'response' => $partners] );
	}

}
