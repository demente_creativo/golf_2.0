<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use Illuminate\Http\Respons;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Golf\Reservation;
use Golf\Member;
use Golf\MemberReservation;
use Golf\PartnerReservation;
use Golf\StarterProcess;
use Golf\ReservationDefinition;
use Golf\CommentsReservation;
class StarterController extends Controller
{
	public function index()
	{
		if ( Auth::user()->role_id != 2 && Auth::user()->role_id != 5 ) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');
		$greens = ReservationDefinition::all();
		$members = Member::all();
		$reservations = Reservation::whereDate('date', Carbon::now()->toDateString())->with(['members','partners'])->orderBy('start_time','asc')->get();
		foreach($reservations as $reservation){
			foreach($reservation->partners as $partner){
				$payed = PartnerReservation::where('reservation_id', $partner->pivot->reservation_id)->where('partner_id',$partner->pivot->partner_id )->first();
				$partner->payed = $payed->payed;
			}
		}
		return view('starters.reservation_day', [
			'reservations' => $reservations,
			'greens' => $greens,
			'members' => $members,
			'validations' => $this->validations
			] );

	}


	public function status( Request $request )
	{

		if ( Auth::user()->role_id != 2 && Auth::user()->role_id != 5 ) {
			return redirect()->route('home');
		}

		try {
			$reservation = Reservation::find($request->reservation_id);

			if ($reservation) {
				$reservation->status = $request->status;
				$reservation->start_time = date('H:i:s', strtotime($request->start_time));
				//$reservation->hora_disfrutada = date( 'H:i', strtotime($request->time));
				if ($reservation->save()) {
					/*$starter_process = new StarterProcess();
					$starter_process->reservation_id = $request->reservation_id;
					$starter_process->time = date('H:i:s', strtotime($request->start_time));

					if ($starter_process->save()) {*/

						$comment = new CommentsReservation();
						$comment->reservation_id = $request->reservation_id;
						$comment->date = date('Y-m-d');
						$comment->time =   date('H:i:s',strtotime($request->start_time));

						$comment->comment = $request->comment;
						$comment->user_id = Auth::user()->id;
						$comment->save();
					//}

					//dd($starter_process);
				}else {
					throw new \Exception("Error al actualizar la reserva", 400);
				}
			} else {
				throw new \Exception("Error: no se encontró la reserva", 400);
			}
		} catch (\Exception $e) {
			return response()->json(['line' => $e->getLine(),
								'code' => $e->getCode(),
								'status' => $e->getMessage(),
								'type' => 'error']);
		}
		$results['response'] = true;
		return response()->json("Se ha actualizado la reserva exitosamente", 200);
	}


	public function change_hour(Request $request)
	{

		setlocale(LC_TIME, 'es_ES.UTF-8');
		// dd($request->start_time, $request->id);
		try{
			$reservation_end  = Reservation::whereDate('date', Carbon::now()->toDateString())
										->where('start_time', date('H:i:s', strtotime($request->start_time)))
										->first();
			$reservation_to_change = Reservation::find($request->id);
		//	dd($reservation_end, $reservation_to_change);
			if ($reservation_end) {
				if ($reservation_end->id != $reservation_to_change->id){
					$new_time= $reservation_to_change->start_time;
					$reservation_to_change->start_time = date('H:i:s', strtotime($request->start_time));
					if(!$reservation_to_change->save()){
						throw new \Exception("Error al actualizar la reserva", 400);
					}
					$reservation_end->start_time= date('H:i:s', strtotime($new_time));
					if(!$reservation_end->save()){
						throw new \Exception("Error al actualizar la reserva", 400);
					}
						if ($request->comment) {

							$comment = new CommentsReservation();
							$comment->reservation_id = $reservation_end->id;
							$comment->date = date('Y-m-d');
							$comment->time =   date('H:i:s');

							$comment->comment = $request->comment;
							$comment->user_id = Auth::user()->id;
							$comment->save();
						}
				}
			}
			else{
				$reservation_to_change->start_time = date('H:i:s', strtotime($request->start_time));
				if (!$reservation_to_change->save()) {
					throw new \Exception("Error al actualizar la reserva", 400);
				}
					if ($request->comment) {
						$comment = new CommentsReservation();

						$comment->reservation_id = $reservation_to_change->id;
						$comment->date = date('Y-m-d');
						$comment->time =   date('H:i:s');

						$comment->comment = $request->comment;
						$comment->user_id = Auth::user()->id;
						$comment->save();
					}
			}
		}catch(\Exception $e){
			return response()->json(['status' => false, 'message'=> 'ha ocurrido un error inseperado']);
		}
		return response()->json(['status' => true, 'message'=> null]) ;
	}

	public function departure_list(Request $request)
	{
		// Se valida si no es un usuario de rol 5 y 7
		if (Auth::user()->role_id != 5 && Auth::user()->role_id != 7 && Auth::user()->role_id != 8) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');

		// La fecha del dia de hoy
		$actual_date = Carbon::today();
		// Se lista las reservaciones donde la fecha sea igual a la fecha de hoy
		$reservations = Reservation::whereDate('date', '=', $actual_date->toDateString())->with(['comments']);

		// Se ordena la tabla por fecha de forma ascendente junto con la hora
		$reservations = $reservations->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->get();

		// Se recorre cada socio en invitado del arreglo
		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
		}

		// Se crea la tabla
		$table_reservations = Table::create($reservations, ['date' => 'Fecha', 'start_time' => 'Hora']);

		// Se crean las columnas
		$table_reservations->addColumn('invite', 'Participantes', function($reservation) {

			$invitados = '<strong>Socios</strong> </br>';
			foreach ($reservation->members as $member) {
				$invitados .= $member->name.' '.$member->last_name.' C.I: '.$member->identity_card.'<br>';
			}
			if (sizeof($reservation->partners) > 0) {
				$invitados .= '<strong>Invitados</strong> </br>';
				foreach ($reservation->partners as $partner) {
					$invitados .= $partner->name.' '.$partner->last_name.' C.I: '.$partner->identity_card.'<br>';
				}
			}

			return $invitados;
		});

		$table_reservations->addColumn('status', 'Estado', function($reservation) {
			return $reservation->status;
		});

		// Se retorna la vista de lista de salida
		return view('starters.departure_list', [
			'reservations' => $reservations,
			'table_reservations' => $table_reservations,
			'input' => $request->input()
		]);

	}
	public function change_participant(Request $request)
	{
		//dd($request->request);
		if ( Auth::user()->role_id != 2 && Auth::user()->role_id != 5 ) {
			return redirect()->route('home');
		}
		DB::beginTransaction();
		try{

			if (!isset($request->participante_id_2)) {
				if ($request->tipo_participante_1 ==1 ) {
					//es socio
					$reservation = MemberReservation::where('member_id',$request->participante_1 )
													  ->where('reservation_id',$request->reservation_id_2 )->first();

					$reservation->reservation_id = $request->reservation_id_1;
					$reservation->save();
				}else{
					//es invitado
					$reservation = PartnerReservation::where('partner_id',$request->participante_1 )
													   ->where('reservation_id',$request->reservation_id_2 )->first();



					$reservation->reservation_id = $request->reservation_id_1;
					$reservation->save();
				}

			}
			else {
				if ($request->tipo_participante_1 ==1 ) {
					//es socio
					$reservation = MemberReservation::where('member_id',$request->participante_id_1 )
					 								->where('reservation_id',$request->reservation_id_2 )->first();
					$reservation->reservation_id = $request->reservation_id_1;

					if ($request->tipo_participante_2 == 1) {
						$reservation2 = MemberReservation::where('member_id',$request->participante_id_2 )
															->where('reservation_id',$request->reservation_id_1 )->first();
						$reservation2->reservation_id = $request->reservation_id_2;

					}
					else{
						$reservation2 = PartnerReservation::where('partner_id',$request->participante_id_2 )
															->where('reservation_id',$request->reservation_id_1 )->first();

						$reservation2->reservation_id = $request->reservation_id_2;
						if(!$this->check_reservations($request->reservation_id_1, $request->reservation_id_2)){
							return response()->json(false);
						}
					}
					$reservation2->save();
					$reservation->save();
				}else{
					//es invitado
					$reservation = PartnerReservation::where('partner_id',$request->participante_id_1 )
														->where('reservation_id',$request->reservation_id_2 )->first();
					$reservation->reservation_id = $request->reservation_id_1;

					if ($request->tipo_participante_2 ==1) {
						$reservation2 = MemberReservation::where('member_id',$request->participante_id_2 )
															->where('reservation_id',$request->reservation_id_1 )->first();
						$reservation2->reservation_id = $request->reservation_id_2;
						if(!$this->check_reservations($request->reservation_id_1, $request->reservation_id_2)){
							return response()->json(false);
						}

					}
					else{
						$reservation2 = PartnerReservation::where('partner_id',$request->participante_id_2 )
														->where('reservation_id',$request->reservation_id_1 )->first();
						$reservation2->reservation_id = $request->reservation_id_2;
					//	dd($reservation2 );

					}
					$reservation2->save();
					$reservation->save();

				}
				$result = $this->check_reservations($reservation->id, $reservation2->id);
				if ($result) {
					DB::rollBack();
					return response()->json($result);
				}
				else{
					DB::commit();
				}
				if ($request->comment) {
					$comment = new CommentsReservation();
					$comment->reservation_id = $request->reservation_id;
					$comment->date = date('Y-m-d');
					$comment->time =   date('H:i:s');
					$comment->comment = $request->comment;
					$comment->user_id = Auth::user()->id;
					$comment->save();
				}

			}

		}catch(\Exception $e){
			DB::rollBack();
			return response()->json(['status' => false, 'message'=> 'ha ocurrido un error inseperado']);
		}
		return response()->json(['status' => true, 'message'=> null]);
	}

	public function reservaciones_del_dia(Request $request)
	{

		setlocale(LC_TIME, 'es_ES.UTF-8');

		$reservations = Reservation::whereDate('date', $request->date)->with(['members','partners','comments'])->orderBy('start_time','asc')->get();
		foreach($reservations as $reservation){
			foreach($reservation->partners as $partner){
				$payed = PartnerReservation::where('reservation_id', $partner->pivot->reservation_id)->where('partner_id',$partner->pivot->partner_id )->first();
				$partner->payed = $payed->payed;
			}
		}
		return response()->json($reservations);

		// return view('starters.reservation_day', [
		// 	'reservations' => $reservations,
		// 	] );

	}

	public function add_comment(Request $request)
	{

		if ( Auth::user()->role_id != 2 && Auth::user()->role_id != 7 ) {
			return redirect()->route('home');
		}
		try{
			$fecha = explode('-',$request->date);
			$fecha = $fecha[2]."-".$fecha[1]."-".$fecha[0];

			$comment = new CommentsReservation();
			$comment->reservation_id = $request->reservation_id;
			$comment->date = date('Y-m-d',strtotime($fecha));
			$comment->time =   date('H:i:s',strtotime($request->time));
			$comment->comment = $request->comment;
			$comment->user_id = Auth::user()->id;
			if (!$comment->save()) {
				throw new Exception();
			}
		}catch(\Exception $e){
			return response()->json(false);
		}

		// if ( Auth::user()->role_id != 2 && Auth::user()->role_id != 5 ) {
		// 	return redirect()->route('home');
		// }
		// try{
		// 	$comment = new CommentsReservation();
		// 	$comment->reservation_id = $request->reservation_id;
		// 	$comment->date = $request->date;
		// 	$comment->time =  $request->time;
		// 	$comment->comment = $request->comment;
		// 	if (!$comment->save()) {
		// 		throw new Exception();
		// 	}
		// }catch(\Exception $e){
		// 	return response()->json(false);
		// }
		//
		 return response()->json(true);
	}
		public function add_participant(Request $request)
	{
		DB::beginTransaction();
		try{

			if ($request->type_id == 1) {//es socio
				$member = new MemberReservation();
				$member->enabled = 1;
				$member->member_id = $request->participant_id;
				$member->reservation_id = $request->reservation_id;
				$member->save();
				if ($request->comment) {
					$comment = new CommentsReservation();
					$comment->reservation_id = $request->reservation_id;
					$comment->date = date('Y-m-d');
					$comment->time =   date('H:i:s');
					$comment->comment = $request->comment;
					$comment->user_id = Auth::user()->id;
					$comment->save();
				}
			}//deprecated
			else if($request->type_id == 2 )  {//es invitado
				$partner = new PartnerReservation();
				$partner->enabled = 1;
				$partner->exonerated = 0;
				$partner->green_fee = 0.00;
				$partner->partner_id = $request->participant_id;
				$partner->reservation_id = $request->reservation_id;
				$partner->save();
				if ($request->comment) {
					$comment = new CommentsReservation();
					$comment->reservation_id = $request->reservation_id;
					$comment->date = date('Y-m-d');
					$comment->time =   date('H:i:s');
					$comment->comment = $request->comment;
					$comment->user_id = Auth::user()->id;
					$comment->save();
				}
			}
			else{
				return response()->json(['status' => false, 'message'=> 'no cumple con las condiciones']);
			}

			//validation
			$result = $this->check_reservations($request->reservation_id);
			$result = ($result)? $result: $this->check_participant(1, $request->reservation_id, $request->participant_id);
			if($result){
				DB::rollBack();
				return response()->json($result);
			}else{
				DB::commit();
			}
			
		}catch(\Exception $e){
			DB::rollBack();
			return response()->json(['status' => false, 'message'=> null]);
		}
		return response()->json(['status' => true, 'message'=> null]);
	}

	public function delete_participant(Request $request)
	{

		try{
			DB::beginTransaction();
			if ($request->type_id == 1) {
				

					$member = MemberReservation::where('member_id', $request->participant_id)
												->where('reservation_id', $request->reservation_id)->first();
					$member->forceDelete();
					if ($request->comment) {
						$comment = new CommentsReservation();
						$comment->reservation_id = $request->reservation_id;
						$comment->date = date('Y-m-d');
						$comment->time =   date('H:i:s');
						$comment->comment = $request->comment;
						$comment->user_id = Auth::user()->id;
						$comment->save();
					}
				

			}else{
				$partner = PartnerReservation::where('partner_id', $request->participant_id)
											->where('reservation_id', $request->reservation_id)->first();
				$partner->forceDelete();
				if ($request->comment) {
					$comment = new CommentsReservation();
					$comment->reservation_id = $request->reservation_id;
					$comment->date = date('Y-m-d');
					$comment->time =   date('H:i:s');
					$comment->comment = $request->comment;
					$comment->user_id = Auth::user()->id;
					$comment->save();
				}
			}
			$result = $this->check_reservations($request->reservation_id);
			if ($result) {
				DB::rollBack();
				return response()->json($result);
			}
			else{
				DB::commit();
			}
		}catch(\Exception $e){
			return response()->json(['status' => false, 'message' => 'ha ocurrido un error']);
		}
		return response()->json(['status' => true, 'message' => null]);
	}
	//--------------------------------------------------------------------
	/*hacer funcion de validacion*/

	//--------------------------------------------------------------------------
	protected function check_reservations($origin, $final = null)
	{
		$reserva_origin = Reservation::where('id', $origin )->with(['members', 'partners'])->first();

		if((count($reserva_origin->partners) + count($reserva_origin->members)) > $this->validations->max_participants_direct  || (count($reserva_origin->partners) + count($reserva_origin->members)) < $this->validations->min_participants_direct){

			return ['status' => false, 'message'=> 'la cantidad de participantes en la reserva de origen no es la permitida'];
		}
		elseif( (count($reserva_origin->partners) / count($reserva_origin->members)) < $this->validations->max_partners_members){
			return ['status' => false, 'message'=> 'la reserva de origen no puede quedar sin socios'];
		}
		elseif( (count($reserva_origin->members)) < $this->validations->min_member_reservation){
			return ['status' => false, 'message'=> 'la reserva de origen no puede quedar sin socios'];

		}
		if ($final) {
			
			$reserva_final = Reservation::where('id', $final )->with(['members', 'partners'])->first();
			if((count($reserva_final->partners) + count($reserva_final->members)) > $this->validations->max_participants_direct  || (count($reserva_origin->partners) + count($reserva_origin->members)) < $this->validations->min_participants_direct){

				return ['status' => false, 'message'=> 'la cantidad de participantes en la reserva de origen no es la permitida'];
			}
			elseif( (count($reserva_final->partners) / count($reserva_final->members)) < $this->validations->max_partners_members){
				return ['status' => false, 'message'=> 'la reserva de origen no puede quedar sin socios'];
			}
			elseif( (count($reserva_final->members)) < $this->validations->min_member_reservation){
				return ['status' => false, 'message'=> 'la reserva de origen no puede quedar sin socios'];

			}
		}
		return false;

	}
	public function check_participant($type, $reservation_id, $participant)
	{
		$count = 0;
		if ($type == 1) {//es socio
			$reservation = Reservation::find($reservation_id);
			$date = $reservation->date;
			$reservations = Reservation::whereDate('date', '=' , date('Y-m-d', strtotime($date)))->with(['members'])->get();
			foreach ($reservations as $key => $reser) {
				foreach ($reser->members as $key => $member) {
					if ($member->id == $participant) {
						$count++;
					}
				}
			}
			if ($count > $this->validations->max_reservation_day_member) {
				return ['status' => false, 'message'=> 'ya el socio posee la mayor cantidad de reservas permitidas por dia'];
			}
		}else{
			 $reservations = Reservation::whereMonth('date', '=' , date('m', strtotime($date)))->whereYear('date', date('Y', strtotime($date)))->with(['partners'])->get();
            foreach ($reservations as $key => $reser) {
				foreach ($reser->partners as $key => $partner) {
					if ($partner->id == $participant) {
                        if ($reser->date == $date) {
                           $count++;
                        }
						$count_month ++;
					}
				}
			}

			if ($count > $this->validations->max_reservation_day_member) {
                 return ['status' => 'ya el invitado posee la mayor cantidad de reservas permitidas por dia', 'type' => 'error'];
				
			}
            if ($count_month  > $this->validations->max_reservation_month_partner) {
                return ['status' => 'ya el invitado posee la mayor cantidad de reservas permitidas por mes', 'type' => 'error'];
			}
		}
		return false;
	}

}
