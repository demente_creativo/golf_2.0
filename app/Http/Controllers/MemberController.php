<?php namespace Golf\Http\Controllers;

use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Mail;
// use Illuminate\Support\Facades\Input;
// models
use Golf\Member;
use Golf\User;


class MemberController extends Controller
{
	public $menu = "menu_admin";

	public function autocomplete()
	{
		$term = Input::get('term');
		$results = array();
		$queries = Member::where('name', 'LIKE', '%'.$term.'%')
		->orWhere('last_name', 'LIKE', '%'.$term.'%')
		->orWhere('identity_card', 'LIKE', '%'.$term.'%')
		->orWhere('number_action', 'LIKE', '%'.$term.'%')
		->take(10)->get();

		foreach ($queries as $query)
		{
			$results[] = [
					'id' => $query->id,
					'name' => $query->name,
					'last_name' => $query->last_name,
					'identity_card' => $query->identity_card,
					'number_action' => $query->number_action,
					'value' => $query->name.' '.$query->last_name,
					'user' => Auth::user()->role_id
			];
		}
		return response()->json($results);
	}

	public function index()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		/*$sex = ['Femenino','Masculino'];
		$members = Member::get();
		foreach ($members as $key => $member) {
			$member->sex = $sex[rand(0,1)];
			$member->save();
		}*/
		$members = Member::where([])->sorted()->orderBy('created_at', 'desc')->get();

		return view('members.all_members', [
			'menu' => 'menu_admin',
			'members' => $members,
		]);
	}

	public function show(Request $request, $user_id, $member_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		try {
			$member = Member::find( $member_id );

			if (!empty($member->user->email))
			{
				$member->user = $member->user->email;
			}

			if (empty($member)) {
				throw new \Exception("Error al obtener el socio", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['member' => $member,
				'type' => 'success']);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// Se valida que los usuarios de rol 2 s�lo pueden ver el listado
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
					'name' => 'required|min:3|max:25',
					'last_name' => 'required|min:3|max:25',
					'identity_card' => 'required|numeric|min:6',
					'number_action' => 'required|numeric|min:5',
					'phone' => 'required|min:5|max:14',
					'email' => 'required|email|min:3|max:50',
					'sex' => 'required|min:1|max:1',

					'password' => 'required|min:6|max:25|confirmed'
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se crea un nuevo usuario
			$user = new User();
			$user->name = $request->email;
			$user->email = $request->email;
			$user->password = bcrypt($request->password);
			$user->role_id = 6;
			$user->is_subscripted=true;
			// Se condiciona si el usuario se guard�
			if ($user->save()) {

				// Se crea un nuevo miembro a partir del usuario guardado
				$member = new Member();
				$member->name = ucwords(strtolower($request->name));
				$member->last_name = ucwords(strtolower($request->last_name));
				$member->identity_card = $request->identity_card;
				$member->number_action = $request->number_action;
				$member->phone = $request->phone;
				$member->handicap = $request->handicap;
				$member->sex = $request->sex;
				$member->user_id = $user->id;

				// Se valida si el miembro no guard� para mostrar el error
				if (!$member->save()) {
					throw new \Exception('Error al almacenar el socio', 400);
				}

				// Envío del correo de registro al socio
				$data = array( 'email' => $request->email, 'password' => $request->password );
				$fromEmail = 'pr@dementecreativo.com';
				$fromName =	'Not Reply';

				Mail::send('emails.registry', $data, function($message) use ($fromName, $fromEmail)
				{
					$message->to($fromEmail, $fromName); // direccion para quien es el correo $request->email
					$message->from($fromEmail, $fromName); //quien envia
					$message->subject('Credenciales de Acceso');
				});

			} else {
				throw new \Exception('Error al almacenar el usuario', 400);
			}

			// Se retorna al listado de socios con los valores guardados
			DB::commit();
			return redirect()->route('socios.index', array('user_id' => Auth::user()->id))
			->with([ 'status' => 'Socio se ha creado exitosamente', 'type' => 'success' ]);

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('socios.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de socios con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('socios.index', array('user_id' => Auth::user()->id))
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);

			}
		}
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($user_id, $member_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		$member = Member::where([
				[ 'id', '=', $member_id ]
		])->with('user')->first();

		return view('members.edit_member', ['menu' => 'menu_admin_reservations', 'member' => $member]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $member_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $user_id, $member_id)
	{

		// Se valida que los usuarios de rol 2 s�lo pueden ver el listado
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();
		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->all(), [

				'name' => 'required|min:3|max:25',
				'last_name' => 'required|min:3|max:25',
				'identity_card' => 'required|numeric|min:6',
				'number_action' => 'required|numeric|min:5',
				'phone' => 'required|min:5',
				'email' => 'required|email|min:3',

				'sex' => 'required|min:1|max:1',

			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se obtiene los socios ordenados por ID
			$member = Member::where([
					[ 'id', '=', $member_id ]
			])->first();

			// Se valida si la contrase�a y su confirmaci�n coincide
			if ( $request->password != $request->password_confirmation ) {
				throw new \Exception('La confirmación del password no es correcta', 400);
			}

			// Se actualiza los datos de los campos
			$member->name = ucwords(strtolower($request->name));
			$member->last_name = ucwords(strtolower($request->last_name));
			$member->identity_card = $request->identity_card;
			$member->number_action = $request->number_action;
			$member->sex = $request->sex;
			$member->handicap = $request->handicap;

			// Validaciones que requieren que los campos no est�n vacios
			if (!empty($request->email)) {
				$member->user->email = $request->email;
			}

			if (!empty($request->password)) {
				$validator = Validator::make($request->all(), [
						'password' => 'min:6|max:25|confirmed',
				]);

				$member->user->password = bcrypt($request->password);
			}

			if (!empty($request->username)) {
				$member->user->name =  ucwords(strtolower($request->name)).' '.ucwords(strtolower($request->last_name));
			}

			$member->phone = $request->phone;
			$name = ucwords(strtolower($request->name)).' '.ucwords(strtolower($request->last_name));

			// Se valida si se pudo guardar los datos en la tabla usuarios y socios
			if ($member->user->save()) {
				if (!$member->save()) {
					throw new \Exception('Error al intentar actualizar los datos del Socio', 400);
				}
			} else {
				throw new \Exception('Error al intentar actualizar los datos del Usuario', 400);
			}

			// Se retorna al listado de socios con los valores guardados
			DB::commit();
			return redirect()->route('socios.index', array('user_id' => Auth::user()->id))
			->with([ 'status' => 'Registro actualizado correctamente.', 'type' => 'success' ]);

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('socios.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de socios con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('socios.index', array('user_id' => Auth::user()->id))
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);

			}
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($user_id, $member_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		DB::beginTransaction();

		try {
			$member = Member::where([
					[ 'id', '=', $member_id ]
			])->first();

			$name = $member->user->name;

			if ($member->user->delete()) {
				if (!$member->delete()) {
					throw new \Exception('Error al intentar eliminar el registro del socio', 400);
				}

			} else {
				throw new \Exception('Error al intentar eliminar el registro del Usuario', 400);
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route('socios.index', array('user_id' => Auth::user()->id))
			->with(['status' => $e->getMessage(),
					'line' => $e->getLine(),
					'code' => $e->getCode(),
					'input' => $request->input(),
					'type' => 'error']);
		}
		DB::commit();
		return redirect()->route('socios.index', array('user_id' => Auth::user()->id))
		->with(['status' => 'Se ha eliminado satisfactoriamente: '.$name, 'type' => 'success']);
	}

	public function validate_identity_card_member(Request $request)
	{
		$validation_in_members = Validator::make($request->input(), [
				'identity_card' => Rule::unique('members')->where(function ($query) use (&$request) {
				if (isset($request->member_id)) {
					$query->where('id', '!=', $request->member_id);
				}
				})
				]);
		$validation_in_partners = Validator::make($request->input(), [
				'identity_card' => Rule::unique('partners')->where(function ($query) use (&$request) {
				if (isset($request->partner_id)) {
					$query->where('id', '!=', $request->partner_id);
				}
				})
				]);
		if ($validation_in_members->fails()) {
			return response()->json(['status' => "Número de cédula ya registrado en el sistema.",
					'code' => 1,
					'type' => 'error']);
		}else {
			if ($validation_in_partners->fails()) {
				return response()->json(['status' => "Número de cédula ya registrado en el sistema.",
						'code' => 2,
						'type' => 'error']);
			}else {
				return response()->json(['status' => "La Cédula ingresada es valida",
						'code' => 200,
						'type' => 'success']);
			}
		}
	}

	public function validate_email_member(Request $request)
	{
		$validation_in_members = Validator::make($request->input(), [
				'email' => Rule::unique('users')->where(function ($query) use (&$request) {
				if (isset($request->member_id)) {
					$user_id = Member::find($request->member_id)->user_id;
					$query->where('id', '!=', $user_id);
				}
				})
				]);

		$validation_in_partners = Validator::make($request->input(), [
				'email' => Rule::unique('partners')->where(function ($query) use (&$request) {
				if (isset($request->partner_id)) {
					$query->where('id', '!=', $request->partner_id);
				}
				})
				]);
		if ($validation_in_members->fails()) {
			return response()->json(['status' => "La dirección del correo se encuenra registrada en el sistema.",
					'code' => 1,
					'type' => 'error']);
		}else {
			if ($validation_in_partners->fails()) {
				return response()->json(['status' => "La dirección de correo ingresada se encuentra registrada en el sistema.",
						'code' => 2,
						'type' => 'error']);
			}else {
				return response()->json(['status' => "El correo ingresado es válido.",
						'code' => 200,
						'type' => 'success']);
			}
		}
	}

	public function change_status( $id )
	{
		if (Auth::user()->role_id != 2)
		{
			return redirect()->route('home');
		}
		try
		{
			$member = Member::where([
					[ 'id', '=', $id ]
			])->first();

			if ( is_null( $member ) )
			{
				throw new \Exception('Socio no localizado.');
			}

			$member->enabled = $member->enabled ? false : true;
			if ( !$member->update() )
			{
				throw new \Exception('Imposible actualizar el estatus.');
			}
			$user = User::withTrashed()->find($member->user_id);
			if ($member->enabled) {
				$user->restore();
				
			}else{
				$user->delete();
			}
		}
		catch(\Exception $e)
		{
			return response()->json( ['changed' => false, 'message' => $e->getMessage() ] );
		}
		return response()->json( ['changed' => true, 'message' => 'Status actualizado correctamente.'] );
	}

	public function alls()
	{
		try
		{
				// $members = Member::where('user_id', '!=', Auth::user()->id)->get();
			$members = Member::leftJoin('users', 'members.user_id', 'users.id')
								->where([
									['users.is_subscripted', true],
								])
								->where('members.user_id', '<>', Auth::user()->id)
								->where('users.deleted_at', null)->get(['members.*']);
								
			foreach ($members as $member) {
				$member->nombre = $member->name. ' ' .$member->last_name;
				$member->user = Auth::user()->role_id;
			}
			
		}
		catch(\Exception $e)
		{
			return response()->json( ['status' => false, 'message' => $e->getMessage() ] );
		}
		return response()->json( ['status' => true, 'response' => $members] );
	}

	public function registered_member()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		$members = User::onlyTrashed()
		->leftJoin('members', 'users.id' ,'=', 'members.user_id')
		->where('users.is_subscripted', false)
		->where('members.deleted_at',null)
		->get()
		;


					return view('members.registered_member', [
						'menu' => 'menu_admin',
						'members' => $members
					]);
	}

	public function status_inscription( $id )
	{

		if (Auth::user()->role_id != 2)
		{
			return redirect()->route('home');
		}

		try
		{
			$user= User::withTrashed()->where([
					[ 'id', '=', $id ]
			])->first();

			if ( is_null( $user) )
			{
				throw new \Exception('Socio no localizado.');
			}

			$user->restore();
			$user->is_subscripted = true;

			if ( !$user->save() )
			{
				throw new \Exception('Imposible actualizar la solicitud.');
			}
			$user->member;
			$user->member->enabled =true;
			$user->member->save();

		}
		catch(\Exception $e)
		{
			return response()->json( ['changed' => false, 'message' => $e->getMessage() ] );
		}
		return response()->json( ['changed' => true, 'message' => 'Solicitud aprobada.'] );
	}

}
