<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Golf\ReservationDefinition;
use Golf\User;
use Gbrock\Table\Facades\Table;

class ReservationDefinitionController extends Controller
{

	public function index()
	{
		if (Auth::user()->role_id != 2 )
		{
			return redirect()->route('home');
		}

		$reservation_definitions = ReservationDefinition::where([])->sorted()->get();

		return view('reservation_definitions.all_reservation_definitions', [
				'reservation_definitions' => $reservation_definitions,
			] );
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{

		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}


		try {
			$reservation_definition = new ReservationDefinition();
			$value = explode(',', $request->green_fee)[0];
			$val = str_replace('.', '', $value);
			$reservation_definition->name = $request->name;
			$reservation_definition->green_fee = floatval($val);
			$reservation_definition->enabled = 1;
			if ( !$reservation_definition->save() ) {
				return redirect()->route( 'definicion_reservas.index',
									array('user_id' => Auth::user()->id) )
								->with( [ 'status' => 'Error al registrar el Green Fee ', 'type' => 'success' ] )
								->withInput();
			}
				return redirect()->route( 'definicion_reservas.index',
									array('user_id' => Auth::user()->id) )
								->with( [ 'status' => ' Green Fee  se ha creado exitosamente', 'type' => 'success' ] );
		} catch(\Exception $e)
		{

			return redirect()->route( 'definicion_reservas.index',
								array('user_id' => Auth::user()->id) )
							->with(['status' => $e->getMessage(),
											'line' => $e->getLine(),
											'code' => $e->getCode(),
											'type' => 'error']);
		}


		return redirect()->route( 'definicion_reservas.index',
							array('user_id' => Auth::user()->id) )
						->with( [ 'status' => 'Green Fee se ha creado exitosamente', 'type' => 'success' ] );
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $user_id, $reservation_definition_id )
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}
		$reservation_definition = ReservationDefinition::where([
							[ 'id', '=', $reservation_definition_id ]
						])->first();

		return view('reservation_definitions.edit_reservation_definition', [
					'reservation_definition' => $reservation_definition,
				] );
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $reservation_definition_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $user_id, $reservation_definition_id )
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$reservation_definition = ReservationDefinition::where([
								[ 'id', '=', $reservation_definition_id ]
							])->first();

			$reservation_definition->name = $request->name;
			$reservation_definition->green_fee = $request->green_fee;

			if ( !$reservation_definition->save() ) {
				return redirect()->route('definicion_reservas.edit')
					->with(['status' => 'Error al actualizar los datos de Definición de reservación 1', 'type' => 'error'])
					->withInput();
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route( 'definicion_reservas.index',
								array('user_id' => Auth::user()->id) )
							->with( [ 'status' => 'Error al actualizar Definición de reservación 3', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();
		return redirect()->route( 'definicion_reservas.index',
							array('user_id' => Auth::user()->id) )
						->with( [ 'status' => 'Definición de Reservación se ha actualizado exitosamente', 'type' => 'success' ] );
	}


	public function update_reservation_definition_status($id)
	{
		$green_fee = ReservationDefinition::find($id);
		if ($green_fee->enabled == 1) {
			$green_fee->enabled = 0;
		}
		else{
			$green_fee->enabled = 1;
		}
		$green_fee->save();
		return response()->json(
			[
				'changed' => true,
				'message' => 'cambio exitoso'
		]);
	}
	public function delete_green($id)
	{
		$id = ReservationDefinition::find( $id )->delete();

		$response['response'] = $id;
		$response['status'] = 'Se ha eliminado satisfactoriamente el green fee';
		$response['type'] = 'success';

		$ruta = '/admin/'.Auth::user()->id.'/definicion_reservas';
		return redirect($ruta)->with($response);
	}
	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	/*public function destroy( $user_id, $reservation_definition_id )
	{
		if ( Auth::user()->role_id != 2 ) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$reservation_definition = ReservationDefinition::where([
								[ 'id', '=', $reservation_definition_id ]
							])->first();

			if ( !$reservation_definition->delete() ) {
				return redirect()->route('definicion_reservas.index',
									array('user_id' => Auth::user()->id ) )
					->with(['status' => 'Error al eliminar el Definición de reservación', 'type' => 'error']);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//dd($e);
			return redirect()->route( 'definicion_reservas.index',
								array('user_id' => Auth::user()->id ) )
							->with( [ 'status' => 'Error al elimina Definición de reservación 3', 'type' => 'error' ] )
							->withInput();
		}

		DB::commit();
		return redirect()->route( 'definicion_reservas.index',
							array('user_id' => Auth::user()->id ) )
						->with( [ 'status' => 'Definición de Reservación se ha eliminado exitosamente', 'type' => 'success' ] );
	}
*/
}
