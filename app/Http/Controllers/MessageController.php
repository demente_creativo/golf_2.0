<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Golf\Message;
use Auth;
class MessageController extends Controller
{
    public function index()
    {
        if (Auth::user()->role_id != 2) {
			return response()->json('unautorized');
		}
        $messages = Message::where('is_readed', false)->get(['id']);

        return response()->json(
            [
                'quantity' => $messages->count(),
                'id' => $messages
            ]
            );
    }
    public function mark_as_readed(Request $request)
    {
       
         if (Auth::user()->role_id != 2) {
			return response()->json('unautorized');
		}
        $ids = $request->ids;
        try{
            
         foreach ($ids as $key => $id) {
             $message = Message::where('id',$id['id'])->first();
             $message->is_readed = true;
             $message->save();
         }

        }catch(\Exception $e){
            return false;
        }
         return response()->json(true);
    }
}
