<?php namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use Illuminate\Http\Respons;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Golf\Reservation;
use Golf\MemberReservation;
use Golf\PartnerReservation;
use Golf\StarterProcess;


class FinancierController extends Controller
{

	public function departure_list(Request $request)
	{
		// Se valida si no es un usuario de rol 3
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');

		// La fecha del dia de hoy
		$actual_date = Carbon::today();

		// Se lista las reservaciones donde la fecha sea igual a la fecha de hoy
		$reservations = Reservation::whereDate('date', '=', $actual_date->toDateString());

		// Se ordena la tabla por fecha de forma ascendente junto con la hora
		$reservations = $reservations->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->get();

		// Se recorre cada socio en invitado del arreglo
		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
		}

		// Se crea la tabla
		$table_reservations = Table::create($reservations, ['date' => 'Fecha', 'start_time' => 'Hora']);

		// Se crean las columnas
		$table_reservations->addColumn('invite', 'Participantes', function($reservation) {

			$invitados = '<strong>Socios</strong> </br>';
			foreach ($reservation->members as $member) {
				$invitados .= $member->name.' '.$member->last_name.' C.I: '.$member->identity_card.'<br>';
			}
			if (sizeof($reservation->partners) > 0) {
				$invitados .= '<strong>Invitados</strong> </br>';
				foreach ($reservation->partners as $partner) {
					$invitados .= $partner->name.' '.$partner->last_name.' C.I: '.$partner->identity_card.'<br>';
				}
			}

			return $invitados;
		});

		$table_reservations->addColumn('status', 'Estado', function($reservation) {
			return $reservation->status;
		});

		// Se retorna la vista de lista de salida
		return view('financiers.departure_list', [
			'reservations' => $reservations,
			'table_reservations' => $table_reservations,
			'input' => $request->input()
		]);
    }
}
