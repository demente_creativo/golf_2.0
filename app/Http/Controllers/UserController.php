<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;
use Golf\Role;
use Golf\User;
use Gbrock\Table\Facades\Table;


class UserController extends Controller
{

	public function autocomplete(){
		$term = Input::get('term');

		$results = array();

		$queries = User::where('name', 'LIKE', '%'.$term.'%')
						->where('role_id', '=', 6)
						->take(10)->get();

		foreach ($queries as $query)
		{
			$results[] = [
				'id' => $query->id,
				'name' => $query->name,
				'last_name' => $query->last_name,
				'identity_card' => $query->identity_card,
				'value' => $query->name.' '.$query->last_name
			];
		}
		return response()->json($results);
	}

	public function index()
	{
		$roles = Role::get();

		$users = User::where([['role_id', '=', 6]])->with('member')->sorted()->get();

		$table_users = Table::create($users, ['name' => 'Nombre', 'email' => 'Correo']);

		$table_users->addColumn('role_id', 'Rol', function($user) {
			//$ruta = route('usuarios.edit', $user->id);
			//$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			//$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			//$return .= '</a>';
			return $user->role->name;
		});

		$table_users->addColumn('edit', 'Editar', function($user) {
			$ruta = route('usuarios.edit', $user->id);
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});

		$table_users->addColumn('delete', 'Eliminar', function($user) {
			$ruta = route('usuarios.destroy', $user->id);
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});

		return view('users.all_users', ['users' => $users, 'roles' => $roles, 'table_users' => $table_users] );
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{

		$user = new User();
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = bcrypt($request->password);
		$user->role_id = $request->role_id;


		if ($user->save()) {
			return redirect()->route( 'usuarios.index' )
							->with( [ 'status' => 'Invitado se ha creado exitosamente de: '.$request->name, 'type' => 'success' ] );
		}else {
			return redirect()->route( 'usuarios.index' )
							->with( [ 'status' => 'Error al registrar invitado', 'type' => 'success' ] )
							->withInput();
		}
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $user_id )
	{
		$roles = Role::get();

		$user = User::where([
					[ 'id', '=', $user_id ]
				])->first();

		return view('users.edit_user', ['user' => $user, 'roles' => $roles]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $user_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $user_id)
	{

		$user = User::where([
					[ 'id', '=', $user_id ]
				])->first();

		$user->name = $request->name;
		$user->email = $request->email;
		$user->role_id = $request->role_id;

		if ($user->save()) {
			return redirect()->route('usuarios.index')
				->with(['status' => 'Se ha actualizado satisfactoriamente de:'.$request->name, 'type' => 'success']);
		} else {
			return redirect()->route('usuarios.edit')
				->with(['status' => 'Error al actualizar los datos de:'.$request->name, 'type' => 'error'])
				->withInput();
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $user_id )
	{

		$user = User::where([
							[ 'id', '=', $user_id ]
						])->first();

		$name = $user->name;

		if ($user->delete()) {
			return redirect()->route('usuarios.index')
				->with(['status' => 'Se ha eliminado satisfactoriamente:'.$name, 'type' => 'success']);
		} else {
			return redirect()->route('usuarios.index')
				->with(['status' => 'Error al eliminar el invitado:'.$name, 'type' => 'error']);
		}
	}

}
