<?php

namespace Golf\Http\Controllers;

use Gbrock\Table\Facades\Table;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use Golf\PaymentPlan;

class PaymentPlanController extends Controller
{
	public function index()
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}


		$payment_plans = PaymentPlan::where([])->orderBy('name','asc')->get();

		return view('payment_plans.all_payment_plans', [
			'payment_plans' => $payment_plans,
		]);
	}

	public function show(Request $request, $user_id, $payment_plan_id)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		try {
			$payment_plan = PaymentPlan::find( $payment_plan_id );

			if (empty($payment_plan)) {
				throw new \Exception("Error al obtener el plan de pago", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['payment_plan' => $payment_plan,
				'type' => 'success']);
	}

	public function store(Request $request)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'description' => 'required|min:3|max:500',
				'name' => 'required|min:3|max:50',
				'rate' =>'required|numeric',
			]);


			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se crea un nuevo plan de pago
			$payment_plan = new PaymentPlan();
			$payment_plan->name = $request->name;
			$payment_plan->description = $request->description;
			$payment_plan->rate = $request->rate;

			$name = $payment_plan->name;

			// Se condiciona si el plan de pago no se guard�
			if (!$payment_plan->save()) {
				throw new Exception('Error al registrar el plan de pago', 400);
			}

			// Se retorna al listado de socios con los valores guardados
			DB::commit();
			$response['response'] = $payment_plan;
			$response['status'] = 'Se ha registrado satisfactoriamente el estudiante: '.$name;
			$response['type'] = 'success';
			return redirect()->route('planes.index', array('user_id' => Auth::user()->id))
				->with($response);

		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('planes.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}


				// Se retorna al listado de planes de pao con los mensajes de diferentes errores
				DB::rollback();
				$response['response'] = false;
				$response['validations'] = $resultados;
				$response['line'] = $e->getLine();
				$response['code'] = $e->getCode();
				$response['type'] = 'error';
				return redirect()->route('planes.index', array('user_id' => Auth::user()->id))
					->with($response);
			}
		}
	}

	public function store_ajax(Request $request)
	{
		try {
			if (Auth::user()->role_id != 3) {
				return redirect()->route('home');
			}

			DB::beginTransaction();

			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:50',
				'description' => 'required|min:3',
			]);

			//dd($validation->errors());

			if ($validation->fails()) {
				throw new \Exception($validation->errors(), 400);
			}

			$payment_plan = new PaymentPlan();

			$payment_plan->name = $request->name;
			$payment_plan->description = $request->description;
			$payment_plan->rate = $request->rate;

			$name = $payment_plan->name;


			if (!$payment_plan->save()) {
				throw new Exception('Error al registrar el plan de pago', 400);
			}
		} catch (\Exception $e) {
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';

			return response()->json($response);
		}
		DB::commit();
		$response['response'] = $payment_plan;
		$response['status'] = 'Se ha registrado satisfactoriamente el estudiante: '.$name;
		$response['type'] = 'success';
		return response()->json($response);
	}

	public function edit($user_id, $payment_plan_id)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		$payment_plan = PaymentPlan::where([
						['id', '=', $payment_plan_id]
						])->first();

		return view('payment_plans.edit_payment_plan', ['payment_plan' => $payment_plan ]);

	}

	public function update(Request $request, $user_id, $payment_plan_id)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();
		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'description' => 'required|min:3|max:500',
				'name' => 'required|min:3|max:50',
				'rate' =>'required|numeric',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se obtiene los planes de pago ordenados por ID
			$payment_plan = PaymentPlan::where([
								['id', '=', $payment_plan_id]
							])->first();

			$payment_plan->name = $request->name;
			$payment_plan->description = $request->description;
			$payment_plan->rate = $request->rate;

			$name = $payment_plan->name;

			// Se valida si el plan de pago no se guardo seguido de un mensaje de error
			if (!$payment_plan->save()) {
				throw new Exception('Error al actualizar los datos del plan de pago', 400);
			}

			// Se retorna al listado de planes de pago con los valores guardados
			DB::commit();
			$response['response'] = $payment_plan;
			$response['status'] = 'Se ha actualizado satisfactoriamente el plan de pago: '.$name;
			$response['type'] = 'success';
			return redirect()->route('planes.index', array('user_id' => Auth::user()->id))
							->with($response);

		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				case ($e instanceof \PDOException):
					// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
					return redirect()->route('planes.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

				// Se retorna al listado de planes de pago con los mensajes de diferentes errores
				DB::rollback();
				$response['response'] = false;
				$response['validations'] = $resultados;
				$response['line'] = $e->getLine();
				$response['code'] = $e->getCode();
				$response['type'] = 'error';

				return redirect()->route('planes.index', array('user_id' => Auth::user()->id))
					->with($response);
			}
		}
	}

	public function destroy($user_id, $payment_plan_id)
	{
		try {
			if (Auth::user()->role_id != 3) {
				return redirect()->route('home');
			}

			$payment_plan = PaymentPlan::where([
								['id', '=', $payment_plan_id]
							])->first();

			$name = $payment_plan->name;

			if (!$payment_plan->delete()) {
				throw new Exception('Error al eliminar los datos del plan de pago', 400);
			}
		} catch (\Exception $e) {
			DB::rollback();
			//dd($e);
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';

			return redirect()->route('planes.index', array('user_id' => Auth::user()->id))
							->with($response);
		}
		DB::commit();
		$response['response'] = $payment_plan;
		$response['status'] = 'Se ha eliminado satisfactoriamente el plan de pago: '.$name;
		$response['type'] = 'success';
		return redirect()->route('planes.index', array('user_id' => Auth::user()->id))
						->with($response);
	}
}
