<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Golf\Storage;

class StorageController extends Controller
{

	public function index()
	{
		if ( Auth::user()->role_id != 4 ) {
			return redirect()->route('home');
		}

		$storages = Storage::where([
						])->orderBy('name', 'asc')->get();

		return view('storages.all_storages', ['storages' => $storages] );
	}

	/**
	 * Display view to create a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		if ( Auth::user()->role_id != 4 ) {
			return redirect()->route('home');
		}

		$storage = Storage::where([
						])->first();

		return view('storages.create_storage', ['storage' => $storage]);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{
		if ( Auth::user()->role_id != 4 ) {
			return redirect()->route('home');
		}

		$storage = new Storage();
		$storage->name = $request->name;
		$storage->enabled = 1;


		if ($storage->save()) {
			return redirect()->route( 'all_storages' )
							->with( [ 'status' => 'Invitado se ha creado exitosamente', 'type' => 'success' ] );
		}else {
			return redirect()->route( 'create_storage' )
							->with( [ 'status' => 'Error al registrar invitado ', 'type' => 'success' ] )
							->withInput();
		}
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $storage_id )
	{
		if ( Auth::user()->role_id != 4 ) {
			return redirect()->route('home');
		}

		$storage = Storage::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $storage_id ]
						])->first();

		return view('storages.edit_storage', ['storage' => $storage]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $storage_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $storage_id)
	{
		if ( Auth::user()->role_id != 4 ) {
			return redirect()->route('home');
		}

		$storage = Storage::where([
							[ 'id', '=', $storage_id ]
						])->first();
		$storage->name = $request->name;

		if ($storage->save()) {
			return redirect()->route('all_storages')
				->with(['status' => 'Se ha actualizado satisfactoriamente de:'.$storage->name.' '.$storage->last_name, 'type' => 'success']);
		} else {
			return redirect()->route('edit_storage')
				->with(['status' => 'Error al actualizar los datos de:'.$storage->name. ' ' .$storage->last_name, 'type' => 'error'])
				->withInput();
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $storage_id )
	{
		if ( Auth::user()->role_id != 4 ) {
			return redirect()->route('home');
		}

		$storage = Storage::where([
							[ 'id', '=', $storage_id ]
						])->first();

		$name = $storage->name.' '.$storage->last_name;

		if ($storage->delete()) {
			return redirect()->route('all_storages')
				->with(['status' => 'Se ha eliminado satisfactoriamente:'.$name, 'type' => 'success']);
		} else {
			return redirect()->route('all_storages')
				->with(['status' => 'Error al eliminar el invitado:'.$name, 'type' => 'error']);
		}
	}

}
