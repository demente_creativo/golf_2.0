<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Facades\Golf\Facades\ValidateRecurringData;
use Faker\Factory;
use Gbrock\Table\Facades\Table;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Log;
use Golf\DayDraw;
use Golf\TimeBlocked;
use Golf\DayBlocked;
use Golf\ListDrawn;
use Golf\Draw;
use Golf\DrawReservation;
use Golf\TimeDraw;
use Golf\Tournament;
use Golf\Transaction;
use Golf\Reservation;
use Golf\User;
use Golf\Http\Util\MailToMember;
use Mail;
use Validator;

class DayBlockedController extends Controller
{

	public function index()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		$draws = Draw::where('draw_status', '=', 0)->get();
		foreach ($draws as $key => $draw) {
			$draw->day_draws;
			foreach ($draw->day_draws as $key => $day_draw) {
				$day_draw->time_draws;
			}
		}
		$day_draws = DayDraw::where('date', '>=', Carbon::now()->format('Y-m-d') )->with('time_draws')->get();
		// dd($day_draws);
		$tournaments = Tournament::where('end_date', '>=', Carbon::now()->format('Y-m-d') )->get();

		$day_blockeds = DayBlocked::with('time_blockeds')->get();

		return view('days_blocked.all_days_blocked', [
			'draws' => $draws,
			'tournaments' => $tournaments,
			'day_blockeds' => $day_blockeds,
			'day_draws' => $day_draws,
		]);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		// Se asigna las validaciones a los campos
		$validation = Validator::make($request->input(), [
			'observation' => 'min:3|max:255',
		]);

		// Validación si falla las validaciones
		if($validation->fails()){
			$response['status'] = $validation->messages();
			$response['type'] = 'error';
			return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
				->with($response)
			;
	    }

		// Empieza la transacción
		DB::beginTransaction();

		try {
			$blocked_dates = json_decode($request->days_blocked);

			foreach ($blocked_dates as $blocked_date) {
				$day_blocked = new DayBlocked();
				//$day_blocked->observation = $blocked_date->comment;
				$day_blocked->observation = trim($blocked_date->comment);

				$day_blocked->date = $blocked_date->date;

				try {
					$day_blocked->date = Carbon::createFromFormat('Y-m-d', $blocked_date->date)->toDateString();
				} catch (\Exception $e) {
					return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'La fecha no corresponde a un formato válido', 'type' => 'error' ]);
				}

				if($day_blocked->save()){
					foreach ($blocked_date->times as $time) {
						$time_blocked = new TimeBlocked();

						try {
							$time_blocked->start_time = date('H:i:s', strtotime($time->start_time));
							$time_blocked->end_time = date('H:i:s', strtotime($time->end_time));
						} catch (\Exception $e) {
							return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
								->with([ 'status' => 'La(s) hora(s) no corresponden a un formato válido', 'type' => 'error' ]);
						}

						$time_blocked->day_blocked_id = $day_blocked->id;

						// Se condiciona si el día bloqueado se guardó
						if (!$time_blocked->save()) {
							throw new \Exception("Error al almacenar el día bloqueado", 400);
						}
					}
				}

				if (!$day_blocked->save()) {
					throw new \Exception('Error al almacenar fecha del día bloqueado', 400);
				}
			}

			// Se retorna al listado de grupos con los valores guardados
			DB::commit();
			$response['status'] = 'Día bloqueado se ha creado exitosamente.';
			$response['type'] = 'success';
			return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
				->with($response)
			;

		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				case ($e instanceof \PDOException):
					return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
				// Se retorna al listado de dias bloqueados con los mensajes de diferentes errores
				DB::rollback();
				$response['status'] = $e->getMessage();
				$response['line'] = $e->getLine();
				$response['code'] = $e->getCode();
				$response['input'] = $request->input();
				$response['type'] = 'error';

				return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
					->with($response);
			}
		}
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */

	public function update(Request $request, $user_id, $day_blocked_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		// Se asigna las validaciones a los campos
		$validation = Validator::make($request->input(), [
			'observation' => 'min:3|max:255',
		]);

		// Validación si falla las validaciones
		if($validation->fails()){
			$response['status'] = $validation->messages();
			$response['type'] = 'error';
			return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
				->with($response)
			;
	    }

		// Empieza la transacción
		DB::beginTransaction();

		try {
			$blocked_dates = json_decode($request->days_blocked);

			foreach ($blocked_dates as $blocked_date) {
				$day_blocked = DayBlocked::where([
					[ 'id', '=', $day_blocked_id ]
				])->first();

				$day_blocked->observation = trim($blocked_date->comment);

				try {
					$day_blocked->date = Carbon::createFromFormat('Y-m-d', $blocked_date->date)->toDateString();
				} catch (\Exception $e) {
					return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'La fecha no corresponde a un formato válido', 'type' => 'error' ]);
				}

				foreach ($day_blocked->time_blockeds as $key => $time_blocked) {
					$time_blocked->delete();
				}

				if($day_blocked->update()){
					foreach ($blocked_date->times as $time) {
						$time_blocked = TimeBlocked::where([
							["start_time", "=", date('H:i:s', strtotime($time->start_time))],
							["end_time", "=", date('H:i:s', strtotime($time->end_time))],
							[ 'day_blocked_id', '=', $day_blocked->id ]
						])->withTrashed()->first();

						if ($time_blocked) {
							if (!$time_blocked->restore()){
								throw new \Exception("Error al actualizar el día bloqueado", 400);
							}
						} else {
							$time_blocked = new TimeBlocked();
							$time_blocked->start_time = date('H:i:s', strtotime($time->start_time)) ;
							$time_blocked->end_time = date('H:i:s', strtotime($time->end_time));
							$time_blocked->day_blocked_id = $day_blocked->id;

							// Se condiciona si el día bloqueado se guardó
							if (!$time_blocked->save()) {
								throw new \Exception("Error al actualizar el día bloqueado", 400);
							}
						}
					}
				}

				if (!$day_blocked->update()) {
					throw new \Exception('Error al actualizar fecha del día bloqueado', 400);
				}
			}

			// Se retorna al listado de grupos con los valores guardados
			DB::commit();
			$response['status'] = 'Día bloqueado se ha creado exitosamente.';
			$response['type'] = 'success';
			return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
				->with($response)
			;

		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				case ($e instanceof \PDOException):
					return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
				// Se retorna al listado de dias bloqueados con los mensajes de diferentes errores
				DB::rollback();
				$response['status'] = $e->getMessage();
				$response['line'] = $e->getLine();
				$response['code'] = $e->getCode();
				$response['input'] = $request->input();
				$response['type'] = 'error';

				return redirect()->route('dias_bloqueados.index', array('user_id' => Auth::user()->id))
					->with($response);
			}
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */

	public function destroy($user_id, $day_blocked_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {
			$day_blocked = DayBlocked::where([
								['id', '=', $day_blocked_id]
							])->first();

			if (!$day_blocked->delete()) {
				return redirect()->route('dias_bloqueados.index', array('user_id' =>  Auth::user()->id))
					->with(['status' => 'Error al eliminar el día bloqueado', 'type' => 'error']);
			}
		} catch(\Exception $e) {
			DB::rollback();
			return redirect()->route('dias_bloqueados.index', array('user_id' =>  Auth::user()->id))
				->with(['status' => $e->getMessage(),
					'line' => $e->getLine(),
					'code' => $e->getCode(),
					'input' => $request->input(),
					'type' => 'error']);
		}

		DB::commit();
		return redirect()->route('dias_bloqueados.index', array('user_id' =>  Auth::user()->id))
			->with(['status' => 'Se ha eliminado satisfactoriamente el día bloqueado', 'type' => 'success']);
	}
	/**
	 * Enable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
}
