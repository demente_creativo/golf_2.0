<?php

namespace Golf\Http\Controllers;

use Carbon\Carbon;
use Gbrock\Table\Facades\Table;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;
use Golf\Reservation;
use PDF;

class ReportController extends Controller
{

	public function index(Request $request)
	{
		return view('reports.all_reports');
	}

	public function prueba(Request $request)
	{
		$reservations = Reservation::where([]);

		if (!is_null($request->start_date)) {
			//dd($request->start_date);
			$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
			$reservations->whereDate('date', '>=', $start_date->toDateString());
		}

		if (!is_null($request->end_date)) {
			//dd($request->end_date);
			$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
			$reservations->whereDate('date', '<=', $end_date->toDateString());
		}else{
			if (!is_null($request->start_date)) {
				$reservations->whereDate('date', '<=', $start_date->toDateString());
			}
		}
		if ($request->status != "null" && !is_null($request->status)) {
			//dd($request->status);
			$reservations->where('status', $request->status);
		}
		$reservations = $reservations->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->paginate(10);

		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
		}
		$table_reservations = Table::create($reservations, ['date' => 'Fecha', 'start_time' => 'Hora']);

		$table_reservations->addColumn('member_id', 'Creado por', function($reservation) {
			return (isset($reservation->user->member)) ? $reservation->user->email : $reservation->user->email;
		});

		$table_reservations->addColumn('invite', 'Participantes', function($reservation) {
			$invitados = '';
			foreach ($reservation->members as $member) {
				$invitados .= '<strong>'. $member->name.' '.$member->last_name.' C.I: '.$member->identity_card.'<br> </strong>';
			}
			foreach ($reservation->partners as $partner) {
				$invitados .= $partner->name.' '.$partner->last_name.' C.I: '.$partner->identity_card.'<br>';
			}
			return $invitados;
		});

		$table_reservations->addColumn('status', 'Estatus', function($reservation) {
			return $reservation->status;
		});

		return view('reports.list_report', [
					'reservations' => $reservations,
					'table_reservations' => $table_reservations,
					'input' => $request->input()
				]);
	}

	public function pdf_create($data, $vista_url)
	{

		try {
			$items = array();
			foreach ($data as $name => $value) {
				$items += [$name => $value];
			}

			$app = \App::make('dompdf.wrapper');
		    view()->share($items);
			$pdf = PDF::loadView($vista_url, $data);
		    $pdf->setOptions(['isPhpEnabled' => true]);
			//return view($vista_url, $data);
			//return $pdf->download('reporte.pdf');
			return $pdf->stream('reporte');

		} catch(\Exception $e)
		{
			return ['status' => $e->getMessage(),
					'line' => $e->getLine(),
					'code' => $e->getCode(),
					'type' => 'error'];
		}
	}

	public function pdf_report_create(Request $request)
	{
		$vista_url = 'reports.pdf_report_reservations';

		$reservations = Reservation::where([]);

		if (!is_null($request->start_date)) {
			//dd($request->start_date);
			$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
			$reservations->whereDate('date', '>=', $start_date->toDateString());
		}

		if (!is_null($request->end_date)) {
			//dd($request->end_date);
			$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
			$reservations->whereDate('date', '<=', $end_date->toDateString());
		}else{
			if (!is_null($request->start_date)) {
				$reservations->whereDate('date', '<=', $start_date->toDateString());
			}
		}
		if ($request->status != "null" && !is_null($request->status)) {
			//dd($request->status);
			$reservations->where('status', $request->status);
		}
		$reservations = $reservations->orderBy('date','asc')->orderBy('start_time','asc')->paginate(99999);

		//dd($this->pdf_create(['reservations' => $reservations, 'title' => "Reporte de Reservaciones"], $vista_url));

		return $this->pdf_create(['reservations' => $reservations, 'title' => "Reporte de Reservaciones"], $vista_url);
		// dd('hola');
	}

	public function xml_create($data, $vista_url)
	{

		try {
			$items = array();
			foreach ($data as $name => $value) {
				$items += [$name => $value];
			}

		    view()->share('items',$items);
			$pdf = PDF::loadView($vista_url);
			return $pdf->stream('reporte');

			//este deja guindado la aplicacion
			$data = $data;
			$date = date('Y-m-d');
			$view = \View($vista_url, compact('data','date'))->render();
			$pdf = \App('dompdf.wrapper');
			$pdf = PDF::loadHTML($view);

			return $pdf->stream('reporte');

		} catch(\Exception $e)
		{

			return ['status' => $e->getMessage(),
					'line' => $e->getLine(),
					'code' => $e->getCode(),
					'type' => 'error'];
		}
	}

	public function xml_report_create(Request $request)
	{
		$vista_url = 'reports.xml_report_reservations';

		$reservations = Reservation::where([]);

		if (!is_null($request->start_date)) {
			//dd($request->start_date);
			$start_date = Carbon::createFromFormat('d-m-Y', $request->start_date);
			$reservations->whereDate('date', '>=', $start_date->toDateString());
		}

		if (!is_null($request->end_date)) {
			//dd($request->end_date);
			$end_date = Carbon::createFromFormat('d-m-Y', $request->end_date);
			$reservations->whereDate('date', '<=', $end_date->toDateString());
		}else{
			if (!is_null($request->start_date)) {
				$reservations->whereDate('date', '<=', $start_date->toDateString());
			}
		}
		if ($request->status != "null" && !is_null($request->status)) {
			//dd($request->status);
			$reservations->where('status', $request->status);
		}
		$reservations = $reservations->orderBy('date','asc')->orderBy('start_time','asc')->paginate(10);
		dd($reservations);

		return $this->xml_create($reservations, $vista_url);
	}
}
