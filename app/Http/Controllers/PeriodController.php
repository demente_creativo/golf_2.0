<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use Gbrock\Table\Facades\Table;

use Golf\Group;
use Golf\Period;
use Golf\User;

use Validator;

class PeriodController extends Controller
{

	public function index()
	{

		$periods = Period::where([])->sorted()->get();
		$groups = Group::where([])->get();

		return view('periods.all_periods', [
			'periods' => $periods ,
			'groups' => $groups
		]);
	}
	
	public function show(Request $request, $user_id, $period_id)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}
	
		try {
			$period = Period::find( $period_id );
	
			if (!empty($period->group->name))
			{
				$period->group = $period->group->name;
			}
	
			if (empty($period)) {
				throw new \Exception("Error al obtener el estudiante", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}
	
		return response()->json(['period' => $period,
				'type']);
	}

	public function create(Request $request)
	{
		return view('periods.create_period');
	}

	public function store(Request $request)
	{
		// Se valida que los usuarios de rol 3 sólo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacción
		DB::beginTransaction();

		try {

			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:25',
				'date_start' => 'required|date|date_format:d-m-Y',
				'date_end' => 'required|date|date_format:d-m-Y',
				'group_id' => 'required',
			]);

			// Validación si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepción de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se crea un nuevo periodo
			$period = new Period();
			$period->date_start = $request->date_start;
			$period->date_end = $request->date_end;
			$period->price_inscription = 0;
			$period->group_id = $request->group_id;
			$period->name = $request->name;

			$name = $period->name;

			// Si periodo no se guarda lleva a un mensaje de error
			if (!$period->save()) {
				throw new \Exception("Error al registrar los datos de: ".$name, 400);
			}

			// Redirecciona al listado de periodos con el dato guardado
			DB::commit();
			$response['response'] = $period;
			$response['status'] = 'Se ha creado satisfactoriamente el periodo';
			$response['type'] = 'success';

			if (isset($request->mas)) {
				return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
					->with($response);
			} else {
				return redirect()->route('periodos.index', array('user_id' => Auth::user()->id))
								->with($response);
			}

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('periodos.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a través de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de periodos con los mensajes de diferentes errores
					DB::rollback();
					$response['response'] = false;
					$response['validations'] = $resultados;
					$response['line'] = $e->getLine();
					$response['code'] = $e->getCode();
					$response['input'] = $request->input();
					$response['type'] = 'error';

					return redirect()->route('periodos.index', array('user_id' => Auth::user()->id))
						->with($response);
			}
		}
	}

	public function edit($user_id, $period_id)
	{
		if (Period::find($period_id))
		{

			$period = Period::find($period_id);
			// dd($period);
			$groups = Group::where([])->get();

			return view('periods.edit_period', ['period' => $period, 'groups' => $groups]);
		}
		else
		{
			return redirect()->route('periodos.index', array('user_id' => Auth::user()->id))
				->with(['status' => 'Error al modificar Periodo', 'type' => 'error'])
				->withInput();
		}
	}

	public function update(Request $request, $user_id, $period_id)
	{
		// Se valida que los usuarios de rol 3 sólo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacción
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:25',
				'date_start' => 'required|date|date_format:d-m-Y',
				'date_end' => 'required|date|date_format:d-m-Y',
				'group_id' => 'required',
			]);

			// Validación si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepción de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se busca el periodo por id para actualizar los campos
			$period = Period::where([
								['id', '=', $period_id]
							])->first();

			$period->name = $request->name;
			$period->date_start = $request->date_start;
			$period->date_end = $request->date_end;
			$period->price_inscription = 0;
			$period->group_id = $request->group_id;

			$name = $period->name;

			// Si el periodo no se guardó lleva a un mensaje de error
			if (!$period->save()) {
				throw new \Exception("Error al actualizar los datos de: ".$name, 400);
			}

			// Se retorna al listado de periodos con los valores guardados
			DB::commit();
			$response['response'] = $period;
			$response['status'] = 'Se ha actualizado satisfactoriamente el periodo';
			$response['type'] = 'success';
			return redirect()->route('periodos.index', array('user_id' => Auth::user()->id))
				->with($response);

		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('periodos.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
				// Este es el caso por defecto y son mensajes que provienen de las validaciones a través de las excepciones
				$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

				// Se retorna al listado de periodos con los mensajes de diferentes errores
				DB::rollback();
				$response['response'] = false;
				$response['validations'] = $resultados;
				$response['line'] = $e->getLine();
				$response['code'] = $e->getCode();
				$response['input'] = $request->input();
				$response['type'] = 'error';

				return redirect()->route('periodos.index', array('user_id' => Auth::user()->id))
					->with($response);
			}

		}

	}

}
