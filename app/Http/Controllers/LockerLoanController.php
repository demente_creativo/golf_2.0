<?php

namespace Golf\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Gbrock\Table\Facades\Table;

use Golf\Locker;
use Golf\LockerLoan;
use Golf\Member;

class LockerLoanController extends Controller
{
	public $response = array();

	public function index()
	{
		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}

		$locker_loans = LockerLoan::where([])->with(['member', 'locker'])->withTrashed()->sorted()->get();
		$table_locker_loans = Table::create($locker_loans,['id' => 'ID']);
		$table_locker_loans->addColumn('locker_id','Casillero', function($locker_loan) {
			if ( empty($locker_loan->locker)) {
				return;
			} else {
				return $locker_loan->locker->name;
			}
		});
		$table_locker_loans->addColumn('member_id', 'Casillero', function($locker_loan) {
			if ( empty($locker_loan->member)) {
				return;
			} else {
				return $locker_loan->member->name;
			}
		});
		$table_locker_loans->addColumn('observations_in' ,'Observaciones de entrada');
		$table_locker_loans->addColumn('observations_out', 'Observaciones de salida');
		$table_locker_loans->addColumn('edit', 'Editar', function($locker_loan) {
			$ruta = route('prestamo_casilleros.edit', array('user_id' => Auth::user()->id, 'locker_loan_id' => $locker_loan->id));
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});
		$table_locker_loans->addColumn('delete', 'Eliminar', function($locker_loan) {
			$ruta = route('prestamo_casilleros.destroy', array('user_id' => Auth::user()->id, 'locker_loan_id' => $locker_loan->id));
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});

		$members = Member::get();

		$locker = Locker::where('status', 'Disponible')->first();

		return view('locker_loans.all_locker_loans', ['locker_loans' => $locker_loans, 'locker' => $locker, 'table_locker_loans' => $table_locker_loans, 'members' => $members]);
	}


	/**
	 * Get a resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function post()
	{
		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}

		$locker_loans = ['locker_loans' => LockerLoan::get()];

		response()->json($locker_loans, 200);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		try {
			if (Auth::user()->role_id != 4) {
				return redirect()->route('home');
			}

			DB::beginTransaction();

			$validation = Validator::make($request->input(), [
				'observations_out' => 'min:3|max:255',
				'locker_id' => 'required|numeric',
				'member_id' => 'required|numeric',
			]);

			//dd($validation->errors());

			if ($validation->fails()) {
				throw new \Exception($validation->errors(), 400);
			}
			$locker_loan = new LockerLoan();

			$locker_loan->observations_out = $request->observations_out;
			$locker_loan->locker_id = $request->locker_id;
			$locker_loan->member_id = $request->member_id;


			if ($locker_loan->save()){
				$locker_loan->locker;
				$locker_loan->locker->status = "Prestamo";
				// $locker_loan->locker->members = [$members];
				if (!$locker_loan->locker->save()) {
					throw new \Exception('Error al registrar el Préstamo', 400);
				}else {
					$locker_loan->locker->in = route('prestamo_casilleros.store', array( 'user_id' => Auth::user()->id ));
					$locker_loan->locker->out = route('prestamo_casilleros.update', array( 'user_id' => Auth::user()->id, 'loan_id' => $locker_loan->id ));
					$locker_loan->locker->remove = route('casilleros.destroy', array( 'user_id' => Auth::user()->id, 'locker_id' => $locker_loan->locker->id ));

				}
			} else{
				throw new \Exception('Error al registrar el Préstamo', 400);
			}
			$locker = $locker_loan->locker;
			if (!empty($locker->locker_loans)) {
				foreach ($locker->locker_loans as $key => $locker_loan) {
					$locker_loan->member;
				}
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['input'] = $request->input();
			$response['type'] = 'error';
			//dd($e);
			return response()->json($response);
		}

		DB::commit();
		return response()->json(['response' => $locker, 'status' => 'El Préstamo se ha creado exitosamente', 'type' => 'success']);
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($user_id, $locker_loan_id)
	{
		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}

		$locker_loan = LockerLoan::where([
							['id', '=', $locker_loan_id]
						])->with(['member','locker'])->first();

		$members = Member::get();

		return view('locker_loans.edit_locker_loan', ['locker_loan' => $locker_loan, 'members' => $members]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $locker_loan_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $user_id, $locker_loan_id)
	{
		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$validation = Validator::make($request->input(), [
				'observations_in' => 'min:3|max:255',
				'locker_id' => 'required|numeric',
				'member_id' => 'required|numeric',
			]);

			//dd($validation->errors());

			if ($validation->fails()) {
				throw new \Exception($validation->errors(), 400);
			}

			$locker_loan = LockerLoan::where([
								['id', '=', $locker_loan_id]
							])->first();

			$locker_loan->observations_in = $request->observations_in;
			$locker_loan->locker_id = $request->locker_id;
			$locker_loan->member_id = $request->member_id;

			if ($locker_loan->save()){
				$locker_loan->locker;
				$locker_loan->locker->status = "Disponible";
				if ($locker_loan->locker->save()) {
					if (!$locker_loan->delete()) {
						throw new \Exception('Error al finalizar el Préstamo', 400);
					}else {
						$locker_loan->locker->in = route('prestamo_casilleros.store', array( 'user_id' => Auth::user()->id ));
						$locker_loan->locker->out = route('prestamo_casilleros.update', array( 'user_id' => Auth::user()->id, 'loan_id' => $locker_loan->id ));

						$locker_loan->locker->remove = route('casilleros.destroy', array( 'user_id' => Auth::user()->id, 'locker_id' => $locker_loan->locker->id ));
					}
				} else{
					throw new \Exception('Error al finalizar el Préstamo', 400);
				}
			} else{
				throw new \Exception('Error al finalizar el Préstamo', 400);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['input'] = $request->input();
			$response['type'] = 'error';
			return response()->json($response);
		}

		DB::commit();
		return response()->json(['response' => $locker_loan->locker, 'status' => 'Se ha finalizado satisfactoriamente el Prestamo', 'type' => 'success']);
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($user_id, $locker_loan_id)
	{
		try {
			if (Auth::user()->role_id != 4) {
				return redirect()->route('home');
			}

			$locker_loan = LockerLoan::where([
								['id', '=', $locker_loan_id]
							])->first();

			if ($locker_loan->delete()) {
				return redirect()->route('prestamo_casilleros.index', array('user_id' => Auth::user()->id))
					->with(['status' => 'Se ha eliminado satisfactoriamente', 'type' => 'success']);
			} else {
				throw new \Exception('Error al eliminar casillero', 400);
			}

		} catch (Exception $e) {
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';
			return redirect()->route('prestamo_casilleros.index', array('user_id' => Auth::user()->id))
				->with($response);
		}
	}
}
