<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use Golf\Group;
use Golf\Member;
use Golf\Student;
use Golf\User;
use Illuminate\Validation\Rule;

use Log;
use PDF;

use Gbrock\Table\Facades\Table;

class StudentController extends Controller
{
	public function autocomplete(){
		$term = Input::get('term');


		$results = array();



		if (Auth::user()->role_id == 3) {
			$queries = Student::where('name', 'LIKE', '%'.$term.'%')
								->orWhere('last_name', 'LIKE', '%'.$term.'%')
								->orWhere('identity_card', 'LIKE', '%'.$term.'%')
								->take(10)->get();
		}else {
			$queries = Student::where([
									['name', 'LIKE', '%'.$term.'%'],
									['member_id', '=', Auth::user()->member->id]
								])->orWhere([
									['last_name', 'LIKE', '%'.$term.'%'],
									['member_id', '=', Auth::user()->member->id]
								])->orWhere([
									['identity_card', 'LIKE', '%'.$term.'%'],
									['member_id', '=', Auth::user()->member->id]
								])->take(10)->get();
		}


		foreach ($queries as $query)
		{
			$results[] = [
				'id' => $query->id,
				'name' => $query->name,
				'last_name' => $query->last_name,
				'identity_card' => $query->identity_card,
				'value' => $query->name.' '.$query->last_name
			];
		}
		return response()->json($results);
	}


	public function index()
	{
		
		if  (Auth::user()->role_id != 3 && Auth::user()->role_id != 6 ) {
			return redirect()->route('home');
		}

		if (Auth::user()->role_id != 6) {
			$students = Student::where([])->get();
		}else {
			$students = Student::where([
								['member_id', '=', Auth::user()->member->id],
							])->get();
		}
		$members = Member::where([])->get();

		return view('students.all_students', [
			'students' => $students,
			'members' => $members
		]);
	}

	public function show(Request $request, $user_id, $student_id)
	{
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		try {
			$student = Student::find( $student_id );

			if (!empty($student->member->name) || !empty($student->member->last_name))
			{
				$student->member = $student->member->name.' '.$student->member->last_name;
			}

			if (empty($student)) {
				throw new \Exception("Error al obtener el estudiante", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['student' => $student,
				'type' => 'success']);
	}

	public function create(Request $request)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		$members = Member::get();
		return view('students.create_student',['members' => $members]);
	}

	public function store(Request $request)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'birthdate' => 'required|date|date_format:d/m/Y',
				'identity_card' => 'required|numeric|min:6',
				'last_name' => 'required|min:3|max:25',
				'name' => 'required|min:3|max:25',
				'phone' => 'required|min:5|max:14',
				'member_id' => 'required',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se crea un nuevo estudiante
			$student = new Student();
			$student->identity_card = $request->identity_card;
			$student->name = ucwords(strtolower($request->name));
			$student->last_name = ucwords(strtolower($request->last_name));
			$student->phone = $request->phone;
			$student->birthdate = $request->birthdate;

			// Se valida si es un usuario con el rol 3
			if (Auth::user()->role_id == 3) {
				$student->member_id = $request->member_id;
			}else {
				$student->member_id = Auth::user()->member->id;
			}

			$name = $student->name.' '.$student->last_name;

			// Se condiciona si el alummno se guard�
			if (!$student->save()) {
				throw new Exception('Error al registrar los datos del estudiante', 400);
			}

			// Se retorna al listado de alumnos con los valores guardados
			DB::commit();
			$response['response'] = $student;
			$response['status'] = 'Se ha registrado satisfactoriamente el estudiante: '.$name;
			$response['type'] = 'success';
			return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
				->with($response);

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de alumnos con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);

			}
		}
	}

	public function store_ajax(Request $request)
	{
		try {
			$student = new Student();
			$student->identity_card = $request->identity_card;
			$student->name = ucwords(strtolower($request->name));
			$student->last_name = ucwords(strtolower($request->last_name));
			$student->phone = $request->phone;
			$student->birthdate = $request->birthdate;
			$student->member_id = $request->member_id;

			if (!$student->save()) {
				throw new Exception('Error al registrar los datos del estudiante', 400);
			}else {
				$name = $student->name . ' ' . $student->last_name;
			}
		} catch (\Exception $e) {
			DB::rollback();
			//dd($e);
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';

			return response()->json($response);
		}
		DB::commit();
		$response['response'] = $student;
		$response['status'] = 'Se ha registrado satisfactoriamente el estudiante: '.$name;
		$response['type'] = 'success';
		return response()->json($response);
	}

	public function edit($user_id, $student_id)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		if (Student::find($student_id))
		{

			$student = Student::where([
						[ 'id', '=', $student_id ]
						])->first();

			$members = Member::where([])->get();

			return view('students.edit_student', ['student' => $student, 'members' => $members]);
		}
		else
		{
			return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
				->with([ 'status' => 'Error al modificar Estudiante', 'type' => 'error' ])
				->withInput();
		}
	}

	public function update(Request $request, $user_id, $student_id)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'birthdate' => 'required|date|date_format:d/m/Y',
				'identity_card' => 'required|numeric|min:6',
				'last_name' => 'required|min:3|max:25',
				'name' => 'required|min:3|max:25',
				'phone' => 'required|min:5|max:14',
				'member_id' => 'required',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se obtiene los alumnos ordenados por ID
			$student = Student::where([
								[ 'id', '=', $student_id ]
							])->first();
			$student->identity_card = $request->identity_card;
			$student->name = ucwords(strtolower($request->name));
			$student->last_name = ucwords(strtolower($request->last_name));
			$student->phone = $request->phone;
			$student->birthdate = $request->birthdate;

			// Se valida si es un usuario con el rol 3
			if (Auth::user()->role_id == 3) {
				$student->member_id = $request->member_id;
			}

			$name = $student->name.' '.$student->last_name;

			// Se condiciona si el alummno se guard�
			if (!$student->save()) {
				throw new Exception('Error al actualizar los datos del estudiante', 400);
			}

		// Se retorna al listado de alumnos con los valores guardados
		DB::commit();
		$response['response'] = $student;
		$response['status'] = 'Se ha actualizado satisfactoriamente el estudiante: '.$name;
		$response['type'] = 'success';
		return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
			->with($response);

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de alumnos con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);

			}
		}
	}

	public function destroy($user_id, $student_id)
	{
		// Se valida que los usuarios de rol 3 s�lo pueden ver el listado
		if (Auth::user()->role_id != 3) {
			return redirect()->route('home');
		}

		try {
			$student = Student::where([
								[ 'id', '=', $student_id ]
							])->first();

			$name = $student->name.' '.$student->last_name;

			if (!$student->delete()) {
				throw new Exception('Error al eliminar el estudiante', 400);
			}

		} catch (\Exception $e) {
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';

			return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
							->with($response);
		}
		DB::commit();
		$response['response'] = true;
		$response['status'] = 'Se ha eliminado satisfactoriamente el estudiante: '.$name;
		$response['type'] = 'success';
		return redirect()->route('alumnos.index', array('user_id' => Auth::user()->id))
						->with($response);
	}

	public function AddGroup($student_id){

		$group = new Group();
		$group->name = $request->name;
		$group->price = $request->price;
		$group->teacher_id = $request->teacher_id;

		$name = $group->name;


		if ($group->save()) {
			return redirect()->route('alumnos.create', array('user_id' => Auth::user()->id))
							->with([ 'status' => 'Grupo se ha creado exitosamente de:'.$name, 'type' => 'success' ]);
		}else {
			return redirect()->route('alumnos.create', array('user_id' => Auth::user()->id))
							->with([ 'status' => 'Error al registrar Grupo', 'type' => 'success' ])
							->withInput();
		}
	}

	public function excel(Request $request)
	{
		try {

			$students = Student::select(['students.*']);

			$students = $students->sorted()->orderBy('created_at','asc')->paginate(999999);
			foreach ($students as $key => $student) {
				$estudiantes = '';
				$socios = '';
				if (empty($estudiantes)) {
					$estudiantes = strtoupper($student->name)." ".strtoupper($student->last_name)." ".$student->identity_card;
				} else {
					$estudiantes .= ', '.PHP_EOL.strtoupper($student->name)." ".strtoupper($student->last_name)." ".$student->identity_card;
				}

				$students_array[] = array(
					"ESTUDIANTES" => $estudiantes,
					"CUMPLEANOS" => $student->birthdate,
					"TELEFONOS" => $student->phone,
					"SOCIOS" => strtoupper($student->member->name)." ".strtoupper($student->member->last_name)." ".$student->member->identity_card,
				);
			}

			$data = array();
			$data['titulo'] = "REPORTE DE INSCRIPCIONES DE ALUMNOS";
			$data['students'] = $students;

			$excel = Excel::create('Listado Estudiantes'.date('Ymdhis'), function($excel) use($students_array, $students, $data) {

				$excel->setTitle('Listado de Estudiantes '.date('Y-m-d'));

				$excel->setCreator('Lagunita Country Club')->setCompany('Lagunita Country Club');

				$excel->setDescription('Listado de estudiantes consultados en el Sistema para Alquiler de Canchas de Golf Lagunita Country Club');

				$excel->sheet('Listado', function ($sheet) use ($students_array, $students, $data) {
					$sheet->setOrientation('landscape');
					$row = 3;
					$sheet->row($row, array($data['titulo']));
					++$row;

					$sheet->fromArray($students_array, NULL, 'A'.(1+$row));

					$data = array();
					$data['totales']['titulo'] = "Nº TOTAL DE ESTUDIANTES";
					$data['totales']['cantidad'] = $students->count();

					$row = count($students_array)+$row+3;
					foreach ($data as $key => $value) {
						$row++;
						$sheet->row($row, array($value['titulo']." ".$value['cantidad']));
					}
				});
			});
			$excel->export('xls');

		} catch (\Exception $e) {
			Log::info('Error al crear excel'. $e);
			dd($e);
		}
	}

	public function student_for_member($member_id)
	{
		$students = Student::where([
			['member_id', '=', $member_id],
		])->get();

		if ($students) {
			# code...
			$response['status'] = '';
			$response['type'] = 'success';
			$response['response'] = $students;
		}else {
			$response['status'] = 'El socio no tiene alumnos registrados';
			$response['type'] = 'error';
			$response['response'] = false;

		}


		return response()->json($response);
	}

	public function pdf(Request $request)
    {
        try {

            $students = Student::where([])->get();

            $pdf = PDF::loadView('students.students_table', ['students' =>$students]);

            return $pdf->download('Listado Estudiantes.pdf');

        } catch (\Exception $e) {
            Log::info('Error al crear pdf'. $e);
            dd($e);
        }
    }


	public function validate_identity_card_student(Request $request)
	{
		$validation_in_students = Validator::make($request->input(), [
				'identity_card' => Rule::unique('students')->where(function ($query) use (&$request) {
				if (isset($request->student_id)) {
					$query->where('id', '!=', $request->student_id);
				}
				})
				]);
		if ($validation_in_students->fails()) {
			return response()->json(['status' => "Número de cédula ya registrado en el sistema.",
					'code' => 2,
					'type' => 'error']);
		}else {
			return response()->json(['status' => "La cédula ingresada es valida",
					'code' => 200,
					'type' => 'success']);
		}
	}
}
