<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Golf\DayBlocked;
use Golf\Draw;
use Golf\Transaction;
use Golf\MemberReservation;
use Golf\PartnerReservation;
use Golf\TransactionPayment;
use Golf\Tournament;
use Golf\Member;
use Golf\Partner;
use Carbon\Carbon;
use Golf\Reservation;
use Golf\ReservationDefinition;
use Golf\TypePayment;
use Golf\PaymentMethod;
use Golf\CommentsReservation;
use Golf\Http\Controllers\ReservationController;
use Auth;
use DB;
class CollectorController extends Controller
{
    public function index()
    {
        
        if (  Auth::user()->role_id != 8 ) {
			return redirect()->route('home');
		}
        try{
        $transactions = Transaction::leftJoin('reservations', 'transactions.product_id', 'reservations.id')
                                     ->where('transactions.type_product_id' , 1)
                                     ->where('transactions.transaction_status_id' , 2)
                                     ->whereDate('reservations.date','>=', Carbon::now()->format('Y-m-d'))
                                    ->get(['transactions.*','reservations.date','reservations.start_time','reservations.id as reservation_id' ]);

        foreach($transactions as $transaction){
            $members = array();
            $partners = array();
            $mr= MemberReservation::with('members')->where('reservation_id', $transaction->reservation_id)->get();
            $pr= PartnerReservation::with('partners')->where('reservation_id', $transaction->reservation_id)->get();
            foreach($mr as $m){
                array_push($members,$m->members);
            }
            foreach($pr as $p){
               $p->partners->green = ($p->green)? $p->green->green_fee : null;
               $p->partners->p_reservation = $p->id;
               $p->partners->payed = $p->payed;
               $p->partners->exonerated = $p->exonerated;
                array_push($partners,$p->partners);
            }
            $transaction->participants_members = $members;
            $transaction->participants_partners = $partners;

        }
        $payment_methods = PaymentMethod::all();

         $members = Member::all();
        $partners = Partner::all();
        $greens = ReservationDefinition::all();


        return view('collector.list', [
            'transactions' => $transactions,
            'payment_methods' => $payment_methods,
             'members' => $members,
             'partners' => $partners,
             'greens' => $greens,
             'validations' => $this->validations
        ]);

        }catch(\Exception $e){
            return $e;
        }
    }
    public function store(Request $request)
    {
        if (  Auth::user()->role_id != 8 ) {
			return redirect()->route('home');
		}

        $transaction_request = json_decode($request->transaction);
        $payments = json_decode($request->transaction)->payment;
        $transaction_id = json_decode($request->transaction)->id;
        $partners = $transaction_request->participants_partners;
        try{

            if ($request->isChanged) {

            }else{
                foreach ($payments as $key => $payment) {
                    if (isset($payment->method_payment) && $payment->method_payment != 0) {                       
                        $transaction_pay= new TransactionPayment();
                        $transaction_pay->transaction_id = $transaction_id;
                        $transaction_pay->payment_method_id = $payment->method_payment;
                        if(!$transaction_pay->save()){
                            throw new Exception('error al procesar');

                        }
                        $reservation_p = PartnerReservation::find($payment->p_reservation);
                        $reservation_p->payed = true;
                        if(!$reservation_p->save()){
                            throw new Exception('error al procesar');

                        }
                        
                    }

                }
                foreach( $partners as $partner){
                    if(isset($partner->isDelete) && $partner->isDelete){
                        $reservation_pd = PartnerReservation::find($partner->p_reservation);
                        $reservation_pd->forceDelete();
                    }
                }
                
                $reservation_partners = PartnerReservation::where('reservation_id', $transaction_request->product_id)
                                                            ->where('payed', false)->get()->count();
                if ( $reservation_partners == 0 ) {
                    $transaction = Transaction::find($transaction_request->id);
                   $transaction->transaction_status_id = 5;
                    if(!$transaction->save()){
                        throw new Exception('error al procesar');
                    }
                    $reservation = Reservation::find($transaction_request->product_id)->first();
                    $reservation->status= 'Pagada';
                    if(!$reservation->save()){
                        throw new Exception('error al procesar');
                    }
                
                }
                    
            }
        }catch(\Exception $e){
            if (!$request->isUpdate) {
                return false;
            }
            return redirect('cobranza')->with(['error' => 'Fallo al realizar el pago', 'type' => 'error']);

        }
            if ($request->isUpdate) {
                 return true;
            }else{

                return redirect('cobranza')->with(['status' => 'Pago Registrado exitosamente', 'type' => 'success']);
            }
    }
    public function listPa()
    {
        if (  Auth::user()->role_id != 8 ) {
			return redirect()->route('home');
		}
        $payments = TransactionPayment::all();
        foreach($payments as $payment){
            $payment->transaction;
        }
        return view('collector.payments')->with('payments', $payments);
    }
    public function edit_reservation($user_id, $transaction_id)
    {
        if (  Auth::user()->role_id != 8 ) {
			return redirect()->route('home');
		}
        $draws = Draw::where([
			['draw_date', '>=', Carbon::now()->format('Y-m-d')],
			['draw_status', '=', 0]
		])->get();
		foreach ($draws as $draw) {
			$draw->day_draws;
			foreach ($draw->day_draws as $day_draw) {
				$day_draw->time_draws;
			}
		}
        $payment_methods = PaymentMethod::all();
		$tournaments = Tournament::whereDate('end_date', '>=', Carbon::now()->format('Y-m-d'))
				->where('enabled','=',1)
				->whereYear('start_date','<=',Carbon::now()->format('Y'))
				->get();

		$day_blockeds = DayBlocked::with('time_blockeds')->get();
        $type_payments = TypePayment::get();
        $reservation_definition = ReservationDefinition::where('enabled', 1)->orderBy('created_at', 'desc')->get();

        $transaction = Transaction::leftJoin('reservations', 'transactions.product_id', 'reservations.id')
                                     ->where('transactions.id', $transaction_id)
                                     ->where('transactions.type_product_id' , 1)
                                     ->where('transactions.transaction_status_id' , 2)
                                     ->whereDate('reservations.date','>=', Carbon::now()->format('Y-m-d'))
                                    ->first(['transactions.*','reservations.date','reservations.start_time','reservations.id as reservation_id' ]);

        $members = array();
        $partners = array();
        $mr= MemberReservation::with('members')->where('reservation_id', $transaction->reservation_id)->get();
        $pr= PartnerReservation::with('partners')->where('reservation_id', $transaction->reservation_id)->get();
        foreach($mr as $m){
            array_push($members,$m->members);
        }
        foreach($pr as $p){
            $p->partners->green_id = $p->green_id;
            array_push($partners,$p->partners);
        }
        $transaction->participants_members = $members;
        $transaction->participants_partners = $partners;



        return view('collector.edit_reservation', [
            'draws' => $draws,
            'tournaments' => $tournaments,
            'day_blockeds' => $day_blockeds,
            'type_payments' => $type_payments,
            'reservation_definition' => $reservation_definition,
            'transaction' => $transaction,
            'payment_methods' => $payment_methods
        ]);
    }
    public function update(Request $request)
    {
        if (  Auth::user()->role_id != 8 ) {
			return redirect()->route('home');
		}
        try{
            $request->isUpdate =true;
           $reservation = new ReservationController();
           $transaction = Transaction::find($request->transaction_id);
           $reser = Reservation::find($transaction->product_id);
           $reser->delete();
            //$transaction->delete();
           $result = $reservation->store($request);
           if ($result ) {
               $transaction->product_id =$result->id;
               if ($transaction->save()) {
                  $request->transaction_id =  $transaction->id;
                  $res =  $this->store($request);
               }else{
                   $result->status = 'Pagada';
                   $result->save();
               }
               
             return redirect('cobranza')->with(['status' => 'Pago Registrado exitosamente', 'type' => 'success']);
           }else{
             return redirect('cobranza')->with(['error' => 'Fallo al realizar el pago', 'type' => 'error']);
           }
        }catch(\Exception $e){
             return redirect('cobranza')->with(['error' => 'Fallo al realizar el pago', 'type' => 'error']);
        }

    }
    public function destroy($reservation_id)
    {
         if (  Auth::user()->role_id != 8 ) {
			return redirect()->route('home');
		}
        $transaction = Transaction::find($reservation_id);
        if($transaction){
            $reser = Reservation::find($transaction->product_id);
            $reser->delete();
            $transaction->delete();
             return redirect('cobranza')->with(['status' => 'Pago eliminado exitosamente', 'type' => 'success']);
        }else{
             return redirect('cobranza')->with(['error' => 'Fallo al eliminar el pago', 'type' => 'error']);
        }

    }

    	public function add_participant(Request $request)
	{
        if (  Auth::user()->role_id != 8 ) {
			return redirect()->route('home');
		}
		try{
            DB::beginTransaction();
			if ($request->type == 1 ) {//es socio
				$member = new MemberReservation();
				$member->enabled = 1;
				$member->member_id = $request->participant_id;
				$member->reservation_id = $request->reservation_id;
				$member->save();
				if ($request->comment) {
					$comment = new CommentsReservation();
					$comment->reservation_id = $request->reservation_id;
					$comment->date = date('Y-m-d');
					$comment->time =   date('H:i:s');
					$comment->comment = $request->comment;
					$comment->user_id = Auth::user()->id;
					$comment->save();
				}
                
			}//deprecated
			else if($request->type == 2 )  {//es invitado
				$partner = new PartnerReservation();
				$partner->enabled = 1;
                $partner->exonerated = ($request->green_fee_id == 0) ?  1:0;
				$partner->green_id = $request->green_fee_id; 
				$partner->partner_id = $request->participant_id;
				$partner->reservation_id = $request->reservation_id;
                $partner->payed = ($partner->exonerated ) ? true : false ;
				$partner->save();
				if ($request->comment) {
					$comment = new CommentsReservation();
					$comment->reservation_id = $request->reservation_id;
					$comment->date = date('Y-m-d');
					$comment->time =   date('H:i:s');
					$comment->comment = $request->comment;
					$comment->user_id = Auth::user()->id;
					$comment->save();
				}
			}
			else{
				return redirect('cobranza')->with(['status' => 'No cumple con las condiciones', 'type' => 'error']);
			}
            $result = $this->check_reservations($request->reservation_id);
            $result = ($result)? $result : $this->check_participant($request->type, $request->reservation_id, $request->participant_id);
            if ($result) {
                DB::rollBack();
                    return redirect('cobranza')->with($result);
            }else{
                DB::commit();
            }
		}catch(\Exception $e){
            DB::rollBack();
			return redirect('cobranza')->with(['status' => 'Ha ocurrido un error al agregar el participante', 'type' => 'error']);
		}
		return redirect('cobranza')->with(['status' => 'Participante agregado exitosamente', 'type' => 'success']);
	}

    protected function check_reservations($origin)
	{
		$reserva_origin = Reservation::where('id', $origin )->with(['members', 'partners'])->first();

		if((count($reserva_origin->partners) + count($reserva_origin->members)) > $this->validations->max_participants_direct  || (count($reserva_origin->partners) + count($reserva_origin->members)) < $this->validations->min_participants_direct){
            return ['status' => 'La cantidad de participantes en la reserva de origen no es la permitida', 'type' => 'error'];
			
		}
		elseif( (count($reserva_origin->partners) / count($reserva_origin->members)) < $this->validations->max_partners_members){
			return ['status' => 'cantidad de miembros por socio no permitida', 'type' => 'error'];
            
		}
		elseif( (count($reserva_origin->members)) < $this->validations->min_member_reservation){
			return ['status' => 'la reserva de origen no puede quedar sin socios', 'type' => 'error'];

		}
		return false;
	}

    	public function check_participant($type, $reservation_id, $participant)
	{
		$count = 0;
        $count_month = 0;
        $reservation = Reservation::find($reservation_id);
        $date = $reservation->date;
		if ($type == 1) {//es socio
			
			
			$reservations = Reservation::whereDate('date', '=' , date('Y-m-d', strtotime($date)))->with(['members'])->get();
			foreach ($reservations as $key => $reser) {
				foreach ($reser->members as $key => $member) {
					if ($member->id == $participant) {
						$count++;
					}
				}
			}
			if ($count > $this->validations->max_reservation_day_member) {
                return ['status' => 'ya el socio posee la mayor cantidad de reservas permitidas por dia', 'type' => 'error'];

			}
		}else{
            $reservations = Reservation::whereMonth('date', '=' , date('m', strtotime($date)))->whereYear('date', date('Y', strtotime($date)))->with(['partners'])->get();
            foreach ($reservations as $key => $reser) {
				foreach ($reser->partners as $key => $partner) {
					if ($partner->id == $participant) {
                        if ($reser->date == $date) {
                           $count++;
                        }
						$count_month ++;
					}
				}
			}

			if ($count > $this->validations->max_reservation_day_member) {
                 return ['status' => 'ya el invitado posee la mayor cantidad de reservas permitidas por dia', 'type' => 'error'];
				
			}
            if ($count_month  > $this->validations->max_reservation_month_partner) {
                return ['status' => 'ya el invitado posee la mayor cantidad de reservas permitidas por mes', 'type' => 'error'];
			}
		}
		return false;
	}

}
