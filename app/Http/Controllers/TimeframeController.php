<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Golf\Timeframe;
use Golf\User;

class TimeframeController extends Controller
{

	public function index()
	{
		

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$timeframes = Timeframe::where([
		//					['user_id', '=', $user->user_id],
						])->orderBy('name', 'asc')->get();

		// echo "<pre>";  var_dump($timeframes); echo "</pre>";
		// return;

		return view('timeframes.all_timeframes', ['timeframes' => $timeframes] );
	}

	/**
	 * Display view to create a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create_timeframe()
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$timeframe = Timeframe::where([
		//					[ 'user_id', '=', $user->user_id ]
						])->first();

		return view('timeframes.create_timeframe', ['timeframe' => $timeframe]);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store_timeframe( Request $request )
	{

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$timeframe = new Timeframe();
		//$timeframe->user_id = $user->user_id;
		$timeframe->user_id = 1;
		$timeframe->receipt_payment = $request->receipt_payment;
		$timeframe->processed = $request->processed;
		$timeframe->detail = $request->detail;
		$timeframe->date = $request->date;
		$timeframe->enabled = 1;


		if ($timeframe->save()) {
			return redirect()->route( 'all_timeframes' )
							->with( [ 'status' => 'Invitado se ha creado exitosamente', 'type' => 'success' ] );
		}else {
			return redirect()->route( 'create_timeframe' )
							->with( [ 'status' => 'Error al registrar invitado ', 'type' => 'success' ] )
							->withInput();
		}
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit_timeframe( $timeframe_id )
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$timeframe = Timeframe::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $timeframe_id ]
						])->first();

		return view('timeframes.edit_timeframe', ['timeframe' => $timeframe]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $timeframe_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update_timeframe(Request $request, $timeframe_id)
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$timeframe = Timeframe::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $timeframe_id ]
						])->first();

		$timeframe->receipt_payment = $request->receipt_payment;
		$timeframe->processed = $request->processed;
		$timeframe->detail = $request->detail;
		$timeframe->date = $request->date;

		if ($timeframe->save()) {
			return redirect()->route('all_timeframes')
				->with(['status' => 'Se ha actualizado satisfactoriamente de:'.$timeframe->name.' '.$timeframe->last_name, 'type' => 'success']);
		} else {
			return redirect()->route('edit_timeframe')
				->with(['status' => 'Error al actualizar los datos de:'.$timeframe->name. ' ' .$timeframe->last_name, 'type' => 'error'])
				->withInput();
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function delete_timeframe( $timeframe_id )
	{

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$timeframe = Timeframe::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $timeframe_id ]
						])->first();

		$name = $timeframe->name.' '.$timeframe->last_name;

		if ($timeframe->delete()) {
			return redirect()->route('all_timeframes')
				->with(['status' => 'Se ha eliminado satisfactoriamente:'.$name, 'type' => 'success']);
		} else {
			return redirect()->route('all_timeframes')
				->with(['status' => 'Error al eliminar el invitado:'.$name, 'type' => 'error']);
		}
	}

}
