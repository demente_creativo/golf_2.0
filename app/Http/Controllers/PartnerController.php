<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

use DB;
use Log;
use Mail;

use Gbrock\Table\Facades\Table;
use Golf\Club;
use Golf\Partner;
use Golf\User;

use Validator;

/**
 * PartnerController for Administrator
 */

class PartnerController extends Controller
{

	public function autocomplete()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		$term = Input::get('term');

		if (empty($partner_ids)) {
			$partner_ids = array();
		}
		$results = array();

		$queries = Partner::where('name', 'LIKE', '%'.$term.'%')
					->orWhere('last_name', 'LIKE', '%'.$term.'%')
					->orWhere('identity_card', 'LIKE', '%'.$term.'%')
					->get();

		foreach ($queries as $query)
		{
			$results[] = [
				'id' => $query->id,
				'name' => $query->name,
				'last_name' => $query->last_name,
				'identity_card' => $query->identity_card,
				'exonerated' => $query->exonerated,
				'value' => $query->name.' '.$query->last_name
			];
		}
		return response()->json($results);
	}

	public function index()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		$clubes = Club::get();

		$partners = Partner::with('club')->orderBy('created_at', 'desc')->get();
		
		/* Creando Tabla */
		return view('partners.all_partners', [
			'partners' => $partners,
			'clubes' => $clubes,
		]);
	}

	/**
	 * Display view to create a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		return view('partners.create_partner');
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		// Se valida que los usuarios administradores sólo pueden procesar esta function
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		// Empieza la transacción
		DB::beginTransaction();
		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:25',
				'last_name' => 'required|min:3|max:25',
				'identity_card' => 'required|numeric|min:6',
				'phone' => 'required|min:5|max:14',
				'email' => 'required|email|min:3|max:50',
				'sex' => 'required|min:1|max:1',
				'club_id' => 'required',
				'exonerated' => 'required',
			]);
			// Validación si falla las validaciones
			if ( $validation->fails() ) {
				// Esto nos trate la excepción de validaciones
				$this->throwValidationException($request, $validation);
			}
			// Se crea un nuevo invitado
			$partner = new Partner();
			$partner->name = ucwords(strtolower($request->name));
			$partner->last_name = ucwords(strtolower($request->last_name));
			$partner->identity_card = $request->identity_card;
			$partner->sex = $request->sex;
			$partner->email = $request->email;
			$partner->phone = $request->phone;
			$partner->handicap = $request->handicap;
			$partner->exonerated = ($request->exonerated == 0) ? Null : $request->exonerated;
			$partner->club_id = $request->club_id;
			$partner->user_id = Auth::user()->id;

			// Se valida si el invitado no guardó y arrojar el mensaje de error
			if (!$partner->save()) {
				DB::rollback();
				throw new \Exception( 'El registro no se pudo almacenar correctamente', 400 );
			}
			DB::commit();
			// Se retorna al listado de invitados con los valores guardados

			$parametros = array();
			$nombre_destinatario = ucwords($partner->name.' '.$partner->last_name);
			$email_destinatario = $partner->email;


				$parametros = [
					'nombre_socio' => ucwords('lagunita country club'),
					'correo_socio' => 'no-reply@clublagunita.com.ve',
					'nombre_destinatario' => $nombre_destinatario,
					'email_destinatario' => $email_destinatario
				];

			Mail::send('emails.registry_partner', $parametros, function ( $m ) use ( $email_destinatario, $nombre_destinatario  ) {

				$m->from(env('MAIL_USERNAME'), 'Reservación Lagunita');
				$m->to($email_destinatario, $nombre_destinatario)->subject('Invitación - Lagunita Contry Club');
			});
			return redirect()->route('invitados.index', array('user_id' => Auth::user()->id))
						->with(['status' => 'El invitado se ha registrado exitosamente', 'type' => 'success']);

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e) {
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('invitados.index', array('user_id' => Auth::user()->id))
						->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a través de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}
					// Se retorna al listado de invitados con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('invitados.index', array('user_id' => Auth::user()->id))
							->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);
			}
		}
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function show(Request $request, $user_id, $partner_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}
		try {
			$partner = Partner::find( $partner_id );

			if ( is_null( $partner->club_id ) or empty( $partner->club_id ) )
			{
				$partner->club = 'No indicado';
			}
			else
			{

				$partner->club = $partner->club->name;
			}
			if ($partner->exonerated == null)
			{
				$partner->exonerated = 'No exonerado';
			}
			elseif ($partner->exonerated == 'Reservacion')
			{
				$partner->exonerated = 'Por reservación';
			}
			if (empty($partner)) {
				throw new \Exception("Error al obtener el recurso", 400);
			}
		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['partner' => $partner,
								'type' => 'success']);
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($user_id, $partner_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		$clubes = Club::get();

		$partner = Partner::where([
							['id', '=', $partner_id]
						])->first();

		return view('partners.edit_partner', ['partner' => $partner, 'clubes' => $clubes]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $partner_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $user_id, $partner_id)
	{
		// Se valida que los usuarios de rol 2 s�lo pueden ver el listado
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		// Empieza la transacci�n
		DB::beginTransaction();

		try {
			// Se asigna las validaciones a los campos
			$validation = Validator::make($request->all(), [
				'name' => 'required|min:3|max:25',
				'last_name' => 'required|min:3|max:25',
				'identity_card' => 'required|numeric|min:6',
				'phone' => 'required|min:5|max:14',
				'email' => 'required|email|min:3|max:50',
				'sex' => 'required|min:1|max:1',
				'club_id' => 'required',
				'exonerated' => 'required',
			]);

			// Validaci�n si falla las validaciones
			if ($validation->fails()) {
				// Esto nos trate la excepci�n de validaciones
				$this->throwValidationException($request, $validation);
			}

			// Se obtiene los invitados ordenados por ID
			$partner = Partner::where([
								['id', '=', $partner_id]
							])->first();

			// Se actualiza los datos de los campos
			$partner->name = ucwords(strtolower($request->name));
			$partner->last_name = ucwords(strtolower($request->last_name));
			$partner->identity_card = $request->identity_card;
			$partner->email = $request->email;
			$partner->phone = $request->phone;
			$partner->handicap = $request->handicap;
			$partner->exonerated = ($request->exonerated == "Null") ? Null : $request->exonerated;
			// $partner->exonerated = $request->exonerated;
			$partner->club_id = $request->club_id;
			$partner->sex = $request->sex;

			$name = $partner->name.' '.$partner->last_name;

			// Se valida si el nombre se actualiz� con respecto al usuario
			if (!$partner->save()) {
				// Se retorna a la vista de editar invitados
				return redirect()->route('invitados.edit', array('user_id' =>  Auth::user()->id, 'partner_id' =>  $partner_id))
					->with(['status' => 'Error al actualizar los datos de:'.$name, 'type' => 'error'])
					->withInput();
			}

		// Se retorna al listado de invitados con los valores guardados
		DB::commit();
		return redirect()->route('invitados.index', array('user_id' =>  Auth::user()->id))
			->with(['status' => 'Se ha actualizado satisfactoriamente de:'.$name, 'type' => 'success']);

		} catch(\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('invitados.index', array('user_id' => Auth::user()->id))
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
				// Resultados que trae el log
				Log::info("Exception :".$e->getMessage().
							" File :".$e->getFile()." Line :".$e->getLine().
							" Trace :".$e->getTraceAsString()." Line :".$e->getLine());

				// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
				$excepciones = $e->validator->messages()->getMessages();
				$resultados = array();

				foreach ($excepciones as $excepcion){
					foreach ($excepcion as $ex){
						array_push($resultados, $ex);
					}
				}

				$response['response'] = false;
				$response['type'] = 'error';
				$response['validations'] = $resultados;
				$response['code'] = $e->getCode();
				$response['line'] = $e->getLine();
				$response['file'] = $e->getFile();

				// Se retorna al listado de invitados con los mensajes de diferentes errores
				DB::rollback();
				return redirect()->route('invitados.index', array('user_id' =>  Auth::user()->id))
								->with($response);
			}

		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($user_id, $partner_id)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$partner = Partner::where([
								['id', '=', $partner_id]
							])->first();

			$name = $partner->name.' '.$partner->last_name;

			if (!$partner->delete()) {
				return redirect()->route('invitados.index', array('user_id' =>  Auth::user()->id))
					->with(['status' => 'Error al eliminar el invitado:'.$name, 'type' => 'error']);
			}
		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route('invitados.index', array('user_id' =>  Auth::user()->id))
							->with(['status' => 'Error al eliminar invitado 3', 'type' => 'error'])
							->withInput();
		}

		DB::commit();

		return redirect()->route('invitados.index', array('user_id' =>  Auth::user()->id))
			->with(['status' => 'Se ha eliminado satisfactoriamente:'.$name, 'type' => 'success']);
	}

	/**
	 * Exonerate a partner in storage.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function exonerate(Request $request)
	{
		if (Auth::user()->role_id != 2) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {
			$partner = Partner::where('id', '=', $request->id)->first();
			$partner->exonerated = $request->exonerated;
			if (!$partner->save()) {
				// Rollback and then redirect

				$response['response'] = $partner;
				$response['message'] = 'Error al exonerar el invitado';
				$response['type'] = 'error';
				$response['code'] = 2;
				return response()->json($response);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			$response['response'] = $partner;
			$response['message'] = 'Error al exonerar el invitado';
			$response['type'] = 'error';
			$response['code'] = 4;
			return response()->json($response);

		}

		DB::commit();

		$response['response'] = $partner;
		$response['message'] = 'Se ha exonerado satisfactoriamente al invitado';
		$response['type'] = 'success';
		$response['code'] = 1;

		return response()->json($response);
	}

	public function validate_identity_card_partner_xxxxx(Request $request)
	{
		try {
			$validacion = ValidateRecurringData::validate_identity_card($request->identity_card);
		} catch (\Exception $e) {
			if ($e->getCode() == 1) {
				$message = "La cédula ingresada le pertenece a un socio registrado en sistema";
			} else if ($e->getCode() == 2) {
				$message = "La cédula ingresada le pertenece a un invitado registrado en sistema";
			}
			return response()->json(['status' => $message,
									'line' => $e->getLine(),
									'code' => $e->getCode(),
									'type' => 'error']);
		}
		return response()->json(['status' => "la cédula ingresada es valida",
								'code' => 200,
								'type' => 'success']);
	}

	public function validate_email_partner_xxxx(Request $request)
	{
		try {
			$validacion = ValidateRecurringData::validate_email($request->email);
		} catch (\Exception $e) {
			if ($e->getCode() == 1) {
				$message = "El correo ingresado le pertenece a un socio registrado en sistema";
			} else if ($e->getCode() == 2) {
				$message = "El correo ingresado le pertenece a un invitado registrado en sistema";
			}
			return response()->json(['status' => $message,
									'line' => $e->getLine(),
									'code' => $e->getCode(),
									'type' => 'error']);
		}
		return response()->json(['status' => "El correo ingresado es valido",
								'code' => 200,
								'type' => 'success']);
	}

	public function alls()
	{

		try
		{
			$partners = Partner::all();
			foreach ($partners as $partner) {
				$partner->authReservation = true;
				$partner->reservated_dates = [];
				$partner->user = Auth::user()->role_id;
				$partner->nombre = $partner->name. ' ' .$partner->last_name;

			}
		}
		catch(\Exception $e)
		{
			return response()->json( ['status' => false, 'message' => $e->getMessage() ] );
		}
		return response()->json( ['status' => true, 'response' => $partners] );
	}

	public function change_status( $id )
	{

		if (Auth::user()->role_id != 2)
		{
			return redirect()->route('home');
		}
		try
		{
			$partner = Partner::where([
								[ 'id', '=', $id ]
							])->first();

			if ( is_null( $partner ) )
			{
				throw new \Exception('Inivitado no localizado.');
			}

			$partner->enabled = $partner->enabled ? false : true;
			if ( !$partner->update() )
			{
				throw new \Exception('Imposible actualizar el estatus .');
			}
		}
		catch(\Exception $e)
		{
			return response()->json( ['changed' => false, 'message' => $e->getMessage() ] );
		}
		return response()->json( ['changed' => true, 'message' => 'Estatus actualizado correctamente.'] );
	}



}
