<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;

use Golf\Company;

use Gbrock\Table\Facades\Table;

class CompanyController extends Controller
{
    public function index()
	{
		// if (Auth::user()->role_id != 2 ) {
		// 	return redirect()->route('home');
		// }

		$companies = Company::where([])->sorted()->get();

        $table_companies = Table::create($companies, ['name' => 'Nombre', 'phone' => 'Teléfono', 'rif' => 'Rif', 'address' => 'Direccion', 'tax' => 'IVA']);


        $table_companies->addColumn('edit', 'Editar', function($company) {
            $ruta = route('edit_company', $company->id);
            $return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
            $return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
            $return .= '</a>';
            return $return;
        });
        $table_companies->addColumn('delete', 'Eliminar', function($company) {
            $ruta = route('destroy_company', $company->id);
            $return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
            $return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
            $return .= '</button>';
            return $return;
        });

		return view('company.all_company', ['companies' => $companies, 'table_companies' => $table_companies]);

	}

	public function create_company(Request $request)
	{
		// if (Auth::user()->role_id != 2 ) {
		// 	return redirect()->route('home');
		// }

		return view('company.create_company');
	}

	public function store_company(Request $request)
	{
		// if (Auth::user()->role_id != 2 ) {
		// 	return redirect()->route('home');
		// }

		$company = new Company();
		$company->name = $request->name;
		$company->phone = $request->phone;
		$company->rif = $request->rif;
		$company->address = $request->address;
		$company->tax = $request->tax;
		

		$name = $company->name;


		if ($company->save()) {
			return redirect()->route( 'all_company' )
							->with( [ 'status' => 'Compañia se ha creado exitosamente de:'.$name, 'type' => 'success' ] );
		}else {
			return redirect()->route( 'create_company' )
							->with( [ 'status' => 'Error al registrar Compañia', 'type' => 'success' ] )
							->withInput();
		}
	}


	public function edit_company($company_id)
	{
		// if (Auth::user()->role_id != 2 ) {
		// 	return redirect()->route('home');
		// }

		if ( Company::find( $company_id ) )
		{

			$company = Company::where([
						[ 'id', '=', $company_id ]
						])->first();

			return view('company.edit_company', ['company' => $company]);
		}
		else
		{
			return redirect()->route( 'all_company' )
				->with( [ 'status' => 'Error al modificar Compañia', 'type' => 'error' ] )
				->withInput();
		}

	}

	public function update_company(Request $request, $company_id)
	{
		// if (Auth::user()->role_id != 2 ) {
		// 	return redirect()->route('home');
		// }

		$company = Company::where([
							[ 'id', '=', $company_id ]
						])->first();
		$company->name = $request->name;
		$company->phone = $request->phone;
		$company->rif = $request->rif;
		$company->address = $request->address;
		$company->tax = $request->tax;

		$name = $company->name;

		if ($company->save()) {
			return redirect()->route('all_company')
				->with(['status' => 'Se ha actualizado satisfactoriamente de:'.$name, 'type' => 'success']);
		} else {
			return redirect()->route('edit_company')
				->with(['status' => 'Error al actualizar los datos de:'.$name, 'type' => 'error'])
				->withInput();
		}
	}
	public function delete_company($company_id)
	{
		// if (Auth::user()->role_id != 2 ) {
		// 	return redirect()->route('home');
		// }
		
		$company = Company::where([
							[ 'id', '=', $company_id ]
						])->first();

		$name = $company->name;

		if ($company->delete()) {
			return redirect()->route('all_company')
				->with(['status' => 'Se ha eliminado satisfactoriamente:'.$name, 'type' => 'success']);
		} else {
			return redirect()->route('all_company')
				->with(['status' => 'Error al eliminar el invitado:'.$name, 'type' => 'error']);
		}
	}


}
