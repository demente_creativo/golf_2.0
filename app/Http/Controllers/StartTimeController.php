<?php

namespace Golf\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Golf\StartTime;
use Gbrock\Table\Facades\Table;

class StartTimeController extends Controller
{

	public function index()
	{


		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$start_times = StartTime::where([])->sorted()->get();

		$table_start_times = Table::create($start_times, ['start_time' => 'Hora de inicio', 'end_time' => 'Hora de salida']);
		$table_start_times->addColumn('edit', 'Editar', function($start_time) {
			$ruta = route('horario_salidas.edit', $start_time->id);
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});
		$table_start_times->addColumn('delete', 'Eliminar', function($start_time) {
			$ruta = route('horario_salidas.destroy', $start_time->id);
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});
		// echo "<pre>";  var_dump($start_times); echo "</pre>";
		// return;

		return view('start_times.all_start_times', ['start_times' => $start_times, 'table_start_times' => $table_start_times] );
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request )
	{

		$start_time = new StartTime();
		$start_time->start_time = $request->start_time;
		$start_time->end_time = $request->end_time;

		if ($start_time->save()) {
			return redirect()->route( 'horario_salidas.index' )
							->with( [ 'status' => 'Invitado se ha creado exitosamente', 'type' => 'success' ] );
		}else {
			return redirect()->route( 'create_start_time' )
							->with( [ 'status' => 'Error al registrar invitado ', 'type' => 'success' ] )
							->withInput();
		}
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit( $start_time_id )
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$start_time = StartTime::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $start_time_id ]
						])->first();

		return view('start_times.edit_start_time', ['start_time' => $start_time]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $start_time_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $start_time_id)
	{

		//	$user = User::where( 'id', '=', Auth::user()->id )->first();

		$start_time = StartTime::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $start_time_id ]
						])->first();

		$start_time->start_time = $request->start_time;
		$start_time->end_time = $request->end_time;

		if ($start_time->save()) {
			return redirect()->route('horario_salidas.index')
				->with(['status' => 'Se ha actualizado satisfactoriamente de el horario', 'type' => 'success']);
		} else {
			return redirect()->route('edit_start_time')
				->with(['status' => 'Error al actualizar los datos de el horario', 'type' => 'error'])
				->withInput();
		}
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $start_time_id )
	{

		//$user = User::where( 'id', '=', Auth::user()->id )->first();

		$start_time = StartTime::where([
		//					[ 'user_id', '=', $user->user_id ],
							[ 'id', '=', $start_time_id ]
						])->first();

		$name = $start_time->name.' '.$start_time->last_name;

		if ($start_time->delete()) {
			return redirect()->route('horario_salidas.index')
				->with(['status' => 'Se ha eliminado satisfactoriamente el horario', 'type' => 'success']);
		} else {
			return redirect()->route('horario_salidas.index')
				->with(['status' => 'Error al eliminar el horario', 'type' => 'error']);
		}
	}
}
