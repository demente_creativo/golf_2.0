<?php

namespace Golf\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Gbrock\Table\Facades\Table;

use Golf\Locker;
use Golf\LockerLoan;
use Golf\Member;
use Golf\Reservation;
use Golf\MemberReservation;
use Golf\PartnerReservation;
use Golf\StarterProcess;
use Carbon\Carbon;


class LockerController extends Controller
{
	public $response = array();

	public function index()
	{
		setlocale(LC_TIME, 'es_ES.UTF-8');

		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}

		$lockers = Locker::where([])->get();

		foreach ($lockers as $key => $locker) {

			if (!empty($locker->locker_loans)) {
				foreach ($locker->locker_loans as $key => $locker_loan) {
					$locker_loan->member;
				}
			}
		}
		foreach ($lockers as $locker) {
			$locker_loans_id = (count($locker->locker_loans) > 0) ? $locker->locker_loans[0]->id : '';
			$locker->in = route('prestamo_casilleros.store', array( 'user_id' => Auth::user()->id ));
			$locker->out = route('prestamo_casilleros.update', array( 'user_id' => Auth::user()->id, 'loan_id' => $locker_loans_id ));
			$locker->remove = route('casilleros.destroy', array( 'user_id' => Auth::user()->id, 'locker_id' => $locker->id ));

		}

		$table_lockers = Table::create($lockers, ['name' => 'Nombre','status' => 'Estado']);
		$table_lockers->addColumn('edit', 'Editar', function($locker) {
			$ruta = route('casilleros.edit', array('user_id' => Auth::user()->id, 'locker_id' => $locker->id));
			$return = '<a href="'.$ruta.'" class="btn btn-primary singlebutton1">';
			$return .= '<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>';
			$return .= '</a>';
			return $return;
		});
		$table_lockers->addColumn('delete', 'Eliminar', function($locker) {
			$ruta = route('casilleros.destroy', array('user_id' => Auth::user()->id, 'locker_id' => $locker->id));
			$return = '<button id="" name="" type="button" class="btn btn-primary singlebutton1 modaldelete" ruta="'.$ruta.'">';
			$return .= '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
			$return .= '</button>';
			return $return;
		});
		// dd(count($lockers));
		return view('lockers.all_lockers', ['lockers' => $lockers, 'table_lockers' => $table_lockers]);
	}

	/**
	 * Get a resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function post()
	{
		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}

		$lockers = ['lockers' => Locker::get()];

		response()->json($lockers, 200);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store (Request $request)
	{
		try {
			if (Auth::user()->role_id != 4) {
				return redirect()->route('home');
			}

			DB::beginTransaction();

			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:255',
				'status' => 'required|in:Disponible,Prestamo,Indisponible',
			]);

			//dd($validation->errors());

			if ($validation->fails()) {
				throw new \Exception($validation->errors(), 400);
			}

			$locker = new Locker();
			$locker->name = $request->name;
			$locker->status = $request->status;


			if ($locker->save()) {
				$locker->in = route('prestamo_casilleros.store', array( 'user_id' => Auth::user()->id, 'loan_id' => $locker->id ));
				$locker->out = route('prestamo_casilleros.update', array( 'user_id' => Auth::user()->id, 'loan_id' => $locker->id ));
				$locker->remove = route('casilleros.destroy', array( 'user_id' => Auth::user()->id, 'locker_id' => $locker->id ));

			}else {
				throw new \Exception('Error al registrar el locker', 400);
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['input'] = $request->input();
			$response['type'] = 'error';
			//dd($e);
			return response()->json($response);
		}

		DB::commit();
		return response()->json(['response' => array('locker' =>  $locker), 'status' => 'El locker se ha creado exitosamente', 'type' => 'success']);
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit ($user_id, $locker_id)
	{
		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}

		$locker = Locker::where([
							['id', '=', $locker_id]
						])->first();

		return view('lockers.edit_locker', ['locker' => $locker]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $locker_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update (Request $request, $user_id, $locker_id)
	{
		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}

		DB::beginTransaction();

		try {

			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:255',
				'status' => 'required|in:Disponible,Prestamo,Indisponible',
			]);

			//dd($validation->errors());

			if ($validation->fails()) {
				throw new \Exception($validation->errors(), 400);
			}

			$locker = Locker::where([
								['id', '=', $locker_id]
							])->first();

			$locker->name = $request->name;
			$locker->status = $request->status;

			if (!$locker->save()) {
				throw new \Exception('Error al actualizar los datos el locker', 400);
			}else {
				$locker->in = route('prestamo_casilleros.store', array( 'user_id' => Auth::user()->id, 'loan_id' => $locker->id ));
				$locker->out = route('prestamo_casilleros.update', array( 'user_id' => Auth::user()->id, 'loan_id' => $locker->id ));
				$locker->remove = route('casilleros.destroy', array( 'user_id' => Auth::user()->id, 'locker_id' => $locker->id ));

			}

		} catch(\Exception $e)
		{
			DB::rollback();
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['input'] = $request->input();
			$response['type'] = 'error';

			//dd($e);

			return response()->json($response);

			// return redirect()->route('casilleros.index', array('user_id' => Auth::user()->id))
							// ->with($response);
		}

		DB::commit();
		return response()->json(['response' => $locker, 'status' => 'Se ha actualizado satisfactoriamente el locker', 'type' => 'success']);

		// return redirect()->route('casilleros.index', array('user_id' => Auth::user()->id))
				// ->with(['status' => 'Se ha actualizado satisfactoriamente el locker', 'type' => 'success']);
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy ($user_id, $locker_id)
	{
		try {
			if (Auth::user()->role_id != 4) {
				return redirect()->route('home');
			}

			$locker = Locker::where([
								['id', '=', $locker_id]
							])->first();

			if ($locker->delete()) {
				// dd();
				return response()->json(['response' => $locker, 'status' => 'Se ha eliminado satisfactoriamente', 'type' => 'success']);
			} else {
				throw new \Exception('Error al eliminar casillero', 400);
			}

		} catch (Exception $e) {
			$response['response'] = false;
			$response['status'] = $e->getMessage();
			$response['line'] = $e->getLine();
			$response['code'] = $e->getCode();
			$response['type'] = 'error';
			return response()->json(['response' => $response]);
		}
	}

	public function departure_list(Request $request)
	{
		// Se valida si no es un usuario de rol 4
		if (Auth::user()->role_id != 4) {
			return redirect()->route('home');
		}
		setlocale(LC_TIME, 'es_ES.UTF-8');

		// La fecha del dia de hoy
		$actual_date = Carbon::today();

		// Se lista las reservaciones donde la fecha sea igual a la fecha de hoy
		$reservations = Reservation::whereDate('date', '=', $actual_date->toDateString());

		// Se ordena la tabla por fecha de forma ascendente junto con la hora
		$reservations = $reservations->sorted()->orderBy('date','asc')->orderBy('start_time','asc')->get();

		// Se recorre cada socio en invitado del arreglo
		foreach ($reservations as $key => $reservation) {
			$reservation->partners;
			$reservation->members;
		}

		// Se crea la tabla
		$table_reservations = Table::create($reservations, ['date' => 'Fecha', 'start_time' => 'Hora']);

		// Se crean las columnas
		$table_reservations->addColumn('invite', 'Participantes', function($reservation) {

			$invitados = '<strong>Socios</strong> </br>';
			foreach ($reservation->members as $member) {
				$invitados .= $member->name.' '.$member->last_name.' C.I: '.$member->identity_card.'<br>';
			}
			if (sizeof($reservation->partners) > 0) {
				$invitados .= '<strong>Invitados</strong> </br>';
				foreach ($reservation->partners as $partner) {
					$invitados .= $partner->name.' '.$partner->last_name.' C.I: '.$partner->identity_card.'<br>';
				}
			}

			return $invitados;
		});

		$table_reservations->addColumn('status', 'Estado', function($reservation) {
			return $reservation->status;
		});

		// Se retorna la vista de lista de salida
		return view('lockers.departure_list', [
			'reservations' => $reservations,
			'table_reservations' => $table_reservations,
			'input' => $request->input()
		]);

	}
}
