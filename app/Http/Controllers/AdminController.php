<?php

namespace Golf\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Response;
use Gbrock\Table\Facades\Table;
use Illuminate\Validation\Rule;
//models
use Golf\Role;
use Golf\User;


class AdminController extends Controller
{

	//eliminar esto es para una prueba no mas

	public function autocomplete(){

		if (Auth::user()->role_id != 1) {
			return response()->json(['status'=> 'Acceso no autoizado', 'type' => 'error']);
		}
		$term = Input::get('term');

		$results = array();

		$queries = User::where('name', 'LIKE', '%Error Processing Request'.$term.'%')
						->where('role_id','<>', 1)
						->where('enabled','=', 1)
						->take(10)->get();
		foreach ($queries as $query)
		{
			$results[] = [
				'id' => $query->id,
				'name' => $query->name,
				'identity_card' => $query->identity_card,
				'value' => $query->name
			];
		}
		return response()->json($results);
	}

	public function index()
	{
		if (Auth::user()->role_id != 1) {
			return redirect()->route('home');
		}

		$roles = Role::where('id', '<>', 1)
			->get()
		;

		$users = User::where([['role_id', '<>', 1], ['role_id', '<>', 6]])
				//->where('enabled','=','1') -> porque es inutil por ahora
				->with('role')
				->sorted()->get();

		return view('admins.all_admins')
			->with('users', $users)
			->with('roles', $roles)
		;	
	}

	public function show(Request $request, $user_id)
	{
		if (Auth::user()->role_id != 1) {
			return redirect()->route('home');
		}

		try {
			$user = User::find( $user_id );

			if (!empty($user->role->name))
			{
				$user->role = $user->role->name;
			}

			if (empty($user)) {
				throw new \Exception("Error al obtener el administrador", 400);
			}

		} catch (\Exception $e) {
			$response['response'] = false;
			$response['message'] = $e->getMessage();
			$response['code'] = $e->getLine();
			return response()->json($response);
		}

		return response()->json(['user' => $user,
				'type' => 'success']);
	}

	/**
	 * Store a new resource.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{

		if (Auth::user()->role_id != 1) {
			return redirect()->route('home');
		}
		try {

			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:25',
				'email' => 'required|email|min:3|max:50|unique:users',
				'password' => 'required|min:6|max:25|confirmed',
				'role_id' => 'required',
			]);

			if ($validation->fails()) {
				//throw new \Exception($validation->errors(), 400);
				$this->throwValidationException($request, $validation);
			}

			$user = new User();
			$user->name = ucwords(strtolower($request->name));
			$user->email = $request->email;
			$user->password = bcrypt($request->password);
			$user->role_id = $request->role_id;
			//$user->status = $request->status;


			if (!$user->save()) {
				throw new \Exception('Error al registrar Administrador', 400);
			}
			
		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('administradores.index')
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de socios con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('administradores.index')
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);

			}
		}
		return redirect()->route('administradores.index')
						->with(['status' => 'Administrador se ha creado exitosamente de: '.$request->name, 'type' => 'success']);
	}

	/**
	 * Edit a resource in storage.
	 *
	 * @param  Integer $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($user_id)
	{
		if (Auth::user()->role_id != 1) {
			return redirect()->route('home');
		}
		$roles = Role::where([['id', '<>', 1], ['id', '<>', 6]])->get();

		$user = User::where([
					['id', '=', $user_id]
				])->first();

		return view('admins.edit_admin', ['user' => $user, 'roles' => $roles]);
	}

	/**
	 * Store an updated resource in storage.
	 *
	 * @param  Integer $user_id
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $user_id)
	{
		if (Auth::user()->role_id != 1) {
			return redirect()->route('home');
		}
		try {
			$validation = Validator::make($request->input(), [
				'name' => 'required|min:3|max:25',
				'email' => 'required|email|min:3|max:50|unique:users,email,'.$user_id,
				'role_id' => 'required',
			]);

			if ($validation->fails()) {
				//throw new \Exception($validation->errors(), 400);
				$this->throwValidationException($request, $validation);
			}

			$users = User::where('email', $request->email)->whereNotIn('id', [$user_id])->first();

			if (!is_null($users)) {
				throw new \Exception('El correo ya se encuentra registrado', 400);
			}

			$user = User::where([
						['id', '=', $user_id]
					])->first();

			$user->name = ucwords(strtolower($request->name));
			$user->email = $request->email;
			$user->role_id = $request->role_id;

			if ($user->save()) {
				return redirect()->route('administradores.index')
					->with(['status' => 'Se ha actualizado satisfactoriamente de: '.$request->name, 'type' => 'success']);
			} else {
				throw new \Exception('Error al actualizar los datos de: '.$request->name, 400);
			}

		} catch (\Exception $e) {
			// Creamos un switch para que nos instancie los casos de excepciones
			switch ($e){
				// Este caso corresponde al error que proviene de base de datos y se reemplaza por este mensaje
				case ($e instanceof \PDOException):
					return redirect()->route('administradores.index')
					->with([ 'status' => 'Disculpe ha ocurrido un error externo al sistema, intente m&aacute;s tarde.', 'type' => 'error' ]);
					break;

				default:
					// Este es el caso por defecto y son mensajes que provienen de las validaciones a trav�s de las excepciones
					$excepciones = $e->validator->messages()->getMessages();
					$resultados = array();

					foreach ($excepciones as $excepcion){
						foreach ($excepcion as $ex){
							array_push($resultados, $ex);
						}
					}

					// Se retorna al listado de socios con los mensajes de diferentes errores
					DB::rollback();
					return redirect()->route('administradores.index')
					->with(['validations' => $resultados,
							'line'  => $e->getLine(),
							'code'  => $e->getCode(),
							'input' => $request->input(),
							'type'  => 'error']);

			}
		}
		return redirect()->route('administradores.index')
						->with(['status' => 'Administrador se ha creado exitosamente de: '.$request->name, 'type' => 'success']);
	}

	/**
	 * Delete a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($user_id)
	{
		if (Auth::user()->role_id != 1) {
			return redirect()->route('home');
		}

		$user = User::where([
			['id', '=', $user_id]
		])->first();

		$name = $user->name;

		if ($user->delete()) {
			return redirect()->route('administradores.index')
				->with(['status' => 'Se ha eliminado satisfactoriamente:'.$name, 'type' => 'success']);
		} else {
			return redirect()->route('administradores.index')
				->with(['status' => 'Error al eliminar:'.$name, 'type' => 'error']);
		}
	}

	/**
	 * Enable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function enable($user_id)
	{

		DB::beginTransaction();

		try {
			if (Auth::user()->role_id != 1) {
				return redirect()->route('home');
			}

			$user = User::where([
								['id', '=', $user_id]
							])->first();

			$name = $user->name;

			$user->enable = 1;

			$user->deleted_at = NULL;

			if (!$user->save()) {
				return redirect()->route('administradores.index')
					->with(['status' => 'Error al habilitar el Administrador:'.$name, 'type' => 'error']);
			} else {
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			//	dd($e);

			return redirect()->route('administradores.index')
							->with(['status' => 'Error al habilitar Administrador', 'type' => 'error'])
							->withInput();
		}

		DB::commit();

		return redirect()->route('administradores.index')
			->with(['status' => 'Se ha habilitado satisfactoriamente:'.$name, 'type' => 'success']);
	}

	/**
	 * Disable a resource in storage.
	 *
	 * @param  $id integer
	 * @return \Illuminate\Http\Response
	 */
	public function disable($user_id)
	{

		DB::beginTransaction();

		try {
			if (Auth::user()->role_id != 1) {
				return redirect()->route('home');
			}

			$user = User::where([
								['id', '=', $user_id]
							])->first();

			$name = $user->name;

			$user->enable = 0;

			if (!$user->save()) {
				return redirect()->route('administradores.index')
					->with(['status' => 'Error al deshabilitar el Administrador:'.$name, 'type' => 'error']);
			} else {
			}

		} catch(\Exception $e)
		{
			DB::rollback();
			return redirect()->route('administradores.index')
							->with(['status' => 'Error al deshabilitar Administrador', 'type' => 'error'])
							->withInput();
		}

		DB::commit();

		return redirect()->route('administradores.index')
			->with(['status' => 'Se ha deshabilitado satisfactoriamente:'.$name, 'type' => 'success']);
	}

	public function validate_email_user(Request $request)
	{
		$validation_in_users = Validator::make($request->input(), [
				'email' => Rule::unique('users')->where(function ($query) use (&$request) {
				if (isset($request->user_id)) {
					$query->where('id', '!=', $request->user_id);
				}
				})
				]);
		if ($validation_in_users->fails()) {
			return response()->json(['status' => "La dirección de correo ingresada se encuentra registrada en el sistema.",
					'code' => 1,
					'type' => 'error']);
		} else {
			return response()->json(['status' => "El correo ingresado es válido.",
					'code' => 200,
					'type' => 'success']);
		}
	}

}
