<?php

namespace Golf;

use Illuminate\Database\Eloquent\Model;

class CommentsReservation extends Model
{
    protected $table= 'comments_reservation';
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('Golf\User', 'user_id', 'id');
    }
}
