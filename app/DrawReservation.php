<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class DrawReservation extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'date', 'time', 'enabled', 'time_draw_id', 'reservation_id'
	];

	public function setDateAttribute($value)
	{
		$this->attributes['date'] = date( 'Y-m-d', strtotime($value) );
	}

	public function getDateAttribute()
	{
		return date( 'd-m-Y', strtotime($this->attributes['date']) );
	}

	public function setTimeAttribute($value)
	{
		$this->attributes['time'] = date( 'H:i', strtotime($value) );
	}

	public function getTimeAttribute()
	{
		return date( 'h:i A', strtotime($this->attributes['time']) );
	}

	public function reservation()
	{
		return $this->hasOne('Golf\Reservation', 'id', 'reservation_id');
	}

	public function time_draw()
	{
		return $this->belongsTo('Golf\TimeDraw', 'time_draw_id', 'id');
	}
}
