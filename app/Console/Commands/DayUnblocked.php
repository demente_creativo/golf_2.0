<?php

namespace Golf\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Golf\DayBlocked;

class DayUnblocked extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'desbloquear:dia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Desbloquear los días viernes de sorteo en la mañana';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $dt = Carbon::now()->format('Y-m-d');
            $day_blockeds = DayBlocked::where('date', '=', $dt)->get();

            foreach ($day_blockeds as $key => $day_blocked) {
                # code...
                if ( is_null( $day_blocked ) )
                {
                    throw new \Exception('Dia bloqueado no localizado.');
                }

                if (!$day_blocked->forceDelete()) 
                {
                    throw new \Exception('Disculpe, ocurrió un error al habilitar el día de hoy.');
                }
            }
        }
        catch(\Exception $e)
        {
            return response()->json( ['changed' => false, 'message' => $e->getMessage() ] );
        }
    }
    
}
