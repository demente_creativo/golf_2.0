<?php

namespace Golf\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Golf\Http\Controllers\DrawController;
use Golf\Draw;
use Illuminate\Http\Request;

class DrawUnblocked extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'desbloquear:sorteo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Desbloquear los días de sorteo en los días jueves';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         try
        {
            $dt = Carbon::now()->format('Y-m-d');

            $request = new Request();

            $draw = Draw::where('draw_date', '=', $dt)
                ->where('enabled', '=', 1)
                ->where('draw_status', '=', 0)
                ->first()
            ;

            if(!$draw) 
            {
                throw new \Exception('Sorteo no localizado no localizado.');
            }
            
            $request->draw_id = $draw->id;

            $draw_controller = new DrawController();
            $draw_controller->draw($request);

        } catch(\Exception $e) {
            return response()->json( ['changed' => false, 'message' => $e->getMessage() ] );
        }
    }
}
