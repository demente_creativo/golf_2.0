<?php

namespace Golf\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Golf\Log;
use Golf\EmailJob;
use Golf\User;
use Golf\Message;
use Auth;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {

        parent::report($exception);
    }



    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */

        public function render($request, Exception $e)
        {
            // dd($e);
            if($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException)
            {

                	return redirect()->route('home');
            }
            elseif ($e instanceof AuthenticationException) {
               return parent::render($request, $e);
            }else{

                $log = new Log();
                $log->stack = $e->getMessage();
                $log->user_id = Auth::user()->id;
                if($log->save()){
                     $log->error_code = 'ERR'.$log->id;
                     $log->save();

                    $parametros= [
                        'error' => $log->error_code
                    ];
                    $task = new EmailJob();
                    $task->subject = 'Upss';
                    $task->to = Auth::user()->email;
                    $task->recipient = Auth::user()->email;
                    $task->data = json_encode($parametros);
                    $task->template = 'emails.errors';
                    $task->intents = 0;

                    if($task->save()){
                        $message = new Message();
                        $message->data = '';
                        $message->save();
                    }
                    if(Auth::user()->role_id != 6){
                        $users = User::where('role_id', 2)->get();
                        foreach($users as $user){
                            $task2 = new EmailJob();
                            $task2->to = Auth::user()->email;
                            $task2->recipient = Auth::user()->email;
                            $task2->data = json_encode($parametros);
                            $task2->template = 'emails.errors';
                            $task2->intents = 0;
                        }
                    }
                }
                return view('errors.500')->with('error',$log->error);
            }
            return parent::render($request, $e);




        }
    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('login');
    }
}
