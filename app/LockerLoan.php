<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class LockerLoan extends Model
{
	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'status', 'observations_in', 'observations_out',
	];
	protected $sortable = [
		'name', 'status', 'observations_in', 'observations_out',
	];

	public function member()
	{
		return $this->belongsTo('Golf\Member', 'member_id', 'id');
	}

	public function locker()
	{
		return $this->belongsTo('Golf\Locker', 'locker_id', 'id');
	}

	public function reservations()
	{
		return $this->belongsTo('Golf\Locker', 'id', 'locker_id');
	}
}
