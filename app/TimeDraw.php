<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class TimeDraw extends Model
{
	use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'time', 'enabled', 'day_draw_id'
	];
	protected $sortable = [
		'time', 'enabled'
	];

	public function setTimeAttribute($value)
	{
		$this->attributes['time'] = date( 'H:i', strtotime($value) );
	}

	public function getStartTimeAttribute()
	{
		return date( 'H:i:s', strtotime($this->attributes['start_time']) );
	}
	public function getEndTimeAttribute()
	{
		return date( 'H:i:s', strtotime($this->attributes['end_time']) );
	}

	public function day_draw()
	{
		return $this->belongsTo('Golf\DayDraw', 'day_draw_id', 'id' );
	}

	public function draw_reservations()
	{
		return $this->hasMany('Golf\DrawReservation', 'time_draw_id', 'id' );
	}
}
