<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class ParticipantTournamentCategoria extends Model
{

	use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'order', 'status', 'enabled', 'participant_id', 'tournament_category_id'
	];
}
