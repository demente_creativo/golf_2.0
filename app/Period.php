<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Period extends Model
{

	use SoftDeletes;
	use Sortable;


	protected $fillable = [
		'date_start','date_end','group_id','name','price_inscription'];
	protected $sortable = [
		'date_start','date_end','group_id','name','price_inscription'
	];

	public function group()
	{
		return $this->hasOne('Golf\Group', 'id', 'group_id');
	}


	
}
