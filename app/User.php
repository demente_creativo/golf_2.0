<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Gbrock\Table\Traits\Sortable;
use Golf\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable
{
	use Notifiable;
	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'phone', 'email', 'password', 'role_id', 'enabled'
	];
	protected $sortable = [
		'name', 'phone', 'email', 'password', 'role_id', 'enabled'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	public function role()
	{
		return $this->belongsTo('Golf\Role', 'role_id', 'id');
	}

	public function member()
	{
		return $this->hasOne('Golf\Member', 'user_id', 'id');
	}

	public function reservation()
	{
		return $this->hasMany('Golf\Reservation', 'user_id', 'id');
	}
	
	public function sendPasswordResetNotification($token)
	{
		// Your your own implementation.
		$this->notify(new ResetPasswordNotification($token));
	}

	public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }
}
