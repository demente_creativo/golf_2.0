<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Storage extends Model
{

	use SoftDeletes;
	use Sortable;


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'enabled'
	];

}
