<?php

namespace Golf;

use Illuminate\Database\Eloquent\Model;

class GreenFee extends Model
{
    public function club(){
       return $this->belongsTo('Golf\Club', 'club_id', 'id');
    }
}
