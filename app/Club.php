<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Club extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'green_fee', 'enable',
	];
	protected $sortable = [
		'name', 'green_fee', 'enable',
	];

	public function partners()
	{
		return $this->hasMany('Golf\Partner', 'club_id', 'id' );
	}

	public function members()
	{
		return $this->hasMany('Golf\Member', 'club_id', 'id' );
	}
}
