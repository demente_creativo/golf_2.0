<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Company extends Model
{

	use SoftDeletes;
	use Sortable;

	 protected $fillable = [
		'id', 'name','phone','rif' , 'address', 'tax'
	];
	protected $sortable = [
		'id','name','phone', 'rif', 'address', 'tax'
	];
}
