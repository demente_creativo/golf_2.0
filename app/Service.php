<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Service extends Model
{

	use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'type_service','name','rode'];
	protected $sortable = [
		'identity_card','name','last_name','group_id','phone','member_id','birthdate','period_id'
	];

	public function group()
	{
		return $this->hasOne('Golf\Group', 'id', 'group_id');
	}

	public function teacher()
	{
		return $this->hasOne('Golf\Teacher', 'id', 'teacher_id');
	}

	public function period()
	{
		return $this->hasOne('Golf\Period', 'id', 'period_id');
	}

	public function member()
	{
		return $this->hasOne('Golf\Member', 'id', 'member_id');
	}
}
