<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class ListDrawn extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'enabled', 'day_draw_id', 'reservation_id',
	];
	protected $sorteable = [
		'enabled', 'day_draw_id', 'reservation_id',
	];

	public function reservation()
	{
		return $this->belongsTo('Golf\Reservation', 'reservation_id', 'id');
	}

	public function day_draw()
	{
		return $this->belongsTo('Golf\DayDraw', 'day_draw_id', 'id');
	}
}
