<?php

namespace Golf\Facades;

use Carbon\Carbon;
use Illuminate\Http\Request;
//models
use Golf\Member;
use Golf\Partner;
use Golf\User;
use Golf\Student;
use Golf\Registration;
use Golf\Transaction;
use Golf\DayDraw;
use Golf\Reservation;
use Golf\Tournament;

class ValidateRecurringData
{

	public function validate_member_reservation(Carbon $date = NULL, $member_id = NULL)
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}
		if (is_string($date)) {
			$date = Carbon::createFromFormat('d-m-Y', $date);
		}


		//	$partner = Partner::where([['id', '=', $partner_id]])->first();
		$reservation = Reservation::whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
						->whereDate('date', $date->toDateString())
						->where('members.id', '=', $member_id)
						->leftjoin('member_reservations', function ($join) {
							$join->on('reservations.id', '=', 'member_reservations.reservation_id');
						})
						->leftjoin('members', function ($join) {
							$join->on('member_reservations.member_id', '=', 'members.id');
						})->first();

		$response = array();
		if (empty($reservation)) {
			$response['validation'] = true;
		}
		else
		{
			$response['validation'] = false;
			$response['message'] = "Este socio ya posee una reserva para esta fecha.";
		}

		return response()->json($response);
	}

	public function validate_partner_reservation(Carbon $date = NULL, $partner_id = NULL)
	{
		if (Auth::user()->role_id != 6) {
			return redirect()->route('home');
		}

		$partner = Partner::where('id', '=', $partner_id)->first();

		if (!empty($partner->exonerated)){
			$response['validation'] = true;
		} else {

			//$date = Carbon::createFromFormat('Y-m-d', $date);

			$date = Carbon::createFromFormat('d-m-Y', $date);

			//	$partner = Partner::where([['id', '=', $partner_id]])->first();
			$reservation = Reservation::whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
							->whereDate('date', $date->toDateString())
							->where('partners.id', '=', $partner_id)
							->leftjoin('partner_reservations', function ($join) {
								$join->on('reservations.id', '=', 'partner_reservations.reservation_id');
							})
							->leftjoin('partners', function ($join) {
								$join->on('partner_reservations.partner_id', '=', 'partners.id');
							})->first();

			//dd(empty($reservation));
			$response = array();
			if (empty($reservation)) {
				$response['validation'] = true;
			}
			else
			{

				$reservations = Reservation::whereIn('status', array('Aprobada','Sorteable','En proceso','Disfrutada'))
								->whereYear('date', $date->year)
								->whereMonth('date', $date->month)
								->where('partners.id', '=', $partner_id)
								->join('partner_reservations', function ($join) {
									$join->on('reservations.id', '=', 'partner_reservations.reservation_id');
								})
								->join('partners', function ($join) {
									$join->on('partner_reservations.partner_id', '=', 'partners.id');
								})->orderBy('partners.id')->orderBy('date')->get();

				$response = array();
				//dd(empty($reservations));
				if (empty($reservations)) {
					$response['validation'] = true;
				}
				else
				{
					if (count($reservations) < 2) {
						$response['validation'] = true;
					} else {
						$response['validation'] = false;
						$response['message'] = "El invitado seleccionado ya exedió el limite de reservaciones por mes.";
					}

				}
				$response['validation'] = false;
				$response['message'] = "Este invitado ya posee una reserva para esta fecha.";
			}
		}

		return response()->json($response);
	}

	public function validate_identity_card($identity_card, $id =NULL, $model = NULL, $user_id =NULL)
	{
		$member = Member::where('identity_card', '=', $identity_card);
		if (($model == "member")) {
			$member->where('id', $id);
		}
		$member->first();
		if (!empty($member)) {
			throw new \Exception(false, 1);
		}
		$partner = Partner::where('identity_card', '=', $identity_card);
		if (($model == "partner")) {
			//para codicionar en casode ser parner los valores para que no coincidadn con el mismo usuario ni el mismo invitado que se esta validando
			$partner->whereNot([['id', $id], ['user_id', $user_id]]);
		}
		$partner->first();
		if (!empty($partner)) {
			throw new \Exception(false, 1);
		}
		return true;
	}

	public function validate_email($email, $id =NULL, $model = NULL, $user_id =NULL)
	{
		$member = Member::where('email', '=', $email);
		if (($model == "member")) {
			$member->where('id', $id);
		}
		$member->first();
		if (!empty($member)) {
			throw new \Exception(false, 1);
		}
		$partner = Partner::where('email', '=', $email);
		if (($model == "partner")) {
			//para codicionar en casode ser parner los valores para que no coincidadn con el mismo usuario ni el mismo invitado que se esta validando
			$partner->whereNot([['id', $id], ['user_id', $user_id]]);
		}
		$partner->first();
		if (!empty($partner)) {
			throw new \Exception(false, 1);
		}
		return true;
	}

	public function student_is_registered($id)
	{
		$registration = Registration::where('student_id',$id)
						->first();

		if (empty($registration)) {
			return false;
		} else {
			return true;
		}
	}

	public function validate_has_reservation($date = NULL, $start_time = NULL, $end_time = NULL)
	{
		if (is_string($date)) {
			$date = Carbon::createFromFormat('d-m-Y', $date);
		}

		$reservation = Reservation::whereIn('status', array(
															'Aprobada',
															'Sorteable',
															'En proceso',
															'Disfrutada'
														))
						->whereDate('date', $date->toDateString())
						->first();

		$response = array();
		if (empty($reservation)) {
			$response['validation'] = false;
		}
		else
		{
			$response['validation'] = true;
			$response['reservation'] = $reservation;
		}

		return $response;
	}

	public function validate_has_tournament($date = NULL, $start_time = NULL, $end_time = NULL)
	{
		if (is_string($date)) {
			$date = Carbon::createFromFormat('d-m-Y', $date);
		}

		$tournament = Tournament::whereDate('start_date', '<=', $date->toDateString())
								->whereDate('end_date', '>=', $date->toDateString())
								->first();

		$response = array();
		if (empty($tournament)) {
			$response['validation'] = false;
		}
		else
		{
			$response['validation'] = true;
			$response['tournament'] = $tournament;
		}

		return $response;
	}

	public function validate_has_day_draw($date = NULL, $start_time = NULL, $end_time = NULL)
	{
		if (is_string($date)) {
			$date = Carbon::createFromFormat('d-m-Y', $date);
		}

		$day_draw = DayDraw::whereDate('date', $date->toDateString())
						->first();
						//dd($day_draw);
		$response = array();
		if (empty($day_draw)) {
			$response['validation'] = false;
		}
		else
		{
			$day_draw->time_draws;
			$response['validation'] = true;
			$response['day_draw'] = $day_draw;
		}

		return $response;
	}
}