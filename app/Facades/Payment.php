<?php

namespace Golf\Facades;

use Golf\Transaction;

class Payment
{
	public function transaction( $array=array() )
	{

		try {
			if (empty($array)) {
				throw new \Exception("No se pudo registrar la transaccion: los datos no son correctos", 0);
			}
			$transaction = new Transaction();
			$transaction->amount = $array['amount'];
			$transaction->type_payer = $array['type_payer'];
			$transaction->payer_id = $array['payer_id'];
			$transaction->product_id = $array['product_id'];
			$transaction->type_product_id = $array['type_product_id'];
			$transaction->transaction_status_id = $array['transaction_status_id'];
			$transaction->type_payment_id = $array['type_payment_id'];
			if ( !$transaction->save() ) {
				throw new \Exception("No se pudo procesar la transaccion", 0);
			}
		} catch(\Exception $e)
		{
			return $response = ['code' => $e->getCode(),'message' => $e->getMessage(), 'line' => $e->getLine()];
		}
		return $response = ['code' => 1, 'message' =>'Transacción registrada exitosamente'];
	}
}