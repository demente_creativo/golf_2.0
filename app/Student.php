<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class Student extends Model
{

	use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'identity_card','name','last_name','group_id','phone','member_id','birthdate'
	];
	protected $sortable = [
		'identity_card','name','last_name','group_id','phone','member_id','birthdate'
	];

	public function group()
	{
		return $this->hasOne('Golf\Group', 'id', 'group_id');
	}

	public function teacher()
	{
		return $this->hasOne('Golf\Teacher', 'id', 'teacher_id');
	}

	public function member()
	{ 
		return $this->belongsTo('Golf\Member', 'member_id', 'id');
	}

	public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

	public function getLastNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }    	

}
