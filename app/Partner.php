<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class Partner extends Model
{

	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'last_name', 'identity_card', 'sex', 'phone', 'email', 'exonerated', 'handicap', 'club_id', 'member_id', 'enabled'
	];
	protected $sortable = [
		'name', 'last_name', 'identity_card', 'sex', 'phone', 'email', 'exonerated', 'handicap', 'club_id', 'created_at',
	];


	public function club()
	{
		return $this->belongsTo('Golf\Club', 'club_id', 'id');
	}

	public function member()
	{
		return $this->belongsTo('Golf\Member', 'user_id', 'id');
	}

	public function reservations()
	{
		return $this->belongsToMany('Golf\Reservation', 'partner_reservations');
	}

	public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

	public function getLastNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }    
}
