<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class Locker extends Model
{
	use SoftDeletes;
	use Sortable;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name', 'status',
	];
	protected $sortable = [
		'name', 'status',
	];

	public function members()
	{
		//	return $this->belongsToMany('Golf\Members', 'member_id', 'id');
		return $this->belongsToMany('Golf\Member', 'locker_loans')->withTimestamps();
	}

	public function locker_loans()
	{
		//	return $this->belongsToMany('Golf\Members', 'member_id', 'id');
		return $this->hasMany('Golf\LockerLoan', 'locker_id', 'id');
	}
}
