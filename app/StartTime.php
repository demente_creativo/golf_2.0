<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class StartTime extends Model
{
	use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'start_time', 'end_time','enabled'
	];
	protected $sortable = [
		'start_time', 'end_time','enabled'
	];

	public function setStartTimeAttribute($value)
	{
		$this->attributes['start_time'] = date( 'H:i', strtotime($value) );
	}

	public function getStartTimeAttribute()
	{
		return date( 'h:i A', strtotime($this->attributes['start_time']) );
	}

	public function setEndTimeAttribute($value)
	{
		$this->attributes['end_time'] = date( 'H:i', strtotime($value) );
	}

	public function getEndTimeAttribute()
	{
		return date( 'h:i A', strtotime($this->attributes['end_time']) );
	}

	public function categories()
	{
		return $this->belongsToMany('Golf\StartTime','tournament_categories')
			->withPivot('tournament_id','enabled');
	}

	public function tournaments()
	{
		return $this->belongsToMany('Golf\Tournament','tournament_categories')
			->withPivot('category_id','enabled')->withTimestamps();
	}
}
