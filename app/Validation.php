<?php

namespace Golf;

use Illuminate\Database\Eloquent\Model;

class Validation extends Model
{
    protected $timestamp = false;
}
