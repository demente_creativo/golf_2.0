<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class Teacher extends Model
{

	use SoftDeletes;
	use Sortable;


	protected $fillable = [
		'name','last_name','phone','email'
	];
	protected $sortable = [
		'name','last_name','phone','email'
	];

	public function group()
	{
		return $this->hasOne('Golf\Group', 'id', 'group_id');
	}

	public function getNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }

	public function getLastNameAttribute($value)
    {
        return ucwords(strtolower($value));
    }    	

}
