<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;

class GolfCourse extends Model
{

	use SoftDeletes;
	use Sortable;

	protected $fillable = [
		'interval','start_time','end_time','status','enabled',
	];
	protected $sortable = [
		'interval','start_time','end_time','status','enabled',
	];

}
