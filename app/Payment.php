<?php

namespace Golf;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Gbrock\Table\Traits\Sortable;


class Payment extends Model
{

	use SoftDeletes;
	use Sortable;


	protected $fillable = [
		'concept','period_id','monthly_payment_id'
	];
	protected $sortable = [
		'concept','period_id','monthly_payment_id'
	];

	public function period()
	{
		return $this->hasOne('Golf\Period', 'id', 'period_id');
	}
}
